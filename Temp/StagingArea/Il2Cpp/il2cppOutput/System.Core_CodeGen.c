﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B (void);
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 (void);
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 (void);
// 0x00000004 System.Void System.Security.Cryptography.AesManaged::set_FeedbackSize(System.Int32)
extern void AesManaged_set_FeedbackSize_m881146DEC23D6F9FA3480D2ABDA7CBAFBD71C7C9 (void);
// 0x00000005 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E (void);
// 0x00000006 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 (void);
// 0x00000007 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 (void);
// 0x00000008 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC (void);
// 0x00000009 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 (void);
// 0x0000000A System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 (void);
// 0x0000000B System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA (void);
// 0x0000000C System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 (void);
// 0x0000000D System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 (void);
// 0x0000000E System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 (void);
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD (void);
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 (void);
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 (void);
// 0x00000012 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 (void);
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED (void);
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC (void);
// 0x00000015 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 (void);
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E (void);
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC (void);
// 0x00000018 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 (void);
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 (void);
// 0x0000001A System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 (void);
// 0x0000001B System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F (void);
// 0x0000001C System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C (void);
// 0x0000001D System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A (void);
// 0x0000001E System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 (void);
// 0x0000001F System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA (void);
// 0x00000020 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B (void);
// 0x00000021 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F (void);
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_FeedbackSize(System.Int32)
extern void AesCryptoServiceProvider_set_FeedbackSize_m409990EF50B03E207F0BAAE9BE19C23A77EA9C27 (void);
// 0x00000023 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F (void);
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA (void);
// 0x00000025 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 (void);
// 0x00000026 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 (void);
// 0x00000027 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 (void);
// 0x00000028 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA (void);
// 0x00000029 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F (void);
// 0x0000002A System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD (void);
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA (void);
// 0x0000002C System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 (void);
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA (void);
// 0x0000002E System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 (void);
// 0x0000002F System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 (void);
// 0x00000030 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000031 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000032 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000033 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
// 0x00000037 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000038 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Skip(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::SkipIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Concat(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ConcatIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000041 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000042 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000043 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000044 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000045 System.Collections.Generic.Dictionary`2<TKey,TSource> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000046 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x00000047 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000048 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000049 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004A System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004B TSource System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,TSource,TSource>)
// 0x0000004C System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000004D TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000004E System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000004F System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000050 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000051 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000052 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000053 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000054 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000055 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000056 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000057 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000058 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000059 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000005A System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000005B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000005D System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000005E System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000005F System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000060 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000061 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000062 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000063 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000064 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000065 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000066 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000067 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000068 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000069 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000006A System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000006B System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000006C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000006D System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000006E System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x0000006F System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000070 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000071 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000072 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000073 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000074 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000075 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000076 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000077 System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::.ctor(System.Int32)
// 0x00000078 System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::System.IDisposable.Dispose()
// 0x00000079 System.Boolean System.Linq.Enumerable_<SelectIterator>d__5`2::MoveNext()
// 0x0000007A System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::<>m__Finally1()
// 0x0000007B TResult System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000007C System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerator.Reset()
// 0x0000007D System.Object System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerator.get_Current()
// 0x0000007E System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000007F System.Collections.IEnumerator System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000080 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000081 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000082 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000083 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000084 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x00000085 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x00000086 System.Boolean System.Linq.Enumerable_<TakeIterator>d__25`1::MoveNext()
// 0x00000087 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::<>m__Finally1()
// 0x00000088 TSource System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000089 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x0000008A System.Object System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x0000008B System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000008C System.Collections.IEnumerator System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000008D System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::.ctor(System.Int32)
// 0x0000008E System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::System.IDisposable.Dispose()
// 0x0000008F System.Boolean System.Linq.Enumerable_<SkipIterator>d__31`1::MoveNext()
// 0x00000090 System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::<>m__Finally1()
// 0x00000091 TSource System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000092 System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.IEnumerator.Reset()
// 0x00000093 System.Object System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.IEnumerator.get_Current()
// 0x00000094 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000095 System.Collections.IEnumerator System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000096 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::.ctor(System.Int32)
// 0x00000097 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::System.IDisposable.Dispose()
// 0x00000098 System.Boolean System.Linq.Enumerable_<ConcatIterator>d__59`1::MoveNext()
// 0x00000099 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::<>m__Finally1()
// 0x0000009A System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::<>m__Finally2()
// 0x0000009B TSource System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000009C System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerator.Reset()
// 0x0000009D System.Object System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerator.get_Current()
// 0x0000009E System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000009F System.Collections.IEnumerator System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A0 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x000000A1 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x000000A2 System.Boolean System.Linq.Enumerable_<UnionIterator>d__71`1::MoveNext()
// 0x000000A3 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally1()
// 0x000000A4 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally2()
// 0x000000A5 TSource System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000A6 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x000000A7 System.Object System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x000000A8 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000A9 System.Collections.IEnumerator System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AA System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x000000AB System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x000000AC System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x000000AD TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x000000AE System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000AF System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000B0 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000B1 System.Void System.Linq.Set`1::Resize()
// 0x000000B2 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000B3 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000B4 TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000B5 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000B6 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000B7 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000B8 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000B9 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000BA System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x000000BB System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000BC System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000BD System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000BE System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000BF System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000C0 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000C1 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000C2 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000C3 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000C4 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C5 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000C6 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000C7 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000C8 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000C9 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000CA System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000CB System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x000000CC System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x000000CD System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000CE System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000CF System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000D0 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000D1 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x000000D2 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x000000D3 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000D4 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000D5 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000D6 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000D7 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000D8 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000D9 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[217] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_set_FeedbackSize_m881146DEC23D6F9FA3480D2ABDA7CBAFBD71C7C9,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_set_FeedbackSize_m409990EF50B03E207F0BAAE9BE19C23A77EA9C27,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[217] = 
{
	0,
	23,
	10,
	32,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	107,
	14,
	107,
	31,
	23,
	23,
	23,
	23,
	23,
	107,
	107,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	961,
	27,
	37,
	208,
	208,
	3,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[51] = 
{
	{ 0x02000008, { 77, 4 } },
	{ 0x02000009, { 81, 9 } },
	{ 0x0200000A, { 92, 7 } },
	{ 0x0200000B, { 101, 10 } },
	{ 0x0200000C, { 113, 11 } },
	{ 0x0200000D, { 127, 9 } },
	{ 0x0200000E, { 139, 12 } },
	{ 0x0200000F, { 154, 9 } },
	{ 0x02000010, { 163, 1 } },
	{ 0x02000011, { 164, 2 } },
	{ 0x02000012, { 166, 8 } },
	{ 0x02000013, { 174, 8 } },
	{ 0x02000014, { 182, 9 } },
	{ 0x02000015, { 191, 12 } },
	{ 0x02000016, { 203, 4 } },
	{ 0x02000017, { 207, 3 } },
	{ 0x02000018, { 210, 8 } },
	{ 0x0200001A, { 218, 4 } },
	{ 0x0200001B, { 222, 34 } },
	{ 0x0200001D, { 256, 2 } },
	{ 0x06000033, { 0, 10 } },
	{ 0x06000034, { 10, 10 } },
	{ 0x06000035, { 20, 1 } },
	{ 0x06000036, { 21, 2 } },
	{ 0x06000037, { 23, 5 } },
	{ 0x06000038, { 28, 5 } },
	{ 0x06000039, { 33, 1 } },
	{ 0x0600003A, { 34, 2 } },
	{ 0x0600003B, { 36, 1 } },
	{ 0x0600003C, { 37, 2 } },
	{ 0x0600003D, { 39, 1 } },
	{ 0x0600003E, { 40, 2 } },
	{ 0x0600003F, { 42, 1 } },
	{ 0x06000040, { 43, 2 } },
	{ 0x06000041, { 45, 1 } },
	{ 0x06000042, { 46, 5 } },
	{ 0x06000043, { 51, 3 } },
	{ 0x06000044, { 54, 2 } },
	{ 0x06000045, { 56, 3 } },
	{ 0x06000046, { 59, 1 } },
	{ 0x06000047, { 60, 7 } },
	{ 0x06000048, { 67, 3 } },
	{ 0x06000049, { 70, 1 } },
	{ 0x0600004A, { 71, 3 } },
	{ 0x0600004B, { 74, 3 } },
	{ 0x0600005B, { 90, 2 } },
	{ 0x06000060, { 99, 2 } },
	{ 0x06000065, { 111, 2 } },
	{ 0x0600006B, { 124, 3 } },
	{ 0x06000070, { 136, 3 } },
	{ 0x06000075, { 151, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[258] = 
{
	{ (Il2CppRGCTXDataType)2, 27248 },
	{ (Il2CppRGCTXDataType)3, 12204 },
	{ (Il2CppRGCTXDataType)2, 27249 },
	{ (Il2CppRGCTXDataType)2, 27250 },
	{ (Il2CppRGCTXDataType)3, 12205 },
	{ (Il2CppRGCTXDataType)2, 27251 },
	{ (Il2CppRGCTXDataType)2, 27252 },
	{ (Il2CppRGCTXDataType)3, 12206 },
	{ (Il2CppRGCTXDataType)2, 27253 },
	{ (Il2CppRGCTXDataType)3, 12207 },
	{ (Il2CppRGCTXDataType)2, 27254 },
	{ (Il2CppRGCTXDataType)3, 12208 },
	{ (Il2CppRGCTXDataType)2, 27255 },
	{ (Il2CppRGCTXDataType)2, 27256 },
	{ (Il2CppRGCTXDataType)3, 12209 },
	{ (Il2CppRGCTXDataType)2, 27257 },
	{ (Il2CppRGCTXDataType)2, 27258 },
	{ (Il2CppRGCTXDataType)3, 12210 },
	{ (Il2CppRGCTXDataType)2, 27259 },
	{ (Il2CppRGCTXDataType)3, 12211 },
	{ (Il2CppRGCTXDataType)3, 12212 },
	{ (Il2CppRGCTXDataType)2, 27260 },
	{ (Il2CppRGCTXDataType)3, 12213 },
	{ (Il2CppRGCTXDataType)2, 27261 },
	{ (Il2CppRGCTXDataType)3, 12214 },
	{ (Il2CppRGCTXDataType)3, 12215 },
	{ (Il2CppRGCTXDataType)2, 20046 },
	{ (Il2CppRGCTXDataType)3, 12216 },
	{ (Il2CppRGCTXDataType)2, 27262 },
	{ (Il2CppRGCTXDataType)3, 12217 },
	{ (Il2CppRGCTXDataType)3, 12218 },
	{ (Il2CppRGCTXDataType)2, 20053 },
	{ (Il2CppRGCTXDataType)3, 12219 },
	{ (Il2CppRGCTXDataType)3, 12220 },
	{ (Il2CppRGCTXDataType)2, 27263 },
	{ (Il2CppRGCTXDataType)3, 12221 },
	{ (Il2CppRGCTXDataType)3, 12222 },
	{ (Il2CppRGCTXDataType)2, 27264 },
	{ (Il2CppRGCTXDataType)3, 12223 },
	{ (Il2CppRGCTXDataType)3, 12224 },
	{ (Il2CppRGCTXDataType)2, 27265 },
	{ (Il2CppRGCTXDataType)3, 12225 },
	{ (Il2CppRGCTXDataType)3, 12226 },
	{ (Il2CppRGCTXDataType)2, 27266 },
	{ (Il2CppRGCTXDataType)3, 12227 },
	{ (Il2CppRGCTXDataType)3, 12228 },
	{ (Il2CppRGCTXDataType)3, 12229 },
	{ (Il2CppRGCTXDataType)2, 27267 },
	{ (Il2CppRGCTXDataType)2, 20073 },
	{ (Il2CppRGCTXDataType)2, 27268 },
	{ (Il2CppRGCTXDataType)2, 20075 },
	{ (Il2CppRGCTXDataType)2, 27269 },
	{ (Il2CppRGCTXDataType)3, 12230 },
	{ (Il2CppRGCTXDataType)3, 12231 },
	{ (Il2CppRGCTXDataType)2, 20081 },
	{ (Il2CppRGCTXDataType)3, 12232 },
	{ (Il2CppRGCTXDataType)3, 12233 },
	{ (Il2CppRGCTXDataType)2, 27270 },
	{ (Il2CppRGCTXDataType)3, 12234 },
	{ (Il2CppRGCTXDataType)3, 12235 },
	{ (Il2CppRGCTXDataType)2, 20101 },
	{ (Il2CppRGCTXDataType)3, 12236 },
	{ (Il2CppRGCTXDataType)2, 20094 },
	{ (Il2CppRGCTXDataType)2, 27271 },
	{ (Il2CppRGCTXDataType)3, 12237 },
	{ (Il2CppRGCTXDataType)3, 12238 },
	{ (Il2CppRGCTXDataType)3, 12239 },
	{ (Il2CppRGCTXDataType)2, 20102 },
	{ (Il2CppRGCTXDataType)2, 27272 },
	{ (Il2CppRGCTXDataType)3, 12240 },
	{ (Il2CppRGCTXDataType)2, 20105 },
	{ (Il2CppRGCTXDataType)2, 20107 },
	{ (Il2CppRGCTXDataType)2, 27273 },
	{ (Il2CppRGCTXDataType)3, 12241 },
	{ (Il2CppRGCTXDataType)2, 20110 },
	{ (Il2CppRGCTXDataType)2, 27274 },
	{ (Il2CppRGCTXDataType)3, 12242 },
	{ (Il2CppRGCTXDataType)3, 12243 },
	{ (Il2CppRGCTXDataType)3, 12244 },
	{ (Il2CppRGCTXDataType)2, 20115 },
	{ (Il2CppRGCTXDataType)3, 12245 },
	{ (Il2CppRGCTXDataType)3, 12246 },
	{ (Il2CppRGCTXDataType)2, 20127 },
	{ (Il2CppRGCTXDataType)2, 27275 },
	{ (Il2CppRGCTXDataType)3, 12247 },
	{ (Il2CppRGCTXDataType)3, 12248 },
	{ (Il2CppRGCTXDataType)2, 20129 },
	{ (Il2CppRGCTXDataType)2, 27134 },
	{ (Il2CppRGCTXDataType)3, 12249 },
	{ (Il2CppRGCTXDataType)3, 12250 },
	{ (Il2CppRGCTXDataType)2, 27276 },
	{ (Il2CppRGCTXDataType)3, 12251 },
	{ (Il2CppRGCTXDataType)3, 12252 },
	{ (Il2CppRGCTXDataType)2, 20139 },
	{ (Il2CppRGCTXDataType)2, 27277 },
	{ (Il2CppRGCTXDataType)3, 12253 },
	{ (Il2CppRGCTXDataType)3, 12254 },
	{ (Il2CppRGCTXDataType)3, 11617 },
	{ (Il2CppRGCTXDataType)3, 12255 },
	{ (Il2CppRGCTXDataType)2, 27278 },
	{ (Il2CppRGCTXDataType)3, 12256 },
	{ (Il2CppRGCTXDataType)3, 12257 },
	{ (Il2CppRGCTXDataType)2, 20151 },
	{ (Il2CppRGCTXDataType)2, 27279 },
	{ (Il2CppRGCTXDataType)3, 12258 },
	{ (Il2CppRGCTXDataType)3, 12259 },
	{ (Il2CppRGCTXDataType)3, 12260 },
	{ (Il2CppRGCTXDataType)3, 12261 },
	{ (Il2CppRGCTXDataType)3, 12262 },
	{ (Il2CppRGCTXDataType)3, 11623 },
	{ (Il2CppRGCTXDataType)3, 12263 },
	{ (Il2CppRGCTXDataType)2, 27280 },
	{ (Il2CppRGCTXDataType)3, 12264 },
	{ (Il2CppRGCTXDataType)3, 12265 },
	{ (Il2CppRGCTXDataType)2, 20164 },
	{ (Il2CppRGCTXDataType)2, 27281 },
	{ (Il2CppRGCTXDataType)3, 12266 },
	{ (Il2CppRGCTXDataType)3, 12267 },
	{ (Il2CppRGCTXDataType)2, 20166 },
	{ (Il2CppRGCTXDataType)2, 27282 },
	{ (Il2CppRGCTXDataType)3, 12268 },
	{ (Il2CppRGCTXDataType)3, 12269 },
	{ (Il2CppRGCTXDataType)2, 27283 },
	{ (Il2CppRGCTXDataType)3, 12270 },
	{ (Il2CppRGCTXDataType)3, 12271 },
	{ (Il2CppRGCTXDataType)2, 27284 },
	{ (Il2CppRGCTXDataType)3, 12272 },
	{ (Il2CppRGCTXDataType)3, 12273 },
	{ (Il2CppRGCTXDataType)2, 20181 },
	{ (Il2CppRGCTXDataType)2, 27285 },
	{ (Il2CppRGCTXDataType)3, 12274 },
	{ (Il2CppRGCTXDataType)3, 12275 },
	{ (Il2CppRGCTXDataType)3, 12276 },
	{ (Il2CppRGCTXDataType)3, 11634 },
	{ (Il2CppRGCTXDataType)2, 27286 },
	{ (Il2CppRGCTXDataType)3, 12277 },
	{ (Il2CppRGCTXDataType)3, 12278 },
	{ (Il2CppRGCTXDataType)2, 27287 },
	{ (Il2CppRGCTXDataType)3, 12279 },
	{ (Il2CppRGCTXDataType)3, 12280 },
	{ (Il2CppRGCTXDataType)2, 20197 },
	{ (Il2CppRGCTXDataType)2, 27288 },
	{ (Il2CppRGCTXDataType)3, 12281 },
	{ (Il2CppRGCTXDataType)3, 12282 },
	{ (Il2CppRGCTXDataType)3, 12283 },
	{ (Il2CppRGCTXDataType)3, 12284 },
	{ (Il2CppRGCTXDataType)3, 12285 },
	{ (Il2CppRGCTXDataType)3, 12286 },
	{ (Il2CppRGCTXDataType)3, 11640 },
	{ (Il2CppRGCTXDataType)2, 27289 },
	{ (Il2CppRGCTXDataType)3, 12287 },
	{ (Il2CppRGCTXDataType)3, 12288 },
	{ (Il2CppRGCTXDataType)2, 27290 },
	{ (Il2CppRGCTXDataType)3, 12289 },
	{ (Il2CppRGCTXDataType)3, 12290 },
	{ (Il2CppRGCTXDataType)2, 27291 },
	{ (Il2CppRGCTXDataType)2, 27292 },
	{ (Il2CppRGCTXDataType)3, 12291 },
	{ (Il2CppRGCTXDataType)3, 12292 },
	{ (Il2CppRGCTXDataType)2, 20214 },
	{ (Il2CppRGCTXDataType)2, 27293 },
	{ (Il2CppRGCTXDataType)3, 12293 },
	{ (Il2CppRGCTXDataType)3, 12294 },
	{ (Il2CppRGCTXDataType)3, 12295 },
	{ (Il2CppRGCTXDataType)3, 12296 },
	{ (Il2CppRGCTXDataType)3, 12297 },
	{ (Il2CppRGCTXDataType)3, 12298 },
	{ (Il2CppRGCTXDataType)2, 20244 },
	{ (Il2CppRGCTXDataType)2, 20239 },
	{ (Il2CppRGCTXDataType)3, 12299 },
	{ (Il2CppRGCTXDataType)2, 20238 },
	{ (Il2CppRGCTXDataType)2, 27294 },
	{ (Il2CppRGCTXDataType)3, 12300 },
	{ (Il2CppRGCTXDataType)3, 12301 },
	{ (Il2CppRGCTXDataType)3, 12302 },
	{ (Il2CppRGCTXDataType)2, 20254 },
	{ (Il2CppRGCTXDataType)2, 20249 },
	{ (Il2CppRGCTXDataType)3, 12303 },
	{ (Il2CppRGCTXDataType)2, 20248 },
	{ (Il2CppRGCTXDataType)2, 27295 },
	{ (Il2CppRGCTXDataType)3, 12304 },
	{ (Il2CppRGCTXDataType)3, 12305 },
	{ (Il2CppRGCTXDataType)3, 12306 },
	{ (Il2CppRGCTXDataType)3, 12307 },
	{ (Il2CppRGCTXDataType)2, 20264 },
	{ (Il2CppRGCTXDataType)2, 20259 },
	{ (Il2CppRGCTXDataType)3, 12308 },
	{ (Il2CppRGCTXDataType)2, 20258 },
	{ (Il2CppRGCTXDataType)2, 27296 },
	{ (Il2CppRGCTXDataType)3, 12309 },
	{ (Il2CppRGCTXDataType)3, 12310 },
	{ (Il2CppRGCTXDataType)3, 12311 },
	{ (Il2CppRGCTXDataType)3, 12312 },
	{ (Il2CppRGCTXDataType)2, 27297 },
	{ (Il2CppRGCTXDataType)3, 12313 },
	{ (Il2CppRGCTXDataType)2, 20277 },
	{ (Il2CppRGCTXDataType)2, 20269 },
	{ (Il2CppRGCTXDataType)3, 12314 },
	{ (Il2CppRGCTXDataType)3, 12315 },
	{ (Il2CppRGCTXDataType)2, 20268 },
	{ (Il2CppRGCTXDataType)2, 27298 },
	{ (Il2CppRGCTXDataType)3, 12316 },
	{ (Il2CppRGCTXDataType)3, 12317 },
	{ (Il2CppRGCTXDataType)2, 27299 },
	{ (Il2CppRGCTXDataType)3, 12318 },
	{ (Il2CppRGCTXDataType)2, 20281 },
	{ (Il2CppRGCTXDataType)3, 12319 },
	{ (Il2CppRGCTXDataType)2, 27300 },
	{ (Il2CppRGCTXDataType)3, 12320 },
	{ (Il2CppRGCTXDataType)2, 27300 },
	{ (Il2CppRGCTXDataType)3, 12321 },
	{ (Il2CppRGCTXDataType)2, 27301 },
	{ (Il2CppRGCTXDataType)2, 27302 },
	{ (Il2CppRGCTXDataType)3, 12322 },
	{ (Il2CppRGCTXDataType)3, 12323 },
	{ (Il2CppRGCTXDataType)2, 20290 },
	{ (Il2CppRGCTXDataType)3, 12324 },
	{ (Il2CppRGCTXDataType)2, 20291 },
	{ (Il2CppRGCTXDataType)2, 27303 },
	{ (Il2CppRGCTXDataType)2, 20302 },
	{ (Il2CppRGCTXDataType)2, 20300 },
	{ (Il2CppRGCTXDataType)2, 27304 },
	{ (Il2CppRGCTXDataType)3, 12325 },
	{ (Il2CppRGCTXDataType)2, 27305 },
	{ (Il2CppRGCTXDataType)3, 12326 },
	{ (Il2CppRGCTXDataType)3, 12327 },
	{ (Il2CppRGCTXDataType)2, 20309 },
	{ (Il2CppRGCTXDataType)3, 12328 },
	{ (Il2CppRGCTXDataType)2, 20309 },
	{ (Il2CppRGCTXDataType)3, 12329 },
	{ (Il2CppRGCTXDataType)2, 20326 },
	{ (Il2CppRGCTXDataType)3, 12330 },
	{ (Il2CppRGCTXDataType)3, 12331 },
	{ (Il2CppRGCTXDataType)3, 12332 },
	{ (Il2CppRGCTXDataType)2, 27306 },
	{ (Il2CppRGCTXDataType)3, 12333 },
	{ (Il2CppRGCTXDataType)3, 12334 },
	{ (Il2CppRGCTXDataType)3, 12335 },
	{ (Il2CppRGCTXDataType)2, 20306 },
	{ (Il2CppRGCTXDataType)3, 12336 },
	{ (Il2CppRGCTXDataType)3, 12337 },
	{ (Il2CppRGCTXDataType)2, 20311 },
	{ (Il2CppRGCTXDataType)3, 12338 },
	{ (Il2CppRGCTXDataType)1, 27307 },
	{ (Il2CppRGCTXDataType)2, 20310 },
	{ (Il2CppRGCTXDataType)3, 12339 },
	{ (Il2CppRGCTXDataType)1, 20310 },
	{ (Il2CppRGCTXDataType)1, 20306 },
	{ (Il2CppRGCTXDataType)2, 27306 },
	{ (Il2CppRGCTXDataType)2, 20310 },
	{ (Il2CppRGCTXDataType)2, 20308 },
	{ (Il2CppRGCTXDataType)2, 20312 },
	{ (Il2CppRGCTXDataType)3, 12340 },
	{ (Il2CppRGCTXDataType)3, 12341 },
	{ (Il2CppRGCTXDataType)3, 12342 },
	{ (Il2CppRGCTXDataType)2, 20307 },
	{ (Il2CppRGCTXDataType)3, 12343 },
	{ (Il2CppRGCTXDataType)2, 20322 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	217,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	51,
	s_rgctxIndices,
	258,
	s_rgctxValues,
	NULL,
};
