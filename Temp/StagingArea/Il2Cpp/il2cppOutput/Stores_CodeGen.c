﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* AppleStoreImpl_MessageCallback_mD0A78B20AACCAEB5D2ECE032884DE17D435454B6_RuntimeMethod_var;
extern const RuntimeMethod* FacebookStoreImpl_MessageCallback_mBC2F88BC0E8116317E1EC9EE7B5CDFE237B970D9_RuntimeMethod_var;
extern const RuntimeMethod* TizenStoreImpl_MessageCallback_mA4B7D8E14E7399E5405E1DF5C1C9D7F72E1B4E73_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.RuntimePlatform Uniject.IUtil::get_platform()
// 0x00000002 System.String Uniject.IUtil::get_persistentDataPath()
// 0x00000003 System.String Uniject.IUtil::get_cloudProjectId()
// 0x00000004 System.String Uniject.IUtil::get_deviceUniqueIdentifier()
// 0x00000005 System.String Uniject.IUtil::get_unityVersion()
// 0x00000006 System.String Uniject.IUtil::get_userId()
// 0x00000007 System.String Uniject.IUtil::get_gameVersion()
// 0x00000008 System.UInt64 Uniject.IUtil::get_sessionId()
// 0x00000009 System.String Uniject.IUtil::get_operatingSystem()
// 0x0000000A System.Int32 Uniject.IUtil::get_screenWidth()
// 0x0000000B System.Int32 Uniject.IUtil::get_screenHeight()
// 0x0000000C System.Single Uniject.IUtil::get_screenDpi()
// 0x0000000D System.String Uniject.IUtil::get_screenOrientation()
// 0x0000000E System.Object Uniject.IUtil::InitiateCoroutine(System.Collections.IEnumerator)
// 0x0000000F System.Void Uniject.IUtil::InitiateCoroutine(System.Collections.IEnumerator,System.Int32)
// 0x00000010 System.Void Uniject.IUtil::RunOnMainThread(System.Action)
// 0x00000011 System.Void Uniject.IUtil::AddPauseListener(System.Action`1<System.Boolean>)
// 0x00000012 System.Boolean Uniject.IUtil::IsClassOrSubclass(System.Type,System.Type)
// 0x00000013 UnityEngine.AndroidJavaObject UnityEngine.Purchasing.AndroidJavaStore::GetStore()
extern void AndroidJavaStore_GetStore_m2A8FC945796A1F5F889CB73DEFA96CE1FCA664DA (void);
// 0x00000014 System.Void UnityEngine.Purchasing.AndroidJavaStore::.ctor(UnityEngine.AndroidJavaObject)
extern void AndroidJavaStore__ctor_m70EB8A74FDD05554EDA5E4447B8945C0DBE4C062 (void);
// 0x00000015 System.Void UnityEngine.Purchasing.AndroidJavaStore::RetrieveProducts(System.String)
extern void AndroidJavaStore_RetrieveProducts_m904D7F385AB73CD4A0A7404D0446281A33A04872 (void);
// 0x00000016 System.Void UnityEngine.Purchasing.AndroidJavaStore::Purchase(System.String,System.String)
extern void AndroidJavaStore_Purchase_mA1D5EA803F76876AD8105227C947FED3AF409740 (void);
// 0x00000017 System.Void UnityEngine.Purchasing.AndroidJavaStore::FinishTransaction(System.String,System.String)
extern void AndroidJavaStore_FinishTransaction_mB19BB7BE7F950BF6AF0448B7237E09B0BD37E745 (void);
// 0x00000018 System.Void UnityEngine.Purchasing.IUnityCallback::OnSetupFailed(System.String)
// 0x00000019 System.Void UnityEngine.Purchasing.IUnityCallback::OnProductsRetrieved(System.String)
// 0x0000001A System.Void UnityEngine.Purchasing.IUnityCallback::OnPurchaseSucceeded(System.String,System.String,System.String)
// 0x0000001B System.Void UnityEngine.Purchasing.IUnityCallback::OnPurchaseFailed(System.String)
// 0x0000001C System.Void UnityEngine.Purchasing.JavaBridge::.ctor(UnityEngine.Purchasing.IUnityCallback)
extern void JavaBridge__ctor_m73B7AA88A0E9A377B56B3E6EC7F635D78FBCC2D6 (void);
// 0x0000001D System.Void UnityEngine.Purchasing.JavaBridge::.ctor(UnityEngine.Purchasing.IUnityCallback,System.String)
extern void JavaBridge__ctor_m08D6FCA2BDF16A42BDE40C27B4AF410C24959FF3 (void);
// 0x0000001E System.Void UnityEngine.Purchasing.JavaBridge::OnSetupFailed(System.String)
extern void JavaBridge_OnSetupFailed_mF9F6F2093083053A14D63AE6749C6A5D081B2B7C (void);
// 0x0000001F System.Void UnityEngine.Purchasing.JavaBridge::OnProductsRetrieved(System.String)
extern void JavaBridge_OnProductsRetrieved_mD7598324F4BC0FEA4B1C6DA2088C5A40DE003FD5 (void);
// 0x00000020 System.Void UnityEngine.Purchasing.JavaBridge::OnPurchaseSucceeded(System.String,System.String,System.String)
extern void JavaBridge_OnPurchaseSucceeded_mB0DBAC88689C51B419530F201A519240EF761A1D (void);
// 0x00000021 System.Void UnityEngine.Purchasing.JavaBridge::OnPurchaseFailed(System.String)
extern void JavaBridge_OnPurchaseFailed_mC99ADC72398856D38999C9056764488FCA80A614 (void);
// 0x00000022 System.String UnityEngine.Purchasing.SerializationExtensions::TryGetString(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern void SerializationExtensions_TryGetString_m683D88D1A71FF92A3FBD410D8239A7DB8727DDA1 (void);
// 0x00000023 System.String UnityEngine.Purchasing.JSONSerializer::SerializeProductDef(UnityEngine.Purchasing.ProductDefinition)
extern void JSONSerializer_SerializeProductDef_mF2645D8657030E744AED67C4ABCB92937CE3843D (void);
// 0x00000024 System.String UnityEngine.Purchasing.JSONSerializer::SerializeProductDefs(System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.ProductDefinition>)
extern void JSONSerializer_SerializeProductDefs_mDFCF95475F771264408C2B7A0ED7E8761C6BCE0C (void);
// 0x00000025 System.String UnityEngine.Purchasing.JSONSerializer::SerializeProductDescs(System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Extension.ProductDescription>)
extern void JSONSerializer_SerializeProductDescs_mF0D18710CDF0FCAF0088CA514CC1600A47BDC123 (void);
// 0x00000026 System.Collections.Generic.List`1<UnityEngine.Purchasing.Extension.ProductDescription> UnityEngine.Purchasing.JSONSerializer::DeserializeProductDescriptions(System.String)
extern void JSONSerializer_DeserializeProductDescriptions_m3A6EE1586658E8DD642AF3B14DB22245A5B959E3 (void);
// 0x00000027 UnityEngine.Purchasing.Extension.PurchaseFailureDescription UnityEngine.Purchasing.JSONSerializer::DeserializeFailureReason(System.String)
extern void JSONSerializer_DeserializeFailureReason_m2267B2FEBE10B2CF917B6C79FA5F400EBA9FE04F (void);
// 0x00000028 UnityEngine.Purchasing.ProductMetadata UnityEngine.Purchasing.JSONSerializer::DeserializeMetadata(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void JSONSerializer_DeserializeMetadata_mA31DF2D3510240E2ABAFF1B7A200D3CDB3DAE06E (void);
// 0x00000029 System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.JSONSerializer::EncodeProductDef(UnityEngine.Purchasing.ProductDefinition)
extern void JSONSerializer_EncodeProductDef_m27278FB9A153ED42DAD7B00E040570A0A4A9211E (void);
// 0x0000002A System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.JSONSerializer::EncodeProductDesc(UnityEngine.Purchasing.Extension.ProductDescription)
extern void JSONSerializer_EncodeProductDesc_mA281441760D3B5E2E162089CEC86E7DC3D354805 (void);
// 0x0000002B System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.JSONSerializer::EncodeProductMeta(UnityEngine.Purchasing.ProductMetadata)
extern void JSONSerializer_EncodeProductMeta_mBE000E23E1824C50188E38C06BA1618DE6AB32AE (void);
// 0x0000002C System.Void UnityEngine.Purchasing.ScriptingUnityCallback::.ctor(UnityEngine.Purchasing.IUnityCallback,Uniject.IUtil)
extern void ScriptingUnityCallback__ctor_m34521511B2C4812B536F9444BCC5BA18D5F0AF50 (void);
// 0x0000002D System.Void UnityEngine.Purchasing.ScriptingUnityCallback::OnSetupFailed(System.String)
extern void ScriptingUnityCallback_OnSetupFailed_m03FFF3D3B8F04F2FD52EE886E5C1589C0391AC15 (void);
// 0x0000002E System.Void UnityEngine.Purchasing.ScriptingUnityCallback::OnProductsRetrieved(System.String)
extern void ScriptingUnityCallback_OnProductsRetrieved_m2D7EAADBE9D1DE370628720CAEDB1CF5794FEBDA (void);
// 0x0000002F System.Void UnityEngine.Purchasing.ScriptingUnityCallback::OnPurchaseSucceeded(System.String,System.String,System.String)
extern void ScriptingUnityCallback_OnPurchaseSucceeded_m60599B66D8EF2A2B3FCEFA36D740E4A19083AEA2 (void);
// 0x00000030 System.Void UnityEngine.Purchasing.ScriptingUnityCallback::OnPurchaseFailed(System.String)
extern void ScriptingUnityCallback_OnPurchaseFailed_mAE79FE3935D92911F3ECAE6C8EADBB2DA832B4BA (void);
// 0x00000031 System.Void UnityEngine.Purchasing.ScriptingUnityCallback_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m32207C242A668B40DF097AA99DCBF7F6717D470D (void);
// 0x00000032 System.Void UnityEngine.Purchasing.ScriptingUnityCallback_<>c__DisplayClass3_0::<OnSetupFailed>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3COnSetupFailedU3Eb__0_mD7EEC31FF55918387286B9CCE18F29D7CEE81B76 (void);
// 0x00000033 System.Void UnityEngine.Purchasing.ScriptingUnityCallback_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mED8019084593C580AC73852DA9F7EEB4AB99C598 (void);
// 0x00000034 System.Void UnityEngine.Purchasing.ScriptingUnityCallback_<>c__DisplayClass4_0::<OnProductsRetrieved>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3COnProductsRetrievedU3Eb__0_mFD8455179BA3C19D610E8586484958F0289934AD (void);
// 0x00000035 System.Void UnityEngine.Purchasing.ScriptingUnityCallback_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mA95E0EF7841FE300701EDED268EFCED1CCB47394 (void);
// 0x00000036 System.Void UnityEngine.Purchasing.ScriptingUnityCallback_<>c__DisplayClass5_0::<OnPurchaseSucceeded>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3COnPurchaseSucceededU3Eb__0_m28093106DF21868395DCB7733F7591D7D9E92C85 (void);
// 0x00000037 System.Void UnityEngine.Purchasing.ScriptingUnityCallback_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m4E7698CB27A47857E50CEFBDB45292BC98CC8E98 (void);
// 0x00000038 System.Void UnityEngine.Purchasing.ScriptingUnityCallback_<>c__DisplayClass6_0::<OnPurchaseFailed>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3COnPurchaseFailedU3Eb__0_m760BC3F40DBF1E8B724C55726BA431309E57980D (void);
// 0x00000039 System.Void UnityEngine.Purchasing.AmazonAppStoreStoreExtensions::.ctor(UnityEngine.AndroidJavaObject)
extern void AmazonAppStoreStoreExtensions__ctor_m49DCFE64814F8FE54771EA66FB5ECBC198D82077 (void);
// 0x0000003A System.Void UnityEngine.Purchasing.FakeAmazonExtensions::.ctor()
extern void FakeAmazonExtensions__ctor_m1C530544AF4958ABA72E2FE43459E97D89E6A815 (void);
// 0x0000003B System.Void UnityEngine.Purchasing.FakeMoolahConfiguration::set_appKey(System.String)
extern void FakeMoolahConfiguration_set_appKey_m8462D9E8AD3DA1B796E7609D2A77D76E709D13DF (void);
// 0x0000003C System.Void UnityEngine.Purchasing.FakeMoolahConfiguration::set_hashKey(System.String)
extern void FakeMoolahConfiguration_set_hashKey_mD00E161F82DD4113A196FA151542B387240CD0E7 (void);
// 0x0000003D System.Void UnityEngine.Purchasing.FakeMoolahConfiguration::SetMode(UnityEngine.Purchasing.CloudMoolahMode)
extern void FakeMoolahConfiguration_SetMode_m105B2B270C9CF484AD4F6222AD47EC180CF92479 (void);
// 0x0000003E System.Void UnityEngine.Purchasing.FakeMoolahConfiguration::.ctor()
extern void FakeMoolahConfiguration__ctor_m65F59D92BCD1076BE5066FD4458DA401B56DB609 (void);
// 0x0000003F System.Void UnityEngine.Purchasing.FakeMoolahExtensions::RestoreTransactionID(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>)
extern void FakeMoolahExtensions_RestoreTransactionID_m60F39513E3B963287D482C8973877FD99555C6DE (void);
// 0x00000040 System.Void UnityEngine.Purchasing.FakeMoolahExtensions::.ctor()
extern void FakeMoolahExtensions__ctor_m09F7C4E98C30F96C4609C437C5ABA8C60D61741A (void);
// 0x00000041 System.Void UnityEngine.Purchasing.IMoolahConfiguration::set_appKey(System.String)
// 0x00000042 System.Void UnityEngine.Purchasing.IMoolahConfiguration::set_hashKey(System.String)
// 0x00000043 System.Void UnityEngine.Purchasing.IMoolahConfiguration::SetMode(UnityEngine.Purchasing.CloudMoolahMode)
// 0x00000044 System.Void UnityEngine.Purchasing.IMoolahExtension::RestoreTransactionID(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>)
// 0x00000045 System.Void UnityEngine.Purchasing.MoolahStoreImpl::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern void MoolahStoreImpl_Initialize_m3F9443890CD015A050998C7E72FF0A9E17930E26 (void);
// 0x00000046 System.Void UnityEngine.Purchasing.MoolahStoreImpl::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern void MoolahStoreImpl_RetrieveProducts_mE1D6C6D508235803C9435C65D4016D8CEC0EC8EF (void);
// 0x00000047 System.Int32 UnityEngine.Purchasing.MoolahStoreImpl::GetProductTypeIndex(UnityEngine.Purchasing.ProductType)
extern void MoolahStoreImpl_GetProductTypeIndex_m9A9160800FFFDAF0CC888E82EC988BF6BDE760B6 (void);
// 0x00000048 System.Void UnityEngine.Purchasing.MoolahStoreImpl::VaildateProductProcess(System.Boolean,System.String)
extern void MoolahStoreImpl_VaildateProductProcess_mA42639497798AFB33EDABD9CCE323D575A2BA48A (void);
// 0x00000049 System.String UnityEngine.Purchasing.MoolahStoreImpl::GetCurrentString(System.Object)
extern void MoolahStoreImpl_GetCurrentString_mFE3F2818119B89977F2CF3EC299A1E04930392A0 (void);
// 0x0000004A System.Collections.IEnumerator UnityEngine.Purchasing.MoolahStoreImpl::VaildateProduct(System.String,System.String,System.Action`2<System.Boolean,System.String>)
extern void MoolahStoreImpl_VaildateProduct_m8018A52480BBB71EAF457ACD6F06D1906C4BA6D3 (void);
// 0x0000004B System.Void UnityEngine.Purchasing.MoolahStoreImpl::RetrieveProductsSucceeded(System.Collections.Generic.List`1<UnityEngine.Purchasing.Extension.ProductDescription>)
extern void MoolahStoreImpl_RetrieveProductsSucceeded_mAD118054D2158E57F5B62FDA5ED26D8F7925D6DC (void);
// 0x0000004C System.Void UnityEngine.Purchasing.MoolahStoreImpl::RetrieveProductsFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern void MoolahStoreImpl_RetrieveProductsFailed_m3ED20FCCD8D55C2B89F6F83FAC1F519081A18D2C (void);
// 0x0000004D System.Void UnityEngine.Purchasing.MoolahStoreImpl::ClosePayWebView(System.String)
extern void MoolahStoreImpl_ClosePayWebView_m4DB68A14F093DF422D53AA78D1D4D1107416FEB4 (void);
// 0x0000004E System.Void UnityEngine.Purchasing.MoolahStoreImpl::PurchaseRusult(System.String)
extern void MoolahStoreImpl_PurchaseRusult_mF6C7BECA3361B2F8422E67B358819C290038F639 (void);
// 0x0000004F System.Void UnityEngine.Purchasing.MoolahStoreImpl::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void MoolahStoreImpl_Purchase_m7D4061E9EEC2E50978EF9A21546F478B28342BC7 (void);
// 0x00000050 System.String UnityEngine.Purchasing.MoolahStoreImpl::DeviceUniqueIdentifier()
extern void MoolahStoreImpl_DeviceUniqueIdentifier_m020299D4FC51721606F2841F4688801707B4B6B8 (void);
// 0x00000051 System.Void UnityEngine.Purchasing.MoolahStoreImpl::RequestAuthCode(System.String,System.String,System.Action`3<System.String,System.String,System.String>,System.Action`2<System.String,System.String>)
extern void MoolahStoreImpl_RequestAuthCode_m47D7BCF1058698F0B1C72479077F70A0F3ECC82A (void);
// 0x00000052 System.Collections.IEnumerator UnityEngine.Purchasing.MoolahStoreImpl::RequestAuthCode(UnityEngine.WWWForm,System.String,System.String,System.Action`3<System.String,System.String,System.String>,System.Action`2<System.String,System.String>)
extern void MoolahStoreImpl_RequestAuthCode_m257C9CE803D4B7AFEFBDCCFFC5CFA4B6C90C69C0 (void);
// 0x00000053 System.Collections.IEnumerator UnityEngine.Purchasing.MoolahStoreImpl::StartPurchasePolling(System.String,System.String,System.Action`3<System.String,System.String,System.String>,System.Action`3<System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String>)
extern void MoolahStoreImpl_StartPurchasePolling_m15E9BF9628DE86801DBD9F00167DBCDEDE438510 (void);
// 0x00000054 System.Void UnityEngine.Purchasing.MoolahStoreImpl::PurchaseSucceed(System.String,System.String,System.String)
extern void MoolahStoreImpl_PurchaseSucceed_m569EBFB2F074685F0EC6039436BB991B73135C9F (void);
// 0x00000055 System.Void UnityEngine.Purchasing.MoolahStoreImpl::PurchaseFailed(System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String)
extern void MoolahStoreImpl_PurchaseFailed_m6CE431F13387A4897909013E1B2762A98D2257D3 (void);
// 0x00000056 System.Void UnityEngine.Purchasing.MoolahStoreImpl::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void MoolahStoreImpl_FinishTransaction_m8572B7F8035D32209F5732C6D5BD700762941B42 (void);
// 0x00000057 System.String UnityEngine.Purchasing.MoolahStoreImpl::GetStringMD5(System.String)
extern void MoolahStoreImpl_GetStringMD5_m9A96BD7F255B3C87B71AE82447BFA3A78340134B (void);
// 0x00000058 System.String UnityEngine.Purchasing.MoolahStoreImpl::get_appKey()
extern void MoolahStoreImpl_get_appKey_m5E6F079A6E225D99E991D0FDC0003446BFEAEB7C (void);
// 0x00000059 System.Void UnityEngine.Purchasing.MoolahStoreImpl::set_appKey(System.String)
extern void MoolahStoreImpl_set_appKey_m2411D3DDC65EC1EBEAD90275E94479A830D7940F (void);
// 0x0000005A System.String UnityEngine.Purchasing.MoolahStoreImpl::get_hashKey()
extern void MoolahStoreImpl_get_hashKey_mDCF66064B754ED75D43ADD0B6F7AA033531654CD (void);
// 0x0000005B System.Void UnityEngine.Purchasing.MoolahStoreImpl::set_hashKey(System.String)
extern void MoolahStoreImpl_set_hashKey_m7A4C659FBC1DA22EFE4925CA4515CB152F276D52 (void);
// 0x0000005C System.String UnityEngine.Purchasing.MoolahStoreImpl::get_notificationURL()
extern void MoolahStoreImpl_get_notificationURL_m6CAB979675D3F9513A50A42F7879A800BD91BE4A (void);
// 0x0000005D System.Void UnityEngine.Purchasing.MoolahStoreImpl::set_notificationURL(System.String)
extern void MoolahStoreImpl_set_notificationURL_m2B7249708CA47654EA35834904BCE142FB8F0889 (void);
// 0x0000005E System.Void UnityEngine.Purchasing.MoolahStoreImpl::SetMode(UnityEngine.Purchasing.CloudMoolahMode)
extern void MoolahStoreImpl_SetMode_mD57A5FB00B84A521667649915915B05A625C2312 (void);
// 0x0000005F UnityEngine.Purchasing.CloudMoolahMode UnityEngine.Purchasing.MoolahStoreImpl::GetMode()
extern void MoolahStoreImpl_GetMode_m18F79E1B2C54422F200644733B436C6C2BA75A1E (void);
// 0x00000060 System.Void UnityEngine.Purchasing.MoolahStoreImpl::RestoreTransactionID(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>)
extern void MoolahStoreImpl_RestoreTransactionID_m5E0246AA07B0217C1A031581F5E4D86DA92C570B (void);
// 0x00000061 System.Collections.IEnumerator UnityEngine.Purchasing.MoolahStoreImpl::RestoreTransactionIDProcess(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>)
extern void MoolahStoreImpl_RestoreTransactionIDProcess_m92AF1C35C226D0E8F75E9D19D8C18449C65148E9 (void);
// 0x00000062 System.Void UnityEngine.Purchasing.MoolahStoreImpl::ValidateReceipt(System.String,System.String,System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String>)
extern void MoolahStoreImpl_ValidateReceipt_m17ED1BF7D7369BB465760C35F433AED8633176A9 (void);
// 0x00000063 System.Collections.IEnumerator UnityEngine.Purchasing.MoolahStoreImpl::ValidateReceiptProcess(System.String,System.String,System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String>)
extern void MoolahStoreImpl_ValidateReceiptProcess_m2EA609D2AD7C852ECF3AFAD3FAFB484D99E111D4 (void);
// 0x00000064 System.Void UnityEngine.Purchasing.MoolahStoreImpl::.ctor()
extern void MoolahStoreImpl__ctor_mFAC1F88F558A17497C76BCDC9C2058EB85A1562B (void);
// 0x00000065 System.Void UnityEngine.Purchasing.MoolahStoreImpl::.cctor()
extern void MoolahStoreImpl__cctor_m7C188448D97CED80EA0BB3CD8DB793A2212FB654 (void);
// 0x00000066 System.Void UnityEngine.Purchasing.MoolahStoreImpl::<RetrieveProducts>b__9_0(System.Boolean,System.String)
extern void MoolahStoreImpl_U3CRetrieveProductsU3Eb__9_0_mA70E7E82721A2F1F72AB6AADAD49E098B2908AC3 (void);
// 0x00000067 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::.ctor(System.Int32)
extern void U3CVaildateProductU3Ed__13__ctor_mD7131C16EA8928B677C8680B570C00CC09F2E8F1 (void);
// 0x00000068 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::System.IDisposable.Dispose()
extern void U3CVaildateProductU3Ed__13_System_IDisposable_Dispose_mD847F8095490F98D20FFE0907F7A9839FD9C1789 (void);
// 0x00000069 System.Boolean UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::MoveNext()
extern void U3CVaildateProductU3Ed__13_MoveNext_m64E090D65FC459AF1B48DAF5987960144B44D06D (void);
// 0x0000006A System.Object UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CVaildateProductU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D50D1149F3B400899CBF2B963050F2EED21FCAB (void);
// 0x0000006B System.Void UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::System.Collections.IEnumerator.Reset()
extern void U3CVaildateProductU3Ed__13_System_Collections_IEnumerator_Reset_m2C7CB5050F4B600C6672958CAD1A919BD35794F2 (void);
// 0x0000006C System.Object UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CVaildateProductU3Ed__13_System_Collections_IEnumerator_get_Current_m0546AFE5B168974CF49F2DF98FED4C6F97E4BDE4 (void);
// 0x0000006D System.Void UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m2E668B4CF04C610ACB428DA4D5E2821F838BD02B (void);
// 0x0000006E System.Void UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::<Purchase>b__0(System.String,System.String,System.String)
extern void U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__0_m824D79DA93DF5EC7A1CF169E6927C4EEBC700597 (void);
// 0x0000006F System.Void UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::<Purchase>b__1(System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String)
extern void U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__1_mC3A9679284C1AAD7B47C9D454039D3A2A5B86E19 (void);
// 0x00000070 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::<Purchase>b__2(System.String,System.String,System.String)
extern void U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__2_m6F8F894B5370A4B488151BD4A9F9199BCA7B9058 (void);
// 0x00000071 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::<Purchase>b__3(System.String,System.String)
extern void U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__3_m2EC1D38BB5E9B77A137AEFEC784FA9D53E44AAD4 (void);
// 0x00000072 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::.ctor(System.Int32)
extern void U3CRequestAuthCodeU3Ed__22__ctor_m36D849DDBC9DBF849505947938FBE9F2F7EE3244 (void);
// 0x00000073 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::System.IDisposable.Dispose()
extern void U3CRequestAuthCodeU3Ed__22_System_IDisposable_Dispose_m374F8E8CD511EE9ECCC8E4584C10710B3F88F3BF (void);
// 0x00000074 System.Boolean UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::MoveNext()
extern void U3CRequestAuthCodeU3Ed__22_MoveNext_mAFF077966E415E8D43E894942890686D51027A17 (void);
// 0x00000075 System.Object UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestAuthCodeU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6EA14BF15A4A960A4217BCA14C07F2846DA23789 (void);
// 0x00000076 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::System.Collections.IEnumerator.Reset()
extern void U3CRequestAuthCodeU3Ed__22_System_Collections_IEnumerator_Reset_m7CFCCF10AE375362EA01A1EFB32DD16EFAE6FE12 (void);
// 0x00000077 System.Object UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CRequestAuthCodeU3Ed__22_System_Collections_IEnumerator_get_Current_m1737C1780144D641BB88078D0B8E4264545CF9EC (void);
// 0x00000078 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::.ctor(System.Int32)
extern void U3CStartPurchasePollingU3Ed__23__ctor_mB3E1CEBA3BAFD94A43299B63F518D93D1A84D8C8 (void);
// 0x00000079 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::System.IDisposable.Dispose()
extern void U3CStartPurchasePollingU3Ed__23_System_IDisposable_Dispose_m301B2C4DC2D76C82F9463A0FCFE1AFD1213F6FBB (void);
// 0x0000007A System.Boolean UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::MoveNext()
extern void U3CStartPurchasePollingU3Ed__23_MoveNext_mD58DCCEE7FDC0AA520DA7E0981613D34086B50D9 (void);
// 0x0000007B System.Object UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartPurchasePollingU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF15CBB6054571BADC7FE3D446BF5C3C749076F2F (void);
// 0x0000007C System.Void UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::System.Collections.IEnumerator.Reset()
extern void U3CStartPurchasePollingU3Ed__23_System_Collections_IEnumerator_Reset_m9AE3D70AAA203B8132364FB8D133BCAB9406F0C4 (void);
// 0x0000007D System.Object UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CStartPurchasePollingU3Ed__23_System_Collections_IEnumerator_get_Current_m15E7E58BB0B675D5008691A6660F52B8DFDED19F (void);
// 0x0000007E System.Void UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::.ctor(System.Int32)
extern void U3CRestoreTransactionIDProcessU3Ed__45__ctor_mD00F86A281E946BEE7981BC4F7BF3FD0B9E5747A (void);
// 0x0000007F System.Void UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::System.IDisposable.Dispose()
extern void U3CRestoreTransactionIDProcessU3Ed__45_System_IDisposable_Dispose_mA4D8E5392ED0366300A4916961641CD3E07C98A3 (void);
// 0x00000080 System.Boolean UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::MoveNext()
extern void U3CRestoreTransactionIDProcessU3Ed__45_MoveNext_m15449CD4ABCB420BCB2CD1F39A5D20757A774625 (void);
// 0x00000081 System.Object UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD32D80CC7257BC108D3CF7C56E409EC3A7197CC6 (void);
// 0x00000082 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::System.Collections.IEnumerator.Reset()
extern void U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_IEnumerator_Reset_m9A7097DD8EF993496E5CAD955F729F0F1AE0936A (void);
// 0x00000083 System.Object UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::System.Collections.IEnumerator.get_Current()
extern void U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_IEnumerator_get_Current_mA0C6EE86705186177EEFF5A8AB07A9A9D2A616C2 (void);
// 0x00000084 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::.ctor(System.Int32)
extern void U3CValidateReceiptProcessU3Ed__47__ctor_mDD05E6797CA356BE85C9E2BEB7D83D28AF492A46 (void);
// 0x00000085 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::System.IDisposable.Dispose()
extern void U3CValidateReceiptProcessU3Ed__47_System_IDisposable_Dispose_mE5998FBFD6D62AC8CAC47650C0ABE2E3CF6FFE33 (void);
// 0x00000086 System.Boolean UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::MoveNext()
extern void U3CValidateReceiptProcessU3Ed__47_MoveNext_mD1A52FD5FE7BA88384CE3946DC797CFEB3869E09 (void);
// 0x00000087 System.Object UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CValidateReceiptProcessU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD0E5DB2A5D58A1BC0BAA5CD30A1234021E01976 (void);
// 0x00000088 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::System.Collections.IEnumerator.Reset()
extern void U3CValidateReceiptProcessU3Ed__47_System_Collections_IEnumerator_Reset_m412728DCE7F91B35C9C1AD54E4E4A2B09C5E38E8 (void);
// 0x00000089 System.Object UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::System.Collections.IEnumerator.get_Current()
extern void U3CValidateReceiptProcessU3Ed__47_System_Collections_IEnumerator_get_Current_m9639D0F0A121AAF1DA33FA6FB4712B877B993E01 (void);
// 0x0000008A System.Void UnityEngine.Purchasing.PayMethod::showPayWebView(System.String,System.String,System.String,System.String,System.String)
extern void PayMethod_showPayWebView_mB8E25FB438411674827723ABE765A5B8900D0BEF (void);
// 0x0000008B System.String UnityEngine.Purchasing.PayMethod::getDeviceID()
extern void PayMethod_getDeviceID_m0073DAA785743281B1C6C78663A57B6A5005D44A (void);
// 0x0000008C System.Void UnityEngine.Purchasing.FakeGooglePlayStoreExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern void FakeGooglePlayStoreExtensions_RestoreTransactions_mEF684C1EB626E3BDE09DFE40C77BD62BBFD01360 (void);
// 0x0000008D System.Void UnityEngine.Purchasing.FakeGooglePlayStoreExtensions::.ctor()
extern void FakeGooglePlayStoreExtensions__ctor_m464BEBD60E48E93FDF3BDE47980215859E71EDF8 (void);
// 0x0000008E System.Void UnityEngine.Purchasing.GooglePlayAndroidJavaStore::.ctor(UnityEngine.AndroidJavaObject,Uniject.IUtil)
extern void GooglePlayAndroidJavaStore__ctor_m97A8F8ACEDA88C05E81EE76204F1E22C4D79C86C (void);
// 0x0000008F System.Void UnityEngine.Purchasing.GooglePlayAndroidJavaStore::Purchase(System.String,System.String)
extern void GooglePlayAndroidJavaStore_Purchase_mF694542F8377C5BF4EFB47AFEC568216CEAC23AC (void);
// 0x00000090 System.Void UnityEngine.Purchasing.FakeGooglePlayConfiguration::.ctor()
extern void FakeGooglePlayConfiguration__ctor_mD0E7B971BB1AF7E7D9A04222E67ACBE2494C4BB1 (void);
// 0x00000091 System.Void UnityEngine.Purchasing.GooglePlayStoreCallback::.ctor(System.Action`1<System.Boolean>)
extern void GooglePlayStoreCallback__ctor_mAC85FB73D091AFDD4914203F4162E2C6D92E5768 (void);
// 0x00000092 System.Void UnityEngine.Purchasing.GooglePlayStoreCallback::OnTransactionsRestored(System.Boolean)
extern void GooglePlayStoreCallback_OnTransactionsRestored_m78EAD5A60BC2EB7007E3157B21CA7AF7FBEA8F30 (void);
// 0x00000093 System.Void UnityEngine.Purchasing.GooglePlayStoreExtensions::.ctor()
extern void GooglePlayStoreExtensions__ctor_m12E98D6CD646CE0791A604AC22EC81CBA9192BBB (void);
// 0x00000094 System.Void UnityEngine.Purchasing.GooglePlayStoreExtensions::SetAndroidJavaObject(UnityEngine.AndroidJavaObject)
extern void GooglePlayStoreExtensions_SetAndroidJavaObject_mD04F18093F85F93AAB7EF4C71F440B17222E6F53 (void);
// 0x00000095 System.Void UnityEngine.Purchasing.GooglePlayStoreExtensions::SetPublicKey(System.String)
extern void GooglePlayStoreExtensions_SetPublicKey_mEC44549110434E656D736A90A4B5672343068E75 (void);
// 0x00000096 System.Void UnityEngine.Purchasing.GooglePlayStoreExtensions::UpgradeDowngradeSubscription(System.String,System.String)
extern void GooglePlayStoreExtensions_UpgradeDowngradeSubscription_mD66CD85A8D82E06641A2EEA8E1B1017A6BDAA53E (void);
// 0x00000097 System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Purchasing.GooglePlayStoreExtensions::GetProductJSONDictionary()
extern void GooglePlayStoreExtensions_GetProductJSONDictionary_m94913623446E4B7242F68B7DBC07916B1F9746FD (void);
// 0x00000098 System.Void UnityEngine.Purchasing.GooglePlayStoreExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern void GooglePlayStoreExtensions_RestoreTransactions_m07A56A5F888ABF508FFFD4A7825B1D85346F275B (void);
// 0x00000099 System.Void UnityEngine.Purchasing.GooglePlayStoreExtensions::FinishAdditionalTransaction(System.String,System.String)
extern void GooglePlayStoreExtensions_FinishAdditionalTransaction_mFD2E47EA4FCCF1064B0E3B0C50FA560F6BE82BDC (void);
// 0x0000009A System.Void UnityEngine.Purchasing.IGooglePlayStoreExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
// 0x0000009B System.Void UnityEngine.Purchasing.FakeSamsungAppsExtensions::SetMode(UnityEngine.Purchasing.SamsungAppsMode)
extern void FakeSamsungAppsExtensions_SetMode_m08E38EDC56A095693A4CAA43DE5770B04793E2F8 (void);
// 0x0000009C System.Void UnityEngine.Purchasing.FakeSamsungAppsExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern void FakeSamsungAppsExtensions_RestoreTransactions_m8AAD68FD295B9A9E0A19CD4B9DFBB96C76D20938 (void);
// 0x0000009D System.Void UnityEngine.Purchasing.FakeSamsungAppsExtensions::.ctor()
extern void FakeSamsungAppsExtensions__ctor_m2762E5F140664B1D2C06BF98F9E3305657F86F0D (void);
// 0x0000009E System.Void UnityEngine.Purchasing.ISamsungAppsCallback::OnTransactionsRestored(System.Boolean)
// 0x0000009F System.Void UnityEngine.Purchasing.ISamsungAppsConfiguration::SetMode(UnityEngine.Purchasing.SamsungAppsMode)
// 0x000000A0 System.Void UnityEngine.Purchasing.ISamsungAppsExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
// 0x000000A1 System.Void UnityEngine.Purchasing.SamsungAppsJavaBridge::.ctor(UnityEngine.Purchasing.ISamsungAppsCallback)
extern void SamsungAppsJavaBridge__ctor_m3F6C93E33F9AFDC554EEE81A5A3FCFDA12F74C5F (void);
// 0x000000A2 System.Void UnityEngine.Purchasing.SamsungAppsJavaBridge::OnTransactionsRestored(System.Boolean)
extern void SamsungAppsJavaBridge_OnTransactionsRestored_m6C9A32FDF7FA492355F179566C7A583CDA3EC87D (void);
// 0x000000A3 System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::.ctor()
extern void SamsungAppsStoreExtensions__ctor_mDE673A17D1A218BB58F30CCAD4BC156018846BB1 (void);
// 0x000000A4 System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::SetAndroidJavaObject(UnityEngine.AndroidJavaObject)
extern void SamsungAppsStoreExtensions_SetAndroidJavaObject_m505CDB557622B24905E3C4472C815EB9DFAC99D3 (void);
// 0x000000A5 System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::SetMode(UnityEngine.Purchasing.SamsungAppsMode)
extern void SamsungAppsStoreExtensions_SetMode_m7669E45FA8D5D08CA9EFB4C8C8F8447A12E2CB45 (void);
// 0x000000A6 System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern void SamsungAppsStoreExtensions_RestoreTransactions_m73B9A73971ADBD87CE8C4DF9AFC96169F53DEF3E (void);
// 0x000000A7 System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::OnTransactionsRestored(System.Boolean)
extern void SamsungAppsStoreExtensions_OnTransactionsRestored_m4B8E3FC5C0AB8651891C619D91F681E7912F0A03 (void);
// 0x000000A8 System.Void UnityEngine.Purchasing.FakeUnityChannelConfiguration::.ctor()
extern void FakeUnityChannelConfiguration__ctor_m37F2E7C0DB318DF94BC7ED9BBD6A7FFD39E9D6D5 (void);
// 0x000000A9 System.Void UnityEngine.Purchasing.FakeUnityChannelExtensions::.ctor()
extern void FakeUnityChannelExtensions__ctor_m88DC72E89A539BDBAD29F4EFF9D3BFD4B08B3E57 (void);
// 0x000000AA System.Void UnityEngine.Purchasing.FakeUDPExtension::.ctor()
extern void FakeUDPExtension__ctor_mBFECA88FA885EA52972CD112CBACEB31B356014F (void);
// 0x000000AB System.Void UnityEngine.Purchasing.INativeUDPStore::Initialize(System.Action`2<System.Boolean,System.String>)
// 0x000000AC System.Void UnityEngine.Purchasing.INativeUDPStore::Purchase(System.String,System.Action`2<System.Boolean,System.String>,System.String)
// 0x000000AD System.Void UnityEngine.Purchasing.INativeUDPStore::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>,System.Action`2<System.Boolean,System.String>)
// 0x000000AE System.Void UnityEngine.Purchasing.INativeUDPStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
// 0x000000AF System.String UnityEngine.Purchasing.UDP::get_Name()
extern void UDP_get_Name_m297A3949ABF6B197B39C1A18FA460020D9CCD9FF (void);
// 0x000000B0 System.Void UnityEngine.Purchasing.UDPBindings::Initialize(System.Action`2<System.Boolean,System.String>)
extern void UDPBindings_Initialize_m02AECF9D41115AF9EBB64CD2148BF8B0D637ECD9 (void);
// 0x000000B1 System.Void UnityEngine.Purchasing.UDPBindings::Purchase(System.String,System.Action`2<System.Boolean,System.String>,System.String)
extern void UDPBindings_Purchase_m05C42D11B1B2C5D7D619CA299291A1865923C6D5 (void);
// 0x000000B2 System.Void UnityEngine.Purchasing.UDPBindings::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>,System.Action`2<System.Boolean,System.String>)
extern void UDPBindings_RetrieveProducts_m49B87240AF807B38031CA65733BE25570E9D19D2 (void);
// 0x000000B3 System.Void UnityEngine.Purchasing.UDPBindings::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void UDPBindings_FinishTransaction_m413D01154C61B993A719FE3EB562F8296CE1DD22 (void);
// 0x000000B4 UnityEngine.UDP.PurchaseInfo UnityEngine.Purchasing.UDPBindings::FindPurchaseInfo(System.String)
extern void UDPBindings_FindPurchaseInfo_m1C94CA4864598BEB281C619B874A119F064601F7 (void);
// 0x000000B5 System.Void UnityEngine.Purchasing.UDPBindings::OnInitialized(UnityEngine.UDP.UserInfo)
extern void UDPBindings_OnInitialized_m69EE4BB6672A3DDB1C6F50D927F859AFBA1B9502 (void);
// 0x000000B6 System.Void UnityEngine.Purchasing.UDPBindings::OnInitializeFailed(System.String)
extern void UDPBindings_OnInitializeFailed_m1607DBDBDBD73238F248F7E91286A1DD6323D04C (void);
// 0x000000B7 System.Void UnityEngine.Purchasing.UDPBindings::OnPurchase(UnityEngine.UDP.PurchaseInfo)
extern void UDPBindings_OnPurchase_m8AAD85481CF41F9080A38A91B6E47DDEBE8D7050 (void);
// 0x000000B8 System.Void UnityEngine.Purchasing.UDPBindings::OnPurchaseFailed(System.String,UnityEngine.UDP.PurchaseInfo)
extern void UDPBindings_OnPurchaseFailed_m1BD7B81CD0B9141E5B0F62A2F67684BB4434FBDF (void);
// 0x000000B9 System.Void UnityEngine.Purchasing.UDPBindings::OnPurchaseConsume(UnityEngine.UDP.PurchaseInfo)
extern void UDPBindings_OnPurchaseConsume_m64D9A198A8BB38FF9866F1FAF2C9ED16883212B7 (void);
// 0x000000BA System.Void UnityEngine.Purchasing.UDPBindings::OnPurchaseConsumeFailed(System.String,UnityEngine.UDP.PurchaseInfo)
extern void UDPBindings_OnPurchaseConsumeFailed_m819B887671382FFC885C71D1761BB25685BFD42D (void);
// 0x000000BB System.Void UnityEngine.Purchasing.UDPBindings::OnQueryInventory(UnityEngine.UDP.Inventory)
extern void UDPBindings_OnQueryInventory_m87E738D7040B1651BC410F2607F82464176BA879 (void);
// 0x000000BC System.Void UnityEngine.Purchasing.UDPBindings::OnQueryInventoryFailed(System.String)
extern void UDPBindings_OnQueryInventoryFailed_mB11290C76C63C34815FBF38AA4D6DEAD88081244 (void);
// 0x000000BD System.Void UnityEngine.Purchasing.UDPBindings::RetrieveProducts(System.String)
extern void UDPBindings_RetrieveProducts_mE136770E248B5B8BDAAB1C3BA4085E8FD82D8CFF (void);
// 0x000000BE System.Void UnityEngine.Purchasing.UDPBindings::Purchase(System.String,System.String)
extern void UDPBindings_Purchase_m63841A22D107D9F5AB458D7D133C272D683528EA (void);
// 0x000000BF System.Void UnityEngine.Purchasing.UDPBindings::FinishTransaction(System.String,System.String)
extern void UDPBindings_FinishTransaction_mCDA775F49A07D94B3A6B89EBCF1D473B92124265 (void);
// 0x000000C0 System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Purchasing.UDPBindings::StringPropertyToDictionary(System.Object)
extern void UDPBindings_StringPropertyToDictionary_mD8F3ECA6768BBDD3EB12EFD36447AFFF66468A1B (void);
// 0x000000C1 System.Void UnityEngine.Purchasing.UDPBindings::.ctor()
extern void UDPBindings__ctor_mE46AC1CEDDA8E5B90E9C803044939BA58F965190 (void);
// 0x000000C2 System.Void UnityEngine.Purchasing.UDPImpl::SetNativeStore(UnityEngine.Purchasing.INativeUDPStore)
extern void UDPImpl_SetNativeStore_m26D55B30D206C19CDD0A70E44DEA8E10CC022570 (void);
// 0x000000C3 System.Void UnityEngine.Purchasing.UDPImpl::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern void UDPImpl_Initialize_m1D79310D9A0FE2B93EDBD4FC7CC8B8689FE45A73 (void);
// 0x000000C4 System.Void UnityEngine.Purchasing.UDPImpl::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern void UDPImpl_RetrieveProducts_mB6AEEBE33DD200F3CCC5E9C9617AAD507C33A25F (void);
// 0x000000C5 System.Void UnityEngine.Purchasing.UDPImpl::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void UDPImpl_Purchase_m7F16982EE97D6C59A266C8F8D3B1C7DAF77FC1E1 (void);
// 0x000000C6 System.Void UnityEngine.Purchasing.UDPImpl::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void UDPImpl_FinishTransaction_mCB5C2613B4F63395F5595AD2A92A4B9DA713A587 (void);
// 0x000000C7 System.Void UnityEngine.Purchasing.UDPImpl::DictionaryToStringProperty(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Object)
extern void UDPImpl_DictionaryToStringProperty_mA225E2593D0AF8D9FD2B100C5D489F6262732C31 (void);
// 0x000000C8 System.Void UnityEngine.Purchasing.UDPImpl::.ctor()
extern void UDPImpl__ctor_mEF25815280F1C827783357EABD83AD1610F95838 (void);
// 0x000000C9 System.Void UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mB3801BC98C775E8EA7B125A08433C01C94BF01E8 (void);
// 0x000000CA System.Void UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass7_0::<RetrieveProducts>b__0(System.Boolean,System.String)
extern void U3CU3Ec__DisplayClass7_0_U3CRetrieveProductsU3Eb__0_m784DFD90A05246DC50DF2C177C710EFEA8CF55E8 (void);
// 0x000000CB System.Void UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass7_0::<RetrieveProducts>b__1(System.Boolean,System.String)
extern void U3CU3Ec__DisplayClass7_0_U3CRetrieveProductsU3Eb__1_m2094590C377E50F878DD69C15BFF55E366E900C6 (void);
// 0x000000CC System.Void UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mE8F183A145227EC4F9117C6559A6102E5E98284E (void);
// 0x000000CD System.Void UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass8_0::<Purchase>b__0(System.Boolean,System.String)
extern void U3CU3Ec__DisplayClass8_0_U3CPurchaseU3Eb__0_m04AF95989DF87441DFFDEDB10C0557823DFDB4CA (void);
// 0x000000CE System.Void UnityEngine.Purchasing.AppleStoreImpl::.ctor(Uniject.IUtil)
extern void AppleStoreImpl__ctor_mF4E15742FA52E6669461C28977A1F80D1C2D3349 (void);
// 0x000000CF System.Void UnityEngine.Purchasing.AppleStoreImpl::SetNativeStore(UnityEngine.Purchasing.INativeAppleStore)
extern void AppleStoreImpl_SetNativeStore_m2DA4002D8ACC3385AD9510A5097B26E7DA3B170A (void);
// 0x000000D0 System.Void UnityEngine.Purchasing.AppleStoreImpl::OnProductsRetrieved(System.String)
extern void AppleStoreImpl_OnProductsRetrieved_m001E2ECE4FD5F9BD6184C4D29BB3B67FDF00B0B6 (void);
// 0x000000D1 System.Void UnityEngine.Purchasing.AppleStoreImpl::RestoreTransactions(System.Action`1<System.Boolean>)
extern void AppleStoreImpl_RestoreTransactions_mDC111E1FDF419746DFC94D7AF769CA3BB234403D (void);
// 0x000000D2 System.Void UnityEngine.Purchasing.AppleStoreImpl::RegisterPurchaseDeferredListener(System.Action`1<UnityEngine.Purchasing.Product>)
extern void AppleStoreImpl_RegisterPurchaseDeferredListener_mED7C271C087A7CC17F872F74097D63F9A50394B8 (void);
// 0x000000D3 System.Void UnityEngine.Purchasing.AppleStoreImpl::OnPurchaseDeferred(System.String)
extern void AppleStoreImpl_OnPurchaseDeferred_m50D3D0C6D0ECA1F17D833D4101C8476BE7054735 (void);
// 0x000000D4 System.Void UnityEngine.Purchasing.AppleStoreImpl::OnPromotionalPurchaseAttempted(System.String)
extern void AppleStoreImpl_OnPromotionalPurchaseAttempted_mE0822A252C625B0FBABE8C5C0750D49B4E9F555E (void);
// 0x000000D5 System.Void UnityEngine.Purchasing.AppleStoreImpl::OnTransactionsRestoredSuccess()
extern void AppleStoreImpl_OnTransactionsRestoredSuccess_m464BA77AA019E6DBC40D6E9887D2AA803DBD6668 (void);
// 0x000000D6 System.Void UnityEngine.Purchasing.AppleStoreImpl::OnTransactionsRestoredFail(System.String)
extern void AppleStoreImpl_OnTransactionsRestoredFail_m1CC319C3AAC4DB0202B6FBA92228CACECDC8A629 (void);
// 0x000000D7 System.Void UnityEngine.Purchasing.AppleStoreImpl::OnAppReceiptRetrieved(System.String)
extern void AppleStoreImpl_OnAppReceiptRetrieved_mB5BF6B52F0542555A966D67991A431C5751EA630 (void);
// 0x000000D8 System.Void UnityEngine.Purchasing.AppleStoreImpl::OnAppReceiptRefreshedFailed()
extern void AppleStoreImpl_OnAppReceiptRefreshedFailed_m79D95233C3C50E66F8A966CD7CA44BC4075607FD (void);
// 0x000000D9 System.Void UnityEngine.Purchasing.AppleStoreImpl::MessageCallback(System.String,System.String,System.String,System.String)
extern void AppleStoreImpl_MessageCallback_mD0A78B20AACCAEB5D2ECE032884DE17D435454B6 (void);
// 0x000000DA System.Void UnityEngine.Purchasing.AppleStoreImpl::ProcessMessage(System.String,System.String,System.String,System.String)
extern void AppleStoreImpl_ProcessMessage_mBC42AD652176EECB2E0FF48E7DD022C0B3B02A27 (void);
// 0x000000DB System.Void UnityEngine.Purchasing.AppleStoreImpl::OnPurchaseSucceeded(System.String,System.String,System.String)
extern void AppleStoreImpl_OnPurchaseSucceeded_m054A29DFF3FBEBFA7CC8E8939FE40E40EC143275 (void);
// 0x000000DC UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.AppleStoreImpl::getAppleReceiptFromBase64String(System.String)
extern void AppleStoreImpl_getAppleReceiptFromBase64String_m364893746979225CC1AB27603521360692544F73 (void);
// 0x000000DD System.Boolean UnityEngine.Purchasing.AppleStoreImpl::isValidPurchaseState(UnityEngine.Purchasing.Security.AppleReceipt,System.String)
extern void AppleStoreImpl_isValidPurchaseState_mE75B60ADAE50734364CAFD49B1E4C2E3C5FB00CC (void);
// 0x000000DE System.Void UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m7FA995B8F647776E355609C3FFC0367289B3BBB5 (void);
// 0x000000DF System.Boolean UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass23_0::<OnProductsRetrieved>b__0(UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt)
extern void U3CU3Ec__DisplayClass23_0_U3COnProductsRetrievedU3Eb__0_m7C28D18696848E58A8B9FD7055C677CA3434A6B5 (void);
// 0x000000E0 System.Void UnityEngine.Purchasing.AppleStoreImpl_<>c::.cctor()
extern void U3CU3Ec__cctor_m0DB2B8BE23069CBE385EDDC3F52A9B6AE25AE6FB (void);
// 0x000000E1 System.Void UnityEngine.Purchasing.AppleStoreImpl_<>c::.ctor()
extern void U3CU3Ec__ctor_mD2D72C433C495C989EF96012553B588B00B6986B (void);
// 0x000000E2 System.Int32 UnityEngine.Purchasing.AppleStoreImpl_<>c::<OnProductsRetrieved>b__23_1(UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt,UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt)
extern void U3CU3Ec_U3COnProductsRetrievedU3Eb__23_1_m86BC2757E431FC403AFE8F352E24D206148117D4 (void);
// 0x000000E3 System.Int32 UnityEngine.Purchasing.AppleStoreImpl_<>c::<isValidPurchaseState>b__40_1(UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt,UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt)
extern void U3CU3Ec_U3CisValidPurchaseStateU3Eb__40_1_m48B14AC5D2D95A49540D33484188B8B62C5FE59D (void);
// 0x000000E4 System.Void UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mDE3846C8B1B088ACFF698DF19198C1ED93269A88 (void);
// 0x000000E5 System.Void UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass36_0::<MessageCallback>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CMessageCallbackU3Eb__0_m5B3F37B7481A49993869901596407563E6E52F7B (void);
// 0x000000E6 System.Void UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m4CA5E65FCE9E28F3BFFA1DE4D2F8810927AE97C3 (void);
// 0x000000E7 System.Boolean UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass40_0::<isValidPurchaseState>b__0(UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt)
extern void U3CU3Ec__DisplayClass40_0_U3CisValidPurchaseStateU3Eb__0_mB104C5E311A21684B12773D06A106BF0BB63614A (void);
// 0x000000E8 System.Void UnityEngine.Purchasing.FakeAppleConfiguation::.ctor()
extern void FakeAppleConfiguation__ctor_m130C216E602F6A4C7219033A3492AFDC5E2F68C0 (void);
// 0x000000E9 System.Void UnityEngine.Purchasing.FakeAppleExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern void FakeAppleExtensions_RestoreTransactions_mA418ACE9FE6824BFC1D23781B323C8729429D05E (void);
// 0x000000EA System.Void UnityEngine.Purchasing.FakeAppleExtensions::RegisterPurchaseDeferredListener(System.Action`1<UnityEngine.Purchasing.Product>)
extern void FakeAppleExtensions_RegisterPurchaseDeferredListener_m61038BDEB493FC8A93D7058D923D6E808EC22BAA (void);
// 0x000000EB System.Void UnityEngine.Purchasing.FakeAppleExtensions::.ctor()
extern void FakeAppleExtensions__ctor_mAC4EE655EBEB277FBCA176614EDE6C3B0CF0909C (void);
// 0x000000EC System.Void UnityEngine.Purchasing.IAppleExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
// 0x000000ED System.Void UnityEngine.Purchasing.IAppleExtensions::RegisterPurchaseDeferredListener(System.Action`1<UnityEngine.Purchasing.Product>)
// 0x000000EE UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.INativeStoreProvider::GetAndroidStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.AppStore,UnityEngine.Purchasing.Extension.IPurchasingBinder,Uniject.IUtil)
// 0x000000EF UnityEngine.Purchasing.INativeAppleStore UnityEngine.Purchasing.INativeStoreProvider::GetStorekit(UnityEngine.Purchasing.IUnityCallback)
// 0x000000F0 UnityEngine.Purchasing.INativeTizenStore UnityEngine.Purchasing.INativeStoreProvider::GetTizenStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.Extension.IPurchasingBinder)
// 0x000000F1 UnityEngine.Purchasing.INativeFacebookStore UnityEngine.Purchasing.INativeStoreProvider::GetFacebookStore()
// 0x000000F2 System.Void UnityEngine.Purchasing.IStoreInternal::SetModule(UnityEngine.Purchasing.StandardPurchasingModule)
// 0x000000F3 UnityEngine.Purchasing.Product[] UnityEngine.Purchasing.JSONStore::get_storeCatalog()
extern void JSONStore_get_storeCatalog_mF779A76E645E5B6EC497B0A93061AEC65D537CA1 (void);
// 0x000000F4 System.Void UnityEngine.Purchasing.JSONStore::.ctor()
extern void JSONStore__ctor_mB3587A87996CA5DE31FCF8E58661F4C7F1DB7CA8 (void);
// 0x000000F5 System.Void UnityEngine.Purchasing.JSONStore::SetNativeStore(UnityEngine.Purchasing.INativeStore)
extern void JSONStore_SetNativeStore_m4DCF0FB93FC5F686D045F764F664E8203E4731B5 (void);
// 0x000000F6 System.Void UnityEngine.Purchasing.JSONStore::UnityEngine.Purchasing.IStoreInternal.SetModule(UnityEngine.Purchasing.StandardPurchasingModule)
extern void JSONStore_UnityEngine_Purchasing_IStoreInternal_SetModule_m1E374BB3F819C16ED4DFC46F66B6A6A4B5886F12 (void);
// 0x000000F7 System.Void UnityEngine.Purchasing.JSONStore::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern void JSONStore_Initialize_mAF894B8D15EF3F8228D12B0CA93B4E1B1C4A4635 (void);
// 0x000000F8 System.Void UnityEngine.Purchasing.JSONStore::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern void JSONStore_RetrieveProducts_m5E1B9A13E6E257D1190F9F1AEB299469489A7BB5 (void);
// 0x000000F9 System.Void UnityEngine.Purchasing.JSONStore::ProcessManagedStoreResponse(System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition>)
extern void JSONStore_ProcessManagedStoreResponse_mFB62B34AB96B5FDA8A3007F20D4ED31607F1DAF6 (void);
// 0x000000FA System.Void UnityEngine.Purchasing.JSONStore::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void JSONStore_Purchase_m83FBD0A2F3E977BF86FD1C27E7B28957FAAEB94B (void);
// 0x000000FB System.Void UnityEngine.Purchasing.JSONStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void JSONStore_FinishTransaction_m4ED2F4415C2F94AE69638616B0C82FC25AEE6890 (void);
// 0x000000FC System.Void UnityEngine.Purchasing.JSONStore::OnSetupFailed(System.String)
extern void JSONStore_OnSetupFailed_mDD10DCDD5BD9225E4D8849D812D046CB4E1D577D (void);
// 0x000000FD System.Void UnityEngine.Purchasing.JSONStore::OnProductsRetrieved(System.String)
extern void JSONStore_OnProductsRetrieved_m4AFC0C5B8A4706C3BEB3332DBFA4836E4F37B8B0 (void);
// 0x000000FE System.Void UnityEngine.Purchasing.JSONStore::OnPurchaseSucceeded(System.String,System.String,System.String)
extern void JSONStore_OnPurchaseSucceeded_mFF15EA23262E65C625EC04C98FAD07C443F6F258 (void);
// 0x000000FF System.Void UnityEngine.Purchasing.JSONStore::OnPurchaseFailed(System.String)
extern void JSONStore_OnPurchaseFailed_m37F03E8FE631267680BA50A48F4888007DB30D02 (void);
// 0x00000100 System.Void UnityEngine.Purchasing.JSONStore::OnPurchaseFailed(UnityEngine.Purchasing.Extension.PurchaseFailureDescription,System.String)
extern void JSONStore_OnPurchaseFailed_m36F955F502C8CB4635298EC8DA5C885F22C809BD (void);
// 0x00000101 UnityEngine.Purchasing.Extension.PurchaseFailureDescription UnityEngine.Purchasing.JSONStore::GetLastPurchaseFailureDescription()
extern void JSONStore_GetLastPurchaseFailureDescription_mA8AAAEB71C462D48A6FBC791D9AD6F0E94458687 (void);
// 0x00000102 UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode UnityEngine.Purchasing.JSONStore::GetLastStoreSpecificPurchaseErrorCode()
extern void JSONStore_GetLastStoreSpecificPurchaseErrorCode_mA7FE9B2225DB70895E2B9B2EC33D8126C243AE4F (void);
// 0x00000103 System.String UnityEngine.Purchasing.JSONStore::FormatUnifiedReceipt(System.String,System.String)
extern void JSONStore_FormatUnifiedReceipt_mB625B4256FE85440544C80BC029E12D69B079A98 (void);
// 0x00000104 UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode UnityEngine.Purchasing.JSONStore::ParseStoreSpecificPurchaseErrorCode(System.String)
extern void JSONStore_ParseStoreSpecificPurchaseErrorCode_m3F8BBE314D35195EE258162205FC5C68871729D0 (void);
// 0x00000105 UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.NativeStoreProvider::GetAndroidStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.AppStore,UnityEngine.Purchasing.Extension.IPurchasingBinder,Uniject.IUtil)
extern void NativeStoreProvider_GetAndroidStore_mF4FB1D3F79FAF49557EF8FC97FCD76D37913EEC9 (void);
// 0x00000106 UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.NativeStoreProvider::GetAndroidStoreHelper(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.AppStore,UnityEngine.Purchasing.Extension.IPurchasingBinder,Uniject.IUtil)
extern void NativeStoreProvider_GetAndroidStoreHelper_m26277644EF70586FA6A1EEF6846F78816164D970 (void);
// 0x00000107 UnityEngine.Purchasing.INativeAppleStore UnityEngine.Purchasing.NativeStoreProvider::GetStorekit(UnityEngine.Purchasing.IUnityCallback)
extern void NativeStoreProvider_GetStorekit_mAE5CDF5F97F24DC1E18FC0A5714009EE848842D2 (void);
// 0x00000108 UnityEngine.Purchasing.INativeTizenStore UnityEngine.Purchasing.NativeStoreProvider::GetTizenStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.Extension.IPurchasingBinder)
extern void NativeStoreProvider_GetTizenStore_mA8F9D15A05A96CC27EC68F66F68C0F3EA0DCC4AB (void);
// 0x00000109 UnityEngine.Purchasing.INativeFacebookStore UnityEngine.Purchasing.NativeStoreProvider::GetFacebookStore()
extern void NativeStoreProvider_GetFacebookStore_m41C0112C7A6D67A9E6EDAB22B4A0535E74EBDC3F (void);
// 0x0000010A System.Void UnityEngine.Purchasing.NativeStoreProvider::.ctor()
extern void NativeStoreProvider__ctor_mA5063656EFA925BF836CAB857DAC6A3E828225BC (void);
// 0x0000010B UnityEngine.Purchasing.CloudCatalogImpl UnityEngine.Purchasing.CloudCatalogImpl::CreateInstance(System.String)
extern void CloudCatalogImpl_CreateInstance_m0D95A726E5D28C9808889A34F3804A4A17F11947 (void);
// 0x0000010C System.Void UnityEngine.Purchasing.CloudCatalogImpl::.ctor(UnityEngine.Purchasing.IAsyncWebUtil,System.String,UnityEngine.ILogger,System.String,System.String)
extern void CloudCatalogImpl__ctor_m45318C200DF78F65839AEFBEF9B5F7FDD72CB8C5 (void);
// 0x0000010D System.Void UnityEngine.Purchasing.CloudCatalogImpl::FetchProducts(System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>)
extern void CloudCatalogImpl_FetchProducts_m1CC03117C09692D39FBB222FE9B41BE1CA118B64 (void);
// 0x0000010E System.Void UnityEngine.Purchasing.CloudCatalogImpl::FetchProducts(System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>,System.Int32)
extern void CloudCatalogImpl_FetchProducts_mBDDECAD1D2246A39A9DB0E32A908E4EF84BE1044 (void);
// 0x0000010F System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.CloudCatalogImpl::ParseProductsFromJSON(System.String,System.String)
extern void CloudCatalogImpl_ParseProductsFromJSON_m22D9A661C64271EEAFF757AD9093185A007AB826 (void);
// 0x00000110 System.String UnityEngine.Purchasing.CloudCatalogImpl::CamelCaseToSnakeCase(System.String)
extern void CloudCatalogImpl_CamelCaseToSnakeCase_m2047DB6BCC713AA184646B208E304D403A718538 (void);
// 0x00000111 System.Void UnityEngine.Purchasing.CloudCatalogImpl::TryPersistCatalog(System.String)
extern void CloudCatalogImpl_TryPersistCatalog_m4C3807711A0EF333813D9BC35C3CD02F7A70E8B1 (void);
// 0x00000112 System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.CloudCatalogImpl::TryLoadCachedCatalog()
extern void CloudCatalogImpl_TryLoadCachedCatalog_m1CD4B57DDE9F3AB21CD95D3BE814162AA222700B (void);
// 0x00000113 System.Void UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m182085D92ED214D3C86020BCCA8FB7A573F0F8E5 (void);
// 0x00000114 System.Void UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0::<FetchProducts>b__0(System.String)
extern void U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__0_m3FCB6517C013B1C95ECDFD21240F93033CA1520D (void);
// 0x00000115 System.Void UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0::<FetchProducts>b__1(System.String)
extern void U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__1_m89A0DD8D729C59B940D1AEBA8D30C49A12C7D0D1 (void);
// 0x00000116 System.Void UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0::<FetchProducts>b__2()
extern void U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__2_m1648D82A87548CC6A555F710A3C52CA755103D36 (void);
// 0x00000117 System.Void UnityEngine.Purchasing.CloudCatalogImpl_<>c::.cctor()
extern void U3CU3Ec__cctor_m5C0E27819D3F14554C2491B3BB489360EE64A7A4 (void);
// 0x00000118 System.Void UnityEngine.Purchasing.CloudCatalogImpl_<>c::.ctor()
extern void U3CU3Ec__ctor_m2A4E278A66C5C16252E81FE284108227CE5D2FC7 (void);
// 0x00000119 System.String UnityEngine.Purchasing.CloudCatalogImpl_<>c::<CamelCaseToSnakeCase>b__12_0(System.Char,System.Int32)
extern void U3CU3Ec_U3CCamelCaseToSnakeCaseU3Eb__12_0_m40123C7E19E965CA8C5A7E87FD333F6E73E488F1 (void);
// 0x0000011A System.String UnityEngine.Purchasing.CloudCatalogImpl_<>c::<CamelCaseToSnakeCase>b__12_1(System.String,System.String)
extern void U3CU3Ec_U3CCamelCaseToSnakeCaseU3Eb__12_1_m389B70C09C2B09FDD96B4BD7241F06AC2D0A6BF9 (void);
// 0x0000011B System.Void UnityEngine.Purchasing.FakeManagedStoreConfig::.ctor()
extern void FakeManagedStoreConfig__ctor_mC7AAE88842F323010EAD156A9D4CD9768B2A4468 (void);
// 0x0000011C System.Void UnityEngine.Purchasing.FakeManagedStoreExtensions::.ctor()
extern void FakeManagedStoreExtensions__ctor_m0D5F6419FE250C2E07D0B1CC149773844F4D34FE (void);
// 0x0000011D UnityEngine.Purchasing.StoreCatalogImpl UnityEngine.Purchasing.StoreCatalogImpl::CreateInstance(System.String,System.String,UnityEngine.Purchasing.IAsyncWebUtil,UnityEngine.ILogger,Uniject.IUtil)
extern void StoreCatalogImpl_CreateInstance_m1702943ABC3429D8505C318A737DC9830E6DB060 (void);
// 0x0000011E System.Void UnityEngine.Purchasing.StoreCatalogImpl::.ctor(UnityEngine.Purchasing.IAsyncWebUtil,UnityEngine.ILogger,System.String,System.String,UnityEngine.Purchasing.FileReference)
extern void StoreCatalogImpl__ctor_m8ECDDE0CC4955189A8384AC41FBD879578FEE506 (void);
// 0x0000011F System.Void UnityEngine.Purchasing.StoreCatalogImpl::FetchProducts(System.Action`1<System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition>>)
extern void StoreCatalogImpl_FetchProducts_mC406A1BD08017A3759B5BD538D9C7D5BF0628DBE (void);
// 0x00000120 System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.StoreCatalogImpl::ParseProductsFromJSON(System.String,System.String,UnityEngine.ILogger)
extern void StoreCatalogImpl_ParseProductsFromJSON_m974A5BE1DC918D2B5F6C8DD25CD9F833F8543017 (void);
// 0x00000121 System.Void UnityEngine.Purchasing.StoreCatalogImpl::handleCachedCatalog(System.Action`1<System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition>>)
extern void StoreCatalogImpl_handleCachedCatalog_m3EA171069F00A9655E911A1D3C747A03716F9882 (void);
// 0x00000122 System.Void UnityEngine.Purchasing.StoreCatalogImpl_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mA2853E97D9D278E1DE8F108C70F8145A8B212B54 (void);
// 0x00000123 System.Void UnityEngine.Purchasing.StoreCatalogImpl_<>c__DisplayClass10_0::<FetchProducts>b__0(System.String)
extern void U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__0_mBAFBCBE2CC61018BDFA340CE07DD62D70F75AA5D (void);
// 0x00000124 System.Void UnityEngine.Purchasing.StoreCatalogImpl_<>c__DisplayClass10_0::<FetchProducts>b__1(System.String)
extern void U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__1_mBEA547C95DA8608CC304CBCD068E5E42C4499AF0 (void);
// 0x00000125 System.Boolean UnityEngine.Purchasing.AdsIPC::InitAdsIPC(Uniject.IUtil)
extern void AdsIPC_InitAdsIPC_mE775F78022B133AB42469E8C3007D62466F057BB (void);
// 0x00000126 System.Boolean UnityEngine.Purchasing.AdsIPC::VerifyMethodExists()
extern void AdsIPC_VerifyMethodExists_m09ECB32981EC5984DB92ECF0B678513B2F5EBECA (void);
// 0x00000127 System.Boolean UnityEngine.Purchasing.AdsIPC::SendEvent(System.String)
extern void AdsIPC_SendEvent_m30EF2A19EFCC69AADD610FCF97DA565802711C9B (void);
// 0x00000128 System.Void UnityEngine.Purchasing.AdsIPC::.cctor()
extern void AdsIPC__cctor_mB476FCD33FFFA007CD13592193FE370A86B6B08E (void);
// 0x00000129 System.Void UnityEngine.Purchasing.EventQueue::.ctor(Uniject.IUtil,UnityEngine.Purchasing.IAsyncWebUtil)
extern void EventQueue__ctor_m29969CC9232D79EE58D1E068800C57315428AFE6 (void);
// 0x0000012A UnityEngine.Purchasing.EventQueue UnityEngine.Purchasing.EventQueue::Instance(Uniject.IUtil,UnityEngine.Purchasing.IAsyncWebUtil)
extern void EventQueue_Instance_mECFFAEB18295FB1079D8030F7C25FE9F7D093FE9 (void);
// 0x0000012B System.Void UnityEngine.Purchasing.EventQueue::SetAdsUrl(System.String)
extern void EventQueue_SetAdsUrl_m710D1E618C60445A4AB0086D4062C97466BBE84A (void);
// 0x0000012C System.Void UnityEngine.Purchasing.EventQueue::SetIapUrl(System.String)
extern void EventQueue_SetIapUrl_m9385396A35B7456449D8E43480DAC070CE6CBF4C (void);
// 0x0000012D System.Boolean UnityEngine.Purchasing.EventQueue::SendEvent(UnityEngine.Purchasing.EventDestType,System.String,System.String,System.Nullable`1<System.Int32>)
extern void EventQueue_SendEvent_mB6C974CC2706C0EB465978C282B63AF031CFAB0F (void);
// 0x0000012E System.Boolean UnityEngine.Purchasing.EventQueue::SendEvent(System.String)
extern void EventQueue_SendEvent_mD5F8335E5C8B4FA2F62D911BD5E5305C4668B9FD (void);
// 0x0000012F System.Void UnityEngine.Purchasing.EventQueue_<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m87BE625D08BE3E9AFE3B2FD6AD5167B8BF637AF1 (void);
// 0x00000130 System.Void UnityEngine.Purchasing.EventQueue_<>c__DisplayClass11_0::<SendEvent>b__1(System.String)
extern void U3CU3Ec__DisplayClass11_0_U3CSendEventU3Eb__1_m7A6A77FB020D4AA45330F6A6940A0F619E99F81E (void);
// 0x00000131 System.Void UnityEngine.Purchasing.EventQueue_<>c__DisplayClass11_0::<SendEvent>b__4()
extern void U3CU3Ec__DisplayClass11_0_U3CSendEventU3Eb__4_mD1AC489C0677006A4074D7F908EE70284173208B (void);
// 0x00000132 System.Void UnityEngine.Purchasing.EventQueue_<>c::.cctor()
extern void U3CU3Ec__cctor_m9C02C7748C2731BBE3A37F270517DDA618EFF146 (void);
// 0x00000133 System.Void UnityEngine.Purchasing.EventQueue_<>c::.ctor()
extern void U3CU3Ec__ctor_mFE27FBBE0D9EB7A7A6F7C518D6B05D6A44E02507 (void);
// 0x00000134 System.Void UnityEngine.Purchasing.EventQueue_<>c::<SendEvent>b__11_0(System.String)
extern void U3CU3Ec_U3CSendEventU3Eb__11_0_m5B37451B74F4BB4B3D64D882C75EB6E7CEED9B0B (void);
// 0x00000135 System.Void UnityEngine.Purchasing.EventQueue_<>c::<SendEvent>b__11_2(System.String)
extern void U3CU3Ec_U3CSendEventU3Eb__11_2_mBE98F5DC9EC3D7EB28B77D7F90B58570F3C881CD (void);
// 0x00000136 System.Void UnityEngine.Purchasing.EventQueue_<>c::<SendEvent>b__11_3(System.String)
extern void U3CU3Ec_U3CSendEventU3Eb__11_3_m6D0B49D651A2EACAD971395074AA70FBD79E7CFE (void);
// 0x00000137 System.Void UnityEngine.Purchasing.PurchasingEvent::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void PurchasingEvent__ctor_m93B1F85A0EB88478D1969238181095B1070602B4 (void);
// 0x00000138 System.String UnityEngine.Purchasing.PurchasingEvent::FlatJSON(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void PurchasingEvent_FlatJSON_mB5D3474D3085810D18683042D631BEABE4BE09E4 (void);
// 0x00000139 System.Void UnityEngine.Purchasing.PurchasingEvent_<>c::.cctor()
extern void U3CU3Ec__cctor_m6042DD5463324144D90438F2075F5ABC24900A38 (void);
// 0x0000013A System.Void UnityEngine.Purchasing.PurchasingEvent_<>c::.ctor()
extern void U3CU3Ec__ctor_m0A36A41CED277E3DFEB6D5A71E17CE65F33A387B (void);
// 0x0000013B System.String UnityEngine.Purchasing.PurchasingEvent_<>c::<FlatJSON>b__2_0(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern void U3CU3Ec_U3CFlatJSONU3Eb__2_0_m36E49FAF91AB0C4ECF9D33A76AD0F90DDC58ADEA (void);
// 0x0000013C System.Object UnityEngine.Purchasing.PurchasingEvent_<>c::<FlatJSON>b__2_1(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern void U3CU3Ec_U3CFlatJSONU3Eb__2_1_m91191AED7D51A83A4CE5F49DF71C06353736F11A (void);
// 0x0000013D System.Void UnityEngine.Purchasing.AsyncWebUtil::Get(System.String,System.Action`1<System.String>,System.Action`1<System.String>,System.Int32)
extern void AsyncWebUtil_Get_m88FE69C8616683C7E40805270D65A8AF22EFF21B (void);
// 0x0000013E System.Void UnityEngine.Purchasing.AsyncWebUtil::Post(System.String,System.String,System.Action`1<System.String>,System.Action`1<System.String>,System.Int32)
extern void AsyncWebUtil_Post_m53D3A9D2790DBC50738F90160D5F67E5B2AA6AB2 (void);
// 0x0000013F System.Void UnityEngine.Purchasing.AsyncWebUtil::Schedule(System.Action,System.Int32)
extern void AsyncWebUtil_Schedule_mF5FBFDBC05B181B254A08D70E44C6CA698B03F61 (void);
// 0x00000140 System.Collections.IEnumerator UnityEngine.Purchasing.AsyncWebUtil::DoInvoke(System.Action,System.Int32)
extern void AsyncWebUtil_DoInvoke_m0633409BA91DAE7ABF7BCD416A6B264C2F96937A (void);
// 0x00000141 System.Collections.IEnumerator UnityEngine.Purchasing.AsyncWebUtil::Process(UnityEngine.WWW,System.Action`1<System.String>,System.Action`1<System.String>,System.Int32)
extern void AsyncWebUtil_Process_m9C2370009E698323E58B2A670D3CD27BF208D951 (void);
// 0x00000142 System.Void UnityEngine.Purchasing.AsyncWebUtil::.ctor()
extern void AsyncWebUtil__ctor_m5F49085BED472C5BA6030E5009A6ABD3836551B0 (void);
// 0x00000143 System.Void UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::.ctor(System.Int32)
extern void U3CDoInvokeU3Ed__3__ctor_mC6EE00BC8DDB8ABD56BAE6614910359B629C6DD7 (void);
// 0x00000144 System.Void UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::System.IDisposable.Dispose()
extern void U3CDoInvokeU3Ed__3_System_IDisposable_Dispose_mC44F5EC636838C33A8A789FCA13B3BE1362A23F9 (void);
// 0x00000145 System.Boolean UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::MoveNext()
extern void U3CDoInvokeU3Ed__3_MoveNext_m35C974F5213EBB7839DA9C8B80ADC9C24D857039 (void);
// 0x00000146 System.Object UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoInvokeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DBFE3D27EE6E45C1BD65103792DE63A8ADD29C (void);
// 0x00000147 System.Void UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::System.Collections.IEnumerator.Reset()
extern void U3CDoInvokeU3Ed__3_System_Collections_IEnumerator_Reset_m6CCA870DD9325A284667AC50CF778BD1CDD52538 (void);
// 0x00000148 System.Object UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CDoInvokeU3Ed__3_System_Collections_IEnumerator_get_Current_m2196441C04D53F61329468CAFF83939FF46AD4C1 (void);
// 0x00000149 System.Void UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::.ctor(System.Int32)
extern void U3CProcessU3Ed__4__ctor_mBB447167E306E623636B5F5C63C1EAB3D730B81B (void);
// 0x0000014A System.Void UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::System.IDisposable.Dispose()
extern void U3CProcessU3Ed__4_System_IDisposable_Dispose_m254F5AFF9ACB1AE0165D14A801C45BBF7C93F859 (void);
// 0x0000014B System.Boolean UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::MoveNext()
extern void U3CProcessU3Ed__4_MoveNext_mE4A8EBA215143440526AF4EF9EBB04028E6ED7FD (void);
// 0x0000014C System.Object UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5ECBA869460B1F98EA8B136038BB7F6BBA7CDCBE (void);
// 0x0000014D System.Void UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::System.Collections.IEnumerator.Reset()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mBB97238A013A9B4DF57D0F64DD13147664D7C6E5 (void);
// 0x0000014E System.Object UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m257BD65D282C42054AD97B58E2011843F5ABCE91 (void);
// 0x0000014F System.Void UnityEngine.Purchasing.IAsyncWebUtil::Get(System.String,System.Action`1<System.String>,System.Action`1<System.String>,System.Int32)
// 0x00000150 System.Void UnityEngine.Purchasing.IAsyncWebUtil::Post(System.String,System.String,System.Action`1<System.String>,System.Action`1<System.String>,System.Int32)
// 0x00000151 System.Void UnityEngine.Purchasing.IAsyncWebUtil::Schedule(System.Action,System.Int32)
// 0x00000152 System.String UnityEngine.Purchasing.QueryHelper::ToQueryString(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void QueryHelper_ToQueryString_mB7D887C850A895D3FE019D92044EE9BEF3887736 (void);
// 0x00000153 System.Void UnityEngine.Purchasing.Price::OnBeforeSerialize()
extern void Price_OnBeforeSerialize_m42915340CF2F0009AA700CCADC50A1B4D367E30A (void);
// 0x00000154 System.Void UnityEngine.Purchasing.Price::OnAfterDeserialize()
extern void Price_OnAfterDeserialize_m68B6E9BEE8BB36C2E936F25D2BE03BE15BA405F1 (void);
// 0x00000155 System.Void UnityEngine.Purchasing.Price::.ctor()
extern void Price__ctor_m9E849B424E1B142BCC5F6DA2AC17E259438125B7 (void);
// 0x00000156 System.String UnityEngine.Purchasing.LocalizedProductDescription::get_Title()
extern void LocalizedProductDescription_get_Title_mB4128E7F3F80159214D6204AC155A876E9F7CB3C (void);
// 0x00000157 System.String UnityEngine.Purchasing.LocalizedProductDescription::get_Description()
extern void LocalizedProductDescription_get_Description_m0CF6B4CE1668EFAA25D086B0A2AB58A868979FAC (void);
// 0x00000158 System.String UnityEngine.Purchasing.LocalizedProductDescription::DecodeNonLatinCharacters(System.String)
extern void LocalizedProductDescription_DecodeNonLatinCharacters_m260BA15BA17C15BB261706D7B579AD047C391768 (void);
// 0x00000159 System.Void UnityEngine.Purchasing.LocalizedProductDescription::.ctor()
extern void LocalizedProductDescription__ctor_m4EC23D3335DE3FB09F98F8A8C1C05C29BF91F213 (void);
// 0x0000015A System.Void UnityEngine.Purchasing.LocalizedProductDescription_<>c::.cctor()
extern void U3CU3Ec__cctor_m036D291ED8CC94C7CFD6800993DE8AF5880B2647 (void);
// 0x0000015B System.Void UnityEngine.Purchasing.LocalizedProductDescription_<>c::.ctor()
extern void U3CU3Ec__ctor_m35F829E30B1A322BBE52F44B9EE7549C03AFD662 (void);
// 0x0000015C System.String UnityEngine.Purchasing.LocalizedProductDescription_<>c::<DecodeNonLatinCharacters>b__11_0(System.Text.RegularExpressions.Match)
extern void U3CU3Ec_U3CDecodeNonLatinCharactersU3Eb__11_0_m0F8E885D6CF01D6833DBC50FE9B1784095D5CE04 (void);
// 0x0000015D System.String UnityEngine.Purchasing.ProductCatalogPayout::get_typeString()
extern void ProductCatalogPayout_get_typeString_m4FCC4534F5A45CD6765FD5D5B7440260F346721B (void);
// 0x0000015E System.String UnityEngine.Purchasing.ProductCatalogPayout::get_subtype()
extern void ProductCatalogPayout_get_subtype_m5BD4FDC8C2A65CCBCEEEB8F5273518A145D8E4A4 (void);
// 0x0000015F System.Double UnityEngine.Purchasing.ProductCatalogPayout::get_quantity()
extern void ProductCatalogPayout_get_quantity_m5544F7E28BE1BF9D7DF22B7582EB010490E69EC2 (void);
// 0x00000160 System.String UnityEngine.Purchasing.ProductCatalogPayout::get_data()
extern void ProductCatalogPayout_get_data_m84A49E1CAC1686681C972F1E440F4C479A92CD76 (void);
// 0x00000161 System.Void UnityEngine.Purchasing.ProductCatalogPayout::.ctor()
extern void ProductCatalogPayout__ctor_m74268D587D637CCB8E234FD75A1D163A5775C3E5 (void);
// 0x00000162 System.Collections.Generic.IList`1<UnityEngine.Purchasing.ProductCatalogPayout> UnityEngine.Purchasing.ProductCatalogItem::get_Payouts()
extern void ProductCatalogItem_get_Payouts_m56972749038D381580FE79C9FB0779BAAFB2EEA0 (void);
// 0x00000163 System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.StoreID> UnityEngine.Purchasing.ProductCatalogItem::get_allStoreIDs()
extern void ProductCatalogItem_get_allStoreIDs_mE9749219486896327EDB96491DB7B03677B14608 (void);
// 0x00000164 System.Void UnityEngine.Purchasing.ProductCatalogItem::.ctor()
extern void ProductCatalogItem__ctor_mA5E1B972EDF0B24C88F4A49907388C44EB6844C7 (void);
// 0x00000165 System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.ProductCatalogItem> UnityEngine.Purchasing.ProductCatalog::get_allProducts()
extern void ProductCatalog_get_allProducts_m57932235309AD3580368E255E67863DDBBF22FFF (void);
// 0x00000166 System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.ProductCatalogItem> UnityEngine.Purchasing.ProductCatalog::get_allValidProducts()
extern void ProductCatalog_get_allValidProducts_mA3253A3B7CE0CADF31456205C4E95FCCE77C381A (void);
// 0x00000167 System.Void UnityEngine.Purchasing.ProductCatalog::Initialize()
extern void ProductCatalog_Initialize_mE3A76CD3059703EEC40B3FA2BFA3D842E67EE7F7 (void);
// 0x00000168 System.Void UnityEngine.Purchasing.ProductCatalog::Initialize(UnityEngine.Purchasing.IProductCatalogImpl)
extern void ProductCatalog_Initialize_mD9EF9832EA57668A5AFD194B036CA47289E01F13 (void);
// 0x00000169 System.Boolean UnityEngine.Purchasing.ProductCatalog::IsEmpty()
extern void ProductCatalog_IsEmpty_m062291717F13D424F9EC751CF3D72DDA839B07FD (void);
// 0x0000016A UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.ProductCatalog::Deserialize(System.String)
extern void ProductCatalog_Deserialize_m9DD672A27C533E7567D4C72E299F17D3451ABA1B (void);
// 0x0000016B UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.ProductCatalog::FromTextAsset(UnityEngine.TextAsset)
extern void ProductCatalog_FromTextAsset_mE4FBA8CCAA4330D23F917B862E6C2BDF62FAF58F (void);
// 0x0000016C UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.ProductCatalog::LoadDefaultCatalog()
extern void ProductCatalog_LoadDefaultCatalog_m283C9E96ED6641D4603C785E19F1C8619B14D726 (void);
// 0x0000016D System.Void UnityEngine.Purchasing.ProductCatalog::.ctor()
extern void ProductCatalog__ctor_mD6A3BAA2A699611769D953290FF3FDA3A8079BFB (void);
// 0x0000016E System.Void UnityEngine.Purchasing.ProductCatalog_<>c::.cctor()
extern void U3CU3Ec__cctor_m2E418947ABC898FCD169F2619F1786B9756FCC83 (void);
// 0x0000016F System.Void UnityEngine.Purchasing.ProductCatalog_<>c::.ctor()
extern void U3CU3Ec__ctor_mF914ED3FBC126B7ABA7FE68BCBF985ECCEC49D6B (void);
// 0x00000170 System.Boolean UnityEngine.Purchasing.ProductCatalog_<>c::<get_allValidProducts>b__8_0(UnityEngine.Purchasing.ProductCatalogItem)
extern void U3CU3Ec_U3Cget_allValidProductsU3Eb__8_0_m2851D0027CB45CAF3AC9155711E78CC2B6006E69 (void);
// 0x00000171 UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.IProductCatalogImpl::LoadDefaultCatalog()
// 0x00000172 UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.ProductCatalogImpl::LoadDefaultCatalog()
extern void ProductCatalogImpl_LoadDefaultCatalog_m45C5B4F47CD8E70084F561B9FF7077303F89CCA5 (void);
// 0x00000173 System.Void UnityEngine.Purchasing.ProductCatalogImpl::.ctor()
extern void ProductCatalogImpl__ctor_m1FCB2E5D3E39B68F84208FAA1245B9D9B9905C68 (void);
// 0x00000174 System.String UnityEngine.Purchasing.ProfileData::get_AppId()
extern void ProfileData_get_AppId_mB76CD78BDFBF737113683661CC695AD70F8C8A97 (void);
// 0x00000175 System.Void UnityEngine.Purchasing.ProfileData::set_AppId(System.String)
extern void ProfileData_set_AppId_m9E1F31B968D62B7BF0330DF532BF70890BC3BF78 (void);
// 0x00000176 System.String UnityEngine.Purchasing.ProfileData::get_UserId()
extern void ProfileData_get_UserId_m896300FD502E7796269508495A31F964664F2C30 (void);
// 0x00000177 System.Void UnityEngine.Purchasing.ProfileData::set_UserId(System.String)
extern void ProfileData_set_UserId_m7E142ED38198963F3803E37B68D13637B61E3B8D (void);
// 0x00000178 System.UInt64 UnityEngine.Purchasing.ProfileData::get_SessionId()
extern void ProfileData_get_SessionId_mF980BAE334AFF828EEFE31E1793586454AF4BE89 (void);
// 0x00000179 System.Void UnityEngine.Purchasing.ProfileData::set_SessionId(System.UInt64)
extern void ProfileData_set_SessionId_mBADBBDE13A7FC91A558FB5130E56A97BA0A36E99 (void);
// 0x0000017A System.String UnityEngine.Purchasing.ProfileData::get_Platform()
extern void ProfileData_get_Platform_mACF5FECF73EF67AD2DF604AD3E46AA65B9292A16 (void);
// 0x0000017B System.Void UnityEngine.Purchasing.ProfileData::set_Platform(System.String)
extern void ProfileData_set_Platform_m3070287D2C0F4300B4A27E4F622E372258A88647 (void);
// 0x0000017C System.Int32 UnityEngine.Purchasing.ProfileData::get_PlatformId()
extern void ProfileData_get_PlatformId_m478E28CA7F2EFDA661A4626FFF774543F0F30907 (void);
// 0x0000017D System.Void UnityEngine.Purchasing.ProfileData::set_PlatformId(System.Int32)
extern void ProfileData_set_PlatformId_m00CC32FADDD7C1F5FD42B968387ECDED47C3BA2C (void);
// 0x0000017E System.String UnityEngine.Purchasing.ProfileData::get_SdkVer()
extern void ProfileData_get_SdkVer_m0DEA69034CE40AE0B7EB9ACD4CC93065A43AC7E0 (void);
// 0x0000017F System.Void UnityEngine.Purchasing.ProfileData::set_SdkVer(System.String)
extern void ProfileData_set_SdkVer_m52C0A5A9495BBC729DDD116B58E11E9C2E1B2C5E (void);
// 0x00000180 System.String UnityEngine.Purchasing.ProfileData::get_OsVer()
extern void ProfileData_get_OsVer_m46F4FB97BC78156C2CFCFFDA883DE977FA2E293F (void);
// 0x00000181 System.Void UnityEngine.Purchasing.ProfileData::set_OsVer(System.String)
extern void ProfileData_set_OsVer_m51B8FA92D86641EAA4E982719B59B29764EDCA05 (void);
// 0x00000182 System.Int32 UnityEngine.Purchasing.ProfileData::get_ScreenWidth()
extern void ProfileData_get_ScreenWidth_m44AE08924FB9BBF78337B5EEE85BB679E0D24375 (void);
// 0x00000183 System.Void UnityEngine.Purchasing.ProfileData::set_ScreenWidth(System.Int32)
extern void ProfileData_set_ScreenWidth_m80A83E07330532E9EE97EA0DA979EA7103B835A0 (void);
// 0x00000184 System.Int32 UnityEngine.Purchasing.ProfileData::get_ScreenHeight()
extern void ProfileData_get_ScreenHeight_m9DC916E6A43D17DC7F8C3A421425F238C53728CF (void);
// 0x00000185 System.Void UnityEngine.Purchasing.ProfileData::set_ScreenHeight(System.Int32)
extern void ProfileData_set_ScreenHeight_mB1C6871EEF5144F251CD295C34E4022E49A005FC (void);
// 0x00000186 System.Single UnityEngine.Purchasing.ProfileData::get_ScreenDpi()
extern void ProfileData_get_ScreenDpi_m5F850CC804676B2A0F2C76750DF3E6B4107E15AC (void);
// 0x00000187 System.Void UnityEngine.Purchasing.ProfileData::set_ScreenDpi(System.Single)
extern void ProfileData_set_ScreenDpi_m65645247F950C4BCBC9189C613DD79A1A7440E2D (void);
// 0x00000188 System.String UnityEngine.Purchasing.ProfileData::get_ScreenOrientation()
extern void ProfileData_get_ScreenOrientation_m95A2A665784A9DBC8AEB2806E360CF75244D4E16 (void);
// 0x00000189 System.Void UnityEngine.Purchasing.ProfileData::set_ScreenOrientation(System.String)
extern void ProfileData_set_ScreenOrientation_mA4D5370DE961B70B8BB634DEB4D8D35247313478 (void);
// 0x0000018A System.String UnityEngine.Purchasing.ProfileData::get_DeviceId()
extern void ProfileData_get_DeviceId_mE7FDE557A9CEAFDE2057029C77224AB5E541BA69 (void);
// 0x0000018B System.Void UnityEngine.Purchasing.ProfileData::set_DeviceId(System.String)
extern void ProfileData_set_DeviceId_mF9B57A8FD4459418422C21037BD8FDCB46372630 (void);
// 0x0000018C System.String UnityEngine.Purchasing.ProfileData::get_BuildGUID()
extern void ProfileData_get_BuildGUID_mAFFC9CEDCA65D08AE4800E072349B59D16D9DB58 (void);
// 0x0000018D System.String UnityEngine.Purchasing.ProfileData::get_IapVer()
extern void ProfileData_get_IapVer_mAA63BDA3B84A10EDE437EADB0507286359156B79 (void);
// 0x0000018E System.Void UnityEngine.Purchasing.ProfileData::set_IapVer(System.String)
extern void ProfileData_set_IapVer_m5B00AC263487A40A415FA7888AEB7D47DDE690A7 (void);
// 0x0000018F System.String UnityEngine.Purchasing.ProfileData::get_AdsGamerToken()
extern void ProfileData_get_AdsGamerToken_m2168538589DC4AA546573D94DF1C3D910CBB556E (void);
// 0x00000190 System.Void UnityEngine.Purchasing.ProfileData::set_AdsGamerToken(System.String)
extern void ProfileData_set_AdsGamerToken_m165AE2FE059C9B445F0A9E0BF1E91CF5F9BBFD3C (void);
// 0x00000191 System.Nullable`1<System.Boolean> UnityEngine.Purchasing.ProfileData::get_TrackingOptOut()
extern void ProfileData_get_TrackingOptOut_m4FA81FB86ADD6C265F7F79DC2DA41D11EB8CB76C (void);
// 0x00000192 System.Void UnityEngine.Purchasing.ProfileData::set_TrackingOptOut(System.Nullable`1<System.Boolean>)
extern void ProfileData_set_TrackingOptOut_mEB3F15A23581A9808EDF20A76B7C626D4AE98116 (void);
// 0x00000193 System.Nullable`1<System.Int32> UnityEngine.Purchasing.ProfileData::get_AdsABGroup()
extern void ProfileData_get_AdsABGroup_m3D5B156992614B55D7FC48EA7F81DF9BB9DCED73 (void);
// 0x00000194 System.Void UnityEngine.Purchasing.ProfileData::set_AdsABGroup(System.Nullable`1<System.Int32>)
extern void ProfileData_set_AdsABGroup_m1BD7F8B10950E8CD0C1D7EB316BB0499899EACCC (void);
// 0x00000195 System.String UnityEngine.Purchasing.ProfileData::get_AdsGameId()
extern void ProfileData_get_AdsGameId_m6E21F44B4FF32249F3E5F3A177AC55E11D0ED401 (void);
// 0x00000196 System.Void UnityEngine.Purchasing.ProfileData::set_AdsGameId(System.String)
extern void ProfileData_set_AdsGameId_m1C540002AD43B3A122676057035483BEAB5FEFD6 (void);
// 0x00000197 System.Nullable`1<System.Int32> UnityEngine.Purchasing.ProfileData::get_StoreABGroup()
extern void ProfileData_get_StoreABGroup_m77EEC04AED22E35AEDA45D35A9112F7BB69C0B4D (void);
// 0x00000198 System.Void UnityEngine.Purchasing.ProfileData::set_StoreABGroup(System.Nullable`1<System.Int32>)
extern void ProfileData_set_StoreABGroup_mC5E83597BC466FED09D5AAB6AF153F57C1C8DB4F (void);
// 0x00000199 System.String UnityEngine.Purchasing.ProfileData::get_CatalogId()
extern void ProfileData_get_CatalogId_m4A36730621A8B53F97778267C9F88D33555054A5 (void);
// 0x0000019A System.Void UnityEngine.Purchasing.ProfileData::set_CatalogId(System.String)
extern void ProfileData_set_CatalogId_m84CC9A84A13259A72EC045DBC5E521614F02AB43 (void);
// 0x0000019B System.String UnityEngine.Purchasing.ProfileData::get_MonetizationId()
extern void ProfileData_get_MonetizationId_m0670F1B55BF56A56AAB335D0461C39C1F1D80B4F (void);
// 0x0000019C System.String UnityEngine.Purchasing.ProfileData::get_StoreName()
extern void ProfileData_get_StoreName_m718DFA5439E8716BE57D464888307DBC5C6D12F9 (void);
// 0x0000019D System.Void UnityEngine.Purchasing.ProfileData::set_StoreName(System.String)
extern void ProfileData_set_StoreName_m5F72223FD8488E6629A352909DB31064E08C263A (void);
// 0x0000019E System.String UnityEngine.Purchasing.ProfileData::get_GameVersion()
extern void ProfileData_get_GameVersion_m6414F1906E2C6516394C8E86E50E5B0F89397BB3 (void);
// 0x0000019F System.Void UnityEngine.Purchasing.ProfileData::set_GameVersion(System.String)
extern void ProfileData_set_GameVersion_mBBBCC2D554A6B10CC05A6C33D760B9F57D14012A (void);
// 0x000001A0 System.Nullable`1<System.Boolean> UnityEngine.Purchasing.ProfileData::get_StoreTestEnabled()
extern void ProfileData_get_StoreTestEnabled_mE5F73CB7E20C4ACF802A913AE7E350C9C06F2118 (void);
// 0x000001A1 System.Void UnityEngine.Purchasing.ProfileData::.ctor(Uniject.IUtil)
extern void ProfileData__ctor_mE427B2BD4E0115117186BAB48B5792C8F76FC79A (void);
// 0x000001A2 System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.ProfileData::GetProfileDict()
extern void ProfileData_GetProfileDict_mC6004F7556B698AB2133AB43EB4F42D24D2B416C (void);
// 0x000001A3 System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.ProfileData::GetProfileIds()
extern void ProfileData_GetProfileIds_mF714A652371683D86F8DD1AEF06E185EB4BCDE17 (void);
// 0x000001A4 UnityEngine.Purchasing.ProfileData UnityEngine.Purchasing.ProfileData::Instance(Uniject.IUtil)
extern void ProfileData_Instance_mC66C8230B0E0D8B00BDF9E1F66928F36D1D6DA27 (void);
// 0x000001A5 System.Void UnityEngine.Purchasing.ProfileData::SetGamerToken(System.String)
extern void ProfileData_SetGamerToken_m6705460DBB32FB38E26ADF0EEA395B34E82391E5 (void);
// 0x000001A6 System.Void UnityEngine.Purchasing.ProfileData::SetTrackingOptOut(System.Nullable`1<System.Boolean>)
extern void ProfileData_SetTrackingOptOut_mE82201E8A6968F98726E15CAD55330080BF3DE74 (void);
// 0x000001A7 System.Void UnityEngine.Purchasing.ProfileData::SetGameId(System.String)
extern void ProfileData_SetGameId_mE84EAF3B00CC459F6A75EEC0250FD17D2B149696 (void);
// 0x000001A8 System.Void UnityEngine.Purchasing.ProfileData::SetABGroup(System.Nullable`1<System.Int32>)
extern void ProfileData_SetABGroup_mA0E17E2D40EA612DA0E85C0C135C56E81166F857 (void);
// 0x000001A9 System.Void UnityEngine.Purchasing.ProfileData::SetStoreABGroup(System.Nullable`1<System.Int32>)
extern void ProfileData_SetStoreABGroup_m447899BAA43196544EAF5F1FCED2FF46DD1A4A0A (void);
// 0x000001AA System.Void UnityEngine.Purchasing.ProfileData::SetCatalogId(System.String)
extern void ProfileData_SetCatalogId_m37167B7F3E70C785149001AE6B4701E1B72C5692 (void);
// 0x000001AB System.Void UnityEngine.Purchasing.ProfileData::SetStoreName(System.String)
extern void ProfileData_SetStoreName_mACCFF9D0EAD03EB68EFFE7DC35E0471C592732A7 (void);
// 0x000001AC System.Boolean UnityEngine.Purchasing.Promo::IsReady()
extern void Promo_IsReady_m715B4672F846E11034666D7CFA513358749787F3 (void);
// 0x000001AD System.String UnityEngine.Purchasing.Promo::Version()
extern void Promo_Version_m7DF205A5C8591C70D9600E1FF838FBFBFF2FE548 (void);
// 0x000001AE System.Void UnityEngine.Purchasing.Promo::.ctor()
extern void Promo__ctor_m4C871C25F260FAFE3DA5D5EBB4ECD8BCC50DF5C6 (void);
// 0x000001AF System.Void UnityEngine.Purchasing.Promo::InitPromo(UnityEngine.RuntimePlatform,UnityEngine.ILogger,Uniject.IUtil,UnityEngine.Purchasing.IAsyncWebUtil)
extern void Promo_InitPromo_mBFAF9FEB43F71EBE93906B7D2DF37F577904706A (void);
// 0x000001B0 System.Void UnityEngine.Purchasing.Promo::InitPromo(UnityEngine.RuntimePlatform,UnityEngine.ILogger,System.String,Uniject.IUtil,UnityEngine.Purchasing.IAsyncWebUtil)
extern void Promo_InitPromo_mE5648F47B8FF4E17E5CFAAD0FA4BFF69B1D82BF3 (void);
// 0x000001B1 System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Product> UnityEngine.Purchasing.Promo::UpdatePromoProductList()
extern void Promo_UpdatePromoProductList_m7D66E97B992002A774B83573E8CEDD29E2AF1796 (void);
// 0x000001B2 System.Void UnityEngine.Purchasing.Promo::ProvideProductsToAds(UnityEngine.Purchasing.JSONStore,UnityEngine.Purchasing.Extension.IStoreCallback)
extern void Promo_ProvideProductsToAds_m9806E5D41CCC191656F2FB3609EC4CB8DBEE76D0 (void);
// 0x000001B3 System.Void UnityEngine.Purchasing.Promo::ProvideProductsToAds(System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Product>)
extern void Promo_ProvideProductsToAds_m23C7D7A04E8013323C23546FC46C57CAC0C29610 (void);
// 0x000001B4 System.String UnityEngine.Purchasing.Promo::QueryPromoProducts()
extern void Promo_QueryPromoProducts_m06BCB6E34A7910244ADE0501C6F3B8B04FA3EA6C (void);
// 0x000001B5 System.Boolean UnityEngine.Purchasing.Promo::InitiatePromoPurchase(System.String)
extern void Promo_InitiatePromoPurchase_m5263CCCEC7F077FD817468A96A35EF62CC6C8879 (void);
// 0x000001B6 System.Boolean UnityEngine.Purchasing.Promo::InitiatePurchasingCommand(System.String)
extern void Promo_InitiatePurchasingCommand_mD1A99C20870029E4AD649C84CC99693CC92288B8 (void);
// 0x000001B7 System.Boolean UnityEngine.Purchasing.Promo::ExecPromoPurchase(System.String)
extern void Promo_ExecPromoPurchase_mA76A42831AB5F737BD83DD9ADFDCEE1A6A3E7EE5 (void);
// 0x000001B8 System.Void UnityEngine.Purchasing.Promo::.cctor()
extern void Promo__cctor_mF070773D2A494A2A7A0EC0AD6C7ECE284B50C2FD (void);
// 0x000001B9 Uniject.IUtil UnityEngine.Purchasing.StandardPurchasingModule::get_util()
extern void StandardPurchasingModule_get_util_mF27932BFEB542A0EB524E09124E6C0BBCB9042F9 (void);
// 0x000001BA System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_util(Uniject.IUtil)
extern void StandardPurchasingModule_set_util_m012516B0F2A913B01F758F94D3A5DDA832BA9BA7 (void);
// 0x000001BB UnityEngine.ILogger UnityEngine.Purchasing.StandardPurchasingModule::get_logger()
extern void StandardPurchasingModule_get_logger_m22702082BD1ADF7D1AE7EE7B5A88DDA2234E3BB9 (void);
// 0x000001BC System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_logger(UnityEngine.ILogger)
extern void StandardPurchasingModule_set_logger_m6CBA70094C3AC349F66C503900B1EE900E98976C (void);
// 0x000001BD UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.StandardPurchasingModule::get_webUtil()
extern void StandardPurchasingModule_get_webUtil_m75D15473078B64AF7AC29F5A7701A6D37CA3295F (void);
// 0x000001BE System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_webUtil(UnityEngine.Purchasing.IAsyncWebUtil)
extern void StandardPurchasingModule_set_webUtil_m6A71DB533032003B4681625C8DB312F1BB6BC2AF (void);
// 0x000001BF UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance UnityEngine.Purchasing.StandardPurchasingModule::get_storeInstance()
extern void StandardPurchasingModule_get_storeInstance_mDE877C008D6891F3D6CC5AD23FCE7EAA77B2EB8F (void);
// 0x000001C0 System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_storeInstance(UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance)
extern void StandardPurchasingModule_set_storeInstance_m55E1773A9CBE6536CFE8BA120EBBB59DA456C824 (void);
// 0x000001C1 System.Void UnityEngine.Purchasing.StandardPurchasingModule::.ctor(Uniject.IUtil,UnityEngine.Purchasing.IAsyncWebUtil,UnityEngine.ILogger,UnityEngine.Purchasing.INativeStoreProvider,UnityEngine.RuntimePlatform,UnityEngine.Purchasing.AppStore,System.Boolean)
extern void StandardPurchasingModule__ctor_m7F8DC18DDC0A90E2156438955FB7288621D9BD07 (void);
// 0x000001C2 UnityEngine.Purchasing.AppStore UnityEngine.Purchasing.StandardPurchasingModule::get_appStore()
extern void StandardPurchasingModule_get_appStore_m3FD6D2E86D0865A130B60922211CF1DA50025859 (void);
// 0x000001C3 UnityEngine.Purchasing.FakeStoreUIMode UnityEngine.Purchasing.StandardPurchasingModule::get_useFakeStoreUIMode()
extern void StandardPurchasingModule_get_useFakeStoreUIMode_mB5C3DC0E195BB5C4836B19D7DD4B0B0FF48AF037 (void);
// 0x000001C4 System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_useFakeStoreUIMode(UnityEngine.Purchasing.FakeStoreUIMode)
extern void StandardPurchasingModule_set_useFakeStoreUIMode_m1DF958E879FF2A8B0EF00C2DEDC2A9DC15FD0F81 (void);
// 0x000001C5 System.Boolean UnityEngine.Purchasing.StandardPurchasingModule::get_useFakeStoreAlways()
extern void StandardPurchasingModule_get_useFakeStoreAlways_mB8536F2943BBD730A3BBEB63CE52E12DB8397B4F (void);
// 0x000001C6 System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_useFakeStoreAlways(System.Boolean)
extern void StandardPurchasingModule_set_useFakeStoreAlways_mFF827931135A19C8E056088E58B43821AFB843D6 (void);
// 0x000001C7 UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule::Instance()
extern void StandardPurchasingModule_Instance_m53AC662D78E6E4ABDB2CAB28B57C82B5C40DDC0F (void);
// 0x000001C8 UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule::Instance(UnityEngine.Purchasing.AppStore)
extern void StandardPurchasingModule_Instance_m6FDCB6F3DF479F8EAE642456997E2047E85CCC6E (void);
// 0x000001C9 System.Void UnityEngine.Purchasing.StandardPurchasingModule::Configure()
extern void StandardPurchasingModule_Configure_mEB46A26919A70D7433874B14DA69DF85B825DA8B (void);
// 0x000001CA UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance UnityEngine.Purchasing.StandardPurchasingModule::InstantiateStore()
extern void StandardPurchasingModule_InstantiateStore_m5424D8021F4017E884557536318A76065BDD8E04 (void);
// 0x000001CB UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateAndroid()
extern void StandardPurchasingModule_InstantiateAndroid_mB638FAB309895A6F0C7E74E0BAF5520EF0EB119F (void);
// 0x000001CC UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateUDP()
extern void StandardPurchasingModule_InstantiateUDP_mC7717983EF5F66613BF820BC2EE495B58C0890D6 (void);
// 0x000001CD UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateAndroidHelper(UnityEngine.Purchasing.JSONStore)
extern void StandardPurchasingModule_InstantiateAndroidHelper_mDAD7B93C1F35490EBFC824CCB5F8A351986DD77B (void);
// 0x000001CE UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.StandardPurchasingModule::GetAndroidNativeStore(UnityEngine.Purchasing.JSONStore)
extern void StandardPurchasingModule_GetAndroidNativeStore_m7306315A603E25ACD7384B229B2984B7441BEDD7 (void);
// 0x000001CF UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateCloudMoolah()
extern void StandardPurchasingModule_InstantiateCloudMoolah_m68E1079CC40842F6393BE240D607D8E991BC6856 (void);
// 0x000001D0 UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateApple()
extern void StandardPurchasingModule_InstantiateApple_m766197A2288043C07C458235BE74FDE3F96F6409 (void);
// 0x000001D1 System.Void UnityEngine.Purchasing.StandardPurchasingModule::UseMockWindowsStore(System.Boolean)
extern void StandardPurchasingModule_UseMockWindowsStore_m57ED53E3389D06BC62DDDB375ECEC938F1E4EEEA (void);
// 0x000001D2 UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::instantiateWindowsStore()
extern void StandardPurchasingModule_instantiateWindowsStore_m2C07A8AA5FBD4051A9C82FFAB0BA6B31E02DC4F2 (void);
// 0x000001D3 UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateTizen()
extern void StandardPurchasingModule_InstantiateTizen_m89A803CF3887574012951884A4A31DEBC6294496 (void);
// 0x000001D4 UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateFacebook()
extern void StandardPurchasingModule_InstantiateFacebook_m7C2BB6D5735043477F5B069A6DEFDB7C71414340 (void);
// 0x000001D5 UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateFakeStore()
extern void StandardPurchasingModule_InstantiateFakeStore_m1D76FA3EE731581A194301D950A17DDCFB9F5ADA (void);
// 0x000001D6 System.Void UnityEngine.Purchasing.StandardPurchasingModule::.cctor()
extern void StandardPurchasingModule__cctor_mD7AD3357D5AC77A2D8300F95841211B4AC2EBFCF (void);
// 0x000001D7 System.Void UnityEngine.Purchasing.StandardPurchasingModule::<Configure>b__45_0(System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>)
extern void StandardPurchasingModule_U3CConfigureU3Eb__45_0_m3990E52526D3A5A79D07BABB43F3848F906E278A (void);
// 0x000001D8 System.String UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance::get_storeName()
extern void StoreInstance_get_storeName_mFB3E9C97578D42C738F01CE397B7DD9634363515 (void);
// 0x000001D9 UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance::get_instance()
extern void StoreInstance_get_instance_mD04ECA7D1E048BA532BFA0002B03F1BC8518C7E3 (void);
// 0x000001DA System.Void UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance::.ctor(System.String,UnityEngine.Purchasing.Extension.IStore)
extern void StoreInstance__ctor_m511F9A0FF03F0DED6E2834F35B6B88EA9136A246 (void);
// 0x000001DB System.Void UnityEngine.Purchasing.StandardPurchasingModule_MicrosoftConfiguration::.ctor(UnityEngine.Purchasing.StandardPurchasingModule)
extern void MicrosoftConfiguration__ctor_m8200843A7687638DEFB25B5E6098E8DCB9F7739C (void);
// 0x000001DC System.Void UnityEngine.Purchasing.StandardPurchasingModule_MicrosoftConfiguration::set_useMockBillingSystem(System.Boolean)
extern void MicrosoftConfiguration_set_useMockBillingSystem_mC12BA7BFAA4835FF5005A99711E7C534831D7140 (void);
// 0x000001DD UnityEngine.Purchasing.AppStore UnityEngine.Purchasing.StoreConfiguration::get_androidStore()
extern void StoreConfiguration_get_androidStore_m71F9858773B7A33D42604867C868C8C929216FA9 (void);
// 0x000001DE System.Void UnityEngine.Purchasing.StoreConfiguration::set_androidStore(UnityEngine.Purchasing.AppStore)
extern void StoreConfiguration_set_androidStore_m754AE5DD346AF141A83B65891244851427216678 (void);
// 0x000001DF System.Void UnityEngine.Purchasing.StoreConfiguration::.ctor(UnityEngine.Purchasing.AppStore)
extern void StoreConfiguration__ctor_m73D2B60EBB9F17EF6E14B4D0A05B7894405FABE7 (void);
// 0x000001E0 UnityEngine.Purchasing.StoreConfiguration UnityEngine.Purchasing.StoreConfiguration::Deserialize(System.String)
extern void StoreConfiguration_Deserialize_mAADD487B388060F192F2098FC7CAACBF378CD09E (void);
// 0x000001E1 System.Void UnityEngine.Purchasing.SubscriptionInfo::.ctor(UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt,System.String)
extern void SubscriptionInfo__ctor_m1DF843DEC98A2D334FA6F99E67BE721917F63E0D (void);
// 0x000001E2 UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::isExpired()
extern void SubscriptionInfo_isExpired_m936BED463FFC138136EEB4BE61A5216FBF7DC2E2 (void);
// 0x000001E3 System.Void UnityEngine.Purchasing.ReceiptParserException::.ctor()
extern void ReceiptParserException__ctor_m845F697858F06D1E1A5D14DC27F3FAE24B9A1061 (void);
// 0x000001E4 System.Void UnityEngine.Purchasing.InvalidProductTypeException::.ctor()
extern void InvalidProductTypeException__ctor_m5CF81559999FF23B7E699273DDB48053E2A5EC31 (void);
// 0x000001E5 UnityEngine.Purchasing.Extension.PurchaseFailureDescription UnityEngine.Purchasing.FakeTransactionHistoryExtensions::GetLastPurchaseFailureDescription()
extern void FakeTransactionHistoryExtensions_GetLastPurchaseFailureDescription_m1728DD1121DB7BD2A513DEB6CD57D21CD0E966CC (void);
// 0x000001E6 UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode UnityEngine.Purchasing.FakeTransactionHistoryExtensions::GetLastStoreSpecificPurchaseErrorCode()
extern void FakeTransactionHistoryExtensions_GetLastStoreSpecificPurchaseErrorCode_m5AC0C4FCF6B01542E9F178811DEE4A414CC9C061 (void);
// 0x000001E7 System.Void UnityEngine.Purchasing.FakeTransactionHistoryExtensions::.ctor()
extern void FakeTransactionHistoryExtensions__ctor_m170C5ECFBC6118B5E9110BEC7858664A7EAAE9E8 (void);
// 0x000001E8 UnityEngine.Purchasing.Extension.PurchaseFailureDescription UnityEngine.Purchasing.ITransactionHistoryExtensions::GetLastPurchaseFailureDescription()
// 0x000001E9 UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode UnityEngine.Purchasing.ITransactionHistoryExtensions::GetLastStoreSpecificPurchaseErrorCode()
// 0x000001EA System.Void UnityEngine.Purchasing.FakeMicrosoftExtensions::RestoreTransactions()
extern void FakeMicrosoftExtensions_RestoreTransactions_m92178A383AEA57F5E2290D2E8A09D2A444B5EFF2 (void);
// 0x000001EB System.Void UnityEngine.Purchasing.FakeMicrosoftExtensions::.ctor()
extern void FakeMicrosoftExtensions__ctor_m55BF33C20BA1CC30C5F93986D4597CEA0CDC70D0 (void);
// 0x000001EC System.Void UnityEngine.Purchasing.IMicrosoftConfiguration::set_useMockBillingSystem(System.Boolean)
// 0x000001ED System.Void UnityEngine.Purchasing.IMicrosoftExtensions::RestoreTransactions()
// 0x000001EE System.Void UnityEngine.Purchasing.WinRTStore::.ctor(UnityEngine.Purchasing.Default.IWindowsIAP,Uniject.IUtil,UnityEngine.ILogger)
extern void WinRTStore__ctor_mC247036BEB97E832E54CD04F46C1B17307CD2A41 (void);
// 0x000001EF System.Void UnityEngine.Purchasing.WinRTStore::SetWindowsIAP(UnityEngine.Purchasing.Default.IWindowsIAP)
extern void WinRTStore_SetWindowsIAP_m64DA918FA1ED543DF40CFF663391359288F81ADA (void);
// 0x000001F0 System.Void UnityEngine.Purchasing.WinRTStore::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern void WinRTStore_Initialize_mD638F8649C4ADDB8912B0884252022DB299A28F2 (void);
// 0x000001F1 System.Void UnityEngine.Purchasing.WinRTStore::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern void WinRTStore_RetrieveProducts_m1843A0C71EA5627C83D46EEB44BD1A7967E85ADA (void);
// 0x000001F2 System.Void UnityEngine.Purchasing.WinRTStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void WinRTStore_FinishTransaction_m1234FFF6F33D68313D1E19DA6F8DF14D827F3889 (void);
// 0x000001F3 System.Void UnityEngine.Purchasing.WinRTStore::init(System.Int32)
extern void WinRTStore_init_mC53C127474CC761323E1E0AB7233601B536D011C (void);
// 0x000001F4 System.Void UnityEngine.Purchasing.WinRTStore::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void WinRTStore_Purchase_m425472074EEDDF0EA46755B3C8AE4B5C18DD4628 (void);
// 0x000001F5 System.Void UnityEngine.Purchasing.WinRTStore::restoreTransactions(System.Boolean)
extern void WinRTStore_restoreTransactions_mD41EB4B9E2D1A00C2A8F508730C4B6208FB3F101 (void);
// 0x000001F6 System.Void UnityEngine.Purchasing.WinRTStore::RestoreTransactions()
extern void WinRTStore_RestoreTransactions_m20FBD308A3340250AD5976890930BFBD555C6FA7 (void);
// 0x000001F7 System.Void UnityEngine.Purchasing.WinRTStore_<>c::.cctor()
extern void U3CU3Ec__cctor_mB152352D2E612A6A60E5751C056B5352AC35DEA0 (void);
// 0x000001F8 System.Void UnityEngine.Purchasing.WinRTStore_<>c::.ctor()
extern void U3CU3Ec__ctor_mF94F1B03FADF27CF46CBD161CDA511B917DBF7DD (void);
// 0x000001F9 System.Boolean UnityEngine.Purchasing.WinRTStore_<>c::<RetrieveProducts>b__8_0(UnityEngine.Purchasing.ProductDefinition)
extern void U3CU3Ec_U3CRetrieveProductsU3Eb__8_0_m8EA8F9CDBA6CD46179AE1F6605E7DE1B1FCAADB8 (void);
// 0x000001FA UnityEngine.Purchasing.Default.WinProductDescription UnityEngine.Purchasing.WinRTStore_<>c::<RetrieveProducts>b__8_1(UnityEngine.Purchasing.ProductDefinition)
extern void U3CU3Ec_U3CRetrieveProductsU3Eb__8_1_m6A7FA4D02DADA12400E65BF7981A0A39AE36B417 (void);
// 0x000001FB System.Void UnityEngine.Purchasing.ITizenStoreConfiguration::SetGroupId(System.String)
// 0x000001FC System.Void UnityEngine.Purchasing.TizenStoreImpl::.ctor(Uniject.IUtil)
extern void TizenStoreImpl__ctor_m1C4A2359A096EE778531F8F43F2C53EA134BE84D (void);
// 0x000001FD System.Void UnityEngine.Purchasing.TizenStoreImpl::SetNativeStore(UnityEngine.Purchasing.INativeTizenStore)
extern void TizenStoreImpl_SetNativeStore_m566D9D3094C6BD91482FDBF79DB490E2C15981FC (void);
// 0x000001FE System.Void UnityEngine.Purchasing.TizenStoreImpl::SetGroupId(System.String)
extern void TizenStoreImpl_SetGroupId_mEC0BCF8473583ABC808E145B0B1E4253EE401DEB (void);
// 0x000001FF System.Void UnityEngine.Purchasing.TizenStoreImpl::MessageCallback(System.String,System.String,System.String,System.String)
extern void TizenStoreImpl_MessageCallback_mA4B7D8E14E7399E5405E1DF5C1C9D7F72E1B4E73 (void);
// 0x00000200 System.Void UnityEngine.Purchasing.TizenStoreImpl::ProcessMessage(System.String,System.String,System.String,System.String)
extern void TizenStoreImpl_ProcessMessage_mD12CA21A450AF70540CCAA34333FCAACB979F77A (void);
// 0x00000201 System.Void UnityEngine.Purchasing.FakeTizenStoreConfiguration::SetGroupId(System.String)
extern void FakeTizenStoreConfiguration_SetGroupId_m05F71176065DFDB3DC90B9B6FABF9489E6AD3D21 (void);
// 0x00000202 System.Void UnityEngine.Purchasing.FakeTizenStoreConfiguration::.ctor()
extern void FakeTizenStoreConfiguration__ctor_m3B58A10ABB10C0253D1EB1A7BB303AD6BFB215C7 (void);
// 0x00000203 System.Void UnityEngine.Purchasing.FacebookStoreImpl::.ctor(Uniject.IUtil)
extern void FacebookStoreImpl__ctor_m7DC7E94516388C068DB0C6002ABDC33F0FD41301 (void);
// 0x00000204 System.Void UnityEngine.Purchasing.FacebookStoreImpl::SetNativeStore(UnityEngine.Purchasing.INativeFacebookStore)
extern void FacebookStoreImpl_SetNativeStore_m603C313638B5668500E4CFFF9FAE8B6E317E002E (void);
// 0x00000205 System.Void UnityEngine.Purchasing.FacebookStoreImpl::MessageCallback(System.String,System.String,System.String,System.String)
extern void FacebookStoreImpl_MessageCallback_mBC2F88BC0E8116317E1EC9EE7B5CDFE237B970D9 (void);
// 0x00000206 System.Void UnityEngine.Purchasing.FacebookStoreImpl::ProcessMessage(System.String,System.String,System.String,System.String)
extern void FacebookStoreImpl_ProcessMessage_mC772DB04397B9570B2374420BD457CD751132922 (void);
// 0x00000207 System.Void UnityEngine.Purchasing.FacebookStoreImpl_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m09B266059CBDADC4F7E6894F1C7A686D7FE41025 (void);
// 0x00000208 System.Void UnityEngine.Purchasing.FacebookStoreImpl_<>c__DisplayClass6_0::<MessageCallback>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CMessageCallbackU3Eb__0_mB1018682C4F417AD534952462415104D7D175374 (void);
// 0x00000209 System.String UnityEngine.Purchasing.FakeStore::get_unavailableProductId()
extern void FakeStore_get_unavailableProductId_m065B3DEAA12D3AC7983C4E2A6B9E064C91E776EB (void);
// 0x0000020A System.Void UnityEngine.Purchasing.FakeStore::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern void FakeStore_Initialize_m64FD3624836CEE485E75ADF2038AFECB3DC41E27 (void);
// 0x0000020B System.Void UnityEngine.Purchasing.FakeStore::RetrieveProducts(System.String)
extern void FakeStore_RetrieveProducts_mD14F708950B7202ABA56BD9F8DE3047209CC2578 (void);
// 0x0000020C System.Void UnityEngine.Purchasing.FakeStore::StoreRetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern void FakeStore_StoreRetrieveProducts_m3F7582AA3F67F53992239B47DDD8FEE0260B53AA (void);
// 0x0000020D System.Void UnityEngine.Purchasing.FakeStore::Purchase(System.String,System.String)
extern void FakeStore_Purchase_mAEBCD146904CF51D8E98F2E44280C75B82EEFDCE (void);
// 0x0000020E System.Void UnityEngine.Purchasing.FakeStore::FakePurchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void FakeStore_FakePurchase_m72437F867A150BFF64349CD71396F5D29421F1BA (void);
// 0x0000020F System.Void UnityEngine.Purchasing.FakeStore::FinishTransaction(System.String,System.String)
extern void FakeStore_FinishTransaction_mCE869F1B78980B247994A426DF65FCC146AB71B7 (void);
// 0x00000210 System.Void UnityEngine.Purchasing.FakeStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void FakeStore_FinishTransaction_m8F249C9F72A842144B3D4F347020D8B5E80F1EEE (void);
// 0x00000211 System.Boolean UnityEngine.Purchasing.FakeStore::StartUI(System.Object,UnityEngine.Purchasing.FakeStore_DialogType,System.Action`2<System.Boolean,T>)
// 0x00000212 System.Void UnityEngine.Purchasing.FakeStore::.ctor()
extern void FakeStore__ctor_mED168BB8C745957275FD038C75ACFDD3F682FA80 (void);
// 0x00000213 System.Void UnityEngine.Purchasing.FakeStore::<>n__0(System.String,System.String,System.String)
extern void FakeStore_U3CU3En__0_m29968C99CF98256E99D5A4E254EE1A0D94C2EA20 (void);
// 0x00000214 System.Void UnityEngine.Purchasing.FakeStore_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m8383096E77E4D1DC21E11DB5B314679397041CC8 (void);
// 0x00000215 System.Void UnityEngine.Purchasing.FakeStore_<>c__DisplayClass13_0::<StoreRetrieveProducts>b__0(System.Boolean,UnityEngine.Purchasing.InitializationFailureReason)
extern void U3CU3Ec__DisplayClass13_0_U3CStoreRetrieveProductsU3Eb__0_m33C79AB8A557AD73E4AF040A632C1FC0EC9DB8CF (void);
// 0x00000216 System.Void UnityEngine.Purchasing.FakeStore_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m3FEC6AECA9C1931D60E6FFF67AE61886E8A74BD1 (void);
// 0x00000217 System.Void UnityEngine.Purchasing.FakeStore_<>c__DisplayClass15_0::<FakePurchase>b__0(System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason)
extern void U3CU3Ec__DisplayClass15_0_U3CFakePurchaseU3Eb__0_mD1A5FD2BA2EDEEDB8B7892527CB5CBDDF1B09BD2 (void);
// 0x00000218 System.Void UnityEngine.Purchasing.UIFakeStore::.ctor()
extern void UIFakeStore__ctor_mF5E7D50CE0AA83CEBECD8E3E8ADBAA2BC792AB64 (void);
// 0x00000219 System.Boolean UnityEngine.Purchasing.UIFakeStore::StartUI(System.Object,UnityEngine.Purchasing.FakeStore_DialogType,System.Action`2<System.Boolean,T>)
// 0x0000021A System.Boolean UnityEngine.Purchasing.UIFakeStore::StartUI(System.String,System.String,System.String,System.Collections.Generic.List`1<System.String>,System.Action`2<System.Boolean,System.Int32>)
extern void UIFakeStore_StartUI_mA8DC00C8BB6324597920174E3ADEBBCC9C476E74 (void);
// 0x0000021B System.Void UnityEngine.Purchasing.UIFakeStore::InstantiateDialog()
extern void UIFakeStore_InstantiateDialog_m67BB65DF82BAC44B4142F469C1430DDC220A1AE8 (void);
// 0x0000021C System.String UnityEngine.Purchasing.UIFakeStore::CreatePurchaseQuestion(UnityEngine.Purchasing.ProductDefinition)
extern void UIFakeStore_CreatePurchaseQuestion_m96557A06A0271F68A4089F2F06FA97ADF8FD600B (void);
// 0x0000021D System.String UnityEngine.Purchasing.UIFakeStore::CreateRetrieveProductsQuestion(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern void UIFakeStore_CreateRetrieveProductsQuestion_m7050FA5BB6E4EF559C65A6EC4654A503934F178E (void);
// 0x0000021E UnityEngine.UI.Button UnityEngine.Purchasing.UIFakeStore::GetOkayButton()
extern void UIFakeStore_GetOkayButton_mF7E658EECAC072F62C0CAAF2CF44CA5796AA55D0 (void);
// 0x0000021F UnityEngine.UI.Button UnityEngine.Purchasing.UIFakeStore::GetCancelButton()
extern void UIFakeStore_GetCancelButton_m177E9FB2644F764DD6CF969B8A2931E90F61D030 (void);
// 0x00000220 UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::GetCancelButtonGameObject()
extern void UIFakeStore_GetCancelButtonGameObject_m2932538927D6A0843BDCA8719DA746F28B363505 (void);
// 0x00000221 UnityEngine.UI.Text UnityEngine.Purchasing.UIFakeStore::GetOkayButtonText()
extern void UIFakeStore_GetOkayButtonText_m6AAF15348A86250FA96E8C1E642F7B426773D80B (void);
// 0x00000222 UnityEngine.UI.Text UnityEngine.Purchasing.UIFakeStore::GetCancelButtonText()
extern void UIFakeStore_GetCancelButtonText_mFDFEA4FD07BFE76F0F5BFEBED6200C195EE75E01 (void);
// 0x00000223 UnityEngine.UI.Dropdown UnityEngine.Purchasing.UIFakeStore::GetDropdown()
extern void UIFakeStore_GetDropdown_mE653F99C0FB80B9AA6D4C33D2A9BF13C6A16EA26 (void);
// 0x00000224 UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::GetDropdownContainerGameObject()
extern void UIFakeStore_GetDropdownContainerGameObject_mB6E3B3B973C10B99297DB1D817DADB110A30C18C (void);
// 0x00000225 System.Void UnityEngine.Purchasing.UIFakeStore::OkayButtonClicked()
extern void UIFakeStore_OkayButtonClicked_m811E895EC49767C2223DDB136344EF46B5C26166 (void);
// 0x00000226 System.Void UnityEngine.Purchasing.UIFakeStore::CancelButtonClicked()
extern void UIFakeStore_CancelButtonClicked_mE3EBAD2E786785E57D432ACC1469674D77A0B875 (void);
// 0x00000227 System.Void UnityEngine.Purchasing.UIFakeStore::DropdownValueChanged(System.Int32)
extern void UIFakeStore_DropdownValueChanged_m809639FBCE58E84B30A1E52ACAB4E8728DBA12E9 (void);
// 0x00000228 System.Void UnityEngine.Purchasing.UIFakeStore::CloseDialog()
extern void UIFakeStore_CloseDialog_mBC82E019CE7121C8E2DCBA3F770A683070682F7B (void);
// 0x00000229 System.Boolean UnityEngine.Purchasing.UIFakeStore::IsShowingDialog()
extern void UIFakeStore_IsShowingDialog_m165A16762CF04D260A361FAEA855EE29598D2F40 (void);
// 0x0000022A System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>b__16_0()
extern void UIFakeStore_U3CInstantiateDialogU3Eb__16_0_mFA74D5B8702FE7D94E27E750EBA7F8DCE5F7903B (void);
// 0x0000022B System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>b__16_1()
extern void UIFakeStore_U3CInstantiateDialogU3Eb__16_1_m9A1558F35327964F5769BCB9EFB5D5AE4AE1ECD8 (void);
// 0x0000022C System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>b__16_2()
extern void UIFakeStore_U3CInstantiateDialogU3Eb__16_2_m10A27A3286CFBA502C73ABDEA075E9B565F3809A (void);
// 0x0000022D System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>b__16_3(System.Int32)
extern void UIFakeStore_U3CInstantiateDialogU3Eb__16_3_mB67CA9956F8F607385AB827F3FB152B4DD9E2F21 (void);
// 0x0000022E System.Void UnityEngine.Purchasing.UIFakeStore_DialogRequest::.ctor()
extern void DialogRequest__ctor_m15E01BCCF9BA8FB4051DFB1191E79723E15FE636 (void);
// 0x0000022F System.Void UnityEngine.Purchasing.UIFakeStore_LifecycleNotifier::OnDestroy()
extern void LifecycleNotifier_OnDestroy_m80986F402D56C67A7EBE7FD54896FBD65EB51BA0 (void);
// 0x00000230 System.Void UnityEngine.Purchasing.UIFakeStore_LifecycleNotifier::.ctor()
extern void LifecycleNotifier__ctor_mF6579954DD064CCF856C0315605609FAA94A9758 (void);
// 0x00000231 System.Void UnityEngine.Purchasing.UIFakeStore_<>c__DisplayClass14_0`1::.ctor()
// 0x00000232 System.Void UnityEngine.Purchasing.UIFakeStore_<>c__DisplayClass14_0`1::<StartUI>b__0(System.Boolean,System.Int32)
// 0x00000233 System.Void UnityEngine.Purchasing.UIFakeStore_<>c::.cctor()
extern void U3CU3Ec__cctor_m9E8EBEA86DB17FEEF9A95F75AD4A3F03B20B1495 (void);
// 0x00000234 System.Void UnityEngine.Purchasing.UIFakeStore_<>c::.ctor()
extern void U3CU3Ec__ctor_mEE44D046E13EE47F95A78E5D681D24B29B10A2B6 (void);
// 0x00000235 System.String UnityEngine.Purchasing.UIFakeStore_<>c::<CreateRetrieveProductsQuestion>b__18_0(UnityEngine.Purchasing.ProductDefinition)
extern void U3CU3Ec_U3CCreateRetrieveProductsQuestionU3Eb__18_0_mE71AC3B010F0AA573786C0908D992D6A2DBE38FD (void);
// 0x00000236 UnityEngine.Purchasing.FileReference UnityEngine.Purchasing.FileReference::CreateInstance(System.String,UnityEngine.ILogger,Uniject.IUtil)
extern void FileReference_CreateInstance_m2480AF4AFF6D5084D5598CE492D5177E6C2B0240 (void);
// 0x00000237 System.Void UnityEngine.Purchasing.FileReference::.ctor(System.String,UnityEngine.ILogger)
extern void FileReference__ctor_m70CE741633F86E0AEA6071F8C263AAC94540754E (void);
// 0x00000238 System.Void UnityEngine.Purchasing.FileReference::Save(System.String)
extern void FileReference_Save_m554C2C82EFA4B92A04E2C335117DDF7EA10218AF (void);
// 0x00000239 System.String UnityEngine.Purchasing.FileReference::Load()
extern void FileReference_Load_m1BEC26A7A712092A3F4986489197959ED9C26A36 (void);
// 0x0000023A System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.ProductDefinitionExtensions::DecodeJSON(System.Collections.Generic.List`1<System.Object>,System.String)
extern void ProductDefinitionExtensions_DecodeJSON_mD8CB385394E30826BF3C9207DBF1E0C783B6B3E3 (void);
// 0x0000023B T[] UnityEngine.Purchasing.Extension.UnityUtil::GetAnyComponentsOfType()
// 0x0000023C System.DateTime UnityEngine.Purchasing.Extension.UnityUtil::get_currentTime()
extern void UnityUtil_get_currentTime_mE0517B0267DD8A42F08DDB6E8ADBFA08DC71FEAA (void);
// 0x0000023D System.String UnityEngine.Purchasing.Extension.UnityUtil::get_persistentDataPath()
extern void UnityUtil_get_persistentDataPath_m2A8FDA4B9D43B765E3FC03232891F98FC4C60FF7 (void);
// 0x0000023E System.String UnityEngine.Purchasing.Extension.UnityUtil::get_deviceUniqueIdentifier()
extern void UnityUtil_get_deviceUniqueIdentifier_mCCF1538EA703E76342F751F31D2DFAE36ECB4409 (void);
// 0x0000023F System.String UnityEngine.Purchasing.Extension.UnityUtil::get_unityVersion()
extern void UnityUtil_get_unityVersion_mE9E277B817CA49F007E3BAD98D3EA2CB3B290C6E (void);
// 0x00000240 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_cloudProjectId()
extern void UnityUtil_get_cloudProjectId_mC8889E4EFCF7D739544E19AAE597EA5935CB6117 (void);
// 0x00000241 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_userId()
extern void UnityUtil_get_userId_m88748CDB01D6D306C066373E5832D6EF246EFC49 (void);
// 0x00000242 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_gameVersion()
extern void UnityUtil_get_gameVersion_m31936F5B76839D6DF1970AAACF853E5D45AFD985 (void);
// 0x00000243 System.UInt64 UnityEngine.Purchasing.Extension.UnityUtil::get_sessionId()
extern void UnityUtil_get_sessionId_m7E6B9F793C793D5105B892F33F99F65BBC4442F0 (void);
// 0x00000244 UnityEngine.RuntimePlatform UnityEngine.Purchasing.Extension.UnityUtil::get_platform()
extern void UnityUtil_get_platform_mF606F06FE303EDBD7124C2271339A1D58D0444E0 (void);
// 0x00000245 System.Boolean UnityEngine.Purchasing.Extension.UnityUtil::get_isEditor()
extern void UnityUtil_get_isEditor_m165D3077CFD7CA1084F1A97390D76B439E4A8687 (void);
// 0x00000246 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_deviceModel()
extern void UnityUtil_get_deviceModel_mDCA6013837CA1F838BED8CD0E5DE4457D979478F (void);
// 0x00000247 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_deviceName()
extern void UnityUtil_get_deviceName_m45A4B711EB162F7A3E2234C975C7FBEB79207335 (void);
// 0x00000248 UnityEngine.DeviceType UnityEngine.Purchasing.Extension.UnityUtil::get_deviceType()
extern void UnityUtil_get_deviceType_m0D78117B41822D285CC9ADFD14D01AF9F955B08F (void);
// 0x00000249 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_operatingSystem()
extern void UnityUtil_get_operatingSystem_m842B055FD14B71F2971DA92BD055F7EEBDB24027 (void);
// 0x0000024A System.Int32 UnityEngine.Purchasing.Extension.UnityUtil::get_screenWidth()
extern void UnityUtil_get_screenWidth_m2028268F39D36736FC091408175C3400B4757018 (void);
// 0x0000024B System.Int32 UnityEngine.Purchasing.Extension.UnityUtil::get_screenHeight()
extern void UnityUtil_get_screenHeight_m9DC59F962C9F40C7F4F7A8008C509A42747D5717 (void);
// 0x0000024C System.Single UnityEngine.Purchasing.Extension.UnityUtil::get_screenDpi()
extern void UnityUtil_get_screenDpi_m65E20C5EE7FAC2904891673EC0DC715202B764D4 (void);
// 0x0000024D System.String UnityEngine.Purchasing.Extension.UnityUtil::get_screenOrientation()
extern void UnityUtil_get_screenOrientation_m25C925CAC035D580C1C1C16253B472A95857CB66 (void);
// 0x0000024E System.Object UnityEngine.Purchasing.Extension.UnityUtil::Uniject.IUtil.InitiateCoroutine(System.Collections.IEnumerator)
extern void UnityUtil_Uniject_IUtil_InitiateCoroutine_mC6A9BC4EFA5ED3AFB70E5596F676D7BF81C3975D (void);
// 0x0000024F System.Void UnityEngine.Purchasing.Extension.UnityUtil::Uniject.IUtil.InitiateCoroutine(System.Collections.IEnumerator,System.Int32)
extern void UnityUtil_Uniject_IUtil_InitiateCoroutine_m02A75FBC47ED72DE099976F8123549A3905BBD5B (void);
// 0x00000250 System.Void UnityEngine.Purchasing.Extension.UnityUtil::RunOnMainThread(System.Action)
extern void UnityUtil_RunOnMainThread_mD87E0072DA7A5037ED7E54064907F1B5E0E11652 (void);
// 0x00000251 System.Object UnityEngine.Purchasing.Extension.UnityUtil::GetWaitForSeconds(System.Int32)
extern void UnityUtil_GetWaitForSeconds_mCCA700B90D4CA34E60D288FFDBA267C866BBE2DE (void);
// 0x00000252 System.Void UnityEngine.Purchasing.Extension.UnityUtil::Start()
extern void UnityUtil_Start_mE496390074ED7C96E33A2131F94F6FB4A076FC86 (void);
// 0x00000253 T UnityEngine.Purchasing.Extension.UnityUtil::FindInstanceOfType()
// 0x00000254 T UnityEngine.Purchasing.Extension.UnityUtil::LoadResourceInstanceOfType()
// 0x00000255 System.Boolean UnityEngine.Purchasing.Extension.UnityUtil::PcPlatform()
extern void UnityUtil_PcPlatform_mAE39342DC5B9517F34D7B78C1A1E71F5218E505B (void);
// 0x00000256 System.Void UnityEngine.Purchasing.Extension.UnityUtil::DebugLog(System.String,System.Object[])
extern void UnityUtil_DebugLog_m988452F9D30616D70E1AD480B73A1A3BEE39639A (void);
// 0x00000257 System.Collections.IEnumerator UnityEngine.Purchasing.Extension.UnityUtil::DelayedCoroutine(System.Collections.IEnumerator,System.Int32)
extern void UnityUtil_DelayedCoroutine_mB9731082B84D84207E7F3FF2D426E53FAADFA2E4 (void);
// 0x00000258 System.Void UnityEngine.Purchasing.Extension.UnityUtil::Update()
extern void UnityUtil_Update_m928A08FD29363BF1776DB411C2B6DDE169AA25B2 (void);
// 0x00000259 System.Void UnityEngine.Purchasing.Extension.UnityUtil::AddPauseListener(System.Action`1<System.Boolean>)
extern void UnityUtil_AddPauseListener_mFFB616F77C07F4FD3E9E52FD2E72DF012564BD5F (void);
// 0x0000025A System.Void UnityEngine.Purchasing.Extension.UnityUtil::OnApplicationPause(System.Boolean)
extern void UnityUtil_OnApplicationPause_m581AA60D0E4B529E0E5BBC3A4AF10EDB8A166790 (void);
// 0x0000025B System.Boolean UnityEngine.Purchasing.Extension.UnityUtil::IsClassOrSubclass(System.Type,System.Type)
extern void UnityUtil_IsClassOrSubclass_m21082FFD714984294526DD90C34461C3DB0B5128 (void);
// 0x0000025C System.Void UnityEngine.Purchasing.Extension.UnityUtil::.ctor()
extern void UnityUtil__ctor_m12915491833B8AC50A13E956BE797256B1010E07 (void);
// 0x0000025D System.Void UnityEngine.Purchasing.Extension.UnityUtil::.cctor()
extern void UnityUtil__cctor_mAFE259562F3D67630D164BDE8CDE10C73D27D88E (void);
// 0x0000025E System.Void UnityEngine.Purchasing.Extension.UnityUtil_<DelayedCoroutine>d__49::.ctor(System.Int32)
extern void U3CDelayedCoroutineU3Ed__49__ctor_m0CF794D5E0CF9888565D74DBF0CB7A781E3FFC33 (void);
// 0x0000025F System.Void UnityEngine.Purchasing.Extension.UnityUtil_<DelayedCoroutine>d__49::System.IDisposable.Dispose()
extern void U3CDelayedCoroutineU3Ed__49_System_IDisposable_Dispose_m4F25109E3EE4412BB40F7B4FB0AD8A49B8FC9C4B (void);
// 0x00000260 System.Boolean UnityEngine.Purchasing.Extension.UnityUtil_<DelayedCoroutine>d__49::MoveNext()
extern void U3CDelayedCoroutineU3Ed__49_MoveNext_mBCFBA9637AA6DBCA8DCD542272C160B7758173FC (void);
// 0x00000261 System.Object UnityEngine.Purchasing.Extension.UnityUtil_<DelayedCoroutine>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedCoroutineU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7E96A164117AB133B0A66F78DE7238B586601D28 (void);
// 0x00000262 System.Void UnityEngine.Purchasing.Extension.UnityUtil_<DelayedCoroutine>d__49::System.Collections.IEnumerator.Reset()
extern void U3CDelayedCoroutineU3Ed__49_System_Collections_IEnumerator_Reset_m4BBD4503F8C17B6E6BB32B568CD5AF9E579718C2 (void);
// 0x00000263 System.Object UnityEngine.Purchasing.Extension.UnityUtil_<DelayedCoroutine>d__49::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedCoroutineU3Ed__49_System_Collections_IEnumerator_get_Current_mBCA487AE2461E12FC954964F399C5A13AE160C54 (void);
// 0x00000264 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m53F9152F2CD10CB765A06BD1CCAE7C7B426C008A (void);
static Il2CppMethodPointer s_methodPointers[612] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AndroidJavaStore_GetStore_m2A8FC945796A1F5F889CB73DEFA96CE1FCA664DA,
	AndroidJavaStore__ctor_m70EB8A74FDD05554EDA5E4447B8945C0DBE4C062,
	AndroidJavaStore_RetrieveProducts_m904D7F385AB73CD4A0A7404D0446281A33A04872,
	AndroidJavaStore_Purchase_mA1D5EA803F76876AD8105227C947FED3AF409740,
	AndroidJavaStore_FinishTransaction_mB19BB7BE7F950BF6AF0448B7237E09B0BD37E745,
	NULL,
	NULL,
	NULL,
	NULL,
	JavaBridge__ctor_m73B7AA88A0E9A377B56B3E6EC7F635D78FBCC2D6,
	JavaBridge__ctor_m08D6FCA2BDF16A42BDE40C27B4AF410C24959FF3,
	JavaBridge_OnSetupFailed_mF9F6F2093083053A14D63AE6749C6A5D081B2B7C,
	JavaBridge_OnProductsRetrieved_mD7598324F4BC0FEA4B1C6DA2088C5A40DE003FD5,
	JavaBridge_OnPurchaseSucceeded_mB0DBAC88689C51B419530F201A519240EF761A1D,
	JavaBridge_OnPurchaseFailed_mC99ADC72398856D38999C9056764488FCA80A614,
	SerializationExtensions_TryGetString_m683D88D1A71FF92A3FBD410D8239A7DB8727DDA1,
	JSONSerializer_SerializeProductDef_mF2645D8657030E744AED67C4ABCB92937CE3843D,
	JSONSerializer_SerializeProductDefs_mDFCF95475F771264408C2B7A0ED7E8761C6BCE0C,
	JSONSerializer_SerializeProductDescs_mF0D18710CDF0FCAF0088CA514CC1600A47BDC123,
	JSONSerializer_DeserializeProductDescriptions_m3A6EE1586658E8DD642AF3B14DB22245A5B959E3,
	JSONSerializer_DeserializeFailureReason_m2267B2FEBE10B2CF917B6C79FA5F400EBA9FE04F,
	JSONSerializer_DeserializeMetadata_mA31DF2D3510240E2ABAFF1B7A200D3CDB3DAE06E,
	JSONSerializer_EncodeProductDef_m27278FB9A153ED42DAD7B00E040570A0A4A9211E,
	JSONSerializer_EncodeProductDesc_mA281441760D3B5E2E162089CEC86E7DC3D354805,
	JSONSerializer_EncodeProductMeta_mBE000E23E1824C50188E38C06BA1618DE6AB32AE,
	ScriptingUnityCallback__ctor_m34521511B2C4812B536F9444BCC5BA18D5F0AF50,
	ScriptingUnityCallback_OnSetupFailed_m03FFF3D3B8F04F2FD52EE886E5C1589C0391AC15,
	ScriptingUnityCallback_OnProductsRetrieved_m2D7EAADBE9D1DE370628720CAEDB1CF5794FEBDA,
	ScriptingUnityCallback_OnPurchaseSucceeded_m60599B66D8EF2A2B3FCEFA36D740E4A19083AEA2,
	ScriptingUnityCallback_OnPurchaseFailed_mAE79FE3935D92911F3ECAE6C8EADBB2DA832B4BA,
	U3CU3Ec__DisplayClass3_0__ctor_m32207C242A668B40DF097AA99DCBF7F6717D470D,
	U3CU3Ec__DisplayClass3_0_U3COnSetupFailedU3Eb__0_mD7EEC31FF55918387286B9CCE18F29D7CEE81B76,
	U3CU3Ec__DisplayClass4_0__ctor_mED8019084593C580AC73852DA9F7EEB4AB99C598,
	U3CU3Ec__DisplayClass4_0_U3COnProductsRetrievedU3Eb__0_mFD8455179BA3C19D610E8586484958F0289934AD,
	U3CU3Ec__DisplayClass5_0__ctor_mA95E0EF7841FE300701EDED268EFCED1CCB47394,
	U3CU3Ec__DisplayClass5_0_U3COnPurchaseSucceededU3Eb__0_m28093106DF21868395DCB7733F7591D7D9E92C85,
	U3CU3Ec__DisplayClass6_0__ctor_m4E7698CB27A47857E50CEFBDB45292BC98CC8E98,
	U3CU3Ec__DisplayClass6_0_U3COnPurchaseFailedU3Eb__0_m760BC3F40DBF1E8B724C55726BA431309E57980D,
	AmazonAppStoreStoreExtensions__ctor_m49DCFE64814F8FE54771EA66FB5ECBC198D82077,
	FakeAmazonExtensions__ctor_m1C530544AF4958ABA72E2FE43459E97D89E6A815,
	FakeMoolahConfiguration_set_appKey_m8462D9E8AD3DA1B796E7609D2A77D76E709D13DF,
	FakeMoolahConfiguration_set_hashKey_mD00E161F82DD4113A196FA151542B387240CD0E7,
	FakeMoolahConfiguration_SetMode_m105B2B270C9CF484AD4F6222AD47EC180CF92479,
	FakeMoolahConfiguration__ctor_m65F59D92BCD1076BE5066FD4458DA401B56DB609,
	FakeMoolahExtensions_RestoreTransactionID_m60F39513E3B963287D482C8973877FD99555C6DE,
	FakeMoolahExtensions__ctor_m09F7C4E98C30F96C4609C437C5ABA8C60D61741A,
	NULL,
	NULL,
	NULL,
	NULL,
	MoolahStoreImpl_Initialize_m3F9443890CD015A050998C7E72FF0A9E17930E26,
	MoolahStoreImpl_RetrieveProducts_mE1D6C6D508235803C9435C65D4016D8CEC0EC8EF,
	MoolahStoreImpl_GetProductTypeIndex_m9A9160800FFFDAF0CC888E82EC988BF6BDE760B6,
	MoolahStoreImpl_VaildateProductProcess_mA42639497798AFB33EDABD9CCE323D575A2BA48A,
	MoolahStoreImpl_GetCurrentString_mFE3F2818119B89977F2CF3EC299A1E04930392A0,
	MoolahStoreImpl_VaildateProduct_m8018A52480BBB71EAF457ACD6F06D1906C4BA6D3,
	MoolahStoreImpl_RetrieveProductsSucceeded_mAD118054D2158E57F5B62FDA5ED26D8F7925D6DC,
	MoolahStoreImpl_RetrieveProductsFailed_m3ED20FCCD8D55C2B89F6F83FAC1F519081A18D2C,
	MoolahStoreImpl_ClosePayWebView_m4DB68A14F093DF422D53AA78D1D4D1107416FEB4,
	MoolahStoreImpl_PurchaseRusult_mF6C7BECA3361B2F8422E67B358819C290038F639,
	MoolahStoreImpl_Purchase_m7D4061E9EEC2E50978EF9A21546F478B28342BC7,
	MoolahStoreImpl_DeviceUniqueIdentifier_m020299D4FC51721606F2841F4688801707B4B6B8,
	MoolahStoreImpl_RequestAuthCode_m47D7BCF1058698F0B1C72479077F70A0F3ECC82A,
	MoolahStoreImpl_RequestAuthCode_m257C9CE803D4B7AFEFBDCCFFC5CFA4B6C90C69C0,
	MoolahStoreImpl_StartPurchasePolling_m15E9BF9628DE86801DBD9F00167DBCDEDE438510,
	MoolahStoreImpl_PurchaseSucceed_m569EBFB2F074685F0EC6039436BB991B73135C9F,
	MoolahStoreImpl_PurchaseFailed_m6CE431F13387A4897909013E1B2762A98D2257D3,
	MoolahStoreImpl_FinishTransaction_m8572B7F8035D32209F5732C6D5BD700762941B42,
	MoolahStoreImpl_GetStringMD5_m9A96BD7F255B3C87B71AE82447BFA3A78340134B,
	MoolahStoreImpl_get_appKey_m5E6F079A6E225D99E991D0FDC0003446BFEAEB7C,
	MoolahStoreImpl_set_appKey_m2411D3DDC65EC1EBEAD90275E94479A830D7940F,
	MoolahStoreImpl_get_hashKey_mDCF66064B754ED75D43ADD0B6F7AA033531654CD,
	MoolahStoreImpl_set_hashKey_m7A4C659FBC1DA22EFE4925CA4515CB152F276D52,
	MoolahStoreImpl_get_notificationURL_m6CAB979675D3F9513A50A42F7879A800BD91BE4A,
	MoolahStoreImpl_set_notificationURL_m2B7249708CA47654EA35834904BCE142FB8F0889,
	MoolahStoreImpl_SetMode_mD57A5FB00B84A521667649915915B05A625C2312,
	MoolahStoreImpl_GetMode_m18F79E1B2C54422F200644733B436C6C2BA75A1E,
	MoolahStoreImpl_RestoreTransactionID_m5E0246AA07B0217C1A031581F5E4D86DA92C570B,
	MoolahStoreImpl_RestoreTransactionIDProcess_m92AF1C35C226D0E8F75E9D19D8C18449C65148E9,
	MoolahStoreImpl_ValidateReceipt_m17ED1BF7D7369BB465760C35F433AED8633176A9,
	MoolahStoreImpl_ValidateReceiptProcess_m2EA609D2AD7C852ECF3AFAD3FAFB484D99E111D4,
	MoolahStoreImpl__ctor_mFAC1F88F558A17497C76BCDC9C2058EB85A1562B,
	MoolahStoreImpl__cctor_m7C188448D97CED80EA0BB3CD8DB793A2212FB654,
	MoolahStoreImpl_U3CRetrieveProductsU3Eb__9_0_mA70E7E82721A2F1F72AB6AADAD49E098B2908AC3,
	U3CVaildateProductU3Ed__13__ctor_mD7131C16EA8928B677C8680B570C00CC09F2E8F1,
	U3CVaildateProductU3Ed__13_System_IDisposable_Dispose_mD847F8095490F98D20FFE0907F7A9839FD9C1789,
	U3CVaildateProductU3Ed__13_MoveNext_m64E090D65FC459AF1B48DAF5987960144B44D06D,
	U3CVaildateProductU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D50D1149F3B400899CBF2B963050F2EED21FCAB,
	U3CVaildateProductU3Ed__13_System_Collections_IEnumerator_Reset_m2C7CB5050F4B600C6672958CAD1A919BD35794F2,
	U3CVaildateProductU3Ed__13_System_Collections_IEnumerator_get_Current_m0546AFE5B168974CF49F2DF98FED4C6F97E4BDE4,
	U3CU3Ec__DisplayClass18_0__ctor_m2E668B4CF04C610ACB428DA4D5E2821F838BD02B,
	U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__0_m824D79DA93DF5EC7A1CF169E6927C4EEBC700597,
	U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__1_mC3A9679284C1AAD7B47C9D454039D3A2A5B86E19,
	U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__2_m6F8F894B5370A4B488151BD4A9F9199BCA7B9058,
	U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__3_m2EC1D38BB5E9B77A137AEFEC784FA9D53E44AAD4,
	U3CRequestAuthCodeU3Ed__22__ctor_m36D849DDBC9DBF849505947938FBE9F2F7EE3244,
	U3CRequestAuthCodeU3Ed__22_System_IDisposable_Dispose_m374F8E8CD511EE9ECCC8E4584C10710B3F88F3BF,
	U3CRequestAuthCodeU3Ed__22_MoveNext_mAFF077966E415E8D43E894942890686D51027A17,
	U3CRequestAuthCodeU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6EA14BF15A4A960A4217BCA14C07F2846DA23789,
	U3CRequestAuthCodeU3Ed__22_System_Collections_IEnumerator_Reset_m7CFCCF10AE375362EA01A1EFB32DD16EFAE6FE12,
	U3CRequestAuthCodeU3Ed__22_System_Collections_IEnumerator_get_Current_m1737C1780144D641BB88078D0B8E4264545CF9EC,
	U3CStartPurchasePollingU3Ed__23__ctor_mB3E1CEBA3BAFD94A43299B63F518D93D1A84D8C8,
	U3CStartPurchasePollingU3Ed__23_System_IDisposable_Dispose_m301B2C4DC2D76C82F9463A0FCFE1AFD1213F6FBB,
	U3CStartPurchasePollingU3Ed__23_MoveNext_mD58DCCEE7FDC0AA520DA7E0981613D34086B50D9,
	U3CStartPurchasePollingU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF15CBB6054571BADC7FE3D446BF5C3C749076F2F,
	U3CStartPurchasePollingU3Ed__23_System_Collections_IEnumerator_Reset_m9AE3D70AAA203B8132364FB8D133BCAB9406F0C4,
	U3CStartPurchasePollingU3Ed__23_System_Collections_IEnumerator_get_Current_m15E7E58BB0B675D5008691A6660F52B8DFDED19F,
	U3CRestoreTransactionIDProcessU3Ed__45__ctor_mD00F86A281E946BEE7981BC4F7BF3FD0B9E5747A,
	U3CRestoreTransactionIDProcessU3Ed__45_System_IDisposable_Dispose_mA4D8E5392ED0366300A4916961641CD3E07C98A3,
	U3CRestoreTransactionIDProcessU3Ed__45_MoveNext_m15449CD4ABCB420BCB2CD1F39A5D20757A774625,
	U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD32D80CC7257BC108D3CF7C56E409EC3A7197CC6,
	U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_IEnumerator_Reset_m9A7097DD8EF993496E5CAD955F729F0F1AE0936A,
	U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_IEnumerator_get_Current_mA0C6EE86705186177EEFF5A8AB07A9A9D2A616C2,
	U3CValidateReceiptProcessU3Ed__47__ctor_mDD05E6797CA356BE85C9E2BEB7D83D28AF492A46,
	U3CValidateReceiptProcessU3Ed__47_System_IDisposable_Dispose_mE5998FBFD6D62AC8CAC47650C0ABE2E3CF6FFE33,
	U3CValidateReceiptProcessU3Ed__47_MoveNext_mD1A52FD5FE7BA88384CE3946DC797CFEB3869E09,
	U3CValidateReceiptProcessU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD0E5DB2A5D58A1BC0BAA5CD30A1234021E01976,
	U3CValidateReceiptProcessU3Ed__47_System_Collections_IEnumerator_Reset_m412728DCE7F91B35C9C1AD54E4E4A2B09C5E38E8,
	U3CValidateReceiptProcessU3Ed__47_System_Collections_IEnumerator_get_Current_m9639D0F0A121AAF1DA33FA6FB4712B877B993E01,
	PayMethod_showPayWebView_mB8E25FB438411674827723ABE765A5B8900D0BEF,
	PayMethod_getDeviceID_m0073DAA785743281B1C6C78663A57B6A5005D44A,
	FakeGooglePlayStoreExtensions_RestoreTransactions_mEF684C1EB626E3BDE09DFE40C77BD62BBFD01360,
	FakeGooglePlayStoreExtensions__ctor_m464BEBD60E48E93FDF3BDE47980215859E71EDF8,
	GooglePlayAndroidJavaStore__ctor_m97A8F8ACEDA88C05E81EE76204F1E22C4D79C86C,
	GooglePlayAndroidJavaStore_Purchase_mF694542F8377C5BF4EFB47AFEC568216CEAC23AC,
	FakeGooglePlayConfiguration__ctor_mD0E7B971BB1AF7E7D9A04222E67ACBE2494C4BB1,
	GooglePlayStoreCallback__ctor_mAC85FB73D091AFDD4914203F4162E2C6D92E5768,
	GooglePlayStoreCallback_OnTransactionsRestored_m78EAD5A60BC2EB7007E3157B21CA7AF7FBEA8F30,
	GooglePlayStoreExtensions__ctor_m12E98D6CD646CE0791A604AC22EC81CBA9192BBB,
	GooglePlayStoreExtensions_SetAndroidJavaObject_mD04F18093F85F93AAB7EF4C71F440B17222E6F53,
	GooglePlayStoreExtensions_SetPublicKey_mEC44549110434E656D736A90A4B5672343068E75,
	GooglePlayStoreExtensions_UpgradeDowngradeSubscription_mD66CD85A8D82E06641A2EEA8E1B1017A6BDAA53E,
	GooglePlayStoreExtensions_GetProductJSONDictionary_m94913623446E4B7242F68B7DBC07916B1F9746FD,
	GooglePlayStoreExtensions_RestoreTransactions_m07A56A5F888ABF508FFFD4A7825B1D85346F275B,
	GooglePlayStoreExtensions_FinishAdditionalTransaction_mFD2E47EA4FCCF1064B0E3B0C50FA560F6BE82BDC,
	NULL,
	FakeSamsungAppsExtensions_SetMode_m08E38EDC56A095693A4CAA43DE5770B04793E2F8,
	FakeSamsungAppsExtensions_RestoreTransactions_m8AAD68FD295B9A9E0A19CD4B9DFBB96C76D20938,
	FakeSamsungAppsExtensions__ctor_m2762E5F140664B1D2C06BF98F9E3305657F86F0D,
	NULL,
	NULL,
	NULL,
	SamsungAppsJavaBridge__ctor_m3F6C93E33F9AFDC554EEE81A5A3FCFDA12F74C5F,
	SamsungAppsJavaBridge_OnTransactionsRestored_m6C9A32FDF7FA492355F179566C7A583CDA3EC87D,
	SamsungAppsStoreExtensions__ctor_mDE673A17D1A218BB58F30CCAD4BC156018846BB1,
	SamsungAppsStoreExtensions_SetAndroidJavaObject_m505CDB557622B24905E3C4472C815EB9DFAC99D3,
	SamsungAppsStoreExtensions_SetMode_m7669E45FA8D5D08CA9EFB4C8C8F8447A12E2CB45,
	SamsungAppsStoreExtensions_RestoreTransactions_m73B9A73971ADBD87CE8C4DF9AFC96169F53DEF3E,
	SamsungAppsStoreExtensions_OnTransactionsRestored_m4B8E3FC5C0AB8651891C619D91F681E7912F0A03,
	FakeUnityChannelConfiguration__ctor_m37F2E7C0DB318DF94BC7ED9BBD6A7FFD39E9D6D5,
	FakeUnityChannelExtensions__ctor_m88DC72E89A539BDBAD29F4EFF9D3BFD4B08B3E57,
	FakeUDPExtension__ctor_mBFECA88FA885EA52972CD112CBACEB31B356014F,
	NULL,
	NULL,
	NULL,
	NULL,
	UDP_get_Name_m297A3949ABF6B197B39C1A18FA460020D9CCD9FF,
	UDPBindings_Initialize_m02AECF9D41115AF9EBB64CD2148BF8B0D637ECD9,
	UDPBindings_Purchase_m05C42D11B1B2C5D7D619CA299291A1865923C6D5,
	UDPBindings_RetrieveProducts_m49B87240AF807B38031CA65733BE25570E9D19D2,
	UDPBindings_FinishTransaction_m413D01154C61B993A719FE3EB562F8296CE1DD22,
	UDPBindings_FindPurchaseInfo_m1C94CA4864598BEB281C619B874A119F064601F7,
	UDPBindings_OnInitialized_m69EE4BB6672A3DDB1C6F50D927F859AFBA1B9502,
	UDPBindings_OnInitializeFailed_m1607DBDBDBD73238F248F7E91286A1DD6323D04C,
	UDPBindings_OnPurchase_m8AAD85481CF41F9080A38A91B6E47DDEBE8D7050,
	UDPBindings_OnPurchaseFailed_m1BD7B81CD0B9141E5B0F62A2F67684BB4434FBDF,
	UDPBindings_OnPurchaseConsume_m64D9A198A8BB38FF9866F1FAF2C9ED16883212B7,
	UDPBindings_OnPurchaseConsumeFailed_m819B887671382FFC885C71D1761BB25685BFD42D,
	UDPBindings_OnQueryInventory_m87E738D7040B1651BC410F2607F82464176BA879,
	UDPBindings_OnQueryInventoryFailed_mB11290C76C63C34815FBF38AA4D6DEAD88081244,
	UDPBindings_RetrieveProducts_mE136770E248B5B8BDAAB1C3BA4085E8FD82D8CFF,
	UDPBindings_Purchase_m63841A22D107D9F5AB458D7D133C272D683528EA,
	UDPBindings_FinishTransaction_mCDA775F49A07D94B3A6B89EBCF1D473B92124265,
	UDPBindings_StringPropertyToDictionary_mD8F3ECA6768BBDD3EB12EFD36447AFFF66468A1B,
	UDPBindings__ctor_mE46AC1CEDDA8E5B90E9C803044939BA58F965190,
	UDPImpl_SetNativeStore_m26D55B30D206C19CDD0A70E44DEA8E10CC022570,
	UDPImpl_Initialize_m1D79310D9A0FE2B93EDBD4FC7CC8B8689FE45A73,
	UDPImpl_RetrieveProducts_mB6AEEBE33DD200F3CCC5E9C9617AAD507C33A25F,
	UDPImpl_Purchase_m7F16982EE97D6C59A266C8F8D3B1C7DAF77FC1E1,
	UDPImpl_FinishTransaction_mCB5C2613B4F63395F5595AD2A92A4B9DA713A587,
	UDPImpl_DictionaryToStringProperty_mA225E2593D0AF8D9FD2B100C5D489F6262732C31,
	UDPImpl__ctor_mEF25815280F1C827783357EABD83AD1610F95838,
	U3CU3Ec__DisplayClass7_0__ctor_mB3801BC98C775E8EA7B125A08433C01C94BF01E8,
	U3CU3Ec__DisplayClass7_0_U3CRetrieveProductsU3Eb__0_m784DFD90A05246DC50DF2C177C710EFEA8CF55E8,
	U3CU3Ec__DisplayClass7_0_U3CRetrieveProductsU3Eb__1_m2094590C377E50F878DD69C15BFF55E366E900C6,
	U3CU3Ec__DisplayClass8_0__ctor_mE8F183A145227EC4F9117C6559A6102E5E98284E,
	U3CU3Ec__DisplayClass8_0_U3CPurchaseU3Eb__0_m04AF95989DF87441DFFDEDB10C0557823DFDB4CA,
	AppleStoreImpl__ctor_mF4E15742FA52E6669461C28977A1F80D1C2D3349,
	AppleStoreImpl_SetNativeStore_m2DA4002D8ACC3385AD9510A5097B26E7DA3B170A,
	AppleStoreImpl_OnProductsRetrieved_m001E2ECE4FD5F9BD6184C4D29BB3B67FDF00B0B6,
	AppleStoreImpl_RestoreTransactions_mDC111E1FDF419746DFC94D7AF769CA3BB234403D,
	AppleStoreImpl_RegisterPurchaseDeferredListener_mED7C271C087A7CC17F872F74097D63F9A50394B8,
	AppleStoreImpl_OnPurchaseDeferred_m50D3D0C6D0ECA1F17D833D4101C8476BE7054735,
	AppleStoreImpl_OnPromotionalPurchaseAttempted_mE0822A252C625B0FBABE8C5C0750D49B4E9F555E,
	AppleStoreImpl_OnTransactionsRestoredSuccess_m464BA77AA019E6DBC40D6E9887D2AA803DBD6668,
	AppleStoreImpl_OnTransactionsRestoredFail_m1CC319C3AAC4DB0202B6FBA92228CACECDC8A629,
	AppleStoreImpl_OnAppReceiptRetrieved_mB5BF6B52F0542555A966D67991A431C5751EA630,
	AppleStoreImpl_OnAppReceiptRefreshedFailed_m79D95233C3C50E66F8A966CD7CA44BC4075607FD,
	AppleStoreImpl_MessageCallback_mD0A78B20AACCAEB5D2ECE032884DE17D435454B6,
	AppleStoreImpl_ProcessMessage_mBC42AD652176EECB2E0FF48E7DD022C0B3B02A27,
	AppleStoreImpl_OnPurchaseSucceeded_m054A29DFF3FBEBFA7CC8E8939FE40E40EC143275,
	AppleStoreImpl_getAppleReceiptFromBase64String_m364893746979225CC1AB27603521360692544F73,
	AppleStoreImpl_isValidPurchaseState_mE75B60ADAE50734364CAFD49B1E4C2E3C5FB00CC,
	U3CU3Ec__DisplayClass23_0__ctor_m7FA995B8F647776E355609C3FFC0367289B3BBB5,
	U3CU3Ec__DisplayClass23_0_U3COnProductsRetrievedU3Eb__0_m7C28D18696848E58A8B9FD7055C677CA3434A6B5,
	U3CU3Ec__cctor_m0DB2B8BE23069CBE385EDDC3F52A9B6AE25AE6FB,
	U3CU3Ec__ctor_mD2D72C433C495C989EF96012553B588B00B6986B,
	U3CU3Ec_U3COnProductsRetrievedU3Eb__23_1_m86BC2757E431FC403AFE8F352E24D206148117D4,
	U3CU3Ec_U3CisValidPurchaseStateU3Eb__40_1_m48B14AC5D2D95A49540D33484188B8B62C5FE59D,
	U3CU3Ec__DisplayClass36_0__ctor_mDE3846C8B1B088ACFF698DF19198C1ED93269A88,
	U3CU3Ec__DisplayClass36_0_U3CMessageCallbackU3Eb__0_m5B3F37B7481A49993869901596407563E6E52F7B,
	U3CU3Ec__DisplayClass40_0__ctor_m4CA5E65FCE9E28F3BFFA1DE4D2F8810927AE97C3,
	U3CU3Ec__DisplayClass40_0_U3CisValidPurchaseStateU3Eb__0_mB104C5E311A21684B12773D06A106BF0BB63614A,
	FakeAppleConfiguation__ctor_m130C216E602F6A4C7219033A3492AFDC5E2F68C0,
	FakeAppleExtensions_RestoreTransactions_mA418ACE9FE6824BFC1D23781B323C8729429D05E,
	FakeAppleExtensions_RegisterPurchaseDeferredListener_m61038BDEB493FC8A93D7058D923D6E808EC22BAA,
	FakeAppleExtensions__ctor_mAC4EE655EBEB277FBCA176614EDE6C3B0CF0909C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JSONStore_get_storeCatalog_mF779A76E645E5B6EC497B0A93061AEC65D537CA1,
	JSONStore__ctor_mB3587A87996CA5DE31FCF8E58661F4C7F1DB7CA8,
	JSONStore_SetNativeStore_m4DCF0FB93FC5F686D045F764F664E8203E4731B5,
	JSONStore_UnityEngine_Purchasing_IStoreInternal_SetModule_m1E374BB3F819C16ED4DFC46F66B6A6A4B5886F12,
	JSONStore_Initialize_mAF894B8D15EF3F8228D12B0CA93B4E1B1C4A4635,
	JSONStore_RetrieveProducts_m5E1B9A13E6E257D1190F9F1AEB299469489A7BB5,
	JSONStore_ProcessManagedStoreResponse_mFB62B34AB96B5FDA8A3007F20D4ED31607F1DAF6,
	JSONStore_Purchase_m83FBD0A2F3E977BF86FD1C27E7B28957FAAEB94B,
	JSONStore_FinishTransaction_m4ED2F4415C2F94AE69638616B0C82FC25AEE6890,
	JSONStore_OnSetupFailed_mDD10DCDD5BD9225E4D8849D812D046CB4E1D577D,
	JSONStore_OnProductsRetrieved_m4AFC0C5B8A4706C3BEB3332DBFA4836E4F37B8B0,
	JSONStore_OnPurchaseSucceeded_mFF15EA23262E65C625EC04C98FAD07C443F6F258,
	JSONStore_OnPurchaseFailed_m37F03E8FE631267680BA50A48F4888007DB30D02,
	JSONStore_OnPurchaseFailed_m36F955F502C8CB4635298EC8DA5C885F22C809BD,
	JSONStore_GetLastPurchaseFailureDescription_mA8AAAEB71C462D48A6FBC791D9AD6F0E94458687,
	JSONStore_GetLastStoreSpecificPurchaseErrorCode_mA7FE9B2225DB70895E2B9B2EC33D8126C243AE4F,
	JSONStore_FormatUnifiedReceipt_mB625B4256FE85440544C80BC029E12D69B079A98,
	JSONStore_ParseStoreSpecificPurchaseErrorCode_m3F8BBE314D35195EE258162205FC5C68871729D0,
	NativeStoreProvider_GetAndroidStore_mF4FB1D3F79FAF49557EF8FC97FCD76D37913EEC9,
	NativeStoreProvider_GetAndroidStoreHelper_m26277644EF70586FA6A1EEF6846F78816164D970,
	NativeStoreProvider_GetStorekit_mAE5CDF5F97F24DC1E18FC0A5714009EE848842D2,
	NativeStoreProvider_GetTizenStore_mA8F9D15A05A96CC27EC68F66F68C0F3EA0DCC4AB,
	NativeStoreProvider_GetFacebookStore_m41C0112C7A6D67A9E6EDAB22B4A0535E74EBDC3F,
	NativeStoreProvider__ctor_mA5063656EFA925BF836CAB857DAC6A3E828225BC,
	CloudCatalogImpl_CreateInstance_m0D95A726E5D28C9808889A34F3804A4A17F11947,
	CloudCatalogImpl__ctor_m45318C200DF78F65839AEFBEF9B5F7FDD72CB8C5,
	CloudCatalogImpl_FetchProducts_m1CC03117C09692D39FBB222FE9B41BE1CA118B64,
	CloudCatalogImpl_FetchProducts_mBDDECAD1D2246A39A9DB0E32A908E4EF84BE1044,
	CloudCatalogImpl_ParseProductsFromJSON_m22D9A661C64271EEAFF757AD9093185A007AB826,
	CloudCatalogImpl_CamelCaseToSnakeCase_m2047DB6BCC713AA184646B208E304D403A718538,
	CloudCatalogImpl_TryPersistCatalog_m4C3807711A0EF333813D9BC35C3CD02F7A70E8B1,
	CloudCatalogImpl_TryLoadCachedCatalog_m1CD4B57DDE9F3AB21CD95D3BE814162AA222700B,
	U3CU3Ec__DisplayClass10_0__ctor_m182085D92ED214D3C86020BCCA8FB7A573F0F8E5,
	U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__0_m3FCB6517C013B1C95ECDFD21240F93033CA1520D,
	U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__1_m89A0DD8D729C59B940D1AEBA8D30C49A12C7D0D1,
	U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__2_m1648D82A87548CC6A555F710A3C52CA755103D36,
	U3CU3Ec__cctor_m5C0E27819D3F14554C2491B3BB489360EE64A7A4,
	U3CU3Ec__ctor_m2A4E278A66C5C16252E81FE284108227CE5D2FC7,
	U3CU3Ec_U3CCamelCaseToSnakeCaseU3Eb__12_0_m40123C7E19E965CA8C5A7E87FD333F6E73E488F1,
	U3CU3Ec_U3CCamelCaseToSnakeCaseU3Eb__12_1_m389B70C09C2B09FDD96B4BD7241F06AC2D0A6BF9,
	FakeManagedStoreConfig__ctor_mC7AAE88842F323010EAD156A9D4CD9768B2A4468,
	FakeManagedStoreExtensions__ctor_m0D5F6419FE250C2E07D0B1CC149773844F4D34FE,
	StoreCatalogImpl_CreateInstance_m1702943ABC3429D8505C318A737DC9830E6DB060,
	StoreCatalogImpl__ctor_m8ECDDE0CC4955189A8384AC41FBD879578FEE506,
	StoreCatalogImpl_FetchProducts_mC406A1BD08017A3759B5BD538D9C7D5BF0628DBE,
	StoreCatalogImpl_ParseProductsFromJSON_m974A5BE1DC918D2B5F6C8DD25CD9F833F8543017,
	StoreCatalogImpl_handleCachedCatalog_m3EA171069F00A9655E911A1D3C747A03716F9882,
	U3CU3Ec__DisplayClass10_0__ctor_mA2853E97D9D278E1DE8F108C70F8145A8B212B54,
	U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__0_mBAFBCBE2CC61018BDFA340CE07DD62D70F75AA5D,
	U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__1_mBEA547C95DA8608CC304CBCD068E5E42C4499AF0,
	AdsIPC_InitAdsIPC_mE775F78022B133AB42469E8C3007D62466F057BB,
	AdsIPC_VerifyMethodExists_m09ECB32981EC5984DB92ECF0B678513B2F5EBECA,
	AdsIPC_SendEvent_m30EF2A19EFCC69AADD610FCF97DA565802711C9B,
	AdsIPC__cctor_mB476FCD33FFFA007CD13592193FE370A86B6B08E,
	EventQueue__ctor_m29969CC9232D79EE58D1E068800C57315428AFE6,
	EventQueue_Instance_mECFFAEB18295FB1079D8030F7C25FE9F7D093FE9,
	EventQueue_SetAdsUrl_m710D1E618C60445A4AB0086D4062C97466BBE84A,
	EventQueue_SetIapUrl_m9385396A35B7456449D8E43480DAC070CE6CBF4C,
	EventQueue_SendEvent_mB6C974CC2706C0EB465978C282B63AF031CFAB0F,
	EventQueue_SendEvent_mD5F8335E5C8B4FA2F62D911BD5E5305C4668B9FD,
	U3CU3Ec__DisplayClass11_0__ctor_m87BE625D08BE3E9AFE3B2FD6AD5167B8BF637AF1,
	U3CU3Ec__DisplayClass11_0_U3CSendEventU3Eb__1_m7A6A77FB020D4AA45330F6A6940A0F619E99F81E,
	U3CU3Ec__DisplayClass11_0_U3CSendEventU3Eb__4_mD1AC489C0677006A4074D7F908EE70284173208B,
	U3CU3Ec__cctor_m9C02C7748C2731BBE3A37F270517DDA618EFF146,
	U3CU3Ec__ctor_mFE27FBBE0D9EB7A7A6F7C518D6B05D6A44E02507,
	U3CU3Ec_U3CSendEventU3Eb__11_0_m5B37451B74F4BB4B3D64D882C75EB6E7CEED9B0B,
	U3CU3Ec_U3CSendEventU3Eb__11_2_mBE98F5DC9EC3D7EB28B77D7F90B58570F3C881CD,
	U3CU3Ec_U3CSendEventU3Eb__11_3_m6D0B49D651A2EACAD971395074AA70FBD79E7CFE,
	PurchasingEvent__ctor_m93B1F85A0EB88478D1969238181095B1070602B4,
	PurchasingEvent_FlatJSON_mB5D3474D3085810D18683042D631BEABE4BE09E4,
	U3CU3Ec__cctor_m6042DD5463324144D90438F2075F5ABC24900A38,
	U3CU3Ec__ctor_m0A36A41CED277E3DFEB6D5A71E17CE65F33A387B,
	U3CU3Ec_U3CFlatJSONU3Eb__2_0_m36E49FAF91AB0C4ECF9D33A76AD0F90DDC58ADEA,
	U3CU3Ec_U3CFlatJSONU3Eb__2_1_m91191AED7D51A83A4CE5F49DF71C06353736F11A,
	AsyncWebUtil_Get_m88FE69C8616683C7E40805270D65A8AF22EFF21B,
	AsyncWebUtil_Post_m53D3A9D2790DBC50738F90160D5F67E5B2AA6AB2,
	AsyncWebUtil_Schedule_mF5FBFDBC05B181B254A08D70E44C6CA698B03F61,
	AsyncWebUtil_DoInvoke_m0633409BA91DAE7ABF7BCD416A6B264C2F96937A,
	AsyncWebUtil_Process_m9C2370009E698323E58B2A670D3CD27BF208D951,
	AsyncWebUtil__ctor_m5F49085BED472C5BA6030E5009A6ABD3836551B0,
	U3CDoInvokeU3Ed__3__ctor_mC6EE00BC8DDB8ABD56BAE6614910359B629C6DD7,
	U3CDoInvokeU3Ed__3_System_IDisposable_Dispose_mC44F5EC636838C33A8A789FCA13B3BE1362A23F9,
	U3CDoInvokeU3Ed__3_MoveNext_m35C974F5213EBB7839DA9C8B80ADC9C24D857039,
	U3CDoInvokeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DBFE3D27EE6E45C1BD65103792DE63A8ADD29C,
	U3CDoInvokeU3Ed__3_System_Collections_IEnumerator_Reset_m6CCA870DD9325A284667AC50CF778BD1CDD52538,
	U3CDoInvokeU3Ed__3_System_Collections_IEnumerator_get_Current_m2196441C04D53F61329468CAFF83939FF46AD4C1,
	U3CProcessU3Ed__4__ctor_mBB447167E306E623636B5F5C63C1EAB3D730B81B,
	U3CProcessU3Ed__4_System_IDisposable_Dispose_m254F5AFF9ACB1AE0165D14A801C45BBF7C93F859,
	U3CProcessU3Ed__4_MoveNext_mE4A8EBA215143440526AF4EF9EBB04028E6ED7FD,
	U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5ECBA869460B1F98EA8B136038BB7F6BBA7CDCBE,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mBB97238A013A9B4DF57D0F64DD13147664D7C6E5,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m257BD65D282C42054AD97B58E2011843F5ABCE91,
	NULL,
	NULL,
	NULL,
	QueryHelper_ToQueryString_mB7D887C850A895D3FE019D92044EE9BEF3887736,
	Price_OnBeforeSerialize_m42915340CF2F0009AA700CCADC50A1B4D367E30A,
	Price_OnAfterDeserialize_m68B6E9BEE8BB36C2E936F25D2BE03BE15BA405F1,
	Price__ctor_m9E849B424E1B142BCC5F6DA2AC17E259438125B7,
	LocalizedProductDescription_get_Title_mB4128E7F3F80159214D6204AC155A876E9F7CB3C,
	LocalizedProductDescription_get_Description_m0CF6B4CE1668EFAA25D086B0A2AB58A868979FAC,
	LocalizedProductDescription_DecodeNonLatinCharacters_m260BA15BA17C15BB261706D7B579AD047C391768,
	LocalizedProductDescription__ctor_m4EC23D3335DE3FB09F98F8A8C1C05C29BF91F213,
	U3CU3Ec__cctor_m036D291ED8CC94C7CFD6800993DE8AF5880B2647,
	U3CU3Ec__ctor_m35F829E30B1A322BBE52F44B9EE7549C03AFD662,
	U3CU3Ec_U3CDecodeNonLatinCharactersU3Eb__11_0_m0F8E885D6CF01D6833DBC50FE9B1784095D5CE04,
	ProductCatalogPayout_get_typeString_m4FCC4534F5A45CD6765FD5D5B7440260F346721B,
	ProductCatalogPayout_get_subtype_m5BD4FDC8C2A65CCBCEEEB8F5273518A145D8E4A4,
	ProductCatalogPayout_get_quantity_m5544F7E28BE1BF9D7DF22B7582EB010490E69EC2,
	ProductCatalogPayout_get_data_m84A49E1CAC1686681C972F1E440F4C479A92CD76,
	ProductCatalogPayout__ctor_m74268D587D637CCB8E234FD75A1D163A5775C3E5,
	ProductCatalogItem_get_Payouts_m56972749038D381580FE79C9FB0779BAAFB2EEA0,
	ProductCatalogItem_get_allStoreIDs_mE9749219486896327EDB96491DB7B03677B14608,
	ProductCatalogItem__ctor_mA5E1B972EDF0B24C88F4A49907388C44EB6844C7,
	ProductCatalog_get_allProducts_m57932235309AD3580368E255E67863DDBBF22FFF,
	ProductCatalog_get_allValidProducts_mA3253A3B7CE0CADF31456205C4E95FCCE77C381A,
	ProductCatalog_Initialize_mE3A76CD3059703EEC40B3FA2BFA3D842E67EE7F7,
	ProductCatalog_Initialize_mD9EF9832EA57668A5AFD194B036CA47289E01F13,
	ProductCatalog_IsEmpty_m062291717F13D424F9EC751CF3D72DDA839B07FD,
	ProductCatalog_Deserialize_m9DD672A27C533E7567D4C72E299F17D3451ABA1B,
	ProductCatalog_FromTextAsset_mE4FBA8CCAA4330D23F917B862E6C2BDF62FAF58F,
	ProductCatalog_LoadDefaultCatalog_m283C9E96ED6641D4603C785E19F1C8619B14D726,
	ProductCatalog__ctor_mD6A3BAA2A699611769D953290FF3FDA3A8079BFB,
	U3CU3Ec__cctor_m2E418947ABC898FCD169F2619F1786B9756FCC83,
	U3CU3Ec__ctor_mF914ED3FBC126B7ABA7FE68BCBF985ECCEC49D6B,
	U3CU3Ec_U3Cget_allValidProductsU3Eb__8_0_m2851D0027CB45CAF3AC9155711E78CC2B6006E69,
	NULL,
	ProductCatalogImpl_LoadDefaultCatalog_m45C5B4F47CD8E70084F561B9FF7077303F89CCA5,
	ProductCatalogImpl__ctor_m1FCB2E5D3E39B68F84208FAA1245B9D9B9905C68,
	ProfileData_get_AppId_mB76CD78BDFBF737113683661CC695AD70F8C8A97,
	ProfileData_set_AppId_m9E1F31B968D62B7BF0330DF532BF70890BC3BF78,
	ProfileData_get_UserId_m896300FD502E7796269508495A31F964664F2C30,
	ProfileData_set_UserId_m7E142ED38198963F3803E37B68D13637B61E3B8D,
	ProfileData_get_SessionId_mF980BAE334AFF828EEFE31E1793586454AF4BE89,
	ProfileData_set_SessionId_mBADBBDE13A7FC91A558FB5130E56A97BA0A36E99,
	ProfileData_get_Platform_mACF5FECF73EF67AD2DF604AD3E46AA65B9292A16,
	ProfileData_set_Platform_m3070287D2C0F4300B4A27E4F622E372258A88647,
	ProfileData_get_PlatformId_m478E28CA7F2EFDA661A4626FFF774543F0F30907,
	ProfileData_set_PlatformId_m00CC32FADDD7C1F5FD42B968387ECDED47C3BA2C,
	ProfileData_get_SdkVer_m0DEA69034CE40AE0B7EB9ACD4CC93065A43AC7E0,
	ProfileData_set_SdkVer_m52C0A5A9495BBC729DDD116B58E11E9C2E1B2C5E,
	ProfileData_get_OsVer_m46F4FB97BC78156C2CFCFFDA883DE977FA2E293F,
	ProfileData_set_OsVer_m51B8FA92D86641EAA4E982719B59B29764EDCA05,
	ProfileData_get_ScreenWidth_m44AE08924FB9BBF78337B5EEE85BB679E0D24375,
	ProfileData_set_ScreenWidth_m80A83E07330532E9EE97EA0DA979EA7103B835A0,
	ProfileData_get_ScreenHeight_m9DC916E6A43D17DC7F8C3A421425F238C53728CF,
	ProfileData_set_ScreenHeight_mB1C6871EEF5144F251CD295C34E4022E49A005FC,
	ProfileData_get_ScreenDpi_m5F850CC804676B2A0F2C76750DF3E6B4107E15AC,
	ProfileData_set_ScreenDpi_m65645247F950C4BCBC9189C613DD79A1A7440E2D,
	ProfileData_get_ScreenOrientation_m95A2A665784A9DBC8AEB2806E360CF75244D4E16,
	ProfileData_set_ScreenOrientation_mA4D5370DE961B70B8BB634DEB4D8D35247313478,
	ProfileData_get_DeviceId_mE7FDE557A9CEAFDE2057029C77224AB5E541BA69,
	ProfileData_set_DeviceId_mF9B57A8FD4459418422C21037BD8FDCB46372630,
	ProfileData_get_BuildGUID_mAFFC9CEDCA65D08AE4800E072349B59D16D9DB58,
	ProfileData_get_IapVer_mAA63BDA3B84A10EDE437EADB0507286359156B79,
	ProfileData_set_IapVer_m5B00AC263487A40A415FA7888AEB7D47DDE690A7,
	ProfileData_get_AdsGamerToken_m2168538589DC4AA546573D94DF1C3D910CBB556E,
	ProfileData_set_AdsGamerToken_m165AE2FE059C9B445F0A9E0BF1E91CF5F9BBFD3C,
	ProfileData_get_TrackingOptOut_m4FA81FB86ADD6C265F7F79DC2DA41D11EB8CB76C,
	ProfileData_set_TrackingOptOut_mEB3F15A23581A9808EDF20A76B7C626D4AE98116,
	ProfileData_get_AdsABGroup_m3D5B156992614B55D7FC48EA7F81DF9BB9DCED73,
	ProfileData_set_AdsABGroup_m1BD7F8B10950E8CD0C1D7EB316BB0499899EACCC,
	ProfileData_get_AdsGameId_m6E21F44B4FF32249F3E5F3A177AC55E11D0ED401,
	ProfileData_set_AdsGameId_m1C540002AD43B3A122676057035483BEAB5FEFD6,
	ProfileData_get_StoreABGroup_m77EEC04AED22E35AEDA45D35A9112F7BB69C0B4D,
	ProfileData_set_StoreABGroup_mC5E83597BC466FED09D5AAB6AF153F57C1C8DB4F,
	ProfileData_get_CatalogId_m4A36730621A8B53F97778267C9F88D33555054A5,
	ProfileData_set_CatalogId_m84CC9A84A13259A72EC045DBC5E521614F02AB43,
	ProfileData_get_MonetizationId_m0670F1B55BF56A56AAB335D0461C39C1F1D80B4F,
	ProfileData_get_StoreName_m718DFA5439E8716BE57D464888307DBC5C6D12F9,
	ProfileData_set_StoreName_m5F72223FD8488E6629A352909DB31064E08C263A,
	ProfileData_get_GameVersion_m6414F1906E2C6516394C8E86E50E5B0F89397BB3,
	ProfileData_set_GameVersion_mBBBCC2D554A6B10CC05A6C33D760B9F57D14012A,
	ProfileData_get_StoreTestEnabled_mE5F73CB7E20C4ACF802A913AE7E350C9C06F2118,
	ProfileData__ctor_mE427B2BD4E0115117186BAB48B5792C8F76FC79A,
	ProfileData_GetProfileDict_mC6004F7556B698AB2133AB43EB4F42D24D2B416C,
	ProfileData_GetProfileIds_mF714A652371683D86F8DD1AEF06E185EB4BCDE17,
	ProfileData_Instance_mC66C8230B0E0D8B00BDF9E1F66928F36D1D6DA27,
	ProfileData_SetGamerToken_m6705460DBB32FB38E26ADF0EEA395B34E82391E5,
	ProfileData_SetTrackingOptOut_mE82201E8A6968F98726E15CAD55330080BF3DE74,
	ProfileData_SetGameId_mE84EAF3B00CC459F6A75EEC0250FD17D2B149696,
	ProfileData_SetABGroup_mA0E17E2D40EA612DA0E85C0C135C56E81166F857,
	ProfileData_SetStoreABGroup_m447899BAA43196544EAF5F1FCED2FF46DD1A4A0A,
	ProfileData_SetCatalogId_m37167B7F3E70C785149001AE6B4701E1B72C5692,
	ProfileData_SetStoreName_mACCFF9D0EAD03EB68EFFE7DC35E0471C592732A7,
	Promo_IsReady_m715B4672F846E11034666D7CFA513358749787F3,
	Promo_Version_m7DF205A5C8591C70D9600E1FF838FBFBFF2FE548,
	Promo__ctor_m4C871C25F260FAFE3DA5D5EBB4ECD8BCC50DF5C6,
	Promo_InitPromo_mBFAF9FEB43F71EBE93906B7D2DF37F577904706A,
	Promo_InitPromo_mE5648F47B8FF4E17E5CFAAD0FA4BFF69B1D82BF3,
	Promo_UpdatePromoProductList_m7D66E97B992002A774B83573E8CEDD29E2AF1796,
	Promo_ProvideProductsToAds_m9806E5D41CCC191656F2FB3609EC4CB8DBEE76D0,
	Promo_ProvideProductsToAds_m23C7D7A04E8013323C23546FC46C57CAC0C29610,
	Promo_QueryPromoProducts_m06BCB6E34A7910244ADE0501C6F3B8B04FA3EA6C,
	Promo_InitiatePromoPurchase_m5263CCCEC7F077FD817468A96A35EF62CC6C8879,
	Promo_InitiatePurchasingCommand_mD1A99C20870029E4AD649C84CC99693CC92288B8,
	Promo_ExecPromoPurchase_mA76A42831AB5F737BD83DD9ADFDCEE1A6A3E7EE5,
	Promo__cctor_mF070773D2A494A2A7A0EC0AD6C7ECE284B50C2FD,
	StandardPurchasingModule_get_util_mF27932BFEB542A0EB524E09124E6C0BBCB9042F9,
	StandardPurchasingModule_set_util_m012516B0F2A913B01F758F94D3A5DDA832BA9BA7,
	StandardPurchasingModule_get_logger_m22702082BD1ADF7D1AE7EE7B5A88DDA2234E3BB9,
	StandardPurchasingModule_set_logger_m6CBA70094C3AC349F66C503900B1EE900E98976C,
	StandardPurchasingModule_get_webUtil_m75D15473078B64AF7AC29F5A7701A6D37CA3295F,
	StandardPurchasingModule_set_webUtil_m6A71DB533032003B4681625C8DB312F1BB6BC2AF,
	StandardPurchasingModule_get_storeInstance_mDE877C008D6891F3D6CC5AD23FCE7EAA77B2EB8F,
	StandardPurchasingModule_set_storeInstance_m55E1773A9CBE6536CFE8BA120EBBB59DA456C824,
	StandardPurchasingModule__ctor_m7F8DC18DDC0A90E2156438955FB7288621D9BD07,
	StandardPurchasingModule_get_appStore_m3FD6D2E86D0865A130B60922211CF1DA50025859,
	StandardPurchasingModule_get_useFakeStoreUIMode_mB5C3DC0E195BB5C4836B19D7DD4B0B0FF48AF037,
	StandardPurchasingModule_set_useFakeStoreUIMode_m1DF958E879FF2A8B0EF00C2DEDC2A9DC15FD0F81,
	StandardPurchasingModule_get_useFakeStoreAlways_mB8536F2943BBD730A3BBEB63CE52E12DB8397B4F,
	StandardPurchasingModule_set_useFakeStoreAlways_mFF827931135A19C8E056088E58B43821AFB843D6,
	StandardPurchasingModule_Instance_m53AC662D78E6E4ABDB2CAB28B57C82B5C40DDC0F,
	StandardPurchasingModule_Instance_m6FDCB6F3DF479F8EAE642456997E2047E85CCC6E,
	StandardPurchasingModule_Configure_mEB46A26919A70D7433874B14DA69DF85B825DA8B,
	StandardPurchasingModule_InstantiateStore_m5424D8021F4017E884557536318A76065BDD8E04,
	StandardPurchasingModule_InstantiateAndroid_mB638FAB309895A6F0C7E74E0BAF5520EF0EB119F,
	StandardPurchasingModule_InstantiateUDP_mC7717983EF5F66613BF820BC2EE495B58C0890D6,
	StandardPurchasingModule_InstantiateAndroidHelper_mDAD7B93C1F35490EBFC824CCB5F8A351986DD77B,
	StandardPurchasingModule_GetAndroidNativeStore_m7306315A603E25ACD7384B229B2984B7441BEDD7,
	StandardPurchasingModule_InstantiateCloudMoolah_m68E1079CC40842F6393BE240D607D8E991BC6856,
	StandardPurchasingModule_InstantiateApple_m766197A2288043C07C458235BE74FDE3F96F6409,
	StandardPurchasingModule_UseMockWindowsStore_m57ED53E3389D06BC62DDDB375ECEC938F1E4EEEA,
	StandardPurchasingModule_instantiateWindowsStore_m2C07A8AA5FBD4051A9C82FFAB0BA6B31E02DC4F2,
	StandardPurchasingModule_InstantiateTizen_m89A803CF3887574012951884A4A31DEBC6294496,
	StandardPurchasingModule_InstantiateFacebook_m7C2BB6D5735043477F5B069A6DEFDB7C71414340,
	StandardPurchasingModule_InstantiateFakeStore_m1D76FA3EE731581A194301D950A17DDCFB9F5ADA,
	StandardPurchasingModule__cctor_mD7AD3357D5AC77A2D8300F95841211B4AC2EBFCF,
	StandardPurchasingModule_U3CConfigureU3Eb__45_0_m3990E52526D3A5A79D07BABB43F3848F906E278A,
	StoreInstance_get_storeName_mFB3E9C97578D42C738F01CE397B7DD9634363515,
	StoreInstance_get_instance_mD04ECA7D1E048BA532BFA0002B03F1BC8518C7E3,
	StoreInstance__ctor_m511F9A0FF03F0DED6E2834F35B6B88EA9136A246,
	MicrosoftConfiguration__ctor_m8200843A7687638DEFB25B5E6098E8DCB9F7739C,
	MicrosoftConfiguration_set_useMockBillingSystem_mC12BA7BFAA4835FF5005A99711E7C534831D7140,
	StoreConfiguration_get_androidStore_m71F9858773B7A33D42604867C868C8C929216FA9,
	StoreConfiguration_set_androidStore_m754AE5DD346AF141A83B65891244851427216678,
	StoreConfiguration__ctor_m73D2B60EBB9F17EF6E14B4D0A05B7894405FABE7,
	StoreConfiguration_Deserialize_mAADD487B388060F192F2098FC7CAACBF378CD09E,
	SubscriptionInfo__ctor_m1DF843DEC98A2D334FA6F99E67BE721917F63E0D,
	SubscriptionInfo_isExpired_m936BED463FFC138136EEB4BE61A5216FBF7DC2E2,
	ReceiptParserException__ctor_m845F697858F06D1E1A5D14DC27F3FAE24B9A1061,
	InvalidProductTypeException__ctor_m5CF81559999FF23B7E699273DDB48053E2A5EC31,
	FakeTransactionHistoryExtensions_GetLastPurchaseFailureDescription_m1728DD1121DB7BD2A513DEB6CD57D21CD0E966CC,
	FakeTransactionHistoryExtensions_GetLastStoreSpecificPurchaseErrorCode_m5AC0C4FCF6B01542E9F178811DEE4A414CC9C061,
	FakeTransactionHistoryExtensions__ctor_m170C5ECFBC6118B5E9110BEC7858664A7EAAE9E8,
	NULL,
	NULL,
	FakeMicrosoftExtensions_RestoreTransactions_m92178A383AEA57F5E2290D2E8A09D2A444B5EFF2,
	FakeMicrosoftExtensions__ctor_m55BF33C20BA1CC30C5F93986D4597CEA0CDC70D0,
	NULL,
	NULL,
	WinRTStore__ctor_mC247036BEB97E832E54CD04F46C1B17307CD2A41,
	WinRTStore_SetWindowsIAP_m64DA918FA1ED543DF40CFF663391359288F81ADA,
	WinRTStore_Initialize_mD638F8649C4ADDB8912B0884252022DB299A28F2,
	WinRTStore_RetrieveProducts_m1843A0C71EA5627C83D46EEB44BD1A7967E85ADA,
	WinRTStore_FinishTransaction_m1234FFF6F33D68313D1E19DA6F8DF14D827F3889,
	WinRTStore_init_mC53C127474CC761323E1E0AB7233601B536D011C,
	WinRTStore_Purchase_m425472074EEDDF0EA46755B3C8AE4B5C18DD4628,
	WinRTStore_restoreTransactions_mD41EB4B9E2D1A00C2A8F508730C4B6208FB3F101,
	WinRTStore_RestoreTransactions_m20FBD308A3340250AD5976890930BFBD555C6FA7,
	U3CU3Ec__cctor_mB152352D2E612A6A60E5751C056B5352AC35DEA0,
	U3CU3Ec__ctor_mF94F1B03FADF27CF46CBD161CDA511B917DBF7DD,
	U3CU3Ec_U3CRetrieveProductsU3Eb__8_0_m8EA8F9CDBA6CD46179AE1F6605E7DE1B1FCAADB8,
	U3CU3Ec_U3CRetrieveProductsU3Eb__8_1_m6A7FA4D02DADA12400E65BF7981A0A39AE36B417,
	NULL,
	TizenStoreImpl__ctor_m1C4A2359A096EE778531F8F43F2C53EA134BE84D,
	TizenStoreImpl_SetNativeStore_m566D9D3094C6BD91482FDBF79DB490E2C15981FC,
	TizenStoreImpl_SetGroupId_mEC0BCF8473583ABC808E145B0B1E4253EE401DEB,
	TizenStoreImpl_MessageCallback_mA4B7D8E14E7399E5405E1DF5C1C9D7F72E1B4E73,
	TizenStoreImpl_ProcessMessage_mD12CA21A450AF70540CCAA34333FCAACB979F77A,
	FakeTizenStoreConfiguration_SetGroupId_m05F71176065DFDB3DC90B9B6FABF9489E6AD3D21,
	FakeTizenStoreConfiguration__ctor_m3B58A10ABB10C0253D1EB1A7BB303AD6BFB215C7,
	FacebookStoreImpl__ctor_m7DC7E94516388C068DB0C6002ABDC33F0FD41301,
	FacebookStoreImpl_SetNativeStore_m603C313638B5668500E4CFFF9FAE8B6E317E002E,
	FacebookStoreImpl_MessageCallback_mBC2F88BC0E8116317E1EC9EE7B5CDFE237B970D9,
	FacebookStoreImpl_ProcessMessage_mC772DB04397B9570B2374420BD457CD751132922,
	U3CU3Ec__DisplayClass6_0__ctor_m09B266059CBDADC4F7E6894F1C7A686D7FE41025,
	U3CU3Ec__DisplayClass6_0_U3CMessageCallbackU3Eb__0_mB1018682C4F417AD534952462415104D7D175374,
	FakeStore_get_unavailableProductId_m065B3DEAA12D3AC7983C4E2A6B9E064C91E776EB,
	FakeStore_Initialize_m64FD3624836CEE485E75ADF2038AFECB3DC41E27,
	FakeStore_RetrieveProducts_mD14F708950B7202ABA56BD9F8DE3047209CC2578,
	FakeStore_StoreRetrieveProducts_m3F7582AA3F67F53992239B47DDD8FEE0260B53AA,
	FakeStore_Purchase_mAEBCD146904CF51D8E98F2E44280C75B82EEFDCE,
	FakeStore_FakePurchase_m72437F867A150BFF64349CD71396F5D29421F1BA,
	FakeStore_FinishTransaction_mCE869F1B78980B247994A426DF65FCC146AB71B7,
	FakeStore_FinishTransaction_m8F249C9F72A842144B3D4F347020D8B5E80F1EEE,
	NULL,
	FakeStore__ctor_mED168BB8C745957275FD038C75ACFDD3F682FA80,
	FakeStore_U3CU3En__0_m29968C99CF98256E99D5A4E254EE1A0D94C2EA20,
	U3CU3Ec__DisplayClass13_0__ctor_m8383096E77E4D1DC21E11DB5B314679397041CC8,
	U3CU3Ec__DisplayClass13_0_U3CStoreRetrieveProductsU3Eb__0_m33C79AB8A557AD73E4AF040A632C1FC0EC9DB8CF,
	U3CU3Ec__DisplayClass15_0__ctor_m3FEC6AECA9C1931D60E6FFF67AE61886E8A74BD1,
	U3CU3Ec__DisplayClass15_0_U3CFakePurchaseU3Eb__0_mD1A5FD2BA2EDEEDB8B7892527CB5CBDDF1B09BD2,
	UIFakeStore__ctor_mF5E7D50CE0AA83CEBECD8E3E8ADBAA2BC792AB64,
	NULL,
	UIFakeStore_StartUI_mA8DC00C8BB6324597920174E3ADEBBCC9C476E74,
	UIFakeStore_InstantiateDialog_m67BB65DF82BAC44B4142F469C1430DDC220A1AE8,
	UIFakeStore_CreatePurchaseQuestion_m96557A06A0271F68A4089F2F06FA97ADF8FD600B,
	UIFakeStore_CreateRetrieveProductsQuestion_m7050FA5BB6E4EF559C65A6EC4654A503934F178E,
	UIFakeStore_GetOkayButton_mF7E658EECAC072F62C0CAAF2CF44CA5796AA55D0,
	UIFakeStore_GetCancelButton_m177E9FB2644F764DD6CF969B8A2931E90F61D030,
	UIFakeStore_GetCancelButtonGameObject_m2932538927D6A0843BDCA8719DA746F28B363505,
	UIFakeStore_GetOkayButtonText_m6AAF15348A86250FA96E8C1E642F7B426773D80B,
	UIFakeStore_GetCancelButtonText_mFDFEA4FD07BFE76F0F5BFEBED6200C195EE75E01,
	UIFakeStore_GetDropdown_mE653F99C0FB80B9AA6D4C33D2A9BF13C6A16EA26,
	UIFakeStore_GetDropdownContainerGameObject_mB6E3B3B973C10B99297DB1D817DADB110A30C18C,
	UIFakeStore_OkayButtonClicked_m811E895EC49767C2223DDB136344EF46B5C26166,
	UIFakeStore_CancelButtonClicked_mE3EBAD2E786785E57D432ACC1469674D77A0B875,
	UIFakeStore_DropdownValueChanged_m809639FBCE58E84B30A1E52ACAB4E8728DBA12E9,
	UIFakeStore_CloseDialog_mBC82E019CE7121C8E2DCBA3F770A683070682F7B,
	UIFakeStore_IsShowingDialog_m165A16762CF04D260A361FAEA855EE29598D2F40,
	UIFakeStore_U3CInstantiateDialogU3Eb__16_0_mFA74D5B8702FE7D94E27E750EBA7F8DCE5F7903B,
	UIFakeStore_U3CInstantiateDialogU3Eb__16_1_m9A1558F35327964F5769BCB9EFB5D5AE4AE1ECD8,
	UIFakeStore_U3CInstantiateDialogU3Eb__16_2_m10A27A3286CFBA502C73ABDEA075E9B565F3809A,
	UIFakeStore_U3CInstantiateDialogU3Eb__16_3_mB67CA9956F8F607385AB827F3FB152B4DD9E2F21,
	DialogRequest__ctor_m15E01BCCF9BA8FB4051DFB1191E79723E15FE636,
	LifecycleNotifier_OnDestroy_m80986F402D56C67A7EBE7FD54896FBD65EB51BA0,
	LifecycleNotifier__ctor_mF6579954DD064CCF856C0315605609FAA94A9758,
	NULL,
	NULL,
	U3CU3Ec__cctor_m9E8EBEA86DB17FEEF9A95F75AD4A3F03B20B1495,
	U3CU3Ec__ctor_mEE44D046E13EE47F95A78E5D681D24B29B10A2B6,
	U3CU3Ec_U3CCreateRetrieveProductsQuestionU3Eb__18_0_mE71AC3B010F0AA573786C0908D992D6A2DBE38FD,
	FileReference_CreateInstance_m2480AF4AFF6D5084D5598CE492D5177E6C2B0240,
	FileReference__ctor_m70CE741633F86E0AEA6071F8C263AAC94540754E,
	FileReference_Save_m554C2C82EFA4B92A04E2C335117DDF7EA10218AF,
	FileReference_Load_m1BEC26A7A712092A3F4986489197959ED9C26A36,
	ProductDefinitionExtensions_DecodeJSON_mD8CB385394E30826BF3C9207DBF1E0C783B6B3E3,
	NULL,
	UnityUtil_get_currentTime_mE0517B0267DD8A42F08DDB6E8ADBFA08DC71FEAA,
	UnityUtil_get_persistentDataPath_m2A8FDA4B9D43B765E3FC03232891F98FC4C60FF7,
	UnityUtil_get_deviceUniqueIdentifier_mCCF1538EA703E76342F751F31D2DFAE36ECB4409,
	UnityUtil_get_unityVersion_mE9E277B817CA49F007E3BAD98D3EA2CB3B290C6E,
	UnityUtil_get_cloudProjectId_mC8889E4EFCF7D739544E19AAE597EA5935CB6117,
	UnityUtil_get_userId_m88748CDB01D6D306C066373E5832D6EF246EFC49,
	UnityUtil_get_gameVersion_m31936F5B76839D6DF1970AAACF853E5D45AFD985,
	UnityUtil_get_sessionId_m7E6B9F793C793D5105B892F33F99F65BBC4442F0,
	UnityUtil_get_platform_mF606F06FE303EDBD7124C2271339A1D58D0444E0,
	UnityUtil_get_isEditor_m165D3077CFD7CA1084F1A97390D76B439E4A8687,
	UnityUtil_get_deviceModel_mDCA6013837CA1F838BED8CD0E5DE4457D979478F,
	UnityUtil_get_deviceName_m45A4B711EB162F7A3E2234C975C7FBEB79207335,
	UnityUtil_get_deviceType_m0D78117B41822D285CC9ADFD14D01AF9F955B08F,
	UnityUtil_get_operatingSystem_m842B055FD14B71F2971DA92BD055F7EEBDB24027,
	UnityUtil_get_screenWidth_m2028268F39D36736FC091408175C3400B4757018,
	UnityUtil_get_screenHeight_m9DC59F962C9F40C7F4F7A8008C509A42747D5717,
	UnityUtil_get_screenDpi_m65E20C5EE7FAC2904891673EC0DC715202B764D4,
	UnityUtil_get_screenOrientation_m25C925CAC035D580C1C1C16253B472A95857CB66,
	UnityUtil_Uniject_IUtil_InitiateCoroutine_mC6A9BC4EFA5ED3AFB70E5596F676D7BF81C3975D,
	UnityUtil_Uniject_IUtil_InitiateCoroutine_m02A75FBC47ED72DE099976F8123549A3905BBD5B,
	UnityUtil_RunOnMainThread_mD87E0072DA7A5037ED7E54064907F1B5E0E11652,
	UnityUtil_GetWaitForSeconds_mCCA700B90D4CA34E60D288FFDBA267C866BBE2DE,
	UnityUtil_Start_mE496390074ED7C96E33A2131F94F6FB4A076FC86,
	NULL,
	NULL,
	UnityUtil_PcPlatform_mAE39342DC5B9517F34D7B78C1A1E71F5218E505B,
	UnityUtil_DebugLog_m988452F9D30616D70E1AD480B73A1A3BEE39639A,
	UnityUtil_DelayedCoroutine_mB9731082B84D84207E7F3FF2D426E53FAADFA2E4,
	UnityUtil_Update_m928A08FD29363BF1776DB411C2B6DDE169AA25B2,
	UnityUtil_AddPauseListener_mFFB616F77C07F4FD3E9E52FD2E72DF012564BD5F,
	UnityUtil_OnApplicationPause_m581AA60D0E4B529E0E5BBC3A4AF10EDB8A166790,
	UnityUtil_IsClassOrSubclass_m21082FFD714984294526DD90C34461C3DB0B5128,
	UnityUtil__ctor_m12915491833B8AC50A13E956BE797256B1010E07,
	UnityUtil__cctor_mAFE259562F3D67630D164BDE8CDE10C73D27D88E,
	U3CDelayedCoroutineU3Ed__49__ctor_m0CF794D5E0CF9888565D74DBF0CB7A781E3FFC33,
	U3CDelayedCoroutineU3Ed__49_System_IDisposable_Dispose_m4F25109E3EE4412BB40F7B4FB0AD8A49B8FC9C4B,
	U3CDelayedCoroutineU3Ed__49_MoveNext_mBCFBA9637AA6DBCA8DCD542272C160B7758173FC,
	U3CDelayedCoroutineU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7E96A164117AB133B0A66F78DE7238B586601D28,
	U3CDelayedCoroutineU3Ed__49_System_Collections_IEnumerator_Reset_m4BBD4503F8C17B6E6BB32B568CD5AF9E579718C2,
	U3CDelayedCoroutineU3Ed__49_System_Collections_IEnumerator_get_Current_mBCA487AE2461E12FC954964F399C5A13AE160C54,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m53F9152F2CD10CB765A06BD1CCAE7C7B426C008A,
};
static const int32_t s_InvokerIndices[612] = 
{
	10,
	14,
	14,
	14,
	14,
	14,
	14,
	183,
	14,
	10,
	10,
	742,
	14,
	28,
	144,
	26,
	26,
	90,
	14,
	26,
	26,
	27,
	27,
	26,
	26,
	208,
	26,
	26,
	27,
	26,
	26,
	208,
	26,
	1,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	27,
	26,
	26,
	208,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	26,
	32,
	23,
	26,
	23,
	26,
	26,
	32,
	26,
	26,
	26,
	37,
	88,
	28,
	215,
	26,
	32,
	26,
	26,
	27,
	14,
	459,
	1343,
	138,
	208,
	109,
	27,
	28,
	14,
	26,
	14,
	26,
	14,
	26,
	32,
	10,
	26,
	28,
	208,
	215,
	23,
	3,
	88,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	208,
	109,
	208,
	27,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	1305,
	4,
	26,
	23,
	27,
	27,
	23,
	26,
	31,
	23,
	26,
	26,
	27,
	14,
	26,
	27,
	26,
	32,
	26,
	23,
	31,
	32,
	26,
	26,
	31,
	23,
	26,
	32,
	26,
	31,
	23,
	23,
	23,
	26,
	208,
	27,
	27,
	4,
	26,
	208,
	27,
	27,
	28,
	26,
	26,
	26,
	27,
	26,
	27,
	26,
	26,
	26,
	27,
	27,
	0,
	23,
	26,
	26,
	26,
	27,
	27,
	149,
	23,
	23,
	88,
	88,
	23,
	88,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	26,
	26,
	23,
	1304,
	459,
	208,
	28,
	90,
	23,
	9,
	3,
	23,
	41,
	41,
	23,
	23,
	23,
	9,
	23,
	26,
	26,
	23,
	26,
	26,
	154,
	28,
	107,
	14,
	26,
	14,
	23,
	26,
	26,
	26,
	26,
	26,
	27,
	27,
	26,
	26,
	208,
	26,
	27,
	14,
	10,
	107,
	115,
	154,
	154,
	28,
	107,
	14,
	23,
	0,
	840,
	26,
	144,
	1,
	0,
	26,
	14,
	23,
	26,
	26,
	23,
	3,
	23,
	531,
	107,
	23,
	23,
	526,
	840,
	26,
	2,
	26,
	23,
	26,
	26,
	121,
	49,
	121,
	3,
	27,
	1,
	26,
	26,
	1985,
	9,
	23,
	26,
	23,
	3,
	23,
	26,
	26,
	26,
	26,
	28,
	3,
	23,
	1986,
	1986,
	122,
	1892,
	144,
	58,
	156,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	122,
	1892,
	144,
	0,
	23,
	23,
	23,
	14,
	14,
	0,
	23,
	3,
	23,
	28,
	14,
	14,
	475,
	14,
	23,
	14,
	14,
	23,
	14,
	14,
	3,
	165,
	89,
	0,
	0,
	4,
	23,
	3,
	23,
	9,
	14,
	14,
	23,
	14,
	26,
	14,
	26,
	183,
	210,
	14,
	26,
	10,
	32,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	742,
	352,
	14,
	26,
	14,
	26,
	14,
	14,
	26,
	14,
	26,
	1086,
	1087,
	793,
	1987,
	14,
	26,
	793,
	1987,
	14,
	26,
	14,
	14,
	26,
	14,
	26,
	1086,
	26,
	14,
	14,
	0,
	26,
	1087,
	26,
	1987,
	1987,
	26,
	26,
	49,
	4,
	23,
	1988,
	1989,
	4,
	149,
	165,
	4,
	121,
	121,
	121,
	3,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	1990,
	10,
	10,
	32,
	89,
	31,
	4,
	43,
	23,
	14,
	14,
	14,
	28,
	28,
	14,
	14,
	31,
	14,
	14,
	14,
	14,
	3,
	26,
	14,
	14,
	27,
	26,
	31,
	10,
	32,
	32,
	0,
	27,
	10,
	23,
	23,
	14,
	10,
	23,
	14,
	10,
	23,
	23,
	31,
	23,
	208,
	26,
	26,
	26,
	27,
	32,
	27,
	31,
	23,
	3,
	23,
	9,
	28,
	26,
	26,
	26,
	26,
	1304,
	459,
	26,
	23,
	26,
	26,
	1304,
	459,
	23,
	23,
	14,
	26,
	26,
	26,
	27,
	27,
	27,
	27,
	-1,
	23,
	208,
	23,
	841,
	23,
	841,
	23,
	-1,
	1991,
	23,
	28,
	28,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	32,
	23,
	89,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	-1,
	-1,
	3,
	23,
	28,
	2,
	27,
	26,
	14,
	1,
	-1,
	112,
	14,
	14,
	14,
	14,
	14,
	14,
	183,
	10,
	89,
	14,
	14,
	10,
	14,
	10,
	10,
	742,
	14,
	28,
	144,
	26,
	34,
	23,
	-1,
	-1,
	49,
	149,
	58,
	23,
	26,
	31,
	90,
	23,
	3,
	32,
	23,
	89,
	14,
	23,
	14,
	94,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[3] = 
{
	{ 0x060000D9, 7,  (void**)&AppleStoreImpl_MessageCallback_mD0A78B20AACCAEB5D2ECE032884DE17D435454B6_RuntimeMethod_var, 0 },
	{ 0x060001FF, 9,  (void**)&TizenStoreImpl_MessageCallback_mA4B7D8E14E7399E5405E1DF5C1C9D7F72E1B4E73_RuntimeMethod_var, 0 },
	{ 0x06000205, 8,  (void**)&FacebookStoreImpl_MessageCallback_mBC2F88BC0E8116317E1EC9EE7B5CDFE237B970D9_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[5] = 
{
	{ 0x0200008C, { 5, 2 } },
	{ 0x06000219, { 0, 5 } },
	{ 0x0600023B, { 7, 5 } },
	{ 0x06000253, { 12, 2 } },
	{ 0x06000254, { 14, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[16] = 
{
	{ (Il2CppRGCTXDataType)2, 27346 },
	{ (Il2CppRGCTXDataType)3, 12474 },
	{ (Il2CppRGCTXDataType)1, 23807 },
	{ (Il2CppRGCTXDataType)2, 23807 },
	{ (Il2CppRGCTXDataType)3, 12475 },
	{ (Il2CppRGCTXDataType)2, 23817 },
	{ (Il2CppRGCTXDataType)3, 12476 },
	{ (Il2CppRGCTXDataType)2, 27347 },
	{ (Il2CppRGCTXDataType)3, 12477 },
	{ (Il2CppRGCTXDataType)2, 23827 },
	{ (Il2CppRGCTXDataType)3, 12478 },
	{ (Il2CppRGCTXDataType)3, 12479 },
	{ (Il2CppRGCTXDataType)1, 23828 },
	{ (Il2CppRGCTXDataType)2, 23828 },
	{ (Il2CppRGCTXDataType)1, 23829 },
	{ (Il2CppRGCTXDataType)3, 12480 },
};
extern const Il2CppCodeGenModule g_StoresCodeGenModule;
const Il2CppCodeGenModule g_StoresCodeGenModule = 
{
	"Stores.dll",
	612,
	s_methodPointers,
	s_InvokerIndices,
	3,
	s_reversePInvokeIndices,
	5,
	s_rgctxIndices,
	16,
	s_rgctxValues,
	NULL,
};
