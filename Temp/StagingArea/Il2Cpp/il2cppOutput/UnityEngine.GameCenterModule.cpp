﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.Achievement>
struct List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD;
// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>
struct List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B;
// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.Leaderboard>
struct List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB;
// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.UserProfile>
struct List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64;
// System.String
struct String_t;
// UnityEngine.SocialPlatforms.ISocialPlatform
struct ISocialPlatform_tA84A16F8E6959FFAFE619CE9F69F26DEF58FB347;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t2E09F4A316E4A9274CF8B7BF231DE9AEBE02F062;
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct AchievementU5BU5D_tFAD38AD7FF5A106BD7E7F04CC31D89D28C54ACB2;
// UnityEngine.SocialPlatforms.Impl.Leaderboard[]
struct LeaderboardU5BU5D_t1BDEF309EA07A5E6C330EEE19B37851793EF7D47;
// UnityEngine.SocialPlatforms.Impl.LocalUser
struct LocalUser_tBBCEEB55B6F28DFA7F4677E9273622A34CABB135;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t74EE7690744D8F99BA578ACE0CC65C45837F522F;
// UnityEngine.SocialPlatforms.Local
struct Local_t36C367EABFCE034400B659818ACAFCFB79532DD8;

IL2CPP_EXTERN_C RuntimeClass* ActivePlatform_t0EC8A845B947135B6186549342A319C4732330CD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Local_t36C367EABFCE034400B659818ACAFCFB79532DD8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m112E9FC5F574771B3C6F6C2A1D0542C5135374F4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m1A43BA5D23AD9736CE19353445249FA41CBB0881_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m3AE06D7A687E3A376BDCEABBC9A17708DBDA5F98_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m8A81ACE3D29D0909B83BC4E5F0B8F6345CD73B48_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t ActivePlatform_SelectSocialPlatform_m27C01C886A801C5186461E31572B55A8068961F3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ActivePlatform_get_Instance_mC73120CFC6D5F26A916F84399DE7FE626419AC69_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ActivePlatform_set_Instance_m8F17B7F1A691A9985C31546F272D1BDD7F1504F9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Local__cctor_mF5DDF3546FB61F0A4E373CFFB3351314F5E9C66B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Local__ctor_m18A130860CC620F4FEAA697A0111611959359178_MetadataUsageId;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t188571242096CC1D2BEFEA0CA619B862EF745D19 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.Achievement>
struct  List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AchievementU5BU5D_tFAD38AD7FF5A106BD7E7F04CC31D89D28C54ACB2* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD, ____items_1)); }
	inline AchievementU5BU5D_tFAD38AD7FF5A106BD7E7F04CC31D89D28C54ACB2* get__items_1() const { return ____items_1; }
	inline AchievementU5BU5D_tFAD38AD7FF5A106BD7E7F04CC31D89D28C54ACB2** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AchievementU5BU5D_tFAD38AD7FF5A106BD7E7F04CC31D89D28C54ACB2* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	AchievementU5BU5D_tFAD38AD7FF5A106BD7E7F04CC31D89D28C54ACB2* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD_StaticFields, ____emptyArray_5)); }
	inline AchievementU5BU5D_tFAD38AD7FF5A106BD7E7F04CC31D89D28C54ACB2* get__emptyArray_5() const { return ____emptyArray_5; }
	inline AchievementU5BU5D_tFAD38AD7FF5A106BD7E7F04CC31D89D28C54ACB2** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(AchievementU5BU5D_tFAD38AD7FF5A106BD7E7F04CC31D89D28C54ACB2* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>
struct  List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AchievementDescriptionU5BU5D_t2E09F4A316E4A9274CF8B7BF231DE9AEBE02F062* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B, ____items_1)); }
	inline AchievementDescriptionU5BU5D_t2E09F4A316E4A9274CF8B7BF231DE9AEBE02F062* get__items_1() const { return ____items_1; }
	inline AchievementDescriptionU5BU5D_t2E09F4A316E4A9274CF8B7BF231DE9AEBE02F062** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AchievementDescriptionU5BU5D_t2E09F4A316E4A9274CF8B7BF231DE9AEBE02F062* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	AchievementDescriptionU5BU5D_t2E09F4A316E4A9274CF8B7BF231DE9AEBE02F062* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B_StaticFields, ____emptyArray_5)); }
	inline AchievementDescriptionU5BU5D_t2E09F4A316E4A9274CF8B7BF231DE9AEBE02F062* get__emptyArray_5() const { return ____emptyArray_5; }
	inline AchievementDescriptionU5BU5D_t2E09F4A316E4A9274CF8B7BF231DE9AEBE02F062** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(AchievementDescriptionU5BU5D_t2E09F4A316E4A9274CF8B7BF231DE9AEBE02F062* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.Leaderboard>
struct  List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	LeaderboardU5BU5D_t1BDEF309EA07A5E6C330EEE19B37851793EF7D47* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB, ____items_1)); }
	inline LeaderboardU5BU5D_t1BDEF309EA07A5E6C330EEE19B37851793EF7D47* get__items_1() const { return ____items_1; }
	inline LeaderboardU5BU5D_t1BDEF309EA07A5E6C330EEE19B37851793EF7D47** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(LeaderboardU5BU5D_t1BDEF309EA07A5E6C330EEE19B37851793EF7D47* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	LeaderboardU5BU5D_t1BDEF309EA07A5E6C330EEE19B37851793EF7D47* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB_StaticFields, ____emptyArray_5)); }
	inline LeaderboardU5BU5D_t1BDEF309EA07A5E6C330EEE19B37851793EF7D47* get__emptyArray_5() const { return ____emptyArray_5; }
	inline LeaderboardU5BU5D_t1BDEF309EA07A5E6C330EEE19B37851793EF7D47** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(LeaderboardU5BU5D_t1BDEF309EA07A5E6C330EEE19B37851793EF7D47* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.UserProfile>
struct  List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UserProfileU5BU5D_t74EE7690744D8F99BA578ACE0CC65C45837F522F* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64, ____items_1)); }
	inline UserProfileU5BU5D_t74EE7690744D8F99BA578ACE0CC65C45837F522F* get__items_1() const { return ____items_1; }
	inline UserProfileU5BU5D_t74EE7690744D8F99BA578ACE0CC65C45837F522F** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UserProfileU5BU5D_t74EE7690744D8F99BA578ACE0CC65C45837F522F* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	UserProfileU5BU5D_t74EE7690744D8F99BA578ACE0CC65C45837F522F* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64_StaticFields, ____emptyArray_5)); }
	inline UserProfileU5BU5D_t74EE7690744D8F99BA578ACE0CC65C45837F522F* get__emptyArray_5() const { return ____emptyArray_5; }
	inline UserProfileU5BU5D_t74EE7690744D8F99BA578ACE0CC65C45837F522F** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(UserProfileU5BU5D_t74EE7690744D8F99BA578ACE0CC65C45837F522F* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.Social
struct  Social_t386079ECBB8389690EA3B5B4680AFC2B3A897755  : public RuntimeObject
{
public:

public:
};


// UnityEngine.SocialPlatforms.ActivePlatform
struct  ActivePlatform_t0EC8A845B947135B6186549342A319C4732330CD  : public RuntimeObject
{
public:

public:
};

struct ActivePlatform_t0EC8A845B947135B6186549342A319C4732330CD_StaticFields
{
public:
	// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::_active
	RuntimeObject* ____active_0;

public:
	inline static int32_t get_offset_of__active_0() { return static_cast<int32_t>(offsetof(ActivePlatform_t0EC8A845B947135B6186549342A319C4732330CD_StaticFields, ____active_0)); }
	inline RuntimeObject* get__active_0() const { return ____active_0; }
	inline RuntimeObject** get_address_of__active_0() { return &____active_0; }
	inline void set__active_0(RuntimeObject* value)
	{
		____active_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____active_0), (void*)value);
	}
};


// UnityEngine.SocialPlatforms.Impl.Achievement
struct  Achievement_t853D7B9496E1B0395F9DC4EC4B6C677A82498633  : public RuntimeObject
{
public:

public:
};


// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct  AchievementDescription_t3A0A2B1921C25802FE46B81BF301BFCAA2FEE6E7  : public RuntimeObject
{
public:

public:
};


// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct  Leaderboard_t01480B36811BC84DF398C8A847972B62F5E2E4FE  : public RuntimeObject
{
public:

public:
};


// UnityEngine.SocialPlatforms.Impl.UserProfile
struct  UserProfile_tB1F9D8E54F0480240196974DCCAF2742F8F0A51E  : public RuntimeObject
{
public:

public:
};


// UnityEngine.SocialPlatforms.Local
struct  Local_t36C367EABFCE034400B659818ACAFCFB79532DD8  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.UserProfile> UnityEngine.SocialPlatforms.Local::m_Friends
	List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 * ___m_Friends_1;
	// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.UserProfile> UnityEngine.SocialPlatforms.Local::m_Users
	List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 * ___m_Users_2;
	// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription> UnityEngine.SocialPlatforms.Local::m_AchievementDescriptions
	List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B * ___m_AchievementDescriptions_3;
	// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.Achievement> UnityEngine.SocialPlatforms.Local::m_Achievements
	List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD * ___m_Achievements_4;
	// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.Leaderboard> UnityEngine.SocialPlatforms.Local::m_Leaderboards
	List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB * ___m_Leaderboards_5;

public:
	inline static int32_t get_offset_of_m_Friends_1() { return static_cast<int32_t>(offsetof(Local_t36C367EABFCE034400B659818ACAFCFB79532DD8, ___m_Friends_1)); }
	inline List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 * get_m_Friends_1() const { return ___m_Friends_1; }
	inline List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 ** get_address_of_m_Friends_1() { return &___m_Friends_1; }
	inline void set_m_Friends_1(List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 * value)
	{
		___m_Friends_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Friends_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Users_2() { return static_cast<int32_t>(offsetof(Local_t36C367EABFCE034400B659818ACAFCFB79532DD8, ___m_Users_2)); }
	inline List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 * get_m_Users_2() const { return ___m_Users_2; }
	inline List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 ** get_address_of_m_Users_2() { return &___m_Users_2; }
	inline void set_m_Users_2(List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 * value)
	{
		___m_Users_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Users_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_AchievementDescriptions_3() { return static_cast<int32_t>(offsetof(Local_t36C367EABFCE034400B659818ACAFCFB79532DD8, ___m_AchievementDescriptions_3)); }
	inline List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B * get_m_AchievementDescriptions_3() const { return ___m_AchievementDescriptions_3; }
	inline List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B ** get_address_of_m_AchievementDescriptions_3() { return &___m_AchievementDescriptions_3; }
	inline void set_m_AchievementDescriptions_3(List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B * value)
	{
		___m_AchievementDescriptions_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AchievementDescriptions_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Achievements_4() { return static_cast<int32_t>(offsetof(Local_t36C367EABFCE034400B659818ACAFCFB79532DD8, ___m_Achievements_4)); }
	inline List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD * get_m_Achievements_4() const { return ___m_Achievements_4; }
	inline List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD ** get_address_of_m_Achievements_4() { return &___m_Achievements_4; }
	inline void set_m_Achievements_4(List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD * value)
	{
		___m_Achievements_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Achievements_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Leaderboards_5() { return static_cast<int32_t>(offsetof(Local_t36C367EABFCE034400B659818ACAFCFB79532DD8, ___m_Leaderboards_5)); }
	inline List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB * get_m_Leaderboards_5() const { return ___m_Leaderboards_5; }
	inline List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB ** get_address_of_m_Leaderboards_5() { return &___m_Leaderboards_5; }
	inline void set_m_Leaderboards_5(List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB * value)
	{
		___m_Leaderboards_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Leaderboards_5), (void*)value);
	}
};

struct Local_t36C367EABFCE034400B659818ACAFCFB79532DD8_StaticFields
{
public:
	// UnityEngine.SocialPlatforms.Impl.LocalUser UnityEngine.SocialPlatforms.Local::m_LocalUser
	LocalUser_tBBCEEB55B6F28DFA7F4677E9273622A34CABB135 * ___m_LocalUser_0;

public:
	inline static int32_t get_offset_of_m_LocalUser_0() { return static_cast<int32_t>(offsetof(Local_t36C367EABFCE034400B659818ACAFCFB79532DD8_StaticFields, ___m_LocalUser_0)); }
	inline LocalUser_tBBCEEB55B6F28DFA7F4677E9273622A34CABB135 * get_m_LocalUser_0() const { return ___m_LocalUser_0; }
	inline LocalUser_tBBCEEB55B6F28DFA7F4677E9273622A34CABB135 ** get_address_of_m_LocalUser_0() { return &___m_LocalUser_0; }
	inline void set_m_LocalUser_0(LocalUser_tBBCEEB55B6F28DFA7F4677E9273622A34CABB135 * value)
	{
		___m_LocalUser_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LocalUser_0), (void*)value);
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.SocialPlatforms.Impl.LocalUser
struct  LocalUser_tBBCEEB55B6F28DFA7F4677E9273622A34CABB135  : public UserProfile_tB1F9D8E54F0480240196974DCCAF2742F8F0A51E
{
public:

public:
};


// UnityEngine.SocialPlatforms.Range
struct  Range_t2332F6C6E1E19A355F5C1A93FF4434B92FBDBABC 
{
public:
	// System.Int32 UnityEngine.SocialPlatforms.Range::from
	int32_t ___from_0;
	// System.Int32 UnityEngine.SocialPlatforms.Range::count
	int32_t ___count_1;

public:
	inline static int32_t get_offset_of_from_0() { return static_cast<int32_t>(offsetof(Range_t2332F6C6E1E19A355F5C1A93FF4434B92FBDBABC, ___from_0)); }
	inline int32_t get_from_0() const { return ___from_0; }
	inline int32_t* get_address_of_from_0() { return &___from_0; }
	inline void set_from_0(int32_t value)
	{
		___from_0 = value;
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(Range_t2332F6C6E1E19A355F5C1A93FF4434B92FBDBABC, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}
};


// UnityEngine.SocialPlatforms.TimeScope
struct  TimeScope_t33ED9CE3541B951879D86F5AE6A8D9389E7A2369 
{
public:
	// System.Int32 UnityEngine.SocialPlatforms.TimeScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TimeScope_t33ED9CE3541B951879D86F5AE6A8D9389E7A2369, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.SocialPlatforms.UserScope
struct  UserScope_t85B1CA855A894226BDE6A19B4CBE7EC9F02D16E3 
{
public:
	// System.Int32 UnityEngine.SocialPlatforms.UserScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UserScope_t85B1CA855A894226BDE6A19B4CBE7EC9F02D16E3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.SocialPlatforms.UserState
struct  UserState_t84B00958348DD8A2B8778416E393E229DACA5871 
{
public:
	// System.Int32 UnityEngine.SocialPlatforms.UserState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UserState_t84B00958348DD8A2B8778416E393E229DACA5871, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);

// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ActivePlatform_get_Instance_mC73120CFC6D5F26A916F84399DE7FE626419AC69 (const RuntimeMethod* method);
// System.Void UnityEngine.SocialPlatforms.ActivePlatform::set_Instance(UnityEngine.SocialPlatforms.ISocialPlatform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActivePlatform_set_Instance_m8F17B7F1A691A9985C31546F272D1BDD7F1504F9 (RuntimeObject* ___value0, const RuntimeMethod* method);
// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::SelectSocialPlatform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ActivePlatform_SelectSocialPlatform_m27C01C886A801C5186461E31572B55A8068961F3 (const RuntimeMethod* method);
// System.Void UnityEngine.SocialPlatforms.Local::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Local__ctor_m18A130860CC620F4FEAA697A0111611959359178 (Local_t36C367EABFCE034400B659818ACAFCFB79532DD8 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.UserProfile>::.ctor()
inline void List_1__ctor_m1A43BA5D23AD9736CE19353445249FA41CBB0881 (List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>::.ctor()
inline void List_1__ctor_m112E9FC5F574771B3C6F6C2A1D0542C5135374F4 (List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.Achievement>::.ctor()
inline void List_1__ctor_m3AE06D7A687E3A376BDCEABBC9A17708DBDA5F98 (List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.Impl.Leaderboard>::.ctor()
inline void List_1__ctor_m8A81ACE3D29D0909B83BC4E5F0B8F6345CD73B48 (List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.Social::get_Active()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Social_get_Active_m3A0805FA14B55B8A297411DDF6295127A62C83FD (const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = ActivePlatform_get_Instance_mC73120CFC6D5F26A916F84399DE7FE626419AC69(/*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Social::set_Active(UnityEngine.SocialPlatforms.ISocialPlatform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Social_set_Active_m6068C76CDEDCBB996D5F397BDF9F1038FD99E7C6 (RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___value0;
		ActivePlatform_set_Instance_m8F17B7F1A691A9985C31546F272D1BDD7F1504F9(L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ActivePlatform_get_Instance_mC73120CFC6D5F26A916F84399DE7FE626419AC69 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ActivePlatform_get_Instance_mC73120CFC6D5F26A916F84399DE7FE626419AC69_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RuntimeObject* V_1 = NULL;
	{
		RuntimeObject* L_0 = ((ActivePlatform_t0EC8A845B947135B6186549342A319C4732330CD_StaticFields*)il2cpp_codegen_static_fields_for(ActivePlatform_t0EC8A845B947135B6186549342A319C4732330CD_il2cpp_TypeInfo_var))->get__active_0();
		V_0 = (bool)((((RuntimeObject*)(RuntimeObject*)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		RuntimeObject* L_2 = ActivePlatform_SelectSocialPlatform_m27C01C886A801C5186461E31572B55A8068961F3(/*hidden argument*/NULL);
		((ActivePlatform_t0EC8A845B947135B6186549342A319C4732330CD_StaticFields*)il2cpp_codegen_static_fields_for(ActivePlatform_t0EC8A845B947135B6186549342A319C4732330CD_il2cpp_TypeInfo_var))->set__active_0(L_2);
	}

IL_0017:
	{
		RuntimeObject* L_3 = ((ActivePlatform_t0EC8A845B947135B6186549342A319C4732330CD_StaticFields*)il2cpp_codegen_static_fields_for(ActivePlatform_t0EC8A845B947135B6186549342A319C4732330CD_il2cpp_TypeInfo_var))->get__active_0();
		V_1 = L_3;
		goto IL_001f;
	}

IL_001f:
	{
		RuntimeObject* L_4 = V_1;
		return L_4;
	}
}
// System.Void UnityEngine.SocialPlatforms.ActivePlatform::set_Instance(UnityEngine.SocialPlatforms.ISocialPlatform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActivePlatform_set_Instance_m8F17B7F1A691A9985C31546F272D1BDD7F1504F9 (RuntimeObject* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ActivePlatform_set_Instance_m8F17B7F1A691A9985C31546F272D1BDD7F1504F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___value0;
		((ActivePlatform_t0EC8A845B947135B6186549342A319C4732330CD_StaticFields*)il2cpp_codegen_static_fields_for(ActivePlatform_t0EC8A845B947135B6186549342A319C4732330CD_il2cpp_TypeInfo_var))->set__active_0(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::SelectSocialPlatform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ActivePlatform_SelectSocialPlatform_m27C01C886A801C5186461E31572B55A8068961F3 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ActivePlatform_SelectSocialPlatform_m27C01C886A801C5186461E31572B55A8068961F3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Local_t36C367EABFCE034400B659818ACAFCFB79532DD8 * L_0 = (Local_t36C367EABFCE034400B659818ACAFCFB79532DD8 *)il2cpp_codegen_object_new(Local_t36C367EABFCE034400B659818ACAFCFB79532DD8_il2cpp_TypeInfo_var);
		Local__ctor_m18A130860CC620F4FEAA697A0111611959359178(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.SocialPlatforms.Local::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Local__ctor_m18A130860CC620F4FEAA697A0111611959359178 (Local_t36C367EABFCE034400B659818ACAFCFB79532DD8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Local__ctor_m18A130860CC620F4FEAA697A0111611959359178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 * L_0 = (List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 *)il2cpp_codegen_object_new(List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64_il2cpp_TypeInfo_var);
		List_1__ctor_m1A43BA5D23AD9736CE19353445249FA41CBB0881(L_0, /*hidden argument*/List_1__ctor_m1A43BA5D23AD9736CE19353445249FA41CBB0881_RuntimeMethod_var);
		__this->set_m_Friends_1(L_0);
		List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 * L_1 = (List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64 *)il2cpp_codegen_object_new(List_1_t6D234ADBFACC26518C12281ED788200ED5CC2B64_il2cpp_TypeInfo_var);
		List_1__ctor_m1A43BA5D23AD9736CE19353445249FA41CBB0881(L_1, /*hidden argument*/List_1__ctor_m1A43BA5D23AD9736CE19353445249FA41CBB0881_RuntimeMethod_var);
		__this->set_m_Users_2(L_1);
		List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B * L_2 = (List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B *)il2cpp_codegen_object_new(List_1_t1B0E0960DF0DE59AEAE7AF55FAAA194480923C9B_il2cpp_TypeInfo_var);
		List_1__ctor_m112E9FC5F574771B3C6F6C2A1D0542C5135374F4(L_2, /*hidden argument*/List_1__ctor_m112E9FC5F574771B3C6F6C2A1D0542C5135374F4_RuntimeMethod_var);
		__this->set_m_AchievementDescriptions_3(L_2);
		List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD * L_3 = (List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD *)il2cpp_codegen_object_new(List_1_tD864CE9595841DE7F38850604D479CD0FEAC65CD_il2cpp_TypeInfo_var);
		List_1__ctor_m3AE06D7A687E3A376BDCEABBC9A17708DBDA5F98(L_3, /*hidden argument*/List_1__ctor_m3AE06D7A687E3A376BDCEABBC9A17708DBDA5F98_RuntimeMethod_var);
		__this->set_m_Achievements_4(L_3);
		List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB * L_4 = (List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB *)il2cpp_codegen_object_new(List_1_tD0ACA8003DC14A03B7485BC762068FF2F05DEEBB_il2cpp_TypeInfo_var);
		List_1__ctor_m8A81ACE3D29D0909B83BC4E5F0B8F6345CD73B48(L_4, /*hidden argument*/List_1__ctor_m8A81ACE3D29D0909B83BC4E5F0B8F6345CD73B48_RuntimeMethod_var);
		__this->set_m_Leaderboards_5(L_4);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Local::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Local__cctor_mF5DDF3546FB61F0A4E373CFFB3351314F5E9C66B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Local__cctor_mF5DDF3546FB61F0A4E373CFFB3351314F5E9C66B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Local_t36C367EABFCE034400B659818ACAFCFB79532DD8_StaticFields*)il2cpp_codegen_static_fields_for(Local_t36C367EABFCE034400B659818ACAFCFB79532DD8_il2cpp_TypeInfo_var))->set_m_LocalUser_0((LocalUser_tBBCEEB55B6F28DFA7F4677E9273622A34CABB135 *)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
