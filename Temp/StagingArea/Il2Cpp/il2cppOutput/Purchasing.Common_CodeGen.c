﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String UnityEngine.Purchasing.MiniJson::JsonEncode(System.Object)
extern void MiniJson_JsonEncode_m13E359D5CD064E0A827273037E2452D1DC490F4B (void);
// 0x00000002 System.Object UnityEngine.Purchasing.MiniJson::JsonDecode(System.String)
extern void MiniJson_JsonDecode_m031FAED0A15E631FE47242C388F8DE0EE292F75C (void);
// 0x00000003 System.Void UnityEngine.Purchasing.INativeStore::RetrieveProducts(System.String)
// 0x00000004 System.Void UnityEngine.Purchasing.INativeStore::Purchase(System.String,System.String)
// 0x00000005 System.Void UnityEngine.Purchasing.INativeStore::FinishTransaction(System.String,System.String)
// 0x00000006 System.Void UnityEngine.Purchasing.UnityPurchasingCallback::.ctor(System.Object,System.IntPtr)
extern void UnityPurchasingCallback__ctor_m880CBDA15CB1DDACF4F95420C2DD87E6C4A2136D (void);
// 0x00000007 System.Void UnityEngine.Purchasing.UnityPurchasingCallback::Invoke(System.String,System.String,System.String,System.String)
extern void UnityPurchasingCallback_Invoke_m960FF246D9FFF566BA02C0AB97D07DBDA54A4F5A (void);
// 0x00000008 System.IAsyncResult UnityEngine.Purchasing.UnityPurchasingCallback::BeginInvoke(System.String,System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern void UnityPurchasingCallback_BeginInvoke_m5B1660747A72B4C47B2614F150DEB285AEE51BB3 (void);
// 0x00000009 System.Void UnityEngine.Purchasing.UnityPurchasingCallback::EndInvoke(System.IAsyncResult)
extern void UnityPurchasingCallback_EndInvoke_mECD0AE47EB6855A36064A97BF70CDE5E79D6A30C (void);
// 0x0000000A System.Object UnityEngine.Purchasing.MiniJSON.Json::Deserialize(System.String)
extern void Json_Deserialize_m2D73718E3D9B1C26F68AC864EB575A617A01337E (void);
// 0x0000000B System.String UnityEngine.Purchasing.MiniJSON.Json::Serialize(System.Object)
extern void Json_Serialize_m4DD035574E0E57283960A7F62595E817F10EB4B2 (void);
// 0x0000000C System.Boolean UnityEngine.Purchasing.MiniJSON.Json_Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_m6C847E0A09374808256B8EA3249EFA2F19F1934B (void);
// 0x0000000D System.Void UnityEngine.Purchasing.MiniJSON.Json_Parser::.ctor(System.String)
extern void Parser__ctor_mCAFF97D0B678211889B727CB4ECCC9DA0F4D7AD7 (void);
// 0x0000000E System.Object UnityEngine.Purchasing.MiniJSON.Json_Parser::Parse(System.String)
extern void Parser_Parse_mBFFAF46468D59F90556AA124CB37F9F2CEE86CE9 (void);
// 0x0000000F System.Void UnityEngine.Purchasing.MiniJSON.Json_Parser::Dispose()
extern void Parser_Dispose_m503501CE427C66D30F520798F9B2E64FF726653B (void);
// 0x00000010 System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MiniJSON.Json_Parser::ParseObject()
extern void Parser_ParseObject_m6B0C48D7C8AB7A4C7D16127AA988963C5826EE3B (void);
// 0x00000011 System.Collections.Generic.List`1<System.Object> UnityEngine.Purchasing.MiniJSON.Json_Parser::ParseArray()
extern void Parser_ParseArray_mF38ABF31D0B1689DA9E9B4509AFD6001FA434880 (void);
// 0x00000012 System.Object UnityEngine.Purchasing.MiniJSON.Json_Parser::ParseValue()
extern void Parser_ParseValue_m40A1859D3532EACA23DCB2D31EA763B3A2FEE8AF (void);
// 0x00000013 System.Object UnityEngine.Purchasing.MiniJSON.Json_Parser::ParseByToken(UnityEngine.Purchasing.MiniJSON.Json_Parser_TOKEN)
extern void Parser_ParseByToken_m2DB847FA4FB14CB3D338E5D3CFC3DE64510A1068 (void);
// 0x00000014 System.String UnityEngine.Purchasing.MiniJSON.Json_Parser::ParseString()
extern void Parser_ParseString_mECC0E6D490D70CAFF8F48DF4C359B0FBA97DB2B0 (void);
// 0x00000015 System.Object UnityEngine.Purchasing.MiniJSON.Json_Parser::ParseNumber()
extern void Parser_ParseNumber_m2AB8A46BC07387E0771807F6EE36FBCE72425F07 (void);
// 0x00000016 System.Void UnityEngine.Purchasing.MiniJSON.Json_Parser::EatWhitespace()
extern void Parser_EatWhitespace_m462EBC51534FABBA513B9F7EA5AE0C6EF62CC080 (void);
// 0x00000017 System.Char UnityEngine.Purchasing.MiniJSON.Json_Parser::get_PeekChar()
extern void Parser_get_PeekChar_m4E86B8A9D34903A9DCD2E902B9FE304C1CBD24E4 (void);
// 0x00000018 System.Char UnityEngine.Purchasing.MiniJSON.Json_Parser::get_NextChar()
extern void Parser_get_NextChar_m69F553845E0D2B54D58B692B2F5ACE9DC78AF546 (void);
// 0x00000019 System.String UnityEngine.Purchasing.MiniJSON.Json_Parser::get_NextWord()
extern void Parser_get_NextWord_m562677A16C7843D96244B73BF9A16C25E88C95FD (void);
// 0x0000001A UnityEngine.Purchasing.MiniJSON.Json_Parser_TOKEN UnityEngine.Purchasing.MiniJSON.Json_Parser::get_NextToken()
extern void Parser_get_NextToken_m111D2972AC9D214CA8EBDE7D92D7E2B17200CED7 (void);
// 0x0000001B System.Void UnityEngine.Purchasing.MiniJSON.Json_Serializer::.ctor()
extern void Serializer__ctor_m769555EF521D697998695E7A87F5D36B4D2F3024 (void);
// 0x0000001C System.String UnityEngine.Purchasing.MiniJSON.Json_Serializer::Serialize(System.Object)
extern void Serializer_Serialize_m513470E0B21EA7AAC9BEB653672DD21602E5D682 (void);
// 0x0000001D System.Void UnityEngine.Purchasing.MiniJSON.Json_Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_mCB543C5DFB85D67CFD8731C5AE3981340FA9850C (void);
// 0x0000001E System.Void UnityEngine.Purchasing.MiniJSON.Json_Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_mDF67DE8B2D1E3D506BDC53C49A95E2A8F0F4718A (void);
// 0x0000001F System.Void UnityEngine.Purchasing.MiniJSON.Json_Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_m0BD37A1979EC3CA7A01493E522410953770D1532 (void);
// 0x00000020 System.Void UnityEngine.Purchasing.MiniJSON.Json_Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_m0C262BA943CE695F2BF678773B9A1B310DB20F2D (void);
// 0x00000021 System.Void UnityEngine.Purchasing.MiniJSON.Json_Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_mF187FA2E806FCC8687AB1EE5C00AA9E84C5A735F (void);
// 0x00000022 System.String UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::GetString(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String,System.String)
extern void MiniJsonExtensions_GetString_m4AFB02CA36B2E11A93A6E85CB34BA6588055FB60 (void);
// 0x00000023 System.String UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void MiniJsonExtensions_toJson_m0140585390A01E66B6C481D5C293483DF74912F2 (void);
// 0x00000024 System.String UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void MiniJsonExtensions_toJson_m5D53E855DAC7661D569C92E570456A33D2066650 (void);
// 0x00000025 System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::HashtableFromJson(System.String)
extern void MiniJsonExtensions_HashtableFromJson_m45D10722FE0BDF725D5E23E521DF91285E470F13 (void);
static Il2CppMethodPointer s_methodPointers[37] = 
{
	MiniJson_JsonEncode_m13E359D5CD064E0A827273037E2452D1DC490F4B,
	MiniJson_JsonDecode_m031FAED0A15E631FE47242C388F8DE0EE292F75C,
	NULL,
	NULL,
	NULL,
	UnityPurchasingCallback__ctor_m880CBDA15CB1DDACF4F95420C2DD87E6C4A2136D,
	UnityPurchasingCallback_Invoke_m960FF246D9FFF566BA02C0AB97D07DBDA54A4F5A,
	UnityPurchasingCallback_BeginInvoke_m5B1660747A72B4C47B2614F150DEB285AEE51BB3,
	UnityPurchasingCallback_EndInvoke_mECD0AE47EB6855A36064A97BF70CDE5E79D6A30C,
	Json_Deserialize_m2D73718E3D9B1C26F68AC864EB575A617A01337E,
	Json_Serialize_m4DD035574E0E57283960A7F62595E817F10EB4B2,
	Parser_IsWordBreak_m6C847E0A09374808256B8EA3249EFA2F19F1934B,
	Parser__ctor_mCAFF97D0B678211889B727CB4ECCC9DA0F4D7AD7,
	Parser_Parse_mBFFAF46468D59F90556AA124CB37F9F2CEE86CE9,
	Parser_Dispose_m503501CE427C66D30F520798F9B2E64FF726653B,
	Parser_ParseObject_m6B0C48D7C8AB7A4C7D16127AA988963C5826EE3B,
	Parser_ParseArray_mF38ABF31D0B1689DA9E9B4509AFD6001FA434880,
	Parser_ParseValue_m40A1859D3532EACA23DCB2D31EA763B3A2FEE8AF,
	Parser_ParseByToken_m2DB847FA4FB14CB3D338E5D3CFC3DE64510A1068,
	Parser_ParseString_mECC0E6D490D70CAFF8F48DF4C359B0FBA97DB2B0,
	Parser_ParseNumber_m2AB8A46BC07387E0771807F6EE36FBCE72425F07,
	Parser_EatWhitespace_m462EBC51534FABBA513B9F7EA5AE0C6EF62CC080,
	Parser_get_PeekChar_m4E86B8A9D34903A9DCD2E902B9FE304C1CBD24E4,
	Parser_get_NextChar_m69F553845E0D2B54D58B692B2F5ACE9DC78AF546,
	Parser_get_NextWord_m562677A16C7843D96244B73BF9A16C25E88C95FD,
	Parser_get_NextToken_m111D2972AC9D214CA8EBDE7D92D7E2B17200CED7,
	Serializer__ctor_m769555EF521D697998695E7A87F5D36B4D2F3024,
	Serializer_Serialize_m513470E0B21EA7AAC9BEB653672DD21602E5D682,
	Serializer_SerializeValue_mCB543C5DFB85D67CFD8731C5AE3981340FA9850C,
	Serializer_SerializeObject_mDF67DE8B2D1E3D506BDC53C49A95E2A8F0F4718A,
	Serializer_SerializeArray_m0BD37A1979EC3CA7A01493E522410953770D1532,
	Serializer_SerializeString_m0C262BA943CE695F2BF678773B9A1B310DB20F2D,
	Serializer_SerializeOther_mF187FA2E806FCC8687AB1EE5C00AA9E84C5A735F,
	MiniJsonExtensions_GetString_m4AFB02CA36B2E11A93A6E85CB34BA6588055FB60,
	MiniJsonExtensions_toJson_m0140585390A01E66B6C481D5C293483DF74912F2,
	MiniJsonExtensions_toJson_m5D53E855DAC7661D569C92E570456A33D2066650,
	MiniJsonExtensions_HashtableFromJson_m45D10722FE0BDF725D5E23E521DF91285E470F13,
};
static const int32_t s_InvokerIndices[37] = 
{
	0,
	0,
	26,
	27,
	27,
	137,
	459,
	1067,
	26,
	0,
	0,
	48,
	26,
	0,
	23,
	14,
	14,
	14,
	34,
	14,
	14,
	23,
	249,
	249,
	14,
	10,
	23,
	0,
	26,
	26,
	26,
	26,
	26,
	2,
	0,
	0,
	0,
};
extern const Il2CppCodeGenModule g_Purchasing_CommonCodeGenModule;
const Il2CppCodeGenModule g_Purchasing_CommonCodeGenModule = 
{
	"Purchasing.Common.dll",
	37,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
