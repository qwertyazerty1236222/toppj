﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void IAPDemo::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern void IAPDemo_OnInitialized_m09812EB1EC8BBA41EE3137537FE76F1985776941 (void);
// 0x00000002 UnityEngine.Purchasing.PurchaseProcessingResult IAPDemo::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern void IAPDemo_ProcessPurchase_mF11FD082E2AC70F7C74D691915DD38FC0D124C2B (void);
// 0x00000003 System.Void IAPDemo::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void IAPDemo_OnPurchaseFailed_m9D538C282F6B4A14D7C526AB41CD475AB40A03EC (void);
// 0x00000004 System.Void IAPDemo::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern void IAPDemo_OnInitializeFailed_mFDCFA3E72D8DD6763002AB3DA6302C215226FCAF (void);
// 0x00000005 System.Void IAPDemo::Awake()
extern void IAPDemo_Awake_m2128837EF5B16B8E7DCADD847AB73B139A0D53B6 (void);
// 0x00000006 System.Void IAPDemo::OnTransactionsRestored(System.Boolean)
extern void IAPDemo_OnTransactionsRestored_m65536FCBC3C132696644D596F848C015E4061647 (void);
// 0x00000007 System.Void IAPDemo::OnDeferred(UnityEngine.Purchasing.Product)
extern void IAPDemo_OnDeferred_m86E94791D018CC76F49E9F257221C3E95CFF8E2A (void);
// 0x00000008 System.Void IAPDemo::InitUI(System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>)
extern void IAPDemo_InitUI_mEBA5CE85CA66B0D5A8797C6A4EE73108C12163BC (void);
// 0x00000009 System.Void IAPDemo::PurchaseButtonClick(System.String)
extern void IAPDemo_PurchaseButtonClick_m68E3B1E0E39E8A4E021962F1A4A55DE2D0186D32 (void);
// 0x0000000A System.Void IAPDemo::RestoreButtonClick()
extern void IAPDemo_RestoreButtonClick_m091801F3E7FEE2D39A621DE2A98D0F0681246410 (void);
// 0x0000000B System.Void IAPDemo::ClearProductUIs()
extern void IAPDemo_ClearProductUIs_m75D0954CCB2CB7F166FFD2ECC7C5733719D457D5 (void);
// 0x0000000C System.Void IAPDemo::AddProductUIs(UnityEngine.Purchasing.Product[])
extern void IAPDemo_AddProductUIs_m7CE03873CDFA4B5C7C9062ABC5D2007936CD1DED (void);
// 0x0000000D System.Void IAPDemo::UpdateProductUI(UnityEngine.Purchasing.Product)
extern void IAPDemo_UpdateProductUI_mA3E55E43CDDD31BC2B2B710C0A941CD6E3539665 (void);
// 0x0000000E System.Void IAPDemo::UpdateProductPendingUI(UnityEngine.Purchasing.Product,System.Int32)
extern void IAPDemo_UpdateProductPendingUI_mA66A6DDAA8C73AF557E8F949288231D1C138D3C4 (void);
// 0x0000000F System.Boolean IAPDemo::NeedRestoreButton()
extern void IAPDemo_NeedRestoreButton_m74B174F978D8A6BC71E222E3263DBED8E6B19F30 (void);
// 0x00000010 System.Void IAPDemo::LogProductDefinitions()
extern void IAPDemo_LogProductDefinitions_mFB48DEFF4C240E67F87CAAD3E63B4233C0D6CC60 (void);
// 0x00000011 System.Void IAPDemo::.ctor()
extern void IAPDemo__ctor_m138A28A1415DEE8DC74F29BD7131F7CC94C77AAB (void);
// 0x00000012 System.Void IAPDemo::<RestoreButtonClick>b__25_0(UnityEngine.Purchasing.RestoreTransactionIDState)
extern void IAPDemo_U3CRestoreButtonClickU3Eb__25_0_m6EE2A6168E2D85D012FA378CD39D3AF801A0F1CE (void);
// 0x00000013 System.Void IAPDemoProductUI::SetProduct(UnityEngine.Purchasing.Product,System.Action`1<System.String>)
extern void IAPDemoProductUI_SetProduct_m0224417743A36089E5808E03A5EE18F79FC4057E (void);
// 0x00000014 System.Void IAPDemoProductUI::SetPendingTime(System.Int32)
extern void IAPDemoProductUI_SetPendingTime_m8BF5F4E1000C492C5B38C2CB893D572997C1C324 (void);
// 0x00000015 System.Void IAPDemoProductUI::PurchaseButtonClick()
extern void IAPDemoProductUI_PurchaseButtonClick_m0FCCEC6722A3D10A8EBFBD98D116A710E94179FC (void);
// 0x00000016 System.Void IAPDemoProductUI::ReceiptButtonClick()
extern void IAPDemoProductUI_ReceiptButtonClick_mF247336DE6321F9D20C61A49153D3891C465F846 (void);
// 0x00000017 System.Void IAPDemoProductUI::.ctor()
extern void IAPDemoProductUI__ctor_mFB851E7A6CEE9D2DED0F4F77601A344899A99F28 (void);
// 0x00000018 System.Void AlphaButtonClickMask::Start()
extern void AlphaButtonClickMask_Start_mEFF14FD0C0E68631370255F5936C41963983C8F0 (void);
// 0x00000019 System.Boolean AlphaButtonClickMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern void AlphaButtonClickMask_IsRaycastLocationValid_m4E83446D8B35549DE9EEE9F6D4548A527A3C884D (void);
// 0x0000001A System.Void AlphaButtonClickMask::.ctor()
extern void AlphaButtonClickMask__ctor_m5D0B36A0D207C1AD45B2434C1036141F3D9EA28D (void);
// 0x0000001B System.Void EventSystemChecker::Awake()
extern void EventSystemChecker_Awake_mADD5BA9635C9ADBE91C44BC15DFFCB87798377EB (void);
// 0x0000001C System.Void EventSystemChecker::.ctor()
extern void EventSystemChecker__ctor_m52A33F6E257A581FC40521C4CE4B484D2426DA66 (void);
// 0x0000001D System.Void ForcedReset::Update()
extern void ForcedReset_Update_m35CBD926C48BD4ACCDD3CB14E0813D04F0E83FC0 (void);
// 0x0000001E System.Void ForcedReset::.ctor()
extern void ForcedReset__ctor_mF32F8C351B95343959FD07B1FF0EC0FBC8CB8EE0 (void);
// 0x0000001F System.Void UnityStandardAssets.Utility.ActivateTrigger::DoActivateTrigger()
extern void ActivateTrigger_DoActivateTrigger_m91589CEC5598200BE6AC01C6D8ED7CD3C05ACF03 (void);
// 0x00000020 System.Void UnityStandardAssets.Utility.ActivateTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void ActivateTrigger_OnTriggerEnter_mBB7C6EBA79177C6CBEB136C565F17135353319DB (void);
// 0x00000021 System.Void UnityStandardAssets.Utility.ActivateTrigger::.ctor()
extern void ActivateTrigger__ctor_mE845995D67483BC443AE11D6EB0D4E133358EF15 (void);
// 0x00000022 System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch::OnEnable()
extern void AutoMobileShaderSwitch_OnEnable_m2CC4D934531213CC59B29149430BE5454F220327 (void);
// 0x00000023 System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch::.ctor()
extern void AutoMobileShaderSwitch__ctor_m1D46DC3555ED91D57F71F19BD08DB1F72AA8928F (void);
// 0x00000024 System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::Start()
extern void AutoMoveAndRotate_Start_m8A0766F7DA35E226BEC81C38DC800B7C2395E654 (void);
// 0x00000025 System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::Update()
extern void AutoMoveAndRotate_Update_m23C3C317C277A79CF82C88044544258DD7201411 (void);
// 0x00000026 System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::.ctor()
extern void AutoMoveAndRotate__ctor_m97BEA90D4085E63DA4D810FED6F495216024AC6B (void);
// 0x00000027 System.Void UnityStandardAssets.Utility.CameraRefocus::.ctor(UnityEngine.Camera,UnityEngine.Transform,UnityEngine.Vector3)
extern void CameraRefocus__ctor_mAEC82E5DC336BB11096300A567B554CF5235781C (void);
// 0x00000028 System.Void UnityStandardAssets.Utility.CameraRefocus::ChangeCamera(UnityEngine.Camera)
extern void CameraRefocus_ChangeCamera_mD34A981CD7F9E0F4CB3DB5A3C929A969E0FFC538 (void);
// 0x00000029 System.Void UnityStandardAssets.Utility.CameraRefocus::ChangeParent(UnityEngine.Transform)
extern void CameraRefocus_ChangeParent_m95B7671F5C921BACD887A784A798B9C373C107FD (void);
// 0x0000002A System.Void UnityStandardAssets.Utility.CameraRefocus::GetFocusPoint()
extern void CameraRefocus_GetFocusPoint_m708CC6CD84E14B6285E4404388F0369B9A550C40 (void);
// 0x0000002B System.Void UnityStandardAssets.Utility.CameraRefocus::SetFocusPoint()
extern void CameraRefocus_SetFocusPoint_m07D61D09C19C5F951C126B32B3341733C33C20BF (void);
// 0x0000002C System.Void UnityStandardAssets.Utility.CurveControlledBob::Setup(UnityEngine.Camera,System.Single)
extern void CurveControlledBob_Setup_m2007E3FF9FB757CC191CEA557786000FDD658477 (void);
// 0x0000002D UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::DoHeadBob(System.Single)
extern void CurveControlledBob_DoHeadBob_m00B658784E0C963606ADCAB3E3FA9C347F305C6F (void);
// 0x0000002E System.Void UnityStandardAssets.Utility.CurveControlledBob::.ctor()
extern void CurveControlledBob__ctor_mC6A43DC3D1DD13D04AC060CCB621CCF90965BC90 (void);
// 0x0000002F System.Void UnityStandardAssets.Utility.DragRigidbody::Update()
extern void DragRigidbody_Update_m83FBCA60844264E0D9544B4BA94D8EE3590B1055 (void);
// 0x00000030 System.Collections.IEnumerator UnityStandardAssets.Utility.DragRigidbody::DragObject(System.Single)
extern void DragRigidbody_DragObject_m2BF373EA880D49253F3C5BB726D8724B2288A80C (void);
// 0x00000031 UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody::FindCamera()
extern void DragRigidbody_FindCamera_mAF1386C508DAEB88A9C5FD4D456EF4D4BE617D9F (void);
// 0x00000032 System.Void UnityStandardAssets.Utility.DragRigidbody::.ctor()
extern void DragRigidbody__ctor_m6E05682AB45C4BFE0DE11A644D612926F9E76E7D (void);
// 0x00000033 System.Void UnityStandardAssets.Utility.DynamicShadowSettings::Start()
extern void DynamicShadowSettings_Start_m77EC175AF947D4CD8BE908FEAFA761CB247E6D78 (void);
// 0x00000034 System.Void UnityStandardAssets.Utility.DynamicShadowSettings::Update()
extern void DynamicShadowSettings_Update_mCA0BBCA7CA61B468857533B4C7896AFABBF9175C (void);
// 0x00000035 System.Void UnityStandardAssets.Utility.DynamicShadowSettings::.ctor()
extern void DynamicShadowSettings__ctor_mDC8B0D663149C35F242EADFEFED8BA7D4DBCBC77 (void);
// 0x00000036 System.Void UnityStandardAssets.Utility.FOVKick::Setup(UnityEngine.Camera)
extern void FOVKick_Setup_m8469A5705E531E55305A58D3AA3265FA265D33F6 (void);
// 0x00000037 System.Void UnityStandardAssets.Utility.FOVKick::CheckStatus(UnityEngine.Camera)
extern void FOVKick_CheckStatus_m9A63EE82724EA159E55AC55E840B4CE3701BE714 (void);
// 0x00000038 System.Void UnityStandardAssets.Utility.FOVKick::ChangeCamera(UnityEngine.Camera)
extern void FOVKick_ChangeCamera_m3218EF587B033155B743FE279EDCB6CF1CDF4C6F (void);
// 0x00000039 System.Collections.IEnumerator UnityStandardAssets.Utility.FOVKick::FOVKickUp()
extern void FOVKick_FOVKickUp_m91F264E5432BCE86E9082C169D277E64C481D236 (void);
// 0x0000003A System.Collections.IEnumerator UnityStandardAssets.Utility.FOVKick::FOVKickDown()
extern void FOVKick_FOVKickDown_m8BCEE7A586E2B8FAD73F8D8F5D2FB0808A1EEB75 (void);
// 0x0000003B System.Void UnityStandardAssets.Utility.FOVKick::.ctor()
extern void FOVKick__ctor_m3D6A4FBAE68243DB82771B3772735848FA090280 (void);
// 0x0000003C System.Void UnityStandardAssets.Utility.FPSCounter::Start()
extern void FPSCounter_Start_mA01345F424F9731649187CC62E9E69FF0377EB65 (void);
// 0x0000003D System.Void UnityStandardAssets.Utility.FPSCounter::Update()
extern void FPSCounter_Update_m8E1247E9BA50C20B0439DF5CA383621EB7B9FFE5 (void);
// 0x0000003E System.Void UnityStandardAssets.Utility.FPSCounter::.ctor()
extern void FPSCounter__ctor_m3458B72F16625D58BEFC538E933635817205602D (void);
// 0x0000003F System.Void UnityStandardAssets.Utility.FollowTarget::LateUpdate()
extern void FollowTarget_LateUpdate_m5ADBC5E3D58826659C01A2C4F8968A595F092435 (void);
// 0x00000040 System.Void UnityStandardAssets.Utility.FollowTarget::.ctor()
extern void FollowTarget__ctor_m7ADC7081450A6DA01AE0B5CA1DB84BE961D5DD29 (void);
// 0x00000041 System.Single UnityStandardAssets.Utility.LerpControlledBob::Offset()
extern void LerpControlledBob_Offset_mCC7467CE1E66FB6246E52C5984D1C7BE2C0BF1B2 (void);
// 0x00000042 System.Collections.IEnumerator UnityStandardAssets.Utility.LerpControlledBob::DoBobCycle()
extern void LerpControlledBob_DoBobCycle_mB5C14AC5AEF7114F4A575E19B36BEDF0DE691923 (void);
// 0x00000043 System.Void UnityStandardAssets.Utility.LerpControlledBob::.ctor()
extern void LerpControlledBob__ctor_m8E3865BC67563143CFF3C791D0BBFC25D94B18BE (void);
// 0x00000044 System.Void UnityStandardAssets.Utility.ObjectResetter::Start()
extern void ObjectResetter_Start_mF14852C85D595D52F35F221F45A822606EA013B7 (void);
// 0x00000045 System.Void UnityStandardAssets.Utility.ObjectResetter::DelayedReset(System.Single)
extern void ObjectResetter_DelayedReset_mCFE31FD35B3AC724EFD1E76270A1FE3FD5014394 (void);
// 0x00000046 System.Collections.IEnumerator UnityStandardAssets.Utility.ObjectResetter::ResetCoroutine(System.Single)
extern void ObjectResetter_ResetCoroutine_mD46D3A049C28C60DB794613257CC61C5102B0EFC (void);
// 0x00000047 System.Void UnityStandardAssets.Utility.ObjectResetter::.ctor()
extern void ObjectResetter__ctor_mD14550E617FC16A6C1338BE7D18C5B01141A6354 (void);
// 0x00000048 System.Collections.IEnumerator UnityStandardAssets.Utility.ParticleSystemDestroyer::Start()
extern void ParticleSystemDestroyer_Start_m28FC4048F585ACE30B2FB2B6839F2801F8FFE584 (void);
// 0x00000049 System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer::Stop()
extern void ParticleSystemDestroyer_Stop_mC007423F42F57A16264A616C6970FF62E3A37617 (void);
// 0x0000004A System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer::.ctor()
extern void ParticleSystemDestroyer__ctor_m3A484EB728A7ECF87E3BF235EF10F4EF49E11623 (void);
// 0x0000004B System.Void UnityStandardAssets.Utility.PlatformSpecificContent::OnEnable()
extern void PlatformSpecificContent_OnEnable_m3C87AD39C8B588F5FBA2C7D2808C717A238A5BFB (void);
// 0x0000004C System.Void UnityStandardAssets.Utility.PlatformSpecificContent::CheckEnableContent()
extern void PlatformSpecificContent_CheckEnableContent_m6C285401B4F1F4BFC5E748C7753096628CDAB355 (void);
// 0x0000004D System.Void UnityStandardAssets.Utility.PlatformSpecificContent::EnableContent(System.Boolean)
extern void PlatformSpecificContent_EnableContent_mF21760A340E974403AD8731E6731AD1BD6B6C73C (void);
// 0x0000004E System.Void UnityStandardAssets.Utility.PlatformSpecificContent::.ctor()
extern void PlatformSpecificContent__ctor_m8464E7FB25A5C1EC2C1343995EC02402AA72804F (void);
// 0x0000004F System.Void UnityStandardAssets.Utility.SimpleMouseRotator::Start()
extern void SimpleMouseRotator_Start_m8D06607C94F05C2C99A1D74A1D75AA716B1FE80E (void);
// 0x00000050 System.Void UnityStandardAssets.Utility.SimpleMouseRotator::Update()
extern void SimpleMouseRotator_Update_m43E994D26B1A1396F1C87F119B313442201E7222 (void);
// 0x00000051 System.Void UnityStandardAssets.Utility.SimpleMouseRotator::.ctor()
extern void SimpleMouseRotator__ctor_mC1B6E0FDF0226BE88DF307078BD8EE4E5DC546B8 (void);
// 0x00000052 System.Void UnityStandardAssets.Utility.SmoothFollow::Start()
extern void SmoothFollow_Start_mDCCB4CDC6353E65906B648B5AF23AB18D2E51C72 (void);
// 0x00000053 System.Void UnityStandardAssets.Utility.SmoothFollow::LateUpdate()
extern void SmoothFollow_LateUpdate_m7F7C460DDC3C80A3016F8050F50F54C6D55C5C15 (void);
// 0x00000054 System.Void UnityStandardAssets.Utility.SmoothFollow::.ctor()
extern void SmoothFollow__ctor_m3BA62097DC88A8513EFFEFCF2D5CD8D3DB7B548F (void);
// 0x00000055 System.Void UnityStandardAssets.Utility.TimedObjectActivator::Awake()
extern void TimedObjectActivator_Awake_m79DD17D62A8055E1E336A30C26DB80C4D5407289 (void);
// 0x00000056 System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::Activate(UnityStandardAssets.Utility.TimedObjectActivator_Entry)
extern void TimedObjectActivator_Activate_m12334B3127ED32333F52AD3DE31A5ABC8EA4D415 (void);
// 0x00000057 System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::Deactivate(UnityStandardAssets.Utility.TimedObjectActivator_Entry)
extern void TimedObjectActivator_Deactivate_m6A966711DD7B1088A53D5CAC48760661834A3979 (void);
// 0x00000058 System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::ReloadLevel(UnityStandardAssets.Utility.TimedObjectActivator_Entry)
extern void TimedObjectActivator_ReloadLevel_m80D86EA368E03010F6065856EF104F53D4FAB590 (void);
// 0x00000059 System.Void UnityStandardAssets.Utility.TimedObjectActivator::.ctor()
extern void TimedObjectActivator__ctor_m26FCB0BA5A4631EC3F435E0182C9C4E702B1EAD6 (void);
// 0x0000005A System.Void UnityStandardAssets.Utility.TimedObjectDestructor::Awake()
extern void TimedObjectDestructor_Awake_m76424E72B7BE0F6424D18BEF93CAB84DBAAC34A6 (void);
// 0x0000005B System.Void UnityStandardAssets.Utility.TimedObjectDestructor::DestroyNow()
extern void TimedObjectDestructor_DestroyNow_mE93B19E7C626189327CE5300D972C0BDBBE04851 (void);
// 0x0000005C System.Void UnityStandardAssets.Utility.TimedObjectDestructor::.ctor()
extern void TimedObjectDestructor__ctor_mE45D4C3E5E22221059FB74DF5134F06171EFDE6D (void);
// 0x0000005D System.Single UnityStandardAssets.Utility.WaypointCircuit::get_Length()
extern void WaypointCircuit_get_Length_mFA587B16273CB5FD90CC3BD7479A2810B4D318F8 (void);
// 0x0000005E System.Void UnityStandardAssets.Utility.WaypointCircuit::set_Length(System.Single)
extern void WaypointCircuit_set_Length_m39E0ED03A71FEC5DA4B5851069C677BB5A8A657B (void);
// 0x0000005F UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit::get_Waypoints()
extern void WaypointCircuit_get_Waypoints_m95D546B796B966FA6ADB8AE7B553F25CCF548211 (void);
// 0x00000060 System.Void UnityStandardAssets.Utility.WaypointCircuit::Awake()
extern void WaypointCircuit_Awake_m3443AE18FBD735BE48A510AD2B277A86CEB1CC52 (void);
// 0x00000061 UnityStandardAssets.Utility.WaypointCircuit_RoutePoint UnityStandardAssets.Utility.WaypointCircuit::GetRoutePoint(System.Single)
extern void WaypointCircuit_GetRoutePoint_m8D42746EC9609D8082B0E7CC8E19A2C4F27800B0 (void);
// 0x00000062 UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::GetRoutePosition(System.Single)
extern void WaypointCircuit_GetRoutePosition_m0973D453CE3244E6E1066641A81AD3FF0EADBB6F (void);
// 0x00000063 UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::CatmullRom(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void WaypointCircuit_CatmullRom_mD59EEE7A548ACAE81B8650873BCE7547CEA63077 (void);
// 0x00000064 System.Void UnityStandardAssets.Utility.WaypointCircuit::CachePositionsAndDistances()
extern void WaypointCircuit_CachePositionsAndDistances_m69B6E9F8A8CA46B05F025FD09309FC126FCF6F46 (void);
// 0x00000065 System.Void UnityStandardAssets.Utility.WaypointCircuit::OnDrawGizmos()
extern void WaypointCircuit_OnDrawGizmos_m69BAAF20DC9C4E903B5BA88D0DE0D30D95456287 (void);
// 0x00000066 System.Void UnityStandardAssets.Utility.WaypointCircuit::OnDrawGizmosSelected()
extern void WaypointCircuit_OnDrawGizmosSelected_m07DB42CB844CC4517075DF5AFFDC5F2AAFDE8134 (void);
// 0x00000067 System.Void UnityStandardAssets.Utility.WaypointCircuit::DrawGizmos(System.Boolean)
extern void WaypointCircuit_DrawGizmos_m614C51310D7116AECB903F6CA4A3047F819C9520 (void);
// 0x00000068 System.Void UnityStandardAssets.Utility.WaypointCircuit::.ctor()
extern void WaypointCircuit__ctor_m349B1FD72D470FC31D2C7D18D0625C3DE68C9304 (void);
// 0x00000069 UnityStandardAssets.Utility.WaypointCircuit_RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_targetPoint()
extern void WaypointProgressTracker_get_targetPoint_mF25566A72CA099496F6C921209690D340E7E1067 (void);
// 0x0000006A System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_targetPoint(UnityStandardAssets.Utility.WaypointCircuit_RoutePoint)
extern void WaypointProgressTracker_set_targetPoint_m9FF276072D75E5B62BDFDD69FE2BC764B332E636 (void);
// 0x0000006B UnityStandardAssets.Utility.WaypointCircuit_RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_speedPoint()
extern void WaypointProgressTracker_get_speedPoint_mD1DA662D3473D8A387FA4B9217E024C9F86ECE32 (void);
// 0x0000006C System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_speedPoint(UnityStandardAssets.Utility.WaypointCircuit_RoutePoint)
extern void WaypointProgressTracker_set_speedPoint_m5CCCC560F087199E1A30537AF62CAAE4EFDA1062 (void);
// 0x0000006D UnityStandardAssets.Utility.WaypointCircuit_RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_progressPoint()
extern void WaypointProgressTracker_get_progressPoint_m2CBA2DD7271E5B4DCBEDE3D4A583DDCBAF7843CD (void);
// 0x0000006E System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_progressPoint(UnityStandardAssets.Utility.WaypointCircuit_RoutePoint)
extern void WaypointProgressTracker_set_progressPoint_mDB352CD4E635034EF5155F6661F200CCDE51B5F7 (void);
// 0x0000006F System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Start()
extern void WaypointProgressTracker_Start_m3AE39D9788836A78979E71F8F6BC3109BD843341 (void);
// 0x00000070 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Reset()
extern void WaypointProgressTracker_Reset_m71BA52616EDE5E48445AAC8163F57CD90A5B3602 (void);
// 0x00000071 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Update()
extern void WaypointProgressTracker_Update_mCF073672622D91EF9F70ED8E870A6E4400ABD694 (void);
// 0x00000072 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::OnDrawGizmos()
extern void WaypointProgressTracker_OnDrawGizmos_mAE2FA8689934F94640161CFA136411E825660EC9 (void);
// 0x00000073 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::.ctor()
extern void WaypointProgressTracker__ctor_m6832B310DE7A3327A4AC027F4CD32D1D47D33722 (void);
// 0x00000074 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnEnable()
extern void AfterburnerPhysicsForce_OnEnable_m31ABCDDA8E500224F25244C9D3FE44CBDD45D7CE (void);
// 0x00000075 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::FixedUpdate()
extern void AfterburnerPhysicsForce_FixedUpdate_m90286B7CDABBED931E9D13E5D879E6EC41BF6E8A (void);
// 0x00000076 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnDrawGizmosSelected()
extern void AfterburnerPhysicsForce_OnDrawGizmosSelected_m8E75C894A4544C40EB1FD3DA8883E3358BAF8AA3 (void);
// 0x00000077 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::.ctor()
extern void AfterburnerPhysicsForce__ctor_mE4F415134C355B0C418241BA9F4BE2DAB65BF1A0 (void);
// 0x00000078 System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionFireAndDebris::Start()
extern void ExplosionFireAndDebris_Start_m85654B2EA9348AE14094A4CDCA1ECD714E4CBC22 (void);
// 0x00000079 System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::AddFire(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ExplosionFireAndDebris_AddFire_m781F3E400896C4F2E2B4A24FAEFE52D01ED26C0B (void);
// 0x0000007A System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::.ctor()
extern void ExplosionFireAndDebris__ctor_m6E9B3DD674A9E1DF3B85ECF3E618F7F29B5EB97E (void);
// 0x0000007B System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionPhysicsForce::Start()
extern void ExplosionPhysicsForce_Start_m22750184987B8A375DA674EF615F5ECD1DAD04C5 (void);
// 0x0000007C System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce::.ctor()
extern void ExplosionPhysicsForce__ctor_m3A4FFA852D5C736E4CE47A5A5A3E0CA1E954F660 (void);
// 0x0000007D System.Void UnityStandardAssets.Effects.Explosive::Start()
extern void Explosive_Start_m500D588F6ABB1C03488B93AE78086A368943CDE2 (void);
// 0x0000007E System.Collections.IEnumerator UnityStandardAssets.Effects.Explosive::OnCollisionEnter(UnityEngine.Collision)
extern void Explosive_OnCollisionEnter_mB88EB7BF1526BC0F9418D997273E1A1C875D00E1 (void);
// 0x0000007F System.Void UnityStandardAssets.Effects.Explosive::Reset()
extern void Explosive_Reset_m878D1F8DF175604AFB07827C7A4DA9DA20FFA9DA (void);
// 0x00000080 System.Void UnityStandardAssets.Effects.Explosive::.ctor()
extern void Explosive__ctor_m21EF3DDCC8F02804ED974F48197F95DB5B4900F4 (void);
// 0x00000081 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Start()
extern void ExtinguishableParticleSystem_Start_m442E9165E311C7DC5AE322C5CF8B8D6A2325DC39 (void);
// 0x00000082 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Extinguish()
extern void ExtinguishableParticleSystem_Extinguish_m2E9C8C61E7E4E1083A3059582EE9F68BB0D8EFA8 (void);
// 0x00000083 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::.ctor()
extern void ExtinguishableParticleSystem__ctor_m66F7FA6921D145A9EF55397A60FCC4CA574DA068 (void);
// 0x00000084 System.Void UnityStandardAssets.Effects.FireLight::Start()
extern void FireLight_Start_mBC43BC2A9EDAD47A5D43C96C04E1AECC4405B81D (void);
// 0x00000085 System.Void UnityStandardAssets.Effects.FireLight::Update()
extern void FireLight_Update_mE6D64BEF14D90624104BC092626B738B75D8D0F7 (void);
// 0x00000086 System.Void UnityStandardAssets.Effects.FireLight::Extinguish()
extern void FireLight_Extinguish_m96CC75996F151C74E4D46556599FDF4F39AF6776 (void);
// 0x00000087 System.Void UnityStandardAssets.Effects.FireLight::.ctor()
extern void FireLight__ctor_mFAEAECD04029DD5E24A331FEE4B5C43FFD5B6FBE (void);
// 0x00000088 System.Void UnityStandardAssets.Effects.Hose::Update()
extern void Hose_Update_mF09247CA136AF92D5586DD9C95823C7E9A6AE939 (void);
// 0x00000089 System.Void UnityStandardAssets.Effects.Hose::.ctor()
extern void Hose__ctor_mDB316C87F32CA6A88980009F13FCB35475103094 (void);
// 0x0000008A System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::Start()
extern void ParticleSystemMultiplier_Start_m9629829804A66FA8E1BA25039F0BCD4447D1785D (void);
// 0x0000008B System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::.ctor()
extern void ParticleSystemMultiplier__ctor_mFA383D5A7F8469462B1E9A5EDB01811D52DC0CFC (void);
// 0x0000008C System.Void UnityStandardAssets.Effects.SmokeParticles::Start()
extern void SmokeParticles_Start_m1A4CBEB9EDF7F792FCF7F96D9435B6C0B0A94DD6 (void);
// 0x0000008D System.Void UnityStandardAssets.Effects.SmokeParticles::.ctor()
extern void SmokeParticles__ctor_m6FF23CF3C3FFDCE57F75C6DA08F84101A4953F0C (void);
// 0x0000008E System.Void UnityStandardAssets.Effects.WaterHoseParticles::Start()
extern void WaterHoseParticles_Start_m6CFC5FEBF60CBD00CDE17E0A1EEC12163EC9A792 (void);
// 0x0000008F System.Void UnityStandardAssets.Effects.WaterHoseParticles::OnParticleCollision(UnityEngine.GameObject)
extern void WaterHoseParticles_OnParticleCollision_m8E8399822017058F9AE64E3D9F7B47208553ED70 (void);
// 0x00000090 System.Void UnityStandardAssets.Effects.WaterHoseParticles::.ctor()
extern void WaterHoseParticles__ctor_m9E63BF105FF8511932CBF7FAA1C664713744DCA3 (void);
// 0x00000091 System.Void UnityStandardAssets.Water.WaterBasic::Update()
extern void WaterBasic_Update_mEEEE08B8B6B13BE9FBA6F084A2156358BC26D6C8 (void);
// 0x00000092 System.Void UnityStandardAssets.Water.WaterBasic::.ctor()
extern void WaterBasic__ctor_m49406BA46E86FA0F0F9B38EB5C722A8E6A5818AA (void);
// 0x00000093 System.Void UnityStandardAssets.Water.Displace::Awake()
extern void Displace_Awake_m61C8CDD475DBFF95E5F49AB4BACE14195072498C (void);
// 0x00000094 System.Void UnityStandardAssets.Water.Displace::OnEnable()
extern void Displace_OnEnable_mC321CC696070DF2589D76CCDC75368C26F7229B2 (void);
// 0x00000095 System.Void UnityStandardAssets.Water.Displace::OnDisable()
extern void Displace_OnDisable_mC4144199F8E22EBE3DA4A0F12F8E1DE5242DF2AA (void);
// 0x00000096 System.Void UnityStandardAssets.Water.Displace::.ctor()
extern void Displace__ctor_m4AE5636C99B0D5EA3302D02AEB66EA40CE8373F8 (void);
// 0x00000097 System.Void UnityStandardAssets.Water.GerstnerDisplace::.ctor()
extern void GerstnerDisplace__ctor_m35315933124DD20E54791E269DB4D70C015A2254 (void);
// 0x00000098 System.Void UnityStandardAssets.Water.MeshContainer::.ctor(UnityEngine.Mesh)
extern void MeshContainer__ctor_m7AFFB5FA9DF2A68C6FD1D0D74D454C08FC3C2D6F (void);
// 0x00000099 System.Void UnityStandardAssets.Water.MeshContainer::Update()
extern void MeshContainer_Update_mFB60BB3EA3E2208F955B8B3E5E3B7D6DA02CBE24 (void);
// 0x0000009A System.Void UnityStandardAssets.Water.PlanarReflection::Start()
extern void PlanarReflection_Start_m080C3900D498A758F7C3D91521483C1918ACD017 (void);
// 0x0000009B UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::CreateReflectionCameraFor(UnityEngine.Camera)
extern void PlanarReflection_CreateReflectionCameraFor_mC1D4652399484BA52AE739A3B5BC9097134F3034 (void);
// 0x0000009C System.Void UnityStandardAssets.Water.PlanarReflection::SetStandardCameraParameter(UnityEngine.Camera,UnityEngine.LayerMask)
extern void PlanarReflection_SetStandardCameraParameter_m6EDE582E77457B8979E2541C7AC5254B8B06EB9D (void);
// 0x0000009D UnityEngine.RenderTexture UnityStandardAssets.Water.PlanarReflection::CreateTextureFor(UnityEngine.Camera)
extern void PlanarReflection_CreateTextureFor_mEB5585E46857A99E2B6D5BA5A64D1096142379CC (void);
// 0x0000009E System.Void UnityStandardAssets.Water.PlanarReflection::RenderHelpCameras(UnityEngine.Camera)
extern void PlanarReflection_RenderHelpCameras_m2E41F43A249C5914135F688C2A399A1F1136D8CE (void);
// 0x0000009F System.Void UnityStandardAssets.Water.PlanarReflection::LateUpdate()
extern void PlanarReflection_LateUpdate_mBBC544AFAC39678265F0EA5FA99300F27A4E7A2A (void);
// 0x000000A0 System.Void UnityStandardAssets.Water.PlanarReflection::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern void PlanarReflection_WaterTileBeingRendered_m548DA1220CA714D157B2BCE1FDA9B62452E44BF5 (void);
// 0x000000A1 System.Void UnityStandardAssets.Water.PlanarReflection::OnEnable()
extern void PlanarReflection_OnEnable_mB1326BB27BA4555CAC9D7B377628E29E60671465 (void);
// 0x000000A2 System.Void UnityStandardAssets.Water.PlanarReflection::OnDisable()
extern void PlanarReflection_OnDisable_m0D7B36CE5C749EBE3F83B7F3EB1CB9EBBE74E333 (void);
// 0x000000A3 System.Void UnityStandardAssets.Water.PlanarReflection::RenderReflectionFor(UnityEngine.Camera,UnityEngine.Camera)
extern void PlanarReflection_RenderReflectionFor_m1E44B323B3346A1DF17715767DE62F73678CC525 (void);
// 0x000000A4 System.Void UnityStandardAssets.Water.PlanarReflection::SaneCameraSettings(UnityEngine.Camera)
extern void PlanarReflection_SaneCameraSettings_mC1302B40E9D18A8158C450711F94681F8CB3DEC2 (void);
// 0x000000A5 UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern void PlanarReflection_CalculateObliqueMatrix_mFD6BB9C5ECC438DD6F5C9C0F8F65CFB2F36D1866 (void);
// 0x000000A6 UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateReflectionMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern void PlanarReflection_CalculateReflectionMatrix_mD04A2F12363F240EF2C67FF52519839FA4F14A5C (void);
// 0x000000A7 System.Single UnityStandardAssets.Water.PlanarReflection::Sgn(System.Single)
extern void PlanarReflection_Sgn_m5B43D2788FE8B53ABD0A8F0126AE33F299540B91 (void);
// 0x000000A8 UnityEngine.Vector4 UnityStandardAssets.Water.PlanarReflection::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void PlanarReflection_CameraSpacePlane_m2FC7140D1B54C86880E7AF73418F39F7026D278A (void);
// 0x000000A9 System.Void UnityStandardAssets.Water.PlanarReflection::.ctor()
extern void PlanarReflection__ctor_m43064A2D31F467165DF112691A5C8A24EBF591BE (void);
// 0x000000AA System.Void UnityStandardAssets.Water.SpecularLighting::Start()
extern void SpecularLighting_Start_m10E337FAAECD39FF2A7D9EEF2E8A5DFA70186923 (void);
// 0x000000AB System.Void UnityStandardAssets.Water.SpecularLighting::Update()
extern void SpecularLighting_Update_mA6ACED168A4954AAE814DE5C8A17E832F93D28FE (void);
// 0x000000AC System.Void UnityStandardAssets.Water.SpecularLighting::.ctor()
extern void SpecularLighting__ctor_m575A28F9197B313E49CAFD95AC5B890B36B917FD (void);
// 0x000000AD System.Void UnityStandardAssets.Water.Water::OnWillRenderObject()
extern void Water_OnWillRenderObject_mDE2C1FD54029095E1061CF810E120835733E28A8 (void);
// 0x000000AE System.Void UnityStandardAssets.Water.Water::OnDisable()
extern void Water_OnDisable_mF858B4AF06C20E874842E99A946BAC0E7A2C2CA2 (void);
// 0x000000AF System.Void UnityStandardAssets.Water.Water::Update()
extern void Water_Update_m5EF099F5001646EE42E97C1B5EC27E751F8AA3A1 (void);
// 0x000000B0 System.Void UnityStandardAssets.Water.Water::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern void Water_UpdateCameraModes_m52D76E61B982584208C2A4A98B7695C53C5A55A6 (void);
// 0x000000B1 System.Void UnityStandardAssets.Water.Water::CreateWaterObjects(UnityEngine.Camera,UnityEngine.Camera&,UnityEngine.Camera&)
extern void Water_CreateWaterObjects_mF2E91CE0B823EAB2E86B4D9FF3A70CF95467FB4D (void);
// 0x000000B2 UnityStandardAssets.Water.Water_WaterMode UnityStandardAssets.Water.Water::GetWaterMode()
extern void Water_GetWaterMode_mE8C165BBAB7C969AA8E0C95679DB1E37330501A8 (void);
// 0x000000B3 UnityStandardAssets.Water.Water_WaterMode UnityStandardAssets.Water.Water::FindHardwareWaterSupport()
extern void Water_FindHardwareWaterSupport_mF01DDA5818E2E789020B8069439EE6CEBE7D6CC4 (void);
// 0x000000B4 UnityEngine.Vector4 UnityStandardAssets.Water.Water::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Water_CameraSpacePlane_mF0515845AD37744BF290009E27B5C926951FCD0B (void);
// 0x000000B5 System.Void UnityStandardAssets.Water.Water::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern void Water_CalculateReflectionMatrix_mECC5B0D91E795E07EEEE661E632B26037819DAC1 (void);
// 0x000000B6 System.Void UnityStandardAssets.Water.Water::.ctor()
extern void Water__ctor_mDF27A1B8260BF800899BF28EC4325AFC3BB28741 (void);
// 0x000000B7 System.Void UnityStandardAssets.Water.WaterBase::UpdateShader()
extern void WaterBase_UpdateShader_m9983EF83D7AE22E03EBEAD0A092DA47966AAFD8E (void);
// 0x000000B8 System.Void UnityStandardAssets.Water.WaterBase::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern void WaterBase_WaterTileBeingRendered_mDB8CFDD6A7C116E854488EE804DBCB5709779A5C (void);
// 0x000000B9 System.Void UnityStandardAssets.Water.WaterBase::Update()
extern void WaterBase_Update_m3E55AB0CF58C6A85641A6DD30229A874BB2204B3 (void);
// 0x000000BA System.Void UnityStandardAssets.Water.WaterBase::.ctor()
extern void WaterBase__ctor_m82F74340A1EB4DEA5EC6D8B94835F5C523013B81 (void);
// 0x000000BB System.Void UnityStandardAssets.Water.WaterTile::Start()
extern void WaterTile_Start_m7304AD008CF318463F0C8BF6334D323FE4B2201F (void);
// 0x000000BC System.Void UnityStandardAssets.Water.WaterTile::AcquireComponents()
extern void WaterTile_AcquireComponents_m9AF4968726C7A7A1914C99383AC33F70F0734CAB (void);
// 0x000000BD System.Void UnityStandardAssets.Water.WaterTile::OnWillRenderObject()
extern void WaterTile_OnWillRenderObject_m49C59909BFF5850B610AE766A77A09732700A16E (void);
// 0x000000BE System.Void UnityStandardAssets.Water.WaterTile::.ctor()
extern void WaterTile__ctor_m9F814F1C72EC77EA173735BC25383BB020C9643E (void);
// 0x000000BF System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnEnable()
extern void AxisTouchButton_OnEnable_mCB6F2E22CE0ED2867462D2B6477319CA34EC6923 (void);
// 0x000000C0 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::FindPairedButton()
extern void AxisTouchButton_FindPairedButton_m40E5A08627D81FC2C9B18410E0A315A00AFB8E5E (void);
// 0x000000C1 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnDisable()
extern void AxisTouchButton_OnDisable_mA60CC8A5ACA0AF8EA245F67C774CC15489D26F0D (void);
// 0x000000C2 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void AxisTouchButton_OnPointerDown_mF8ED454A7629EC10D5D65B0806B1575B969CC151 (void);
// 0x000000C3 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void AxisTouchButton_OnPointerUp_m47ACA384B0AB0496024E2BA07670DE7F416442A6 (void);
// 0x000000C4 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::.ctor()
extern void AxisTouchButton__ctor_m92FC868F9C30069B6E82AEFB601E72D7958146EB (void);
// 0x000000C5 System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::OnEnable()
extern void ButtonHandler_OnEnable_mF4257EC750A191164B42AA7ED19E95221E0C9084 (void);
// 0x000000C6 System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetDownState()
extern void ButtonHandler_SetDownState_m22A563B85C0CFC7883586FE49D47EC8F4795740E (void);
// 0x000000C7 System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetUpState()
extern void ButtonHandler_SetUpState_m20D0A07A6047D866EB95EB5D32D6D5C208CF779D (void);
// 0x000000C8 System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisPositiveState()
extern void ButtonHandler_SetAxisPositiveState_m2914614CA82F6CE2BCDEF41198D6505C08692F60 (void);
// 0x000000C9 System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNeutralState()
extern void ButtonHandler_SetAxisNeutralState_m710F8A130253345840FC6A942453EBD386322691 (void);
// 0x000000CA System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNegativeState()
extern void ButtonHandler_SetAxisNegativeState_m57D4FA1CCB8E9F4CCB3D5AB589D57328EBDA782C (void);
// 0x000000CB System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::Update()
extern void ButtonHandler_Update_m84201A612F81A8C08E1E4F78635C47098A9C6BC0 (void);
// 0x000000CC System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::.ctor()
extern void ButtonHandler__ctor_m85B4D66D1CA83E5A73BFC70CAFB4F2D00F559795 (void);
// 0x000000CD System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::.cctor()
extern void CrossPlatformInputManager__cctor_m62BA59D81597A25895815DDB40DD8944A6B804F2 (void);
// 0x000000CE System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SwitchActiveInputMethod(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_ActiveInputMethod)
extern void CrossPlatformInputManager_SwitchActiveInputMethod_m3BAB791534CAB7AA957901F370FE854C63AB1801 (void);
// 0x000000CF System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::AxisExists(System.String)
extern void CrossPlatformInputManager_AxisExists_m13A4ED5F88BAC335FDC42ADD4AAC9BB4CC5809F9 (void);
// 0x000000D0 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::ButtonExists(System.String)
extern void CrossPlatformInputManager_ButtonExists_m6C0E094AF61CF2F0F6592EFA67763D208C867440 (void);
// 0x000000D1 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::RegisterVirtualAxis(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis)
extern void CrossPlatformInputManager_RegisterVirtualAxis_m84945297F5E2C4D218B59B76E9D90D3BD36198A4 (void);
// 0x000000D2 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::RegisterVirtualButton(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton)
extern void CrossPlatformInputManager_RegisterVirtualButton_mA5218520E9EE798325C72DFD0C988DC313D36BCF (void);
// 0x000000D3 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::UnRegisterVirtualAxis(System.String)
extern void CrossPlatformInputManager_UnRegisterVirtualAxis_m33DCEB8DAAF2703BFAB8F156A6633C0F4316C1A4 (void);
// 0x000000D4 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::UnRegisterVirtualButton(System.String)
extern void CrossPlatformInputManager_UnRegisterVirtualButton_m4B8F22F23F0891C1F5D4C07B729564D6A95CB82D (void);
// 0x000000D5 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::VirtualAxisReference(System.String)
extern void CrossPlatformInputManager_VirtualAxisReference_m5864A44C3FE72270B22D4C97FADEEB2AAA77869D (void);
// 0x000000D6 System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String)
extern void CrossPlatformInputManager_GetAxis_m4D45F9BE30A159DA4E72F4BF8294872297566E2D (void);
// 0x000000D7 System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxisRaw(System.String)
extern void CrossPlatformInputManager_GetAxisRaw_mE6D8754EAE5F6838CCF172FB03F4C251648EE987 (void);
// 0x000000D8 System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String,System.Boolean)
extern void CrossPlatformInputManager_GetAxis_mC9F177F6F0D83131B599CF80C3F3A8D7AD4568A0 (void);
// 0x000000D9 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButton(System.String)
extern void CrossPlatformInputManager_GetButton_m728A64B9BC3F6471EB11B9CAF54BD4A10C710207 (void);
// 0x000000DA System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonDown(System.String)
extern void CrossPlatformInputManager_GetButtonDown_mE1BCD85447E0EF510728E49314FBCCEEE1FC7E8D (void);
// 0x000000DB System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonUp(System.String)
extern void CrossPlatformInputManager_GetButtonUp_mD115A6BD45062A08A42EBBC7F0C9EC0D4F764ADD (void);
// 0x000000DC System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonDown(System.String)
extern void CrossPlatformInputManager_SetButtonDown_m4DBFE81592B86D460ACC34D5936C788CD5B50890 (void);
// 0x000000DD System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonUp(System.String)
extern void CrossPlatformInputManager_SetButtonUp_m6228A0BD77568A903DF6429EEACD2267028FA32A (void);
// 0x000000DE System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisPositive(System.String)
extern void CrossPlatformInputManager_SetAxisPositive_mC5C7F88EEF5D6CB7B6B91BF6279FA53A94B4D527 (void);
// 0x000000DF System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisNegative(System.String)
extern void CrossPlatformInputManager_SetAxisNegative_m41A74CBE51E8CB4870C79A8343E66B99B2CA7FDB (void);
// 0x000000E0 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisZero(System.String)
extern void CrossPlatformInputManager_SetAxisZero_mBBD24590C97037F84384A559AAE37D2F8CA51730 (void);
// 0x000000E1 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxis(System.String,System.Single)
extern void CrossPlatformInputManager_SetAxis_m6BCE358D3D1A2E5E393AF281602B3E4745C0C5DA (void);
// 0x000000E2 UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::get_mousePosition()
extern void CrossPlatformInputManager_get_mousePosition_mC886FC2F654E91F06407FDB891DF3201ED576DCD (void);
// 0x000000E3 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionX(System.Single)
extern void CrossPlatformInputManager_SetVirtualMousePositionX_m1800042FCD90010EA2E2D51969D971324DD11964 (void);
// 0x000000E4 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionY(System.Single)
extern void CrossPlatformInputManager_SetVirtualMousePositionY_mDD4A2DF42E6CD673054A91FFE3C7FA61812889A8 (void);
// 0x000000E5 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionZ(System.Single)
extern void CrossPlatformInputManager_SetVirtualMousePositionZ_m121058A0846AE6A974855607C8E3D46C221B376F (void);
// 0x000000E6 System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::Update()
extern void InputAxisScrollbar_Update_m4B6A6BBF4FAED786086BE4F9997E1D2D373BF2FE (void);
// 0x000000E7 System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::HandleInput(System.Single)
extern void InputAxisScrollbar_HandleInput_mF3A427E653ED917C3E91E0CBB1A3990F6110FB11 (void);
// 0x000000E8 System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::.ctor()
extern void InputAxisScrollbar__ctor_mB96FAA176CD2958CCDE6E5F9212DCF2082486243 (void);
// 0x000000E9 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnEnable()
extern void Joystick_OnEnable_m8728113F5BEE6D91514CB1A07550E8E7A3856CEE (void);
// 0x000000EA System.Void UnityStandardAssets.CrossPlatformInput.Joystick::Start()
extern void Joystick_Start_m6164BF9BB7A0A8DD4524223639EA549E0491CFFD (void);
// 0x000000EB System.Void UnityStandardAssets.CrossPlatformInput.Joystick::UpdateVirtualAxes(UnityEngine.Vector3)
extern void Joystick_UpdateVirtualAxes_m5B79E0FBC765F85D9EE7FA9C7D74BDB35F326F3E (void);
// 0x000000EC System.Void UnityStandardAssets.CrossPlatformInput.Joystick::CreateVirtualAxes()
extern void Joystick_CreateVirtualAxes_mCD13DFD2ADED0444F18C3856FD67A78539FD9C2C (void);
// 0x000000ED System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnDrag_m1DC1103944EB982931C5946BD8EBFB8E63073BB6 (void);
// 0x000000EE System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerUp_m06850F5D6C95D16DEB57B3FC4E50CCBCCD0EF7FB (void);
// 0x000000EF System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerDown_m1F10B670117FD67A734079ED71D4A3D36B783718 (void);
// 0x000000F0 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnDisable()
extern void Joystick_OnDisable_m5097E08289FECC9A5499DB0747575F075353CAFB (void);
// 0x000000F1 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::.ctor()
extern void Joystick__ctor_mA2C408B1EB0671CB8B340DBF932CB4153BAC3ABF (void);
// 0x000000F2 System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::OnEnable()
extern void MobileControlRig_OnEnable_mAF3C7A8C67CE239A1FD5E6A8B224F7A91DE8B2E8 (void);
// 0x000000F3 System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::Start()
extern void MobileControlRig_Start_m43792FB70FC02989DA9543801183A54005AD572B (void);
// 0x000000F4 System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::CheckEnableControlRig()
extern void MobileControlRig_CheckEnableControlRig_m59A8FCD09B2A6EA7702AE9EBB24E3BB9605B5CCD (void);
// 0x000000F5 System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::EnableControlRig(System.Boolean)
extern void MobileControlRig_EnableControlRig_m694051D1F28B05510357A3F96561EAF2732CAF8E (void);
// 0x000000F6 System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::.ctor()
extern void MobileControlRig__ctor_mCCFD8CBDA57F8D9B0E2805D4740637F5FFC4B120 (void);
// 0x000000F7 System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnEnable()
extern void TiltInput_OnEnable_mEACF194C56E3620055240D9D46880E6F9C201E9C (void);
// 0x000000F8 System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::Update()
extern void TiltInput_Update_m14D22BD1D9D47DF03965F0F6BBC2FB0E322F2B2A (void);
// 0x000000F9 System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnDisable()
extern void TiltInput_OnDisable_m169AFDCFFA0609747DA889DF88C86D0A5C9C42B0 (void);
// 0x000000FA System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::.ctor()
extern void TiltInput__ctor_m909CBEC7014B584CB5EBF5A0B650E5D8845E4FB7 (void);
// 0x000000FB System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnEnable()
extern void TouchPad_OnEnable_mFED012C0FDD349798B1296799C0AE7A05C2017EE (void);
// 0x000000FC System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::Start()
extern void TouchPad_Start_m986951F12FF80D3A0D4B0DCB49D9FBA13B8A025C (void);
// 0x000000FD System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::CreateVirtualAxes()
extern void TouchPad_CreateVirtualAxes_m214CE099E087A7A6FCDDF3B2740983436B62BA5E (void);
// 0x000000FE System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::UpdateVirtualAxes(UnityEngine.Vector3)
extern void TouchPad_UpdateVirtualAxes_m42D25C5EE9F890FECF580C97219455E73D09AF67 (void);
// 0x000000FF System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void TouchPad_OnPointerDown_m13FDEACD95785D853D85B68E4993AC520A2D771F (void);
// 0x00000100 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::Update()
extern void TouchPad_Update_m0DD077DCE945CC47C3DD4FFDB9FEC5D4BB3A762A (void);
// 0x00000101 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TouchPad_OnPointerUp_mE35C9A5F6CD1909E8819F4D6D7282C7D20B37B88 (void);
// 0x00000102 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnDisable()
extern void TouchPad_OnDisable_mD0E67236EB0D365E3397D26723250C01614168B5 (void);
// 0x00000103 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::.ctor()
extern void TouchPad__ctor_m9FEC2CD43CD850304B41B1C0142CC47F44B01E25 (void);
// 0x00000104 UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::get_virtualMousePosition()
extern void VirtualInput_get_virtualMousePosition_m897C50683722D1C3DF4FA9801524E7BF310B24BD (void);
// 0x00000105 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::set_virtualMousePosition(UnityEngine.Vector3)
extern void VirtualInput_set_virtualMousePosition_mDDF9F35B2C4AC37AB6CCF68772C57315612B1F75 (void);
// 0x00000106 System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::AxisExists(System.String)
extern void VirtualInput_AxisExists_mDB6E7D0AF32ECE3E3CB1C4DA089D4B030D61F3F8 (void);
// 0x00000107 System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::ButtonExists(System.String)
extern void VirtualInput_ButtonExists_mBD9401EC2186C54F8EA7577FEEA500624F2E6083 (void);
// 0x00000108 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::RegisterVirtualAxis(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis)
extern void VirtualInput_RegisterVirtualAxis_m43BC4BC9355B708CC739E3F2D0761A49342BC60F (void);
// 0x00000109 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::RegisterVirtualButton(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton)
extern void VirtualInput_RegisterVirtualButton_mF6874262B94F78D0C2C166F7E20CFA47DD39BF41 (void);
// 0x0000010A System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::UnRegisterVirtualAxis(System.String)
extern void VirtualInput_UnRegisterVirtualAxis_mD3511EE52A02EF720B086FF6EDCF9D4FA11A551F (void);
// 0x0000010B System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::UnRegisterVirtualButton(System.String)
extern void VirtualInput_UnRegisterVirtualButton_mF05E241BD753B335E97CB8D1EDCFECE82A34F554 (void);
// 0x0000010C UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis UnityStandardAssets.CrossPlatformInput.VirtualInput::VirtualAxisReference(System.String)
extern void VirtualInput_VirtualAxisReference_m5AE323533C7DF65D71B551B173A63680BB5850EA (void);
// 0x0000010D System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionX(System.Single)
extern void VirtualInput_SetVirtualMousePositionX_m49716B45CE295686844FDD803083136B9BAC2124 (void);
// 0x0000010E System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionY(System.Single)
extern void VirtualInput_SetVirtualMousePositionY_m80139449D4E09227D929E314419B1C72D57BD001 (void);
// 0x0000010F System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionZ(System.Single)
extern void VirtualInput_SetVirtualMousePositionZ_m9276A4D39BC31E00C1977B2621549B1C1F40E51D (void);
// 0x00000110 System.Single UnityStandardAssets.CrossPlatformInput.VirtualInput::GetAxis(System.String,System.Boolean)
// 0x00000111 System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButton(System.String)
// 0x00000112 System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonDown(System.String)
// 0x00000113 System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonUp(System.String)
// 0x00000114 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonDown(System.String)
// 0x00000115 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonUp(System.String)
// 0x00000116 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisPositive(System.String)
// 0x00000117 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisNegative(System.String)
// 0x00000118 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisZero(System.String)
// 0x00000119 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxis(System.String,System.Single)
// 0x0000011A UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::MousePosition()
// 0x0000011B System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::.ctor()
extern void VirtualInput__ctor_mD6A4228D372182ABC7372ED25F4987CE1EAA27CB (void);
// 0x0000011C System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::AddButton(System.String)
extern void MobileInput_AddButton_m55B4ECB00F31F0904145B5DC71AE2B7289960F34 (void);
// 0x0000011D System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::AddAxes(System.String)
extern void MobileInput_AddAxes_mF5065897FC94197F4FD5BDD15A394E858218496E (void);
// 0x0000011E System.Single UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetAxis(System.String,System.Boolean)
extern void MobileInput_GetAxis_m24CDEC7DA08736467196B8F90F19B3110782421A (void);
// 0x0000011F System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetButtonDown(System.String)
extern void MobileInput_SetButtonDown_mF3C9EEAF5750B7CF53C0D6D04D035CA8F1D27547 (void);
// 0x00000120 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetButtonUp(System.String)
extern void MobileInput_SetButtonUp_m0CEDEA05459505931FB2686C20AC0900A4941448 (void);
// 0x00000121 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisPositive(System.String)
extern void MobileInput_SetAxisPositive_mD4522AE0A5CFA591D720C9FA1E42D38485F66C9A (void);
// 0x00000122 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisNegative(System.String)
extern void MobileInput_SetAxisNegative_mDB7F89D127295F2D4CC4764EB04571D9A46774C4 (void);
// 0x00000123 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisZero(System.String)
extern void MobileInput_SetAxisZero_mCE681BFD720000CFA939C78B0EAEFCA3D5748BA8 (void);
// 0x00000124 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxis(System.String,System.Single)
extern void MobileInput_SetAxis_m40791B9F5D8B28086FEA1030918A6DDBD96D2704 (void);
// 0x00000125 System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButtonDown(System.String)
extern void MobileInput_GetButtonDown_m8B7EC91AD10FF37A6910CC3AD684572F8CC4A403 (void);
// 0x00000126 System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButtonUp(System.String)
extern void MobileInput_GetButtonUp_mFB0BF6CE172238F3AF6B28CB16A73B5A0D714ABB (void);
// 0x00000127 System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButton(System.String)
extern void MobileInput_GetButton_m1805C5AAFEA6C56E1F083C318C7D8A56414742DC (void);
// 0x00000128 UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::MousePosition()
extern void MobileInput_MousePosition_m655B9F793060E92EEAFC358ED5A612124F71B234 (void);
// 0x00000129 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::.ctor()
extern void MobileInput__ctor_m58D4C2380917920DD39E646CB4717F6EFBAA16F0 (void);
// 0x0000012A System.Single UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetAxis(System.String,System.Boolean)
extern void StandaloneInput_GetAxis_m208A36BD2256D5439E8BF99DFEE7C4FBE5C321DB (void);
// 0x0000012B System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButton(System.String)
extern void StandaloneInput_GetButton_m2156BA026DDB9F6FA9F45BBC8FEC871A3090629C (void);
// 0x0000012C System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButtonDown(System.String)
extern void StandaloneInput_GetButtonDown_m3F88DFF900E2AB8729E6F63694BE3C8E2C19BBB7 (void);
// 0x0000012D System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButtonUp(System.String)
extern void StandaloneInput_GetButtonUp_mDF55E35A4B50D58901CEEF8DAEECD050A636398C (void);
// 0x0000012E System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetButtonDown(System.String)
extern void StandaloneInput_SetButtonDown_m5C1B0E5ED19F91DAEE8A23108865EBC57EB3F002 (void);
// 0x0000012F System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetButtonUp(System.String)
extern void StandaloneInput_SetButtonUp_m23ECA36E7E2C9D79650FC93764E2FB47C52A6269 (void);
// 0x00000130 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisPositive(System.String)
extern void StandaloneInput_SetAxisPositive_m45ABA3A91481B6B07E37A24322C345DE4341472D (void);
// 0x00000131 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisNegative(System.String)
extern void StandaloneInput_SetAxisNegative_m8ABB3422B35FA3D5EFEC9A3BCCE7D813C91E1DDA (void);
// 0x00000132 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisZero(System.String)
extern void StandaloneInput_SetAxisZero_mC0123C06F1DD19FF6DB9353DF0D711554B56B428 (void);
// 0x00000133 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxis(System.String,System.Single)
extern void StandaloneInput_SetAxis_m620AB31F30F82FDB44EB995E494F618D8C42F765 (void);
// 0x00000134 UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::MousePosition()
extern void StandaloneInput_MousePosition_m7D158ACA958E8C1101AFF4B3282E12B1A6EF7C82 (void);
// 0x00000135 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::.ctor()
extern void StandaloneInput__ctor_m9D98FDF717857FB17019872D7A8446C224C493F3 (void);
// 0x00000136 System.Void UnityStandardAssets.Vehicles.Car.BrakeLight::Start()
extern void BrakeLight_Start_m6FA4032B993EFE5F1059BE1C01E7C1D2A0BF346D (void);
// 0x00000137 System.Void UnityStandardAssets.Vehicles.Car.BrakeLight::Update()
extern void BrakeLight_Update_m58A9D7EBAE1F58C2D0795168423E073993CF21A2 (void);
// 0x00000138 System.Void UnityStandardAssets.Vehicles.Car.BrakeLight::.ctor()
extern void BrakeLight__ctor_mE02A5C210554431EC725807D1B1AB86F23418297 (void);
// 0x00000139 System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::Awake()
extern void CarAIControl_Awake_mF92BDAA5D715FABE633F84D59500C746B634DCDC (void);
// 0x0000013A System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::FixedUpdate()
extern void CarAIControl_FixedUpdate_m26805A937384424C95C7B64BB05B7D9D021C4D0B (void);
// 0x0000013B System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::OnCollisionStay(UnityEngine.Collision)
extern void CarAIControl_OnCollisionStay_m81DF24158CCA819188959562DDB9BCD42F09DC71 (void);
// 0x0000013C System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::SetTarget(UnityEngine.Transform)
extern void CarAIControl_SetTarget_mCBE313C1482CA8994757D5681C72505E2EECAE12 (void);
// 0x0000013D System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::.ctor()
extern void CarAIControl__ctor_mE9F288A9A54AAD345AEAB775B0C4AC667B733A52 (void);
// 0x0000013E System.Void UnityStandardAssets.Vehicles.Car.CarAudio::StartSound()
extern void CarAudio_StartSound_m49CD6BB9F527D0D8460BB7AFCF73E5D73445233E (void);
// 0x0000013F System.Void UnityStandardAssets.Vehicles.Car.CarAudio::StopSound()
extern void CarAudio_StopSound_mE351DBBDC263DCA8533FA3EE8E77ED4BF9C9C425 (void);
// 0x00000140 System.Void UnityStandardAssets.Vehicles.Car.CarAudio::Update()
extern void CarAudio_Update_mF8869D3AB05CBB0128063D98F4832430DF6E08F8 (void);
// 0x00000141 UnityEngine.AudioSource UnityStandardAssets.Vehicles.Car.CarAudio::SetUpEngineAudioSource(UnityEngine.AudioClip)
extern void CarAudio_SetUpEngineAudioSource_m3721814137E76AD388A2359DB3AD0EBBC86EAC4F (void);
// 0x00000142 System.Single UnityStandardAssets.Vehicles.Car.CarAudio::ULerp(System.Single,System.Single,System.Single)
extern void CarAudio_ULerp_m68093FA00B1EA90D7D6F11A43F45DECE53FC9B25 (void);
// 0x00000143 System.Void UnityStandardAssets.Vehicles.Car.CarAudio::.ctor()
extern void CarAudio__ctor_mC6B7E6375DDD9B1D51181B688F076FAE0CC46483 (void);
// 0x00000144 System.Boolean UnityStandardAssets.Vehicles.Car.CarController::get_Skidding()
extern void CarController_get_Skidding_mED1EF7F4B7EF21E50AE15F9C9AE642EF870D7C08 (void);
// 0x00000145 System.Void UnityStandardAssets.Vehicles.Car.CarController::set_Skidding(System.Boolean)
extern void CarController_set_Skidding_m7643D70E3DBF7A99FD6F95AAB89CF87A45910C79 (void);
// 0x00000146 System.Single UnityStandardAssets.Vehicles.Car.CarController::get_BrakeInput()
extern void CarController_get_BrakeInput_mD6D7BDD8B48342BAE79EF8E175F933DA949BC50B (void);
// 0x00000147 System.Void UnityStandardAssets.Vehicles.Car.CarController::set_BrakeInput(System.Single)
extern void CarController_set_BrakeInput_mCDE2E14F7FA94E46FB770DC10F5D97755A698C6D (void);
// 0x00000148 System.Single UnityStandardAssets.Vehicles.Car.CarController::get_CurrentSteerAngle()
extern void CarController_get_CurrentSteerAngle_m4471982C9B82947566A2B0B543C6FFE7C2C782DD (void);
// 0x00000149 System.Single UnityStandardAssets.Vehicles.Car.CarController::get_CurrentSpeed()
extern void CarController_get_CurrentSpeed_mD6B1EA413895CDCD1A27C332806627EED2FD05BD (void);
// 0x0000014A System.Single UnityStandardAssets.Vehicles.Car.CarController::get_MaxSpeed()
extern void CarController_get_MaxSpeed_mA12355ADBE4068E7FB9D2853D67C2C7256DB9681 (void);
// 0x0000014B System.Single UnityStandardAssets.Vehicles.Car.CarController::get_Revs()
extern void CarController_get_Revs_m642BB53135EFB44913D28B192CF79A46952B5B58 (void);
// 0x0000014C System.Void UnityStandardAssets.Vehicles.Car.CarController::set_Revs(System.Single)
extern void CarController_set_Revs_m0A174149D801834C4FB861004BE55F48DAD539A4 (void);
// 0x0000014D System.Single UnityStandardAssets.Vehicles.Car.CarController::get_AccelInput()
extern void CarController_get_AccelInput_m6B2E21B4598992F917B375D1F20FEAD6F776C98B (void);
// 0x0000014E System.Void UnityStandardAssets.Vehicles.Car.CarController::set_AccelInput(System.Single)
extern void CarController_set_AccelInput_m9C0B3E3084272E47988C62C8621595257BC95722 (void);
// 0x0000014F System.Void UnityStandardAssets.Vehicles.Car.CarController::Start()
extern void CarController_Start_mAD71184E15FCC34EE427C662E9F9B98F92478E29 (void);
// 0x00000150 System.Void UnityStandardAssets.Vehicles.Car.CarController::GearChanging()
extern void CarController_GearChanging_m88CC4B8B99B933BF7C414C172D1FF32259D48EDC (void);
// 0x00000151 System.Single UnityStandardAssets.Vehicles.Car.CarController::CurveFactor(System.Single)
extern void CarController_CurveFactor_m435DFFB28E8D1BF2FCEF3D828E8454E401E986C2 (void);
// 0x00000152 System.Single UnityStandardAssets.Vehicles.Car.CarController::ULerp(System.Single,System.Single,System.Single)
extern void CarController_ULerp_m7AFD80930A0A3EAD6E5AF2BA080570457C7A4CCA (void);
// 0x00000153 System.Void UnityStandardAssets.Vehicles.Car.CarController::CalculateGearFactor()
extern void CarController_CalculateGearFactor_mA27673F6760B7759FB4A76307187C0036AB538F2 (void);
// 0x00000154 System.Void UnityStandardAssets.Vehicles.Car.CarController::CalculateRevs()
extern void CarController_CalculateRevs_m25B70852D908A1AB31AE9EA6EF7377BFDEB3A4F4 (void);
// 0x00000155 System.Void UnityStandardAssets.Vehicles.Car.CarController::Move(System.Single,System.Single,System.Single,System.Single)
extern void CarController_Move_m710DD5723707ECC83BC04A764642BED780DDAB67 (void);
// 0x00000156 System.Void UnityStandardAssets.Vehicles.Car.CarController::CapSpeed()
extern void CarController_CapSpeed_m63B0D340A0A007F27B2C5EEE6D3D4573410EEB8D (void);
// 0x00000157 System.Void UnityStandardAssets.Vehicles.Car.CarController::ApplyDrive(System.Single,System.Single)
extern void CarController_ApplyDrive_m6EC1D7D52A955EC9A1DEBE750E8473630362B6C5 (void);
// 0x00000158 System.Void UnityStandardAssets.Vehicles.Car.CarController::SteerHelper()
extern void CarController_SteerHelper_mF6312DD3E610EF92C21202D79F193355D5186FE6 (void);
// 0x00000159 System.Void UnityStandardAssets.Vehicles.Car.CarController::AddDownForce()
extern void CarController_AddDownForce_mE6479A5330F91DF49FBF98AAB609741CF1834D6C (void);
// 0x0000015A System.Void UnityStandardAssets.Vehicles.Car.CarController::CheckForWheelSpin()
extern void CarController_CheckForWheelSpin_m5061F7DB82BADC9E3C8BAF21A2DE6F128F850B1F (void);
// 0x0000015B System.Void UnityStandardAssets.Vehicles.Car.CarController::TractionControl()
extern void CarController_TractionControl_m23F265B5248063648F5E014BDB68FB6B59718062 (void);
// 0x0000015C System.Void UnityStandardAssets.Vehicles.Car.CarController::AdjustTorque(System.Single)
extern void CarController_AdjustTorque_mA5882A0C0656B68D9E372B6768836B94EB7F3A36 (void);
// 0x0000015D System.Boolean UnityStandardAssets.Vehicles.Car.CarController::AnySkidSoundPlaying()
extern void CarController_AnySkidSoundPlaying_m8B971D76E9867A390546CC838B9D8DD1DCFD09BC (void);
// 0x0000015E System.Void UnityStandardAssets.Vehicles.Car.CarController::.ctor()
extern void CarController__ctor_m43D96461E612E6F36ABD70D10526994C7CD151FE (void);
// 0x0000015F System.Void UnityStandardAssets.Vehicles.Car.CarController::.cctor()
extern void CarController__cctor_m88FF6E1ABA60DA504E1EBD899D7C711F3A2BEEB4 (void);
// 0x00000160 System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::Start()
extern void CarSelfRighting_Start_m34D21601B67D3855133E7C41944F91FE4281A692 (void);
// 0x00000161 System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::Update()
extern void CarSelfRighting_Update_m4E5AAB22314DA341FC7417D62D33462A1EF7D575 (void);
// 0x00000162 System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::RightCar()
extern void CarSelfRighting_RightCar_m8AC643B94AE9BBA6D2BCEF91387167E0200DBB9F (void);
// 0x00000163 System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::.ctor()
extern void CarSelfRighting__ctor_m7816689E202E3531F4F02A214FB0752DDFA1F2BD (void);
// 0x00000164 System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::Awake()
extern void CarUserControl_Awake_mBD2084DF81305CE9361535339E04D0E0C8FA701D (void);
// 0x00000165 System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::FixedUpdate()
extern void CarUserControl_FixedUpdate_m4D383203699A6910E27F96E9A5E8132A2A6C4DA2 (void);
// 0x00000166 System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::.ctor()
extern void CarUserControl__ctor_m5059918AB540551B020BEC800622EC92042C70D7 (void);
// 0x00000167 System.Void UnityStandardAssets.Vehicles.Car.Mudguard::Start()
extern void Mudguard_Start_m9226767403487DA21425EC7C367DCB88066A1194 (void);
// 0x00000168 System.Void UnityStandardAssets.Vehicles.Car.Mudguard::Update()
extern void Mudguard_Update_mACCACB23ADE483B77BC14713B28B96E8A5F972A1 (void);
// 0x00000169 System.Void UnityStandardAssets.Vehicles.Car.Mudguard::.ctor()
extern void Mudguard__ctor_mB1DA1E48B1642E14A9D280F52EAB50AD02DA09E3 (void);
// 0x0000016A System.Collections.IEnumerator UnityStandardAssets.Vehicles.Car.SkidTrail::Start()
extern void SkidTrail_Start_m6EAC8D56A5360AA8D0AC402F872E6F9280A77559 (void);
// 0x0000016B System.Void UnityStandardAssets.Vehicles.Car.SkidTrail::.ctor()
extern void SkidTrail__ctor_m8437FAAA8F97487CD9614A0496F9B822D5197D0F (void);
// 0x0000016C System.Void UnityStandardAssets.Vehicles.Car.Suspension::Start()
extern void Suspension_Start_mAE6F9830A242D87D4468F88A781EBCEA923923B6 (void);
// 0x0000016D System.Void UnityStandardAssets.Vehicles.Car.Suspension::Update()
extern void Suspension_Update_m5AE98DF7A19725655F4579567DAD011EDF808A6D (void);
// 0x0000016E System.Void UnityStandardAssets.Vehicles.Car.Suspension::.ctor()
extern void Suspension__ctor_m9AB521AFE45C92BC134C3A2710C756B239A74FAF (void);
// 0x0000016F System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects::get_skidding()
extern void WheelEffects_get_skidding_m718DA419C2DEC9EF477DCD45FAEB68E94FD28070 (void);
// 0x00000170 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::set_skidding(System.Boolean)
extern void WheelEffects_set_skidding_m4F42423A2EC98533350F0D7003AFFD7037047D1D (void);
// 0x00000171 System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects::get_PlayingAudio()
extern void WheelEffects_get_PlayingAudio_m431797CA8A5F826E63C37541F58B3AA32C813D0E (void);
// 0x00000172 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::set_PlayingAudio(System.Boolean)
extern void WheelEffects_set_PlayingAudio_m4E4C1F97DAFCC638424BAB8EF77C58CEC79E8F8C (void);
// 0x00000173 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::Start()
extern void WheelEffects_Start_m4874BB9D66D03D0389AADBC1D497BC80EB85A431 (void);
// 0x00000174 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::EmitTyreSmoke()
extern void WheelEffects_EmitTyreSmoke_mA14E266D87D309D477F1FEFEADC57846FD72F0DC (void);
// 0x00000175 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::PlayAudio()
extern void WheelEffects_PlayAudio_m9BBE568F5CA041E518CB7979C29759EF3252345A (void);
// 0x00000176 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::StopAudio()
extern void WheelEffects_StopAudio_m1B4DD35BCDC32EB80778CDB29053441801350DC3 (void);
// 0x00000177 System.Collections.IEnumerator UnityStandardAssets.Vehicles.Car.WheelEffects::StartSkidTrail()
extern void WheelEffects_StartSkidTrail_m07F5D9D62535160E1D4EF8CAB1BCB0DEB3D1A4E9 (void);
// 0x00000178 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::EndSkidTrail()
extern void WheelEffects_EndSkidTrail_m75DBF6F2CD4990256F986C45F8D4F26754859A1B (void);
// 0x00000179 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::.ctor()
extern void WheelEffects__ctor_m1A9D3A67287DE2F37D1146854B0DC0753E8E6F02 (void);
// 0x0000017A System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::Awake()
extern void AeroplaneAiControl_Awake_m9A029336C6899361B55E43BE14671BC598C5DE0C (void);
// 0x0000017B System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::Reset()
extern void AeroplaneAiControl_Reset_m8252C9E199162483947A5F688AB6E9579FE488E7 (void);
// 0x0000017C System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::FixedUpdate()
extern void AeroplaneAiControl_FixedUpdate_mFF1DEADE3DB3DFF770798138E02E7B31B4016DB4 (void);
// 0x0000017D System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::SetTarget(UnityEngine.Transform)
extern void AeroplaneAiControl_SetTarget_m92D1BD8BAF5491C380BBE7424C438CE4CB713FC9 (void);
// 0x0000017E System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::.ctor()
extern void AeroplaneAiControl__ctor_m3147BA9E0D0C18E9CC8BA0FCF04C8724D118E715 (void);
// 0x0000017F System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::Awake()
extern void AeroplaneAudio_Awake_mBDA501A01C9C4D1235B7A4D2D260DB909CB5EC34 (void);
// 0x00000180 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::Update()
extern void AeroplaneAudio_Update_mDE4DB712DC69B4255038A33071BAEE2F6C6D41D6 (void);
// 0x00000181 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::.ctor()
extern void AeroplaneAudio__ctor_mFF0EC4C94327233C7E9FC9A87F55B7E92F5ABFC6 (void);
// 0x00000182 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator::Start()
extern void AeroplaneControlSurfaceAnimator_Start_m394939E227828330580FF355EFC203158D4C525A (void);
// 0x00000183 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator::Update()
extern void AeroplaneControlSurfaceAnimator_Update_mD2630FB7F3B956ABEE88FEB9ECB0237AD986D9BB (void);
// 0x00000184 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator::RotateSurface(UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator_ControlSurface,UnityEngine.Quaternion)
extern void AeroplaneControlSurfaceAnimator_RotateSurface_m92D96999DCAB552668517BBDFEEB2912C50175DE (void);
// 0x00000185 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator::.ctor()
extern void AeroplaneControlSurfaceAnimator__ctor_m956F30E568E00B4E5FF9791F2ED7839732D3010C (void);
// 0x00000186 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_Altitude()
extern void AeroplaneController_get_Altitude_mD7201103ED5136DFA948E1BB374D35F8EAC7B5E0 (void);
// 0x00000187 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_Altitude(System.Single)
extern void AeroplaneController_set_Altitude_m001A742194DD428A55BDB81BE7664861D9AEDF39 (void);
// 0x00000188 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_Throttle()
extern void AeroplaneController_get_Throttle_m5C8C9063565BF4155E32685B8239AC683E8D1860 (void);
// 0x00000189 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_Throttle(System.Single)
extern void AeroplaneController_set_Throttle_m7E6012252676A570E173410CDDEC0EB8288B408E (void);
// 0x0000018A System.Boolean UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_AirBrakes()
extern void AeroplaneController_get_AirBrakes_m1CFBAF5AB7C92AB1F61684BB4D1967EA3A928493 (void);
// 0x0000018B System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_AirBrakes(System.Boolean)
extern void AeroplaneController_set_AirBrakes_m7AE541D6CDD1F86D9B4A86DA133AC3E0069A0A2A (void);
// 0x0000018C System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_ForwardSpeed()
extern void AeroplaneController_get_ForwardSpeed_m4C12FF50AC636FCE6DCAAFA0F6E03E06D2764616 (void);
// 0x0000018D System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_ForwardSpeed(System.Single)
extern void AeroplaneController_set_ForwardSpeed_m8FB8A92A2FE310B734E34A7F8BEEA2152276F470 (void);
// 0x0000018E System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_EnginePower()
extern void AeroplaneController_get_EnginePower_mF310ABEA4346D9C3B55B779EF4815F47D6E07CBB (void);
// 0x0000018F System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_EnginePower(System.Single)
extern void AeroplaneController_set_EnginePower_mB403B061246E89F8DD4973585C4D8A7C331C010F (void);
// 0x00000190 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_MaxEnginePower()
extern void AeroplaneController_get_MaxEnginePower_m9280EADC8CF88E999E38B3CB84C4A3EE92F6D4FC (void);
// 0x00000191 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_RollAngle()
extern void AeroplaneController_get_RollAngle_m34CB3EA79F50B97260A62F1CB01C0DAF22434623 (void);
// 0x00000192 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_RollAngle(System.Single)
extern void AeroplaneController_set_RollAngle_m1898D1ACC21640F9A68EDD879A1D5AD698662B7D (void);
// 0x00000193 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_PitchAngle()
extern void AeroplaneController_get_PitchAngle_mF301494C501E8B7EDC6594BAB747A881E21B362C (void);
// 0x00000194 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_PitchAngle(System.Single)
extern void AeroplaneController_set_PitchAngle_mF4798C2AD4464B304153FAB38D852D42B758F4ED (void);
// 0x00000195 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_RollInput()
extern void AeroplaneController_get_RollInput_mA53807BDCA085B6AEA7B4337BA3548F8355BF48C (void);
// 0x00000196 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_RollInput(System.Single)
extern void AeroplaneController_set_RollInput_m518F9EAB08362038401C50D8F9F5AA7075F3A609 (void);
// 0x00000197 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_PitchInput()
extern void AeroplaneController_get_PitchInput_m2E0D1D37BDDD013A63B566CB4347745F6C505CDD (void);
// 0x00000198 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_PitchInput(System.Single)
extern void AeroplaneController_set_PitchInput_m5AF8AB111BD768C178571E1C73CCD1A60D9E3763 (void);
// 0x00000199 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_YawInput()
extern void AeroplaneController_get_YawInput_m72AA1B1E6DF8F75937A6788C11B4ACDA1D4818D5 (void);
// 0x0000019A System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_YawInput(System.Single)
extern void AeroplaneController_set_YawInput_m8871D002FE34675A2CC7368CB12EE6E6623C227E (void);
// 0x0000019B System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_ThrottleInput()
extern void AeroplaneController_get_ThrottleInput_m9A1F72722CA45AFB58D5AA946EE0279A4972DDB4 (void);
// 0x0000019C System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_ThrottleInput(System.Single)
extern void AeroplaneController_set_ThrottleInput_m61682CDCB5059E3A5C98CC2C823ED0BA935FF595 (void);
// 0x0000019D System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::Start()
extern void AeroplaneController_Start_m91DB0FFF9AC9FD0564CC94041B3F91ACD4CC9155 (void);
// 0x0000019E System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::Move(System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern void AeroplaneController_Move_mAC1C689885B75E5641AD5E1C6F152FEFA4F7BE11 (void);
// 0x0000019F System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::ClampInputs()
extern void AeroplaneController_ClampInputs_m016971EC421B9CEFECC9BFCD11F7C135F9A3D29D (void);
// 0x000001A0 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CalculateRollAndPitchAngles()
extern void AeroplaneController_CalculateRollAndPitchAngles_mF4D4FFD7B0F1D11F8A547F5F23BBFEC4540797EA (void);
// 0x000001A1 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::AutoLevel()
extern void AeroplaneController_AutoLevel_mD6E24EE2CEDAC943067992DB737EBF4A30F3337D (void);
// 0x000001A2 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CalculateForwardSpeed()
extern void AeroplaneController_CalculateForwardSpeed_m2D0B2B98A56A5587995855875E952FAE8CB13E9C (void);
// 0x000001A3 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::ControlThrottle()
extern void AeroplaneController_ControlThrottle_m532F9D3E57351424D211FF2B1F40B8A01E6C7322 (void);
// 0x000001A4 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CalculateDrag()
extern void AeroplaneController_CalculateDrag_m8F2E00CA0EE41B7D2E0FE519AF0EAD45AD0A4C70 (void);
// 0x000001A5 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CaluclateAerodynamicEffect()
extern void AeroplaneController_CaluclateAerodynamicEffect_m3BD28CE3E7ACED75AB91099D75E28AA30B18213B (void);
// 0x000001A6 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CalculateLinearForces()
extern void AeroplaneController_CalculateLinearForces_m8BCF085FB44FFE2452A4B65A16DE81B79B14D5E6 (void);
// 0x000001A7 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CalculateTorque()
extern void AeroplaneController_CalculateTorque_m9C599F6684BD6B09AA9DE4C9BFC6D7B19FC0B899 (void);
// 0x000001A8 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CalculateAltitude()
extern void AeroplaneController_CalculateAltitude_mC82570DA9ED0D63DAF9200F6FF68B0C50FE56636 (void);
// 0x000001A9 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::Immobilize()
extern void AeroplaneController_Immobilize_m5D2097492C7D56F9D64F99BDB1810C87A4FC43B4 (void);
// 0x000001AA System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::Reset()
extern void AeroplaneController_Reset_mF40F9E0E6EBDBBEFD1ABE6324859B002FBC3CF6F (void);
// 0x000001AB System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::.ctor()
extern void AeroplaneController__ctor_mF8F75B326B55A734A640E4714F1B3E8382043C6C (void);
// 0x000001AC System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::Awake()
extern void AeroplanePropellerAnimator_Awake_m73F8E4075B7C19461C26222D44653E485C93EF47 (void);
// 0x000001AD System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::Update()
extern void AeroplanePropellerAnimator_Update_m1BDE8DD2603B0A04E2970F9F3170E413E0CF33AB (void);
// 0x000001AE System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::.ctor()
extern void AeroplanePropellerAnimator__ctor_m1586C357A4E00185C6B6D29229BCADF3BD0E8415 (void);
// 0x000001AF System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl2Axis::Awake()
extern void AeroplaneUserControl2Axis_Awake_m09FB41E61246F704CCC3070E1E3568201EB84DCC (void);
// 0x000001B0 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl2Axis::FixedUpdate()
extern void AeroplaneUserControl2Axis_FixedUpdate_m16B1B755DEDF56F134C50482C6D511EDAAE4CE05 (void);
// 0x000001B1 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl2Axis::AdjustInputForMobileControls(System.Single&,System.Single&,System.Single&)
extern void AeroplaneUserControl2Axis_AdjustInputForMobileControls_m2CBC1D3BE5969BE5A264288CC005953AE209ADB4 (void);
// 0x000001B2 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl2Axis::.ctor()
extern void AeroplaneUserControl2Axis__ctor_m23FD94504D00FC293ECDAD2FD12E6D26AC83B98B (void);
// 0x000001B3 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::Awake()
extern void AeroplaneUserControl4Axis_Awake_m941A60DB28E42AE8E4BD9F2B3B1F7A829496EF17 (void);
// 0x000001B4 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::FixedUpdate()
extern void AeroplaneUserControl4Axis_FixedUpdate_mD8EE23F7EC6B7D20DE69C78FC46236EE41233C5D (void);
// 0x000001B5 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::AdjustInputForMobileControls(System.Single&,System.Single&,System.Single&)
extern void AeroplaneUserControl4Axis_AdjustInputForMobileControls_m3499E0435C7E07EA209E56701621020E62E9D8AE (void);
// 0x000001B6 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::.ctor()
extern void AeroplaneUserControl4Axis__ctor_m39F39B3842E11BC4F5AFB74FF9B12A54F421E787 (void);
// 0x000001B7 System.Void UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::Start()
extern void JetParticleEffect_Start_m5B27DF2D7C3FCBDC53C6DDA433C06B7C1B7D0531 (void);
// 0x000001B8 System.Void UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::Update()
extern void JetParticleEffect_Update_m8B8989022284D304D827C5EC7A3119C193969C09 (void);
// 0x000001B9 UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::FindAeroplaneParent()
extern void JetParticleEffect_FindAeroplaneParent_m469DB0B9CC662E181536CE754C8EE52822B59C06 (void);
// 0x000001BA System.Void UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::.ctor()
extern void JetParticleEffect__ctor_mE08093E39EFD497EE56B68EE601A8793CC9CBF3E (void);
// 0x000001BB System.Void UnityStandardAssets.Vehicles.Aeroplane.LandingGear::Start()
extern void LandingGear_Start_m86A572F6D9AD3AEC918C352A22B8EAABEE644C7D (void);
// 0x000001BC System.Void UnityStandardAssets.Vehicles.Aeroplane.LandingGear::Update()
extern void LandingGear_Update_m0ECCD00D8CD63939029C0BFFBCE65C71680A31B8 (void);
// 0x000001BD System.Void UnityStandardAssets.Vehicles.Aeroplane.LandingGear::.ctor()
extern void LandingGear__ctor_mBCFF08F7E8D509EA8BA896A6B874671F140CD5BC (void);
// 0x000001BE System.Void UnityStandardAssets.Vehicles.Ball.Ball::Start()
extern void Ball_Start_mD94D5E17C462F68ED882F7A71510571FE3BC99E2 (void);
// 0x000001BF System.Void UnityStandardAssets.Vehicles.Ball.Ball::Move(UnityEngine.Vector3,System.Boolean)
extern void Ball_Move_m2876C25E0C756BF245A81AD607EFEF4635163D37 (void);
// 0x000001C0 System.Void UnityStandardAssets.Vehicles.Ball.Ball::.ctor()
extern void Ball__ctor_mCAF1E03B050453AC083D54A0395370CF4E322D4A (void);
// 0x000001C1 System.Void UnityStandardAssets.Vehicles.Ball.BallUserControl::Awake()
extern void BallUserControl_Awake_m3053C00DA15A4F03C606E253DF2F1222233D2CD4 (void);
// 0x000001C2 System.Void UnityStandardAssets.Vehicles.Ball.BallUserControl::Update()
extern void BallUserControl_Update_mCF16AA2CFDB461475BEFE7F4A82AD02E27B5A2C9 (void);
// 0x000001C3 System.Void UnityStandardAssets.Vehicles.Ball.BallUserControl::FixedUpdate()
extern void BallUserControl_FixedUpdate_mAFB462499A540A51F3A59B50EACEF013A1FD99C6 (void);
// 0x000001C4 System.Void UnityStandardAssets.Vehicles.Ball.BallUserControl::.ctor()
extern void BallUserControl__ctor_m933AEE908ED128E00AA48445422BAAACE54D27C8 (void);
// 0x000001C5 UnityEngine.AI.NavMeshAgent UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::get_agent()
extern void AICharacterControl_get_agent_mC145C43591E7F58C19D6812346E8B267562DCE2A (void);
// 0x000001C6 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::set_agent(UnityEngine.AI.NavMeshAgent)
extern void AICharacterControl_set_agent_m3AF797E33435DE57A5B68D4A370B38FF5066AC35 (void);
// 0x000001C7 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::get_character()
extern void AICharacterControl_get_character_m5F68CEC317221C119DD993F0BC66E83B46ECAF10 (void);
// 0x000001C8 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::set_character(UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter)
extern void AICharacterControl_set_character_mB648916EAE530CF42ADB6A2133597CD2C5C351AE (void);
// 0x000001C9 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::Start()
extern void AICharacterControl_Start_m94E0B0C90E7F8680D0BA14C0B7F73F1147F1ECBD (void);
// 0x000001CA System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::Update()
extern void AICharacterControl_Update_m3739D8300A1B4ED83717268FACD69BA35E2F2D60 (void);
// 0x000001CB System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::SetTarget(UnityEngine.Transform)
extern void AICharacterControl_SetTarget_m85A54D9230685AEFC19FE47B0844545F3AC76ED7 (void);
// 0x000001CC System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::.ctor()
extern void AICharacterControl__ctor_m64E75FC80DC62E05D71EA1EDE411F5E9118F9895 (void);
// 0x000001CD System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::Start()
extern void ThirdPersonCharacter_Start_m77BED10E07BCE3B9FCDFDAB0A604598E7EF52551 (void);
// 0x000001CE System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::Move(UnityEngine.Vector3,System.Boolean,System.Boolean)
extern void ThirdPersonCharacter_Move_mC404A566646B48F6E4895C4B56B568B2FCD7B315 (void);
// 0x000001CF System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::ScaleCapsuleForCrouching(System.Boolean)
extern void ThirdPersonCharacter_ScaleCapsuleForCrouching_mEF225C57F19EEFB21F3FA5065F835ED295F9FA83 (void);
// 0x000001D0 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::PreventStandingInLowHeadroom()
extern void ThirdPersonCharacter_PreventStandingInLowHeadroom_m11B4FCE921A339A567D1DE504C5781738FE130F4 (void);
// 0x000001D1 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::UpdateAnimator(UnityEngine.Vector3)
extern void ThirdPersonCharacter_UpdateAnimator_mA55D3E6EEE3D094A0377F74680EDC955C915C2F3 (void);
// 0x000001D2 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::HandleAirborneMovement()
extern void ThirdPersonCharacter_HandleAirborneMovement_mA70C2C3E9776D1F93AC11B00DBB614A653D05713 (void);
// 0x000001D3 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::HandleGroundedMovement(System.Boolean,System.Boolean)
extern void ThirdPersonCharacter_HandleGroundedMovement_m9E47FDBABA1394392BC50B9D0E8F617417DCD8EA (void);
// 0x000001D4 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::ApplyExtraTurnRotation()
extern void ThirdPersonCharacter_ApplyExtraTurnRotation_m04D602E8910E9F91436DC3978A237DA976E0528A (void);
// 0x000001D5 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::OnAnimatorMove()
extern void ThirdPersonCharacter_OnAnimatorMove_m1B720209EAC4123222D83A2604CDD9DEB35B4807 (void);
// 0x000001D6 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::CheckGroundStatus()
extern void ThirdPersonCharacter_CheckGroundStatus_m529274699AE7FB0AA68E6259A4009C5119346DD6 (void);
// 0x000001D7 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::.ctor()
extern void ThirdPersonCharacter__ctor_m2A343C6DA11522069E215C58B8C5F0F2916482B9 (void);
// 0x000001D8 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::Start()
extern void ThirdPersonUserControl_Start_m6E6473ABBBE127E5FED209498ED24663D0612682 (void);
// 0x000001D9 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::Update()
extern void ThirdPersonUserControl_Update_m673BE47FF260D53BF312CAB76F77C32D8CA22629 (void);
// 0x000001DA System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::FixedUpdate()
extern void ThirdPersonUserControl_FixedUpdate_mDF454F6DBD58CBB0429BBC36502150EF9D360453 (void);
// 0x000001DB System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::.ctor()
extern void ThirdPersonUserControl__ctor_m7BD727F15D1289F92B24B581C9DDB5A7489A967E (void);
// 0x000001DC System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::Start()
extern void FirstPersonController_Start_m87414ABA8CE33FC0DE5E9856C79A357E501D7308 (void);
// 0x000001DD System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::Update()
extern void FirstPersonController_Update_mF3FC7041AB276DC382BC8A4F61AEAFAFAEA04ED4 (void);
// 0x000001DE System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::PlayLandingSound()
extern void FirstPersonController_PlayLandingSound_m0E02AFCFF243AD8D37BA51E95A07CAB9E92B5B65 (void);
// 0x000001DF System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::FixedUpdate()
extern void FirstPersonController_FixedUpdate_mBA424DA0A9AA19D6C2E50F798C372AA7543FD918 (void);
// 0x000001E0 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::PlayJumpSound()
extern void FirstPersonController_PlayJumpSound_m45E7E5399ABB1944DD6EDD9702E5FFB899886BC4 (void);
// 0x000001E1 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::ProgressStepCycle(System.Single)
extern void FirstPersonController_ProgressStepCycle_mF0FA5D7B32881E407247E42A6F07B1F49AB5717F (void);
// 0x000001E2 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::PlayFootStepAudio()
extern void FirstPersonController_PlayFootStepAudio_mE2350EC1F9F5D34A04464C32EEBE1E7E82134179 (void);
// 0x000001E3 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::UpdateCameraPosition(System.Single)
extern void FirstPersonController_UpdateCameraPosition_m9287722E622F693557E810274D76CF402849E7B5 (void);
// 0x000001E4 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::GetInput(System.Single&)
extern void FirstPersonController_GetInput_m20FF731BB9AE80DB53A15A87BB960E7552AF5730 (void);
// 0x000001E5 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::RotateView()
extern void FirstPersonController_RotateView_m52AE6EADF85961F72D76454E11913AFE0AB0C77A (void);
// 0x000001E6 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern void FirstPersonController_OnControllerColliderHit_m359FEB09F912EF3311435D6CF9CAFB25CA6EBDDC (void);
// 0x000001E7 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::.ctor()
extern void FirstPersonController__ctor_m067AA344391066103FAE6175AFC5C1717B4022F6 (void);
// 0x000001E8 System.Void UnityStandardAssets.Characters.FirstPerson.HeadBob::Start()
extern void HeadBob_Start_m2956DB87FE33B62FC550D8E8581B1197E790BD8F (void);
// 0x000001E9 System.Void UnityStandardAssets.Characters.FirstPerson.HeadBob::Update()
extern void HeadBob_Update_mBF51EAFAB23AC5B5E176E02CCCA668AF6B20F4AC (void);
// 0x000001EA System.Void UnityStandardAssets.Characters.FirstPerson.HeadBob::.ctor()
extern void HeadBob__ctor_mD8C44B9DC99CA8E61E23578A15973F48D1B1D077 (void);
// 0x000001EB System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::Init(UnityEngine.Transform,UnityEngine.Transform)
extern void MouseLook_Init_m6F89547F704698EFB173D980CEC974CD6D11CE1E (void);
// 0x000001EC System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::LookRotation(UnityEngine.Transform,UnityEngine.Transform)
extern void MouseLook_LookRotation_m12F5371B9F69C6E79A959B1707E2079EF176FE47 (void);
// 0x000001ED System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::SetCursorLock(System.Boolean)
extern void MouseLook_SetCursorLock_m74E47ED1CDCF73947446BA6F3109C7305B44A27E (void);
// 0x000001EE System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::UpdateCursorLock()
extern void MouseLook_UpdateCursorLock_mC03FF4763BE1894EDD81D90A96791E644BDA4DF9 (void);
// 0x000001EF System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::InternalLockUpdate()
extern void MouseLook_InternalLockUpdate_m689C8345609827E54B2C45018C9FA2710641AE41 (void);
// 0x000001F0 UnityEngine.Quaternion UnityStandardAssets.Characters.FirstPerson.MouseLook::ClampRotationAroundXAxis(UnityEngine.Quaternion)
extern void MouseLook_ClampRotationAroundXAxis_mB80B11F6AA879942BCC5B59AC99857BEC811CEBB (void);
// 0x000001F1 System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::.ctor()
extern void MouseLook__ctor_m4991C1F282EDF4C515B9C28106EC981F5D157CF2 (void);
// 0x000001F2 UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::get_Velocity()
extern void RigidbodyFirstPersonController_get_Velocity_mA3844E469740CF2B014878B8BF192EF385675B28 (void);
// 0x000001F3 System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::get_Grounded()
extern void RigidbodyFirstPersonController_get_Grounded_mB76301608244EA277030FED9FF07421B1DE37A55 (void);
// 0x000001F4 System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::get_Jumping()
extern void RigidbodyFirstPersonController_get_Jumping_m7E2EACD44C0241B8423B97E1739DBE269FE204A6 (void);
// 0x000001F5 System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::get_Running()
extern void RigidbodyFirstPersonController_get_Running_mE86F6DD182214508725455FC918CAF13BC1A462B (void);
// 0x000001F6 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::Start()
extern void RigidbodyFirstPersonController_Start_m2330F14619B7112B794576FAE87657F51E16F998 (void);
// 0x000001F7 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::Update()
extern void RigidbodyFirstPersonController_Update_mC9D36ED411466D763F3B6726ED0264FE5815D040 (void);
// 0x000001F8 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::FixedUpdate()
extern void RigidbodyFirstPersonController_FixedUpdate_m81ED45B8FE887F4C512FC12EA0EA4A83AECC370C (void);
// 0x000001F9 System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::SlopeMultiplier()
extern void RigidbodyFirstPersonController_SlopeMultiplier_m5BC79F85397DC3492513C1AC8088A8E62DE47B37 (void);
// 0x000001FA System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::StickToGroundHelper()
extern void RigidbodyFirstPersonController_StickToGroundHelper_m1ECAEA0961503D6BBF65AA20D056F4E803C13CA8 (void);
// 0x000001FB UnityEngine.Vector2 UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::GetInput()
extern void RigidbodyFirstPersonController_GetInput_m4DDAC49710C5FA777D29BF16A154A8B35D6EA8D8 (void);
// 0x000001FC System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::RotateView()
extern void RigidbodyFirstPersonController_RotateView_mD3A6BE7C73F1839CE62FBECC3A03E53C68AF8711 (void);
// 0x000001FD System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::GroundCheck()
extern void RigidbodyFirstPersonController_GroundCheck_m7843581359DD47F573ADD9059732FA20246C4FC0 (void);
// 0x000001FE System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::.ctor()
extern void RigidbodyFirstPersonController__ctor_mCCA1D3CC7960FE1BDB756F1CC3BC6D12DC5BC471 (void);
// 0x000001FF System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::Start()
extern void AbstractTargetFollower_Start_m5830AC9B925EF10A96832CB64E17703537ABA4D6 (void);
// 0x00000200 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FixedUpdate()
extern void AbstractTargetFollower_FixedUpdate_m059E3F7EE069E830A1675B7E609B2C0992F5C9FE (void);
// 0x00000201 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::LateUpdate()
extern void AbstractTargetFollower_LateUpdate_mE6D4BAF43EA1C593A3439507476CAF6C360874F5 (void);
// 0x00000202 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::ManualUpdate()
extern void AbstractTargetFollower_ManualUpdate_m5FDFBD137B58BE58593D6D751BAA4C4A5134C158 (void);
// 0x00000203 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single)
// 0x00000204 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FindAndTargetPlayer()
extern void AbstractTargetFollower_FindAndTargetPlayer_m0A72C9D084555F206759AF9BBC56C039170E497A (void);
// 0x00000205 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform)
extern void AbstractTargetFollower_SetTarget_m17CE0F35DAE4E4086CACD31BA14C05DA2589D3A9 (void);
// 0x00000206 UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::get_Target()
extern void AbstractTargetFollower_get_Target_m24234F14398E0958CEA1A476F649E762D6E17814 (void);
// 0x00000207 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::.ctor()
extern void AbstractTargetFollower__ctor_m7CA75F5DEB95588875BF359F17F9A45F1AC28E18 (void);
// 0x00000208 System.Void UnityStandardAssets.Cameras.AutoCam::FollowTarget(System.Single)
extern void AutoCam_FollowTarget_m49D07251C6021E9F838CC8747A0CF83EFCEE8552 (void);
// 0x00000209 System.Void UnityStandardAssets.Cameras.AutoCam::.ctor()
extern void AutoCam__ctor_mD9E4154D81A29B6C8C68297374653FB9CFAB3AC2 (void);
// 0x0000020A System.Void UnityStandardAssets.Cameras.FreeLookCam::Awake()
extern void FreeLookCam_Awake_m9EAF9CF5A48AE92736DB798522FFFA95DF0C33A0 (void);
// 0x0000020B System.Void UnityStandardAssets.Cameras.FreeLookCam::Update()
extern void FreeLookCam_Update_mD83A18376B2B5F9970C8DA78E60DB12C1B508AFC (void);
// 0x0000020C System.Void UnityStandardAssets.Cameras.FreeLookCam::OnDisable()
extern void FreeLookCam_OnDisable_mF4EE05B50BF94C52C65ABE1F26A383F3F614DE54 (void);
// 0x0000020D System.Void UnityStandardAssets.Cameras.FreeLookCam::FollowTarget(System.Single)
extern void FreeLookCam_FollowTarget_mADE713C6EA3D2A61B6FEFBAC2226C9519728478C (void);
// 0x0000020E System.Void UnityStandardAssets.Cameras.FreeLookCam::HandleRotationMovement()
extern void FreeLookCam_HandleRotationMovement_m835C5DFA2FD21F7C64057BDB0BAEA62B99A2D404 (void);
// 0x0000020F System.Void UnityStandardAssets.Cameras.FreeLookCam::.ctor()
extern void FreeLookCam__ctor_mB38818588467BA1204288BB062BBAABDCC0225D9 (void);
// 0x00000210 System.Void UnityStandardAssets.Cameras.HandHeldCam::FollowTarget(System.Single)
extern void HandHeldCam_FollowTarget_m9BF734BC3214DBB892E0D2D507E2F9452B147FDB (void);
// 0x00000211 System.Void UnityStandardAssets.Cameras.HandHeldCam::.ctor()
extern void HandHeldCam__ctor_m57D82D85C30C87D03E7EAB05EBDF656962DA2AB5 (void);
// 0x00000212 System.Void UnityStandardAssets.Cameras.LookatTarget::Start()
extern void LookatTarget_Start_m55D1DA54A4762FF2AF364421AED7C54396705CBA (void);
// 0x00000213 System.Void UnityStandardAssets.Cameras.LookatTarget::FollowTarget(System.Single)
extern void LookatTarget_FollowTarget_m363133638041D45CAB46C88DD61768E4E39DF534 (void);
// 0x00000214 System.Void UnityStandardAssets.Cameras.LookatTarget::.ctor()
extern void LookatTarget__ctor_m765D6EC2839C0E8BDF17F849E28A0D472A37D25D (void);
// 0x00000215 System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::Awake()
extern void PivotBasedCameraRig_Awake_m59237BF4FB7603169D6E12D95C372D7A889E1F27 (void);
// 0x00000216 System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::.ctor()
extern void PivotBasedCameraRig__ctor_m3A67ED42B196F44E73ECF164084D9655F6BE700F (void);
// 0x00000217 System.Boolean UnityStandardAssets.Cameras.ProtectCameraFromWallClip::get_protecting()
extern void ProtectCameraFromWallClip_get_protecting_m1E2FFDFDBBEEA74AD539E8F35522047B7F14F9FB (void);
// 0x00000218 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::set_protecting(System.Boolean)
extern void ProtectCameraFromWallClip_set_protecting_m23B7F1BBEE76FDAFF187820178B1CDAE2A326F55 (void);
// 0x00000219 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::Start()
extern void ProtectCameraFromWallClip_Start_mFD2770D700A02F262AF9926E4C5828B0C8AC544C (void);
// 0x0000021A System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::LateUpdate()
extern void ProtectCameraFromWallClip_LateUpdate_mA71300721FF34772D041EC217AA182F5C9FFEDDC (void);
// 0x0000021B System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::.ctor()
extern void ProtectCameraFromWallClip__ctor_m9BE30002DD3BC07052D7E11A448D6A1330E822A9 (void);
// 0x0000021C System.Void UnityStandardAssets.Cameras.TargetFieldOfView::Start()
extern void TargetFieldOfView_Start_mCBB06309FF6E440654164EDF0557C46AA5C673E7 (void);
// 0x0000021D System.Void UnityStandardAssets.Cameras.TargetFieldOfView::FollowTarget(System.Single)
extern void TargetFieldOfView_FollowTarget_mA92F8333656949ACB3D8A4C383613C2B5791A5FB (void);
// 0x0000021E System.Void UnityStandardAssets.Cameras.TargetFieldOfView::SetTarget(UnityEngine.Transform)
extern void TargetFieldOfView_SetTarget_mFBC0E9995E9CDF97FD74AAC1370400F5FCAB3373 (void);
// 0x0000021F System.Single UnityStandardAssets.Cameras.TargetFieldOfView::MaxBoundsExtent(UnityEngine.Transform,System.Boolean)
extern void TargetFieldOfView_MaxBoundsExtent_mBAAA9C398FF2E1C3F6F80FF5BC51D40CD445E471 (void);
// 0x00000220 System.Void UnityStandardAssets.Cameras.TargetFieldOfView::.ctor()
extern void TargetFieldOfView__ctor_m7F1799AEF5CD5A646C15D7C92B50668263FF525E (void);
// 0x00000221 System.Void UnityStandardAssets._2D.Camera2DFollow::Start()
extern void Camera2DFollow_Start_m71072967C7745D6C231D2AFCFDD0EFDEBCBA7EC5 (void);
// 0x00000222 System.Void UnityStandardAssets._2D.Camera2DFollow::Update()
extern void Camera2DFollow_Update_mD05FC40A3456266BBA46F4992639929727B8C857 (void);
// 0x00000223 System.Void UnityStandardAssets._2D.Camera2DFollow::.ctor()
extern void Camera2DFollow__ctor_m40D25F22E5C17685685B7E7C376DF65E67E72F85 (void);
// 0x00000224 System.Void UnityStandardAssets._2D.CameraFollow::Awake()
extern void CameraFollow_Awake_m2AE9908BCD97FB28E25FF2CE5DA1315D885BE232 (void);
// 0x00000225 System.Boolean UnityStandardAssets._2D.CameraFollow::CheckXMargin()
extern void CameraFollow_CheckXMargin_mC1C2AA7C94A848C8E08AFE9FB8F65DA680088F8A (void);
// 0x00000226 System.Boolean UnityStandardAssets._2D.CameraFollow::CheckYMargin()
extern void CameraFollow_CheckYMargin_m747AE7DD96ACFB05A7CB8FBC71508F297B413F17 (void);
// 0x00000227 System.Void UnityStandardAssets._2D.CameraFollow::Update()
extern void CameraFollow_Update_mAC56DE247C31FBF8F5A056C2C6CF7731F3C24990 (void);
// 0x00000228 System.Void UnityStandardAssets._2D.CameraFollow::TrackPlayer()
extern void CameraFollow_TrackPlayer_m70ECF5BDEC6392E7DD820A7B4581F480E8AC3BC8 (void);
// 0x00000229 System.Void UnityStandardAssets._2D.CameraFollow::.ctor()
extern void CameraFollow__ctor_m95408B3D7ACB4D546C2737F50E0FD63E7C8C93CF (void);
// 0x0000022A System.Void UnityStandardAssets._2D.Platformer2DUserControl::Awake()
extern void Platformer2DUserControl_Awake_m6CCAA55EE7FB2EE3FEDAF3322596837B978DA81D (void);
// 0x0000022B System.Void UnityStandardAssets._2D.Platformer2DUserControl::Update()
extern void Platformer2DUserControl_Update_m2EEEBF981AAAA00BB463E7CF69CE6D47D311998E (void);
// 0x0000022C System.Void UnityStandardAssets._2D.Platformer2DUserControl::FixedUpdate()
extern void Platformer2DUserControl_FixedUpdate_m184BFC5272959EECCEA84FE8F9762B081EAE24B0 (void);
// 0x0000022D System.Void UnityStandardAssets._2D.Platformer2DUserControl::.ctor()
extern void Platformer2DUserControl__ctor_mE97E82E0843CBD49694EF723E483A2D3EBEE3380 (void);
// 0x0000022E System.Void UnityStandardAssets._2D.PlatformerCharacter2D::Awake()
extern void PlatformerCharacter2D_Awake_m0BFC93303AB7CD2421532ACF546493246A2AB21D (void);
// 0x0000022F System.Void UnityStandardAssets._2D.PlatformerCharacter2D::FixedUpdate()
extern void PlatformerCharacter2D_FixedUpdate_m545FE02332DD8F383739DC43D15394F69624E87D (void);
// 0x00000230 System.Void UnityStandardAssets._2D.PlatformerCharacter2D::Move(System.Single,System.Boolean,System.Boolean)
extern void PlatformerCharacter2D_Move_m69E938ADFEBF2FC01C538C2AAE344812FCE7511E (void);
// 0x00000231 System.Void UnityStandardAssets._2D.PlatformerCharacter2D::Flip()
extern void PlatformerCharacter2D_Flip_m1C6BEC4848490BA82CEBDEBA96504BFFD9087045 (void);
// 0x00000232 System.Void UnityStandardAssets._2D.PlatformerCharacter2D::.ctor()
extern void PlatformerCharacter2D__ctor_mCC19D93450D773CCB09D24844FAED11A2710886C (void);
// 0x00000233 System.Void UnityStandardAssets._2D.Restarter::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Restarter_OnTriggerEnter2D_mE683A3E5102CBDC04570D1A39E185017D35A066A (void);
// 0x00000234 System.Void UnityStandardAssets._2D.Restarter::.ctor()
extern void Restarter__ctor_mAFA21081B52C501F5F9C269C6C57DCFC33146E6B (void);
// 0x00000235 System.Void UnityEngine.Purchasing.CodelessIAPStoreListener::InitializeCodelessPurchasingOnLoad()
extern void CodelessIAPStoreListener_InitializeCodelessPurchasingOnLoad_m857FC491AFD5BF425FE16EF88AB6135C38CCBE26 (void);
// 0x00000236 System.Void UnityEngine.Purchasing.CodelessIAPStoreListener::InitializePurchasing()
extern void CodelessIAPStoreListener_InitializePurchasing_m5275372EA20B03DEFD67B5A4F956919C9712A930 (void);
// 0x00000237 System.Void UnityEngine.Purchasing.CodelessIAPStoreListener::.ctor()
extern void CodelessIAPStoreListener__ctor_m180B00F8FE57B89E1E2732E6F93176F0CEF1ED54 (void);
// 0x00000238 UnityEngine.Purchasing.CodelessIAPStoreListener UnityEngine.Purchasing.CodelessIAPStoreListener::get_Instance()
extern void CodelessIAPStoreListener_get_Instance_m6D6C71B6D75F21C64499CE46A23DDC572B3DACAE (void);
// 0x00000239 System.Void UnityEngine.Purchasing.CodelessIAPStoreListener::CreateCodelessIAPStoreListenerInstance()
extern void CodelessIAPStoreListener_CreateCodelessIAPStoreListenerInstance_m4A630850DFC863CB781CE2B0D9206A826C93B8B3 (void);
// 0x0000023A UnityEngine.Purchasing.IStoreController UnityEngine.Purchasing.CodelessIAPStoreListener::get_StoreController()
extern void CodelessIAPStoreListener_get_StoreController_m4FE586D7DEE312A55932FB40044346EB2D4B65A9 (void);
// 0x0000023B UnityEngine.Purchasing.IExtensionProvider UnityEngine.Purchasing.CodelessIAPStoreListener::get_ExtensionProvider()
extern void CodelessIAPStoreListener_get_ExtensionProvider_m6EF90D4617310AF8D82CF0372F0E8E3755B366B4 (void);
// 0x0000023C System.Boolean UnityEngine.Purchasing.CodelessIAPStoreListener::HasProductInCatalog(System.String)
extern void CodelessIAPStoreListener_HasProductInCatalog_m47D805C2D47FBCB3CE1D3132B8C018CE15BE12E8 (void);
// 0x0000023D UnityEngine.Purchasing.Product UnityEngine.Purchasing.CodelessIAPStoreListener::GetProduct(System.String)
extern void CodelessIAPStoreListener_GetProduct_mBD904E64348F60DD664B5D24685EFD98F77C334F (void);
// 0x0000023E System.Void UnityEngine.Purchasing.CodelessIAPStoreListener::AddButton(UnityEngine.Purchasing.IAPButton)
extern void CodelessIAPStoreListener_AddButton_m21CBF0D2CAA3DB2C37986EDA26BC755B8A8EF03C (void);
// 0x0000023F System.Void UnityEngine.Purchasing.CodelessIAPStoreListener::RemoveButton(UnityEngine.Purchasing.IAPButton)
extern void CodelessIAPStoreListener_RemoveButton_m26D317C3F8CB55588914A68E1B361822FFFD6CCC (void);
// 0x00000240 System.Void UnityEngine.Purchasing.CodelessIAPStoreListener::AddListener(UnityEngine.Purchasing.IAPListener)
extern void CodelessIAPStoreListener_AddListener_m6BBCC3C5FD4FE7C7BC0B2FB7841DBB105D2B69EB (void);
// 0x00000241 System.Void UnityEngine.Purchasing.CodelessIAPStoreListener::RemoveListener(UnityEngine.Purchasing.IAPListener)
extern void CodelessIAPStoreListener_RemoveListener_m3DE10AC107870807F9516B33194FE9A0AA9845B2 (void);
// 0x00000242 System.Void UnityEngine.Purchasing.CodelessIAPStoreListener::InitiatePurchase(System.String)
extern void CodelessIAPStoreListener_InitiatePurchase_m47C17C863F18E5A9FF245AA4C06795D76EBE2006 (void);
// 0x00000243 System.Void UnityEngine.Purchasing.CodelessIAPStoreListener::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern void CodelessIAPStoreListener_OnInitialized_m8CA8B9D11FB6BA61BDB2EE43B7BC1C30A88647C4 (void);
// 0x00000244 System.Void UnityEngine.Purchasing.CodelessIAPStoreListener::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern void CodelessIAPStoreListener_OnInitializeFailed_mD27FEA46B1B30942024140E3C2740D46A0C5DD7A (void);
// 0x00000245 UnityEngine.Purchasing.PurchaseProcessingResult UnityEngine.Purchasing.CodelessIAPStoreListener::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern void CodelessIAPStoreListener_ProcessPurchase_m65BBA6E7861D2236652DAC619DA2CD4F840BD5C8 (void);
// 0x00000246 System.Void UnityEngine.Purchasing.CodelessIAPStoreListener::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void CodelessIAPStoreListener_OnPurchaseFailed_mAA5A118AD6E60BC2E66D85A829BF20230FB0815C (void);
// 0x00000247 System.Void UnityEngine.Purchasing.DemoInventory::Fulfill(System.String)
extern void DemoInventory_Fulfill_m0AD2B65A00F24CC71DB1C93CB4C194E2C4F83D8D (void);
// 0x00000248 System.Void UnityEngine.Purchasing.DemoInventory::.ctor()
extern void DemoInventory__ctor_m5B17E344D7C129EE0C3B9C51544E12A2979E51F1 (void);
// 0x00000249 System.Void UnityEngine.Purchasing.IAPButton::Start()
extern void IAPButton_Start_m4AA7E280CEAC12A6791DD5480E00D89831E1B823 (void);
// 0x0000024A System.Void UnityEngine.Purchasing.IAPButton::OnEnable()
extern void IAPButton_OnEnable_m2956708EFC322CD5F747A160554D2CA596A8B013 (void);
// 0x0000024B System.Void UnityEngine.Purchasing.IAPButton::OnDisable()
extern void IAPButton_OnDisable_m15FF6ED10F88FE251AACE1D4B06458326650B535 (void);
// 0x0000024C System.Void UnityEngine.Purchasing.IAPButton::PurchaseProduct()
extern void IAPButton_PurchaseProduct_m03FDF1B08EE596CCD64BA6874EF032F4F00217D1 (void);
// 0x0000024D System.Void UnityEngine.Purchasing.IAPButton::Restore()
extern void IAPButton_Restore_mF9E7AA79D9D19AE9E414DF6A64DBB8787CDA6EE4 (void);
// 0x0000024E System.Void UnityEngine.Purchasing.IAPButton::OnTransactionsRestored(System.Boolean)
extern void IAPButton_OnTransactionsRestored_m2304ED0088AAA420B1AB1D65B2992ED07769CFE8 (void);
// 0x0000024F UnityEngine.Purchasing.PurchaseProcessingResult UnityEngine.Purchasing.IAPButton::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern void IAPButton_ProcessPurchase_mAD1C2C30A543271D9011A0612E37EA62714B2C09 (void);
// 0x00000250 System.Void UnityEngine.Purchasing.IAPButton::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void IAPButton_OnPurchaseFailed_m7147EB0EA21DC3A07676D154824992208E5FD440 (void);
// 0x00000251 System.Void UnityEngine.Purchasing.IAPButton::UpdateText()
extern void IAPButton_UpdateText_m3A7A35C58043862EC01692DC786CBA75C55AD298 (void);
// 0x00000252 System.Void UnityEngine.Purchasing.IAPButton::.ctor()
extern void IAPButton__ctor_m245B69180E5FEF72F72782ED661CD70932349510 (void);
// 0x00000253 System.Void UnityEngine.Purchasing.IAPButton::<Restore>b__15_0(UnityEngine.Purchasing.RestoreTransactionIDState)
extern void IAPButton_U3CRestoreU3Eb__15_0_mBC6ED8A36791E853AE01B77900F2263B27A8A2E2 (void);
// 0x00000254 System.Void UnityEngine.Purchasing.IAPConfigurationHelper::PopulateConfigurationBuilder(UnityEngine.Purchasing.ConfigurationBuilder&,UnityEngine.Purchasing.ProductCatalog)
extern void IAPConfigurationHelper_PopulateConfigurationBuilder_mB2177283D02794DCB7668FEC951AC61AC94CA3DE (void);
// 0x00000255 System.Void UnityEngine.Purchasing.IAPListener::OnEnable()
extern void IAPListener_OnEnable_mAE242FB56BBFBEBA38E86D1DA5A4EC1817EA5FE1 (void);
// 0x00000256 System.Void UnityEngine.Purchasing.IAPListener::OnDisable()
extern void IAPListener_OnDisable_mB75FF6CDE820037D35C4490E0255E1B0F3AC6A33 (void);
// 0x00000257 UnityEngine.Purchasing.PurchaseProcessingResult UnityEngine.Purchasing.IAPListener::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern void IAPListener_ProcessPurchase_mE08DC15E1CC263B6B6C74248B2B342339632FC8A (void);
// 0x00000258 System.Void UnityEngine.Purchasing.IAPListener::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void IAPListener_OnPurchaseFailed_m088716B1A5F44E742992FCD038C486749BC6F597 (void);
// 0x00000259 System.Void UnityEngine.Purchasing.IAPListener::.ctor()
extern void IAPListener__ctor_mDB415F99CF40A742E06D2D0AD27021A342FBD898 (void);
// 0x0000025A System.Byte[] UnityEngine.Purchasing.Security.AppleTangle::Data()
extern void AppleTangle_Data_m9D8150E6665861C333707D0519B5602EF14ECDFC (void);
// 0x0000025B System.Void UnityEngine.Purchasing.Security.AppleTangle::.ctor()
extern void AppleTangle__ctor_mE80A22A474178259DD11042C9A47F2EFFA8E60EC (void);
// 0x0000025C System.Void UnityEngine.Purchasing.Security.AppleTangle::.cctor()
extern void AppleTangle__cctor_m0FBAB41DA0FF906A73D5BD3EA37962A1AA529E48 (void);
// 0x0000025D System.Byte[] UnityEngine.Purchasing.Security.GooglePlayTangle::Data()
extern void GooglePlayTangle_Data_mEF0036018B42640819FBC784A455BDC795D9B731 (void);
// 0x0000025E System.Void UnityEngine.Purchasing.Security.GooglePlayTangle::.ctor()
extern void GooglePlayTangle__ctor_m245442D64C7E6BB1166FDA40FF22CE52FD78E2A7 (void);
// 0x0000025F System.Void UnityEngine.Purchasing.Security.GooglePlayTangle::.cctor()
extern void GooglePlayTangle__cctor_m92C3875F588587BD540B997A339B3CB74DA39215 (void);
// 0x00000260 System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch_ReplacementDefinition::.ctor()
extern void ReplacementDefinition__ctor_m49E65F984C7161F3C2C5DD51172D8C9624AD31E5 (void);
// 0x00000261 System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch_ReplacementList::.ctor()
extern void ReplacementList__ctor_mEC91B69367F2501099D36FD66A5715B772DAACCB (void);
// 0x00000262 System.Void UnityStandardAssets.Utility.AutoMoveAndRotate_Vector3andSpace::.ctor()
extern void Vector3andSpace__ctor_mFE5D5C1F196B500E04A45EC3230CE4FBD138F894 (void);
// 0x00000263 System.Void UnityStandardAssets.Utility.DragRigidbody_<DragObject>d__8::.ctor(System.Int32)
extern void U3CDragObjectU3Ed__8__ctor_m5ED5AC5E5AB22F2FB9178DDCC2D99A933CB2C211 (void);
// 0x00000264 System.Void UnityStandardAssets.Utility.DragRigidbody_<DragObject>d__8::System.IDisposable.Dispose()
extern void U3CDragObjectU3Ed__8_System_IDisposable_Dispose_m011A5F292C63CAACDE3E483BE60B65308D1E1CE4 (void);
// 0x00000265 System.Boolean UnityStandardAssets.Utility.DragRigidbody_<DragObject>d__8::MoveNext()
extern void U3CDragObjectU3Ed__8_MoveNext_mDC1BCC4C79BDB25C44A9DC9735D48FA2E175E071 (void);
// 0x00000266 System.Object UnityStandardAssets.Utility.DragRigidbody_<DragObject>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDragObjectU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m043FAB9E9E779B7ED3968BD00C707E31D47CA661 (void);
// 0x00000267 System.Void UnityStandardAssets.Utility.DragRigidbody_<DragObject>d__8::System.Collections.IEnumerator.Reset()
extern void U3CDragObjectU3Ed__8_System_Collections_IEnumerator_Reset_mC00266C3A2C2B2C5CAA743701D45DC345EC96706 (void);
// 0x00000268 System.Object UnityStandardAssets.Utility.DragRigidbody_<DragObject>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CDragObjectU3Ed__8_System_Collections_IEnumerator_get_Current_m26F191AB7590668B6FE2A7D0F9D5ABC771B90EA0 (void);
// 0x00000269 System.Void UnityStandardAssets.Utility.FOVKick_<FOVKickUp>d__9::.ctor(System.Int32)
extern void U3CFOVKickUpU3Ed__9__ctor_m2E70E0F14634E8A3208961FEA2B1664230D5757C (void);
// 0x0000026A System.Void UnityStandardAssets.Utility.FOVKick_<FOVKickUp>d__9::System.IDisposable.Dispose()
extern void U3CFOVKickUpU3Ed__9_System_IDisposable_Dispose_mD4B00A512ECC65464CEF2F971595D3B28032A8A2 (void);
// 0x0000026B System.Boolean UnityStandardAssets.Utility.FOVKick_<FOVKickUp>d__9::MoveNext()
extern void U3CFOVKickUpU3Ed__9_MoveNext_m512DBF15F770463827161931918513ECBB78759E (void);
// 0x0000026C System.Object UnityStandardAssets.Utility.FOVKick_<FOVKickUp>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFOVKickUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF59D92574D53698089582F9A0BE55EB4A9B9B256 (void);
// 0x0000026D System.Void UnityStandardAssets.Utility.FOVKick_<FOVKickUp>d__9::System.Collections.IEnumerator.Reset()
extern void U3CFOVKickUpU3Ed__9_System_Collections_IEnumerator_Reset_m09B3619F760A7A100802FBC2C44718B3AABD19CE (void);
// 0x0000026E System.Object UnityStandardAssets.Utility.FOVKick_<FOVKickUp>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CFOVKickUpU3Ed__9_System_Collections_IEnumerator_get_Current_mBA658BB1928272A6E7E098ECD397E77C191D04FC (void);
// 0x0000026F System.Void UnityStandardAssets.Utility.FOVKick_<FOVKickDown>d__10::.ctor(System.Int32)
extern void U3CFOVKickDownU3Ed__10__ctor_mAC26AAD43FBF2CEF4164498556AE1B030837944F (void);
// 0x00000270 System.Void UnityStandardAssets.Utility.FOVKick_<FOVKickDown>d__10::System.IDisposable.Dispose()
extern void U3CFOVKickDownU3Ed__10_System_IDisposable_Dispose_mA336F42BB92EDDD223120F28B0A13EF29F9FF9C7 (void);
// 0x00000271 System.Boolean UnityStandardAssets.Utility.FOVKick_<FOVKickDown>d__10::MoveNext()
extern void U3CFOVKickDownU3Ed__10_MoveNext_mF35885E2D181C34AA25B41E9212A468D59DE456C (void);
// 0x00000272 System.Object UnityStandardAssets.Utility.FOVKick_<FOVKickDown>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFOVKickDownU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72EA85FAA1FD7B04B65BB1C719D6472A9B8EC0EC (void);
// 0x00000273 System.Void UnityStandardAssets.Utility.FOVKick_<FOVKickDown>d__10::System.Collections.IEnumerator.Reset()
extern void U3CFOVKickDownU3Ed__10_System_Collections_IEnumerator_Reset_mD9005227142121AFF35782081F84B5AB7F8FD579 (void);
// 0x00000274 System.Object UnityStandardAssets.Utility.FOVKick_<FOVKickDown>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CFOVKickDownU3Ed__10_System_Collections_IEnumerator_get_Current_mFD66D6F9B957FC0522A35FE0D3662667B896A08C (void);
// 0x00000275 System.Void UnityStandardAssets.Utility.LerpControlledBob_<DoBobCycle>d__4::.ctor(System.Int32)
extern void U3CDoBobCycleU3Ed__4__ctor_m3845D1AEC7D920CC91A343EECDAFADD29AA364DF (void);
// 0x00000276 System.Void UnityStandardAssets.Utility.LerpControlledBob_<DoBobCycle>d__4::System.IDisposable.Dispose()
extern void U3CDoBobCycleU3Ed__4_System_IDisposable_Dispose_m337964E05B4A29A899562BA187A6C43AA915FC03 (void);
// 0x00000277 System.Boolean UnityStandardAssets.Utility.LerpControlledBob_<DoBobCycle>d__4::MoveNext()
extern void U3CDoBobCycleU3Ed__4_MoveNext_m9452FC51F84697BCD114A5791B3137BA34CD2DE3 (void);
// 0x00000278 System.Object UnityStandardAssets.Utility.LerpControlledBob_<DoBobCycle>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoBobCycleU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52A882F4C449D3A6AFE38E54266A62CCE9FEB71C (void);
// 0x00000279 System.Void UnityStandardAssets.Utility.LerpControlledBob_<DoBobCycle>d__4::System.Collections.IEnumerator.Reset()
extern void U3CDoBobCycleU3Ed__4_System_Collections_IEnumerator_Reset_m2F0D716C86EF00BBFB2E9150ECA135004FEAC486 (void);
// 0x0000027A System.Object UnityStandardAssets.Utility.LerpControlledBob_<DoBobCycle>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CDoBobCycleU3Ed__4_System_Collections_IEnumerator_get_Current_m7BBD42AB52B89D8F7451CC6A79BA3C660BA93392 (void);
// 0x0000027B System.Void UnityStandardAssets.Utility.ObjectResetter_<ResetCoroutine>d__6::.ctor(System.Int32)
extern void U3CResetCoroutineU3Ed__6__ctor_mE23E11148F2C3B9148F5CF189984AE4CD71E8F8C (void);
// 0x0000027C System.Void UnityStandardAssets.Utility.ObjectResetter_<ResetCoroutine>d__6::System.IDisposable.Dispose()
extern void U3CResetCoroutineU3Ed__6_System_IDisposable_Dispose_m8E5DEBEC17DEB14B5B28C805C11BD627FDAEF4A3 (void);
// 0x0000027D System.Boolean UnityStandardAssets.Utility.ObjectResetter_<ResetCoroutine>d__6::MoveNext()
extern void U3CResetCoroutineU3Ed__6_MoveNext_m62B0E26E3294631050E345318DDC863C31EFAA26 (void);
// 0x0000027E System.Object UnityStandardAssets.Utility.ObjectResetter_<ResetCoroutine>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m59F7FC20AC24226410D75D0B8D5DA40BEEC6C1A1 (void);
// 0x0000027F System.Void UnityStandardAssets.Utility.ObjectResetter_<ResetCoroutine>d__6::System.Collections.IEnumerator.Reset()
extern void U3CResetCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_mDAC82C73DF62065BCD4C1CB44848A55185A3987E (void);
// 0x00000280 System.Object UnityStandardAssets.Utility.ObjectResetter_<ResetCoroutine>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CResetCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m432D1DC8AECAC892944242B8ADDF7CCD6FAD9E9F (void);
// 0x00000281 System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mD0A1B215E5F066646BCAF66C33A5F48D0989E090 (void);
// 0x00000282 System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m1FE667ACD1B0DC25165D56369A3CF8733C0B206A (void);
// 0x00000283 System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m7AB511BE787031F2A04E72326E49E7B5F3B208C9 (void);
// 0x00000284 System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F071490E4E71A9A719D10DAAF032F74B6B31363 (void);
// 0x00000285 System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m32B0CA08CBECCFC5DEF3C43032A5658A24D3C7C0 (void);
// 0x00000286 System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m3E718F7631CCED3184D3AB437310A802E3F5FAF0 (void);
// 0x00000287 System.Void UnityStandardAssets.Utility.TimedObjectActivator_Entry::.ctor()
extern void Entry__ctor_m593D5A5177E927D55B4EDEA893C9A89DA0E3683C (void);
// 0x00000288 System.Void UnityStandardAssets.Utility.TimedObjectActivator_Entries::.ctor()
extern void Entries__ctor_m9FF702A552EFD09D5146A438883B9D3267428151 (void);
// 0x00000289 System.Void UnityStandardAssets.Utility.TimedObjectActivator_<Activate>d__5::.ctor(System.Int32)
extern void U3CActivateU3Ed__5__ctor_m04B00CBAD8695D22C759B14B4228011F72884E9F (void);
// 0x0000028A System.Void UnityStandardAssets.Utility.TimedObjectActivator_<Activate>d__5::System.IDisposable.Dispose()
extern void U3CActivateU3Ed__5_System_IDisposable_Dispose_m6F5CC0A9E69B2ABB18B2F15DC8F1575F3899E175 (void);
// 0x0000028B System.Boolean UnityStandardAssets.Utility.TimedObjectActivator_<Activate>d__5::MoveNext()
extern void U3CActivateU3Ed__5_MoveNext_mF97B09D381486D8A6E40E66208EF442D65683980 (void);
// 0x0000028C System.Object UnityStandardAssets.Utility.TimedObjectActivator_<Activate>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CActivateU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB8461E4FE3B48538BDB9C8FCA99CFB50C4D46C8 (void);
// 0x0000028D System.Void UnityStandardAssets.Utility.TimedObjectActivator_<Activate>d__5::System.Collections.IEnumerator.Reset()
extern void U3CActivateU3Ed__5_System_Collections_IEnumerator_Reset_m10D07A5F6216561973401AE2344F661E0C7E520C (void);
// 0x0000028E System.Object UnityStandardAssets.Utility.TimedObjectActivator_<Activate>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CActivateU3Ed__5_System_Collections_IEnumerator_get_Current_m013FDADA4980E415D472066957CB1D762B166B8B (void);
// 0x0000028F System.Void UnityStandardAssets.Utility.TimedObjectActivator_<Deactivate>d__6::.ctor(System.Int32)
extern void U3CDeactivateU3Ed__6__ctor_m43C0B814DE2C6DF5F445D122620F750158D95A9B (void);
// 0x00000290 System.Void UnityStandardAssets.Utility.TimedObjectActivator_<Deactivate>d__6::System.IDisposable.Dispose()
extern void U3CDeactivateU3Ed__6_System_IDisposable_Dispose_mB5C406251AEA37C27D789602F621349E95B5077E (void);
// 0x00000291 System.Boolean UnityStandardAssets.Utility.TimedObjectActivator_<Deactivate>d__6::MoveNext()
extern void U3CDeactivateU3Ed__6_MoveNext_m690EC1B5BB66A6A7DEA9B82A9EC24767FB669A06 (void);
// 0x00000292 System.Object UnityStandardAssets.Utility.TimedObjectActivator_<Deactivate>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDeactivateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A9DD75E5BEF9E464B4366FB4A5BBFA6C9624FD1 (void);
// 0x00000293 System.Void UnityStandardAssets.Utility.TimedObjectActivator_<Deactivate>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDeactivateU3Ed__6_System_Collections_IEnumerator_Reset_m5C179259768EC874E6C752D62F84805BDC036AA3 (void);
// 0x00000294 System.Object UnityStandardAssets.Utility.TimedObjectActivator_<Deactivate>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDeactivateU3Ed__6_System_Collections_IEnumerator_get_Current_m1F491BC143C92AF323241F437B1854040FBB960B (void);
// 0x00000295 System.Void UnityStandardAssets.Utility.TimedObjectActivator_<ReloadLevel>d__7::.ctor(System.Int32)
extern void U3CReloadLevelU3Ed__7__ctor_m89F904013A652554E256373377BF93992BA1205E (void);
// 0x00000296 System.Void UnityStandardAssets.Utility.TimedObjectActivator_<ReloadLevel>d__7::System.IDisposable.Dispose()
extern void U3CReloadLevelU3Ed__7_System_IDisposable_Dispose_m4EAEA10A25D3BCD8C4A41A7C305673C2702021A9 (void);
// 0x00000297 System.Boolean UnityStandardAssets.Utility.TimedObjectActivator_<ReloadLevel>d__7::MoveNext()
extern void U3CReloadLevelU3Ed__7_MoveNext_m22DFAE90E48E5E9AE2E064903D8AA47C08EB9B48 (void);
// 0x00000298 System.Object UnityStandardAssets.Utility.TimedObjectActivator_<ReloadLevel>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReloadLevelU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F1B7ED565E25B37FC48A22BDB4378F714F5B3A1 (void);
// 0x00000299 System.Void UnityStandardAssets.Utility.TimedObjectActivator_<ReloadLevel>d__7::System.Collections.IEnumerator.Reset()
extern void U3CReloadLevelU3Ed__7_System_Collections_IEnumerator_Reset_mF3AB1DBCA692E293D77D4D9294D0BC47CCB22CFB (void);
// 0x0000029A System.Object UnityStandardAssets.Utility.TimedObjectActivator_<ReloadLevel>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CReloadLevelU3Ed__7_System_Collections_IEnumerator_get_Current_m62C25C33793EE76CA6A5FA59EC19F7EE078C4BD1 (void);
// 0x0000029B System.Void UnityStandardAssets.Utility.WaypointCircuit_WaypointList::.ctor()
extern void WaypointList__ctor_m6F4EDE811589F476A95CC3ECCA3BE59C54AF753A (void);
// 0x0000029C System.Void UnityStandardAssets.Utility.WaypointCircuit_RoutePoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void RoutePoint__ctor_m0CF428B188D139A371423658C3A41B96137CDA09_AdjustorThunk (void);
// 0x0000029D System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mBDD727783F54367BFE2CED7C675B839450955B8C (void);
// 0x0000029E System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6E7E396A3AE4E0720223F5D8DCB4E8A55ED7BF01 (void);
// 0x0000029F System.Boolean UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m4846C74D5F4A4FF39164BCFD48198D976AA0F953 (void);
// 0x000002A0 System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m134C857EC6C5EEB674DF4B22AE43D9CBD4D3596F (void);
// 0x000002A1 System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m886FA065B7C64EA3A746502C9F6BF104B9801F07 (void);
// 0x000002A2 System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mEA0D5C2C7930B6E16648C869029DC54290E095E2 (void);
// 0x000002A3 System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce_<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_m28BA84EE932E5505BA7BD35AC5F8007BA8B96564 (void);
// 0x000002A4 System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce_<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_m2411BD36258EF1A6EF59D488D875E1989C3BAE1C (void);
// 0x000002A5 System.Boolean UnityStandardAssets.Effects.ExplosionPhysicsForce_<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m69D4364C306F1FC8F21C33E29E6C125B2B9EEF2B (void);
// 0x000002A6 System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce_<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m600B8E8FC9018CA7E4700F80CF7114DBD2A89C0F (void);
// 0x000002A7 System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce_<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m8BE9B7F5466A03EDD437E3D16EB3E659B51ABC0E (void);
// 0x000002A8 System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce_<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m60734E8242F26A6B6ADF449A1668715F5E393E00 (void);
// 0x000002A9 System.Void UnityStandardAssets.Effects.Explosive_<OnCollisionEnter>d__8::.ctor(System.Int32)
extern void U3COnCollisionEnterU3Ed__8__ctor_mE0412A81997D86406428B1A95EEDF564D6ED49E0 (void);
// 0x000002AA System.Void UnityStandardAssets.Effects.Explosive_<OnCollisionEnter>d__8::System.IDisposable.Dispose()
extern void U3COnCollisionEnterU3Ed__8_System_IDisposable_Dispose_mE7DC56D7449DC66BB04E6EC60C7B143779E4AEB9 (void);
// 0x000002AB System.Boolean UnityStandardAssets.Effects.Explosive_<OnCollisionEnter>d__8::MoveNext()
extern void U3COnCollisionEnterU3Ed__8_MoveNext_m53C4680C5CFB7DB025A0854EDFBA101B1C74C851 (void);
// 0x000002AC System.Object UnityStandardAssets.Effects.Explosive_<OnCollisionEnter>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnCollisionEnterU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C70CC61071F2B35953F4283AA505C55E6940F69 (void);
// 0x000002AD System.Void UnityStandardAssets.Effects.Explosive_<OnCollisionEnter>d__8::System.Collections.IEnumerator.Reset()
extern void U3COnCollisionEnterU3Ed__8_System_Collections_IEnumerator_Reset_m62403746C385587687DAFDAACBD369E71159685F (void);
// 0x000002AE System.Object UnityStandardAssets.Effects.Explosive_<OnCollisionEnter>d__8::System.Collections.IEnumerator.get_Current()
extern void U3COnCollisionEnterU3Ed__8_System_Collections_IEnumerator_get_Current_m353A3041DBEED2FEBA3CCC26222620D9908BC733 (void);
// 0x000002AF System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::get_name()
extern void VirtualAxis_get_name_mC3959CD36494EE1B06CAEA1675DD19E5FFCB9BD9 (void);
// 0x000002B0 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::set_name(System.String)
extern void VirtualAxis_set_name_m2A44E0BF21BB426C9A14AB057D5EF41616B76096 (void);
// 0x000002B1 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::get_matchWithInputManager()
extern void VirtualAxis_get_matchWithInputManager_m2F68784B6C454EB26934401303E28C159980F315 (void);
// 0x000002B2 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::set_matchWithInputManager(System.Boolean)
extern void VirtualAxis_set_matchWithInputManager_m326813FB9C39A5D63C98D4AE931384D6B67AE944 (void);
// 0x000002B3 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::.ctor(System.String)
extern void VirtualAxis__ctor_m9B094B00B2F0F1C6C474D3DA51419F4549540E53 (void);
// 0x000002B4 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::.ctor(System.String,System.Boolean)
extern void VirtualAxis__ctor_mAC45A3BC043EA253666CCDE2762DB39475FED915 (void);
// 0x000002B5 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::Remove()
extern void VirtualAxis_Remove_m0517C6C37E94CCC84337FD412982D1800E5CEFD6 (void);
// 0x000002B6 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::Update(System.Single)
extern void VirtualAxis_Update_m639BD6EC869B61C712D4519290523C61745FF6C3 (void);
// 0x000002B7 System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::get_GetValue()
extern void VirtualAxis_get_GetValue_mB0D352473A7E1F6A9402335FBD18625ADFCE0A69 (void);
// 0x000002B8 System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::get_GetValueRaw()
extern void VirtualAxis_get_GetValueRaw_mA75834F100AB39C130FEA7AE85677E4928E58397 (void);
// 0x000002B9 System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::get_name()
extern void VirtualButton_get_name_m836058DAC831C5BB481A422120939EB4D14CE55B (void);
// 0x000002BA System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::set_name(System.String)
extern void VirtualButton_set_name_mCC77CE771C89C23B47A2D9B027C7E754666A78A5 (void);
// 0x000002BB System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::get_matchWithInputManager()
extern void VirtualButton_get_matchWithInputManager_mD6924A44FFCFF72519BDDEAD61E3072CC3C3FCF3 (void);
// 0x000002BC System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::set_matchWithInputManager(System.Boolean)
extern void VirtualButton_set_matchWithInputManager_mD438AFD4E212727BED9ECD1F0CBFE6243112AE3D (void);
// 0x000002BD System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::.ctor(System.String)
extern void VirtualButton__ctor_mECADC4A0B8ACF0954720A84061800EA0F00D9FDD (void);
// 0x000002BE System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::.ctor(System.String,System.Boolean)
extern void VirtualButton__ctor_mBC57649412C90DFF3179B681B9D33BB88443FFD9 (void);
// 0x000002BF System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::Pressed()
extern void VirtualButton_Pressed_m596B075C829D1E8C500AF6694155488CF2250402 (void);
// 0x000002C0 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::Released()
extern void VirtualButton_Released_mC4B98C45864A5832601A90437E691119F28E25E6 (void);
// 0x000002C1 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::Remove()
extern void VirtualButton_Remove_m0F66A404819C8B483DA3F02FDCEBDB005867D37D (void);
// 0x000002C2 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::get_GetButton()
extern void VirtualButton_get_GetButton_m228F811AD3C4911C45AFEA7960E35F4A84B7A32D (void);
// 0x000002C3 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::get_GetButtonDown()
extern void VirtualButton_get_GetButtonDown_mB6BBC9E21BB477279E5D74926CFA633E671AC430 (void);
// 0x000002C4 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::get_GetButtonUp()
extern void VirtualButton_get_GetButtonUp_m79C31A03EE6AC926E932FA1A28989A73B0257E43 (void);
// 0x000002C5 System.Void UnityStandardAssets.CrossPlatformInput.TiltInput_AxisMapping::.ctor()
extern void AxisMapping__ctor_m2B8C914999C51C9568C81B4C1E6750BCAF66BE1F (void);
// 0x000002C6 System.Void UnityStandardAssets.Vehicles.Car.SkidTrail_<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_m67CE23090D68646148DEF07ECBEC73F47D6C2B97 (void);
// 0x000002C7 System.Void UnityStandardAssets.Vehicles.Car.SkidTrail_<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_m742D326FAD73D62F6EA840726E5D633444959A2F (void);
// 0x000002C8 System.Boolean UnityStandardAssets.Vehicles.Car.SkidTrail_<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_mDEA0D45B6339E08A90F11699ABD4CC14253C9083 (void);
// 0x000002C9 System.Object UnityStandardAssets.Vehicles.Car.SkidTrail_<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB747C2A701CEEDAF4ED3A50DE8FAED78F6F88E47 (void);
// 0x000002CA System.Void UnityStandardAssets.Vehicles.Car.SkidTrail_<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m39A4E2321FFB793C383C8E723435921B7C786AA3 (void);
// 0x000002CB System.Object UnityStandardAssets.Vehicles.Car.SkidTrail_<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m35A89B378AFF03387749E69BBF345C104D46975B (void);
// 0x000002CC System.Void UnityStandardAssets.Vehicles.Car.WheelEffects_<StartSkidTrail>d__18::.ctor(System.Int32)
extern void U3CStartSkidTrailU3Ed__18__ctor_m344B8023D623A699ED904AA4C6CC701FCF2A3420 (void);
// 0x000002CD System.Void UnityStandardAssets.Vehicles.Car.WheelEffects_<StartSkidTrail>d__18::System.IDisposable.Dispose()
extern void U3CStartSkidTrailU3Ed__18_System_IDisposable_Dispose_mEEC56B8DD0472D10807B89C581C96DA95BFF8F31 (void);
// 0x000002CE System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects_<StartSkidTrail>d__18::MoveNext()
extern void U3CStartSkidTrailU3Ed__18_MoveNext_m4735C2EEFE7E83A4C89738BB83DAB7F69069453B (void);
// 0x000002CF System.Object UnityStandardAssets.Vehicles.Car.WheelEffects_<StartSkidTrail>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartSkidTrailU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCD092F7744E5ABFBC34309F1221837C2EFAAC6A3 (void);
// 0x000002D0 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects_<StartSkidTrail>d__18::System.Collections.IEnumerator.Reset()
extern void U3CStartSkidTrailU3Ed__18_System_Collections_IEnumerator_Reset_mD3A710A3304A7564EF9189858EA480496EC41B8F (void);
// 0x000002D1 System.Object UnityStandardAssets.Vehicles.Car.WheelEffects_<StartSkidTrail>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CStartSkidTrailU3Ed__18_System_Collections_IEnumerator_get_Current_m89DDE8F9BAA46DE9913128C9D92E27ABF635ABC0 (void);
// 0x000002D2 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio_AdvancedSetttings::.ctor()
extern void AdvancedSetttings__ctor_mE02988E47F76E04FFE1264222FDCE2DC9A9A169E (void);
// 0x000002D3 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator_ControlSurface::.ctor()
extern void ControlSurface__ctor_m132B3895DCFAFD6AC23218A4EEB856457D43812A (void);
// 0x000002D4 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController_MovementSettings::UpdateDesiredTargetSpeed(UnityEngine.Vector2)
extern void MovementSettings_UpdateDesiredTargetSpeed_m92BE3A22C9CF55BF12A0D441EBB51FF7A1779D69 (void);
// 0x000002D5 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController_MovementSettings::.ctor()
extern void MovementSettings__ctor_mF768FC3EE16A9A355B9FCD47EF76C8A4879FAF29 (void);
// 0x000002D6 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController_AdvancedSettings::.ctor()
extern void AdvancedSettings__ctor_m845AB1B94472E573D2C5DD585876E67B97C0AD23 (void);
// 0x000002D7 System.Int32 UnityStandardAssets.Cameras.ProtectCameraFromWallClip_RayHitComparer::Compare(System.Object,System.Object)
extern void RayHitComparer_Compare_m6EFBFB91A4F97CC5D2111878BCD211DCEA51DAE3 (void);
// 0x000002D8 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip_RayHitComparer::.ctor()
extern void RayHitComparer__ctor_m2A11DF0646D70F2DAFC0751CD63F55A40D397D11 (void);
// 0x000002D9 System.Void UnityEngine.Purchasing.IAPButton_OnPurchaseCompletedEvent::.ctor()
extern void OnPurchaseCompletedEvent__ctor_mB5BA13357B2FA5082EF0ACFFC7AEA22B77338399 (void);
// 0x000002DA System.Void UnityEngine.Purchasing.IAPButton_OnPurchaseFailedEvent::.ctor()
extern void OnPurchaseFailedEvent__ctor_m63FF8EE5D613396B41404B3FBDA39618069CFBAA (void);
// 0x000002DB System.Void UnityEngine.Purchasing.IAPListener_OnPurchaseCompletedEvent::.ctor()
extern void OnPurchaseCompletedEvent__ctor_m89CDE2E04A0B4D52670EADC46E4E5B84B89A070C (void);
// 0x000002DC System.Void UnityEngine.Purchasing.IAPListener_OnPurchaseFailedEvent::.ctor()
extern void OnPurchaseFailedEvent__ctor_m6729A97F19ECA2F73A9A0BC3654C81A145AFBC2E (void);
static Il2CppMethodPointer s_methodPointers[732] = 
{
	IAPDemo_OnInitialized_m09812EB1EC8BBA41EE3137537FE76F1985776941,
	IAPDemo_ProcessPurchase_mF11FD082E2AC70F7C74D691915DD38FC0D124C2B,
	IAPDemo_OnPurchaseFailed_m9D538C282F6B4A14D7C526AB41CD475AB40A03EC,
	IAPDemo_OnInitializeFailed_mFDCFA3E72D8DD6763002AB3DA6302C215226FCAF,
	IAPDemo_Awake_m2128837EF5B16B8E7DCADD847AB73B139A0D53B6,
	IAPDemo_OnTransactionsRestored_m65536FCBC3C132696644D596F848C015E4061647,
	IAPDemo_OnDeferred_m86E94791D018CC76F49E9F257221C3E95CFF8E2A,
	IAPDemo_InitUI_mEBA5CE85CA66B0D5A8797C6A4EE73108C12163BC,
	IAPDemo_PurchaseButtonClick_m68E3B1E0E39E8A4E021962F1A4A55DE2D0186D32,
	IAPDemo_RestoreButtonClick_m091801F3E7FEE2D39A621DE2A98D0F0681246410,
	IAPDemo_ClearProductUIs_m75D0954CCB2CB7F166FFD2ECC7C5733719D457D5,
	IAPDemo_AddProductUIs_m7CE03873CDFA4B5C7C9062ABC5D2007936CD1DED,
	IAPDemo_UpdateProductUI_mA3E55E43CDDD31BC2B2B710C0A941CD6E3539665,
	IAPDemo_UpdateProductPendingUI_mA66A6DDAA8C73AF557E8F949288231D1C138D3C4,
	IAPDemo_NeedRestoreButton_m74B174F978D8A6BC71E222E3263DBED8E6B19F30,
	IAPDemo_LogProductDefinitions_mFB48DEFF4C240E67F87CAAD3E63B4233C0D6CC60,
	IAPDemo__ctor_m138A28A1415DEE8DC74F29BD7131F7CC94C77AAB,
	IAPDemo_U3CRestoreButtonClickU3Eb__25_0_m6EE2A6168E2D85D012FA378CD39D3AF801A0F1CE,
	IAPDemoProductUI_SetProduct_m0224417743A36089E5808E03A5EE18F79FC4057E,
	IAPDemoProductUI_SetPendingTime_m8BF5F4E1000C492C5B38C2CB893D572997C1C324,
	IAPDemoProductUI_PurchaseButtonClick_m0FCCEC6722A3D10A8EBFBD98D116A710E94179FC,
	IAPDemoProductUI_ReceiptButtonClick_mF247336DE6321F9D20C61A49153D3891C465F846,
	IAPDemoProductUI__ctor_mFB851E7A6CEE9D2DED0F4F77601A344899A99F28,
	AlphaButtonClickMask_Start_mEFF14FD0C0E68631370255F5936C41963983C8F0,
	AlphaButtonClickMask_IsRaycastLocationValid_m4E83446D8B35549DE9EEE9F6D4548A527A3C884D,
	AlphaButtonClickMask__ctor_m5D0B36A0D207C1AD45B2434C1036141F3D9EA28D,
	EventSystemChecker_Awake_mADD5BA9635C9ADBE91C44BC15DFFCB87798377EB,
	EventSystemChecker__ctor_m52A33F6E257A581FC40521C4CE4B484D2426DA66,
	ForcedReset_Update_m35CBD926C48BD4ACCDD3CB14E0813D04F0E83FC0,
	ForcedReset__ctor_mF32F8C351B95343959FD07B1FF0EC0FBC8CB8EE0,
	ActivateTrigger_DoActivateTrigger_m91589CEC5598200BE6AC01C6D8ED7CD3C05ACF03,
	ActivateTrigger_OnTriggerEnter_mBB7C6EBA79177C6CBEB136C565F17135353319DB,
	ActivateTrigger__ctor_mE845995D67483BC443AE11D6EB0D4E133358EF15,
	AutoMobileShaderSwitch_OnEnable_m2CC4D934531213CC59B29149430BE5454F220327,
	AutoMobileShaderSwitch__ctor_m1D46DC3555ED91D57F71F19BD08DB1F72AA8928F,
	AutoMoveAndRotate_Start_m8A0766F7DA35E226BEC81C38DC800B7C2395E654,
	AutoMoveAndRotate_Update_m23C3C317C277A79CF82C88044544258DD7201411,
	AutoMoveAndRotate__ctor_m97BEA90D4085E63DA4D810FED6F495216024AC6B,
	CameraRefocus__ctor_mAEC82E5DC336BB11096300A567B554CF5235781C,
	CameraRefocus_ChangeCamera_mD34A981CD7F9E0F4CB3DB5A3C929A969E0FFC538,
	CameraRefocus_ChangeParent_m95B7671F5C921BACD887A784A798B9C373C107FD,
	CameraRefocus_GetFocusPoint_m708CC6CD84E14B6285E4404388F0369B9A550C40,
	CameraRefocus_SetFocusPoint_m07D61D09C19C5F951C126B32B3341733C33C20BF,
	CurveControlledBob_Setup_m2007E3FF9FB757CC191CEA557786000FDD658477,
	CurveControlledBob_DoHeadBob_m00B658784E0C963606ADCAB3E3FA9C347F305C6F,
	CurveControlledBob__ctor_mC6A43DC3D1DD13D04AC060CCB621CCF90965BC90,
	DragRigidbody_Update_m83FBCA60844264E0D9544B4BA94D8EE3590B1055,
	DragRigidbody_DragObject_m2BF373EA880D49253F3C5BB726D8724B2288A80C,
	DragRigidbody_FindCamera_mAF1386C508DAEB88A9C5FD4D456EF4D4BE617D9F,
	DragRigidbody__ctor_m6E05682AB45C4BFE0DE11A644D612926F9E76E7D,
	DynamicShadowSettings_Start_m77EC175AF947D4CD8BE908FEAFA761CB247E6D78,
	DynamicShadowSettings_Update_mCA0BBCA7CA61B468857533B4C7896AFABBF9175C,
	DynamicShadowSettings__ctor_mDC8B0D663149C35F242EADFEFED8BA7D4DBCBC77,
	FOVKick_Setup_m8469A5705E531E55305A58D3AA3265FA265D33F6,
	FOVKick_CheckStatus_m9A63EE82724EA159E55AC55E840B4CE3701BE714,
	FOVKick_ChangeCamera_m3218EF587B033155B743FE279EDCB6CF1CDF4C6F,
	FOVKick_FOVKickUp_m91F264E5432BCE86E9082C169D277E64C481D236,
	FOVKick_FOVKickDown_m8BCEE7A586E2B8FAD73F8D8F5D2FB0808A1EEB75,
	FOVKick__ctor_m3D6A4FBAE68243DB82771B3772735848FA090280,
	FPSCounter_Start_mA01345F424F9731649187CC62E9E69FF0377EB65,
	FPSCounter_Update_m8E1247E9BA50C20B0439DF5CA383621EB7B9FFE5,
	FPSCounter__ctor_m3458B72F16625D58BEFC538E933635817205602D,
	FollowTarget_LateUpdate_m5ADBC5E3D58826659C01A2C4F8968A595F092435,
	FollowTarget__ctor_m7ADC7081450A6DA01AE0B5CA1DB84BE961D5DD29,
	LerpControlledBob_Offset_mCC7467CE1E66FB6246E52C5984D1C7BE2C0BF1B2,
	LerpControlledBob_DoBobCycle_mB5C14AC5AEF7114F4A575E19B36BEDF0DE691923,
	LerpControlledBob__ctor_m8E3865BC67563143CFF3C791D0BBFC25D94B18BE,
	ObjectResetter_Start_mF14852C85D595D52F35F221F45A822606EA013B7,
	ObjectResetter_DelayedReset_mCFE31FD35B3AC724EFD1E76270A1FE3FD5014394,
	ObjectResetter_ResetCoroutine_mD46D3A049C28C60DB794613257CC61C5102B0EFC,
	ObjectResetter__ctor_mD14550E617FC16A6C1338BE7D18C5B01141A6354,
	ParticleSystemDestroyer_Start_m28FC4048F585ACE30B2FB2B6839F2801F8FFE584,
	ParticleSystemDestroyer_Stop_mC007423F42F57A16264A616C6970FF62E3A37617,
	ParticleSystemDestroyer__ctor_m3A484EB728A7ECF87E3BF235EF10F4EF49E11623,
	PlatformSpecificContent_OnEnable_m3C87AD39C8B588F5FBA2C7D2808C717A238A5BFB,
	PlatformSpecificContent_CheckEnableContent_m6C285401B4F1F4BFC5E748C7753096628CDAB355,
	PlatformSpecificContent_EnableContent_mF21760A340E974403AD8731E6731AD1BD6B6C73C,
	PlatformSpecificContent__ctor_m8464E7FB25A5C1EC2C1343995EC02402AA72804F,
	SimpleMouseRotator_Start_m8D06607C94F05C2C99A1D74A1D75AA716B1FE80E,
	SimpleMouseRotator_Update_m43E994D26B1A1396F1C87F119B313442201E7222,
	SimpleMouseRotator__ctor_mC1B6E0FDF0226BE88DF307078BD8EE4E5DC546B8,
	SmoothFollow_Start_mDCCB4CDC6353E65906B648B5AF23AB18D2E51C72,
	SmoothFollow_LateUpdate_m7F7C460DDC3C80A3016F8050F50F54C6D55C5C15,
	SmoothFollow__ctor_m3BA62097DC88A8513EFFEFCF2D5CD8D3DB7B548F,
	TimedObjectActivator_Awake_m79DD17D62A8055E1E336A30C26DB80C4D5407289,
	TimedObjectActivator_Activate_m12334B3127ED32333F52AD3DE31A5ABC8EA4D415,
	TimedObjectActivator_Deactivate_m6A966711DD7B1088A53D5CAC48760661834A3979,
	TimedObjectActivator_ReloadLevel_m80D86EA368E03010F6065856EF104F53D4FAB590,
	TimedObjectActivator__ctor_m26FCB0BA5A4631EC3F435E0182C9C4E702B1EAD6,
	TimedObjectDestructor_Awake_m76424E72B7BE0F6424D18BEF93CAB84DBAAC34A6,
	TimedObjectDestructor_DestroyNow_mE93B19E7C626189327CE5300D972C0BDBBE04851,
	TimedObjectDestructor__ctor_mE45D4C3E5E22221059FB74DF5134F06171EFDE6D,
	WaypointCircuit_get_Length_mFA587B16273CB5FD90CC3BD7479A2810B4D318F8,
	WaypointCircuit_set_Length_m39E0ED03A71FEC5DA4B5851069C677BB5A8A657B,
	WaypointCircuit_get_Waypoints_m95D546B796B966FA6ADB8AE7B553F25CCF548211,
	WaypointCircuit_Awake_m3443AE18FBD735BE48A510AD2B277A86CEB1CC52,
	WaypointCircuit_GetRoutePoint_m8D42746EC9609D8082B0E7CC8E19A2C4F27800B0,
	WaypointCircuit_GetRoutePosition_m0973D453CE3244E6E1066641A81AD3FF0EADBB6F,
	WaypointCircuit_CatmullRom_mD59EEE7A548ACAE81B8650873BCE7547CEA63077,
	WaypointCircuit_CachePositionsAndDistances_m69B6E9F8A8CA46B05F025FD09309FC126FCF6F46,
	WaypointCircuit_OnDrawGizmos_m69BAAF20DC9C4E903B5BA88D0DE0D30D95456287,
	WaypointCircuit_OnDrawGizmosSelected_m07DB42CB844CC4517075DF5AFFDC5F2AAFDE8134,
	WaypointCircuit_DrawGizmos_m614C51310D7116AECB903F6CA4A3047F819C9520,
	WaypointCircuit__ctor_m349B1FD72D470FC31D2C7D18D0625C3DE68C9304,
	WaypointProgressTracker_get_targetPoint_mF25566A72CA099496F6C921209690D340E7E1067,
	WaypointProgressTracker_set_targetPoint_m9FF276072D75E5B62BDFDD69FE2BC764B332E636,
	WaypointProgressTracker_get_speedPoint_mD1DA662D3473D8A387FA4B9217E024C9F86ECE32,
	WaypointProgressTracker_set_speedPoint_m5CCCC560F087199E1A30537AF62CAAE4EFDA1062,
	WaypointProgressTracker_get_progressPoint_m2CBA2DD7271E5B4DCBEDE3D4A583DDCBAF7843CD,
	WaypointProgressTracker_set_progressPoint_mDB352CD4E635034EF5155F6661F200CCDE51B5F7,
	WaypointProgressTracker_Start_m3AE39D9788836A78979E71F8F6BC3109BD843341,
	WaypointProgressTracker_Reset_m71BA52616EDE5E48445AAC8163F57CD90A5B3602,
	WaypointProgressTracker_Update_mCF073672622D91EF9F70ED8E870A6E4400ABD694,
	WaypointProgressTracker_OnDrawGizmos_mAE2FA8689934F94640161CFA136411E825660EC9,
	WaypointProgressTracker__ctor_m6832B310DE7A3327A4AC027F4CD32D1D47D33722,
	AfterburnerPhysicsForce_OnEnable_m31ABCDDA8E500224F25244C9D3FE44CBDD45D7CE,
	AfterburnerPhysicsForce_FixedUpdate_m90286B7CDABBED931E9D13E5D879E6EC41BF6E8A,
	AfterburnerPhysicsForce_OnDrawGizmosSelected_m8E75C894A4544C40EB1FD3DA8883E3358BAF8AA3,
	AfterburnerPhysicsForce__ctor_mE4F415134C355B0C418241BA9F4BE2DAB65BF1A0,
	ExplosionFireAndDebris_Start_m85654B2EA9348AE14094A4CDCA1ECD714E4CBC22,
	ExplosionFireAndDebris_AddFire_m781F3E400896C4F2E2B4A24FAEFE52D01ED26C0B,
	ExplosionFireAndDebris__ctor_m6E9B3DD674A9E1DF3B85ECF3E618F7F29B5EB97E,
	ExplosionPhysicsForce_Start_m22750184987B8A375DA674EF615F5ECD1DAD04C5,
	ExplosionPhysicsForce__ctor_m3A4FFA852D5C736E4CE47A5A5A3E0CA1E954F660,
	Explosive_Start_m500D588F6ABB1C03488B93AE78086A368943CDE2,
	Explosive_OnCollisionEnter_mB88EB7BF1526BC0F9418D997273E1A1C875D00E1,
	Explosive_Reset_m878D1F8DF175604AFB07827C7A4DA9DA20FFA9DA,
	Explosive__ctor_m21EF3DDCC8F02804ED974F48197F95DB5B4900F4,
	ExtinguishableParticleSystem_Start_m442E9165E311C7DC5AE322C5CF8B8D6A2325DC39,
	ExtinguishableParticleSystem_Extinguish_m2E9C8C61E7E4E1083A3059582EE9F68BB0D8EFA8,
	ExtinguishableParticleSystem__ctor_m66F7FA6921D145A9EF55397A60FCC4CA574DA068,
	FireLight_Start_mBC43BC2A9EDAD47A5D43C96C04E1AECC4405B81D,
	FireLight_Update_mE6D64BEF14D90624104BC092626B738B75D8D0F7,
	FireLight_Extinguish_m96CC75996F151C74E4D46556599FDF4F39AF6776,
	FireLight__ctor_mFAEAECD04029DD5E24A331FEE4B5C43FFD5B6FBE,
	Hose_Update_mF09247CA136AF92D5586DD9C95823C7E9A6AE939,
	Hose__ctor_mDB316C87F32CA6A88980009F13FCB35475103094,
	ParticleSystemMultiplier_Start_m9629829804A66FA8E1BA25039F0BCD4447D1785D,
	ParticleSystemMultiplier__ctor_mFA383D5A7F8469462B1E9A5EDB01811D52DC0CFC,
	SmokeParticles_Start_m1A4CBEB9EDF7F792FCF7F96D9435B6C0B0A94DD6,
	SmokeParticles__ctor_m6FF23CF3C3FFDCE57F75C6DA08F84101A4953F0C,
	WaterHoseParticles_Start_m6CFC5FEBF60CBD00CDE17E0A1EEC12163EC9A792,
	WaterHoseParticles_OnParticleCollision_m8E8399822017058F9AE64E3D9F7B47208553ED70,
	WaterHoseParticles__ctor_m9E63BF105FF8511932CBF7FAA1C664713744DCA3,
	WaterBasic_Update_mEEEE08B8B6B13BE9FBA6F084A2156358BC26D6C8,
	WaterBasic__ctor_m49406BA46E86FA0F0F9B38EB5C722A8E6A5818AA,
	Displace_Awake_m61C8CDD475DBFF95E5F49AB4BACE14195072498C,
	Displace_OnEnable_mC321CC696070DF2589D76CCDC75368C26F7229B2,
	Displace_OnDisable_mC4144199F8E22EBE3DA4A0F12F8E1DE5242DF2AA,
	Displace__ctor_m4AE5636C99B0D5EA3302D02AEB66EA40CE8373F8,
	GerstnerDisplace__ctor_m35315933124DD20E54791E269DB4D70C015A2254,
	MeshContainer__ctor_m7AFFB5FA9DF2A68C6FD1D0D74D454C08FC3C2D6F,
	MeshContainer_Update_mFB60BB3EA3E2208F955B8B3E5E3B7D6DA02CBE24,
	PlanarReflection_Start_m080C3900D498A758F7C3D91521483C1918ACD017,
	PlanarReflection_CreateReflectionCameraFor_mC1D4652399484BA52AE739A3B5BC9097134F3034,
	PlanarReflection_SetStandardCameraParameter_m6EDE582E77457B8979E2541C7AC5254B8B06EB9D,
	PlanarReflection_CreateTextureFor_mEB5585E46857A99E2B6D5BA5A64D1096142379CC,
	PlanarReflection_RenderHelpCameras_m2E41F43A249C5914135F688C2A399A1F1136D8CE,
	PlanarReflection_LateUpdate_mBBC544AFAC39678265F0EA5FA99300F27A4E7A2A,
	PlanarReflection_WaterTileBeingRendered_m548DA1220CA714D157B2BCE1FDA9B62452E44BF5,
	PlanarReflection_OnEnable_mB1326BB27BA4555CAC9D7B377628E29E60671465,
	PlanarReflection_OnDisable_m0D7B36CE5C749EBE3F83B7F3EB1CB9EBBE74E333,
	PlanarReflection_RenderReflectionFor_m1E44B323B3346A1DF17715767DE62F73678CC525,
	PlanarReflection_SaneCameraSettings_mC1302B40E9D18A8158C450711F94681F8CB3DEC2,
	PlanarReflection_CalculateObliqueMatrix_mFD6BB9C5ECC438DD6F5C9C0F8F65CFB2F36D1866,
	PlanarReflection_CalculateReflectionMatrix_mD04A2F12363F240EF2C67FF52519839FA4F14A5C,
	PlanarReflection_Sgn_m5B43D2788FE8B53ABD0A8F0126AE33F299540B91,
	PlanarReflection_CameraSpacePlane_m2FC7140D1B54C86880E7AF73418F39F7026D278A,
	PlanarReflection__ctor_m43064A2D31F467165DF112691A5C8A24EBF591BE,
	SpecularLighting_Start_m10E337FAAECD39FF2A7D9EEF2E8A5DFA70186923,
	SpecularLighting_Update_mA6ACED168A4954AAE814DE5C8A17E832F93D28FE,
	SpecularLighting__ctor_m575A28F9197B313E49CAFD95AC5B890B36B917FD,
	Water_OnWillRenderObject_mDE2C1FD54029095E1061CF810E120835733E28A8,
	Water_OnDisable_mF858B4AF06C20E874842E99A946BAC0E7A2C2CA2,
	Water_Update_m5EF099F5001646EE42E97C1B5EC27E751F8AA3A1,
	Water_UpdateCameraModes_m52D76E61B982584208C2A4A98B7695C53C5A55A6,
	Water_CreateWaterObjects_mF2E91CE0B823EAB2E86B4D9FF3A70CF95467FB4D,
	Water_GetWaterMode_mE8C165BBAB7C969AA8E0C95679DB1E37330501A8,
	Water_FindHardwareWaterSupport_mF01DDA5818E2E789020B8069439EE6CEBE7D6CC4,
	Water_CameraSpacePlane_mF0515845AD37744BF290009E27B5C926951FCD0B,
	Water_CalculateReflectionMatrix_mECC5B0D91E795E07EEEE661E632B26037819DAC1,
	Water__ctor_mDF27A1B8260BF800899BF28EC4325AFC3BB28741,
	WaterBase_UpdateShader_m9983EF83D7AE22E03EBEAD0A092DA47966AAFD8E,
	WaterBase_WaterTileBeingRendered_mDB8CFDD6A7C116E854488EE804DBCB5709779A5C,
	WaterBase_Update_m3E55AB0CF58C6A85641A6DD30229A874BB2204B3,
	WaterBase__ctor_m82F74340A1EB4DEA5EC6D8B94835F5C523013B81,
	WaterTile_Start_m7304AD008CF318463F0C8BF6334D323FE4B2201F,
	WaterTile_AcquireComponents_m9AF4968726C7A7A1914C99383AC33F70F0734CAB,
	WaterTile_OnWillRenderObject_m49C59909BFF5850B610AE766A77A09732700A16E,
	WaterTile__ctor_m9F814F1C72EC77EA173735BC25383BB020C9643E,
	AxisTouchButton_OnEnable_mCB6F2E22CE0ED2867462D2B6477319CA34EC6923,
	AxisTouchButton_FindPairedButton_m40E5A08627D81FC2C9B18410E0A315A00AFB8E5E,
	AxisTouchButton_OnDisable_mA60CC8A5ACA0AF8EA245F67C774CC15489D26F0D,
	AxisTouchButton_OnPointerDown_mF8ED454A7629EC10D5D65B0806B1575B969CC151,
	AxisTouchButton_OnPointerUp_m47ACA384B0AB0496024E2BA07670DE7F416442A6,
	AxisTouchButton__ctor_m92FC868F9C30069B6E82AEFB601E72D7958146EB,
	ButtonHandler_OnEnable_mF4257EC750A191164B42AA7ED19E95221E0C9084,
	ButtonHandler_SetDownState_m22A563B85C0CFC7883586FE49D47EC8F4795740E,
	ButtonHandler_SetUpState_m20D0A07A6047D866EB95EB5D32D6D5C208CF779D,
	ButtonHandler_SetAxisPositiveState_m2914614CA82F6CE2BCDEF41198D6505C08692F60,
	ButtonHandler_SetAxisNeutralState_m710F8A130253345840FC6A942453EBD386322691,
	ButtonHandler_SetAxisNegativeState_m57D4FA1CCB8E9F4CCB3D5AB589D57328EBDA782C,
	ButtonHandler_Update_m84201A612F81A8C08E1E4F78635C47098A9C6BC0,
	ButtonHandler__ctor_m85B4D66D1CA83E5A73BFC70CAFB4F2D00F559795,
	CrossPlatformInputManager__cctor_m62BA59D81597A25895815DDB40DD8944A6B804F2,
	CrossPlatformInputManager_SwitchActiveInputMethod_m3BAB791534CAB7AA957901F370FE854C63AB1801,
	CrossPlatformInputManager_AxisExists_m13A4ED5F88BAC335FDC42ADD4AAC9BB4CC5809F9,
	CrossPlatformInputManager_ButtonExists_m6C0E094AF61CF2F0F6592EFA67763D208C867440,
	CrossPlatformInputManager_RegisterVirtualAxis_m84945297F5E2C4D218B59B76E9D90D3BD36198A4,
	CrossPlatformInputManager_RegisterVirtualButton_mA5218520E9EE798325C72DFD0C988DC313D36BCF,
	CrossPlatformInputManager_UnRegisterVirtualAxis_m33DCEB8DAAF2703BFAB8F156A6633C0F4316C1A4,
	CrossPlatformInputManager_UnRegisterVirtualButton_m4B8F22F23F0891C1F5D4C07B729564D6A95CB82D,
	CrossPlatformInputManager_VirtualAxisReference_m5864A44C3FE72270B22D4C97FADEEB2AAA77869D,
	CrossPlatformInputManager_GetAxis_m4D45F9BE30A159DA4E72F4BF8294872297566E2D,
	CrossPlatformInputManager_GetAxisRaw_mE6D8754EAE5F6838CCF172FB03F4C251648EE987,
	CrossPlatformInputManager_GetAxis_mC9F177F6F0D83131B599CF80C3F3A8D7AD4568A0,
	CrossPlatformInputManager_GetButton_m728A64B9BC3F6471EB11B9CAF54BD4A10C710207,
	CrossPlatformInputManager_GetButtonDown_mE1BCD85447E0EF510728E49314FBCCEEE1FC7E8D,
	CrossPlatformInputManager_GetButtonUp_mD115A6BD45062A08A42EBBC7F0C9EC0D4F764ADD,
	CrossPlatformInputManager_SetButtonDown_m4DBFE81592B86D460ACC34D5936C788CD5B50890,
	CrossPlatformInputManager_SetButtonUp_m6228A0BD77568A903DF6429EEACD2267028FA32A,
	CrossPlatformInputManager_SetAxisPositive_mC5C7F88EEF5D6CB7B6B91BF6279FA53A94B4D527,
	CrossPlatformInputManager_SetAxisNegative_m41A74CBE51E8CB4870C79A8343E66B99B2CA7FDB,
	CrossPlatformInputManager_SetAxisZero_mBBD24590C97037F84384A559AAE37D2F8CA51730,
	CrossPlatformInputManager_SetAxis_m6BCE358D3D1A2E5E393AF281602B3E4745C0C5DA,
	CrossPlatformInputManager_get_mousePosition_mC886FC2F654E91F06407FDB891DF3201ED576DCD,
	CrossPlatformInputManager_SetVirtualMousePositionX_m1800042FCD90010EA2E2D51969D971324DD11964,
	CrossPlatformInputManager_SetVirtualMousePositionY_mDD4A2DF42E6CD673054A91FFE3C7FA61812889A8,
	CrossPlatformInputManager_SetVirtualMousePositionZ_m121058A0846AE6A974855607C8E3D46C221B376F,
	InputAxisScrollbar_Update_m4B6A6BBF4FAED786086BE4F9997E1D2D373BF2FE,
	InputAxisScrollbar_HandleInput_mF3A427E653ED917C3E91E0CBB1A3990F6110FB11,
	InputAxisScrollbar__ctor_mB96FAA176CD2958CCDE6E5F9212DCF2082486243,
	Joystick_OnEnable_m8728113F5BEE6D91514CB1A07550E8E7A3856CEE,
	Joystick_Start_m6164BF9BB7A0A8DD4524223639EA549E0491CFFD,
	Joystick_UpdateVirtualAxes_m5B79E0FBC765F85D9EE7FA9C7D74BDB35F326F3E,
	Joystick_CreateVirtualAxes_mCD13DFD2ADED0444F18C3856FD67A78539FD9C2C,
	Joystick_OnDrag_m1DC1103944EB982931C5946BD8EBFB8E63073BB6,
	Joystick_OnPointerUp_m06850F5D6C95D16DEB57B3FC4E50CCBCCD0EF7FB,
	Joystick_OnPointerDown_m1F10B670117FD67A734079ED71D4A3D36B783718,
	Joystick_OnDisable_m5097E08289FECC9A5499DB0747575F075353CAFB,
	Joystick__ctor_mA2C408B1EB0671CB8B340DBF932CB4153BAC3ABF,
	MobileControlRig_OnEnable_mAF3C7A8C67CE239A1FD5E6A8B224F7A91DE8B2E8,
	MobileControlRig_Start_m43792FB70FC02989DA9543801183A54005AD572B,
	MobileControlRig_CheckEnableControlRig_m59A8FCD09B2A6EA7702AE9EBB24E3BB9605B5CCD,
	MobileControlRig_EnableControlRig_m694051D1F28B05510357A3F96561EAF2732CAF8E,
	MobileControlRig__ctor_mCCFD8CBDA57F8D9B0E2805D4740637F5FFC4B120,
	TiltInput_OnEnable_mEACF194C56E3620055240D9D46880E6F9C201E9C,
	TiltInput_Update_m14D22BD1D9D47DF03965F0F6BBC2FB0E322F2B2A,
	TiltInput_OnDisable_m169AFDCFFA0609747DA889DF88C86D0A5C9C42B0,
	TiltInput__ctor_m909CBEC7014B584CB5EBF5A0B650E5D8845E4FB7,
	TouchPad_OnEnable_mFED012C0FDD349798B1296799C0AE7A05C2017EE,
	TouchPad_Start_m986951F12FF80D3A0D4B0DCB49D9FBA13B8A025C,
	TouchPad_CreateVirtualAxes_m214CE099E087A7A6FCDDF3B2740983436B62BA5E,
	TouchPad_UpdateVirtualAxes_m42D25C5EE9F890FECF580C97219455E73D09AF67,
	TouchPad_OnPointerDown_m13FDEACD95785D853D85B68E4993AC520A2D771F,
	TouchPad_Update_m0DD077DCE945CC47C3DD4FFDB9FEC5D4BB3A762A,
	TouchPad_OnPointerUp_mE35C9A5F6CD1909E8819F4D6D7282C7D20B37B88,
	TouchPad_OnDisable_mD0E67236EB0D365E3397D26723250C01614168B5,
	TouchPad__ctor_m9FEC2CD43CD850304B41B1C0142CC47F44B01E25,
	VirtualInput_get_virtualMousePosition_m897C50683722D1C3DF4FA9801524E7BF310B24BD,
	VirtualInput_set_virtualMousePosition_mDDF9F35B2C4AC37AB6CCF68772C57315612B1F75,
	VirtualInput_AxisExists_mDB6E7D0AF32ECE3E3CB1C4DA089D4B030D61F3F8,
	VirtualInput_ButtonExists_mBD9401EC2186C54F8EA7577FEEA500624F2E6083,
	VirtualInput_RegisterVirtualAxis_m43BC4BC9355B708CC739E3F2D0761A49342BC60F,
	VirtualInput_RegisterVirtualButton_mF6874262B94F78D0C2C166F7E20CFA47DD39BF41,
	VirtualInput_UnRegisterVirtualAxis_mD3511EE52A02EF720B086FF6EDCF9D4FA11A551F,
	VirtualInput_UnRegisterVirtualButton_mF05E241BD753B335E97CB8D1EDCFECE82A34F554,
	VirtualInput_VirtualAxisReference_m5AE323533C7DF65D71B551B173A63680BB5850EA,
	VirtualInput_SetVirtualMousePositionX_m49716B45CE295686844FDD803083136B9BAC2124,
	VirtualInput_SetVirtualMousePositionY_m80139449D4E09227D929E314419B1C72D57BD001,
	VirtualInput_SetVirtualMousePositionZ_m9276A4D39BC31E00C1977B2621549B1C1F40E51D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	VirtualInput__ctor_mD6A4228D372182ABC7372ED25F4987CE1EAA27CB,
	MobileInput_AddButton_m55B4ECB00F31F0904145B5DC71AE2B7289960F34,
	MobileInput_AddAxes_mF5065897FC94197F4FD5BDD15A394E858218496E,
	MobileInput_GetAxis_m24CDEC7DA08736467196B8F90F19B3110782421A,
	MobileInput_SetButtonDown_mF3C9EEAF5750B7CF53C0D6D04D035CA8F1D27547,
	MobileInput_SetButtonUp_m0CEDEA05459505931FB2686C20AC0900A4941448,
	MobileInput_SetAxisPositive_mD4522AE0A5CFA591D720C9FA1E42D38485F66C9A,
	MobileInput_SetAxisNegative_mDB7F89D127295F2D4CC4764EB04571D9A46774C4,
	MobileInput_SetAxisZero_mCE681BFD720000CFA939C78B0EAEFCA3D5748BA8,
	MobileInput_SetAxis_m40791B9F5D8B28086FEA1030918A6DDBD96D2704,
	MobileInput_GetButtonDown_m8B7EC91AD10FF37A6910CC3AD684572F8CC4A403,
	MobileInput_GetButtonUp_mFB0BF6CE172238F3AF6B28CB16A73B5A0D714ABB,
	MobileInput_GetButton_m1805C5AAFEA6C56E1F083C318C7D8A56414742DC,
	MobileInput_MousePosition_m655B9F793060E92EEAFC358ED5A612124F71B234,
	MobileInput__ctor_m58D4C2380917920DD39E646CB4717F6EFBAA16F0,
	StandaloneInput_GetAxis_m208A36BD2256D5439E8BF99DFEE7C4FBE5C321DB,
	StandaloneInput_GetButton_m2156BA026DDB9F6FA9F45BBC8FEC871A3090629C,
	StandaloneInput_GetButtonDown_m3F88DFF900E2AB8729E6F63694BE3C8E2C19BBB7,
	StandaloneInput_GetButtonUp_mDF55E35A4B50D58901CEEF8DAEECD050A636398C,
	StandaloneInput_SetButtonDown_m5C1B0E5ED19F91DAEE8A23108865EBC57EB3F002,
	StandaloneInput_SetButtonUp_m23ECA36E7E2C9D79650FC93764E2FB47C52A6269,
	StandaloneInput_SetAxisPositive_m45ABA3A91481B6B07E37A24322C345DE4341472D,
	StandaloneInput_SetAxisNegative_m8ABB3422B35FA3D5EFEC9A3BCCE7D813C91E1DDA,
	StandaloneInput_SetAxisZero_mC0123C06F1DD19FF6DB9353DF0D711554B56B428,
	StandaloneInput_SetAxis_m620AB31F30F82FDB44EB995E494F618D8C42F765,
	StandaloneInput_MousePosition_m7D158ACA958E8C1101AFF4B3282E12B1A6EF7C82,
	StandaloneInput__ctor_m9D98FDF717857FB17019872D7A8446C224C493F3,
	BrakeLight_Start_m6FA4032B993EFE5F1059BE1C01E7C1D2A0BF346D,
	BrakeLight_Update_m58A9D7EBAE1F58C2D0795168423E073993CF21A2,
	BrakeLight__ctor_mE02A5C210554431EC725807D1B1AB86F23418297,
	CarAIControl_Awake_mF92BDAA5D715FABE633F84D59500C746B634DCDC,
	CarAIControl_FixedUpdate_m26805A937384424C95C7B64BB05B7D9D021C4D0B,
	CarAIControl_OnCollisionStay_m81DF24158CCA819188959562DDB9BCD42F09DC71,
	CarAIControl_SetTarget_mCBE313C1482CA8994757D5681C72505E2EECAE12,
	CarAIControl__ctor_mE9F288A9A54AAD345AEAB775B0C4AC667B733A52,
	CarAudio_StartSound_m49CD6BB9F527D0D8460BB7AFCF73E5D73445233E,
	CarAudio_StopSound_mE351DBBDC263DCA8533FA3EE8E77ED4BF9C9C425,
	CarAudio_Update_mF8869D3AB05CBB0128063D98F4832430DF6E08F8,
	CarAudio_SetUpEngineAudioSource_m3721814137E76AD388A2359DB3AD0EBBC86EAC4F,
	CarAudio_ULerp_m68093FA00B1EA90D7D6F11A43F45DECE53FC9B25,
	CarAudio__ctor_mC6B7E6375DDD9B1D51181B688F076FAE0CC46483,
	CarController_get_Skidding_mED1EF7F4B7EF21E50AE15F9C9AE642EF870D7C08,
	CarController_set_Skidding_m7643D70E3DBF7A99FD6F95AAB89CF87A45910C79,
	CarController_get_BrakeInput_mD6D7BDD8B48342BAE79EF8E175F933DA949BC50B,
	CarController_set_BrakeInput_mCDE2E14F7FA94E46FB770DC10F5D97755A698C6D,
	CarController_get_CurrentSteerAngle_m4471982C9B82947566A2B0B543C6FFE7C2C782DD,
	CarController_get_CurrentSpeed_mD6B1EA413895CDCD1A27C332806627EED2FD05BD,
	CarController_get_MaxSpeed_mA12355ADBE4068E7FB9D2853D67C2C7256DB9681,
	CarController_get_Revs_m642BB53135EFB44913D28B192CF79A46952B5B58,
	CarController_set_Revs_m0A174149D801834C4FB861004BE55F48DAD539A4,
	CarController_get_AccelInput_m6B2E21B4598992F917B375D1F20FEAD6F776C98B,
	CarController_set_AccelInput_m9C0B3E3084272E47988C62C8621595257BC95722,
	CarController_Start_mAD71184E15FCC34EE427C662E9F9B98F92478E29,
	CarController_GearChanging_m88CC4B8B99B933BF7C414C172D1FF32259D48EDC,
	CarController_CurveFactor_m435DFFB28E8D1BF2FCEF3D828E8454E401E986C2,
	CarController_ULerp_m7AFD80930A0A3EAD6E5AF2BA080570457C7A4CCA,
	CarController_CalculateGearFactor_mA27673F6760B7759FB4A76307187C0036AB538F2,
	CarController_CalculateRevs_m25B70852D908A1AB31AE9EA6EF7377BFDEB3A4F4,
	CarController_Move_m710DD5723707ECC83BC04A764642BED780DDAB67,
	CarController_CapSpeed_m63B0D340A0A007F27B2C5EEE6D3D4573410EEB8D,
	CarController_ApplyDrive_m6EC1D7D52A955EC9A1DEBE750E8473630362B6C5,
	CarController_SteerHelper_mF6312DD3E610EF92C21202D79F193355D5186FE6,
	CarController_AddDownForce_mE6479A5330F91DF49FBF98AAB609741CF1834D6C,
	CarController_CheckForWheelSpin_m5061F7DB82BADC9E3C8BAF21A2DE6F128F850B1F,
	CarController_TractionControl_m23F265B5248063648F5E014BDB68FB6B59718062,
	CarController_AdjustTorque_mA5882A0C0656B68D9E372B6768836B94EB7F3A36,
	CarController_AnySkidSoundPlaying_m8B971D76E9867A390546CC838B9D8DD1DCFD09BC,
	CarController__ctor_m43D96461E612E6F36ABD70D10526994C7CD151FE,
	CarController__cctor_m88FF6E1ABA60DA504E1EBD899D7C711F3A2BEEB4,
	CarSelfRighting_Start_m34D21601B67D3855133E7C41944F91FE4281A692,
	CarSelfRighting_Update_m4E5AAB22314DA341FC7417D62D33462A1EF7D575,
	CarSelfRighting_RightCar_m8AC643B94AE9BBA6D2BCEF91387167E0200DBB9F,
	CarSelfRighting__ctor_m7816689E202E3531F4F02A214FB0752DDFA1F2BD,
	CarUserControl_Awake_mBD2084DF81305CE9361535339E04D0E0C8FA701D,
	CarUserControl_FixedUpdate_m4D383203699A6910E27F96E9A5E8132A2A6C4DA2,
	CarUserControl__ctor_m5059918AB540551B020BEC800622EC92042C70D7,
	Mudguard_Start_m9226767403487DA21425EC7C367DCB88066A1194,
	Mudguard_Update_mACCACB23ADE483B77BC14713B28B96E8A5F972A1,
	Mudguard__ctor_mB1DA1E48B1642E14A9D280F52EAB50AD02DA09E3,
	SkidTrail_Start_m6EAC8D56A5360AA8D0AC402F872E6F9280A77559,
	SkidTrail__ctor_m8437FAAA8F97487CD9614A0496F9B822D5197D0F,
	Suspension_Start_mAE6F9830A242D87D4468F88A781EBCEA923923B6,
	Suspension_Update_m5AE98DF7A19725655F4579567DAD011EDF808A6D,
	Suspension__ctor_m9AB521AFE45C92BC134C3A2710C756B239A74FAF,
	WheelEffects_get_skidding_m718DA419C2DEC9EF477DCD45FAEB68E94FD28070,
	WheelEffects_set_skidding_m4F42423A2EC98533350F0D7003AFFD7037047D1D,
	WheelEffects_get_PlayingAudio_m431797CA8A5F826E63C37541F58B3AA32C813D0E,
	WheelEffects_set_PlayingAudio_m4E4C1F97DAFCC638424BAB8EF77C58CEC79E8F8C,
	WheelEffects_Start_m4874BB9D66D03D0389AADBC1D497BC80EB85A431,
	WheelEffects_EmitTyreSmoke_mA14E266D87D309D477F1FEFEADC57846FD72F0DC,
	WheelEffects_PlayAudio_m9BBE568F5CA041E518CB7979C29759EF3252345A,
	WheelEffects_StopAudio_m1B4DD35BCDC32EB80778CDB29053441801350DC3,
	WheelEffects_StartSkidTrail_m07F5D9D62535160E1D4EF8CAB1BCB0DEB3D1A4E9,
	WheelEffects_EndSkidTrail_m75DBF6F2CD4990256F986C45F8D4F26754859A1B,
	WheelEffects__ctor_m1A9D3A67287DE2F37D1146854B0DC0753E8E6F02,
	AeroplaneAiControl_Awake_m9A029336C6899361B55E43BE14671BC598C5DE0C,
	AeroplaneAiControl_Reset_m8252C9E199162483947A5F688AB6E9579FE488E7,
	AeroplaneAiControl_FixedUpdate_mFF1DEADE3DB3DFF770798138E02E7B31B4016DB4,
	AeroplaneAiControl_SetTarget_m92D1BD8BAF5491C380BBE7424C438CE4CB713FC9,
	AeroplaneAiControl__ctor_m3147BA9E0D0C18E9CC8BA0FCF04C8724D118E715,
	AeroplaneAudio_Awake_mBDA501A01C9C4D1235B7A4D2D260DB909CB5EC34,
	AeroplaneAudio_Update_mDE4DB712DC69B4255038A33071BAEE2F6C6D41D6,
	AeroplaneAudio__ctor_mFF0EC4C94327233C7E9FC9A87F55B7E92F5ABFC6,
	AeroplaneControlSurfaceAnimator_Start_m394939E227828330580FF355EFC203158D4C525A,
	AeroplaneControlSurfaceAnimator_Update_mD2630FB7F3B956ABEE88FEB9ECB0237AD986D9BB,
	AeroplaneControlSurfaceAnimator_RotateSurface_m92D96999DCAB552668517BBDFEEB2912C50175DE,
	AeroplaneControlSurfaceAnimator__ctor_m956F30E568E00B4E5FF9791F2ED7839732D3010C,
	AeroplaneController_get_Altitude_mD7201103ED5136DFA948E1BB374D35F8EAC7B5E0,
	AeroplaneController_set_Altitude_m001A742194DD428A55BDB81BE7664861D9AEDF39,
	AeroplaneController_get_Throttle_m5C8C9063565BF4155E32685B8239AC683E8D1860,
	AeroplaneController_set_Throttle_m7E6012252676A570E173410CDDEC0EB8288B408E,
	AeroplaneController_get_AirBrakes_m1CFBAF5AB7C92AB1F61684BB4D1967EA3A928493,
	AeroplaneController_set_AirBrakes_m7AE541D6CDD1F86D9B4A86DA133AC3E0069A0A2A,
	AeroplaneController_get_ForwardSpeed_m4C12FF50AC636FCE6DCAAFA0F6E03E06D2764616,
	AeroplaneController_set_ForwardSpeed_m8FB8A92A2FE310B734E34A7F8BEEA2152276F470,
	AeroplaneController_get_EnginePower_mF310ABEA4346D9C3B55B779EF4815F47D6E07CBB,
	AeroplaneController_set_EnginePower_mB403B061246E89F8DD4973585C4D8A7C331C010F,
	AeroplaneController_get_MaxEnginePower_m9280EADC8CF88E999E38B3CB84C4A3EE92F6D4FC,
	AeroplaneController_get_RollAngle_m34CB3EA79F50B97260A62F1CB01C0DAF22434623,
	AeroplaneController_set_RollAngle_m1898D1ACC21640F9A68EDD879A1D5AD698662B7D,
	AeroplaneController_get_PitchAngle_mF301494C501E8B7EDC6594BAB747A881E21B362C,
	AeroplaneController_set_PitchAngle_mF4798C2AD4464B304153FAB38D852D42B758F4ED,
	AeroplaneController_get_RollInput_mA53807BDCA085B6AEA7B4337BA3548F8355BF48C,
	AeroplaneController_set_RollInput_m518F9EAB08362038401C50D8F9F5AA7075F3A609,
	AeroplaneController_get_PitchInput_m2E0D1D37BDDD013A63B566CB4347745F6C505CDD,
	AeroplaneController_set_PitchInput_m5AF8AB111BD768C178571E1C73CCD1A60D9E3763,
	AeroplaneController_get_YawInput_m72AA1B1E6DF8F75937A6788C11B4ACDA1D4818D5,
	AeroplaneController_set_YawInput_m8871D002FE34675A2CC7368CB12EE6E6623C227E,
	AeroplaneController_get_ThrottleInput_m9A1F72722CA45AFB58D5AA946EE0279A4972DDB4,
	AeroplaneController_set_ThrottleInput_m61682CDCB5059E3A5C98CC2C823ED0BA935FF595,
	AeroplaneController_Start_m91DB0FFF9AC9FD0564CC94041B3F91ACD4CC9155,
	AeroplaneController_Move_mAC1C689885B75E5641AD5E1C6F152FEFA4F7BE11,
	AeroplaneController_ClampInputs_m016971EC421B9CEFECC9BFCD11F7C135F9A3D29D,
	AeroplaneController_CalculateRollAndPitchAngles_mF4D4FFD7B0F1D11F8A547F5F23BBFEC4540797EA,
	AeroplaneController_AutoLevel_mD6E24EE2CEDAC943067992DB737EBF4A30F3337D,
	AeroplaneController_CalculateForwardSpeed_m2D0B2B98A56A5587995855875E952FAE8CB13E9C,
	AeroplaneController_ControlThrottle_m532F9D3E57351424D211FF2B1F40B8A01E6C7322,
	AeroplaneController_CalculateDrag_m8F2E00CA0EE41B7D2E0FE519AF0EAD45AD0A4C70,
	AeroplaneController_CaluclateAerodynamicEffect_m3BD28CE3E7ACED75AB91099D75E28AA30B18213B,
	AeroplaneController_CalculateLinearForces_m8BCF085FB44FFE2452A4B65A16DE81B79B14D5E6,
	AeroplaneController_CalculateTorque_m9C599F6684BD6B09AA9DE4C9BFC6D7B19FC0B899,
	AeroplaneController_CalculateAltitude_mC82570DA9ED0D63DAF9200F6FF68B0C50FE56636,
	AeroplaneController_Immobilize_m5D2097492C7D56F9D64F99BDB1810C87A4FC43B4,
	AeroplaneController_Reset_mF40F9E0E6EBDBBEFD1ABE6324859B002FBC3CF6F,
	AeroplaneController__ctor_mF8F75B326B55A734A640E4714F1B3E8382043C6C,
	AeroplanePropellerAnimator_Awake_m73F8E4075B7C19461C26222D44653E485C93EF47,
	AeroplanePropellerAnimator_Update_m1BDE8DD2603B0A04E2970F9F3170E413E0CF33AB,
	AeroplanePropellerAnimator__ctor_m1586C357A4E00185C6B6D29229BCADF3BD0E8415,
	AeroplaneUserControl2Axis_Awake_m09FB41E61246F704CCC3070E1E3568201EB84DCC,
	AeroplaneUserControl2Axis_FixedUpdate_m16B1B755DEDF56F134C50482C6D511EDAAE4CE05,
	AeroplaneUserControl2Axis_AdjustInputForMobileControls_m2CBC1D3BE5969BE5A264288CC005953AE209ADB4,
	AeroplaneUserControl2Axis__ctor_m23FD94504D00FC293ECDAD2FD12E6D26AC83B98B,
	AeroplaneUserControl4Axis_Awake_m941A60DB28E42AE8E4BD9F2B3B1F7A829496EF17,
	AeroplaneUserControl4Axis_FixedUpdate_mD8EE23F7EC6B7D20DE69C78FC46236EE41233C5D,
	AeroplaneUserControl4Axis_AdjustInputForMobileControls_m3499E0435C7E07EA209E56701621020E62E9D8AE,
	AeroplaneUserControl4Axis__ctor_m39F39B3842E11BC4F5AFB74FF9B12A54F421E787,
	JetParticleEffect_Start_m5B27DF2D7C3FCBDC53C6DDA433C06B7C1B7D0531,
	JetParticleEffect_Update_m8B8989022284D304D827C5EC7A3119C193969C09,
	JetParticleEffect_FindAeroplaneParent_m469DB0B9CC662E181536CE754C8EE52822B59C06,
	JetParticleEffect__ctor_mE08093E39EFD497EE56B68EE601A8793CC9CBF3E,
	LandingGear_Start_m86A572F6D9AD3AEC918C352A22B8EAABEE644C7D,
	LandingGear_Update_m0ECCD00D8CD63939029C0BFFBCE65C71680A31B8,
	LandingGear__ctor_mBCFF08F7E8D509EA8BA896A6B874671F140CD5BC,
	Ball_Start_mD94D5E17C462F68ED882F7A71510571FE3BC99E2,
	Ball_Move_m2876C25E0C756BF245A81AD607EFEF4635163D37,
	Ball__ctor_mCAF1E03B050453AC083D54A0395370CF4E322D4A,
	BallUserControl_Awake_m3053C00DA15A4F03C606E253DF2F1222233D2CD4,
	BallUserControl_Update_mCF16AA2CFDB461475BEFE7F4A82AD02E27B5A2C9,
	BallUserControl_FixedUpdate_mAFB462499A540A51F3A59B50EACEF013A1FD99C6,
	BallUserControl__ctor_m933AEE908ED128E00AA48445422BAAACE54D27C8,
	AICharacterControl_get_agent_mC145C43591E7F58C19D6812346E8B267562DCE2A,
	AICharacterControl_set_agent_m3AF797E33435DE57A5B68D4A370B38FF5066AC35,
	AICharacterControl_get_character_m5F68CEC317221C119DD993F0BC66E83B46ECAF10,
	AICharacterControl_set_character_mB648916EAE530CF42ADB6A2133597CD2C5C351AE,
	AICharacterControl_Start_m94E0B0C90E7F8680D0BA14C0B7F73F1147F1ECBD,
	AICharacterControl_Update_m3739D8300A1B4ED83717268FACD69BA35E2F2D60,
	AICharacterControl_SetTarget_m85A54D9230685AEFC19FE47B0844545F3AC76ED7,
	AICharacterControl__ctor_m64E75FC80DC62E05D71EA1EDE411F5E9118F9895,
	ThirdPersonCharacter_Start_m77BED10E07BCE3B9FCDFDAB0A604598E7EF52551,
	ThirdPersonCharacter_Move_mC404A566646B48F6E4895C4B56B568B2FCD7B315,
	ThirdPersonCharacter_ScaleCapsuleForCrouching_mEF225C57F19EEFB21F3FA5065F835ED295F9FA83,
	ThirdPersonCharacter_PreventStandingInLowHeadroom_m11B4FCE921A339A567D1DE504C5781738FE130F4,
	ThirdPersonCharacter_UpdateAnimator_mA55D3E6EEE3D094A0377F74680EDC955C915C2F3,
	ThirdPersonCharacter_HandleAirborneMovement_mA70C2C3E9776D1F93AC11B00DBB614A653D05713,
	ThirdPersonCharacter_HandleGroundedMovement_m9E47FDBABA1394392BC50B9D0E8F617417DCD8EA,
	ThirdPersonCharacter_ApplyExtraTurnRotation_m04D602E8910E9F91436DC3978A237DA976E0528A,
	ThirdPersonCharacter_OnAnimatorMove_m1B720209EAC4123222D83A2604CDD9DEB35B4807,
	ThirdPersonCharacter_CheckGroundStatus_m529274699AE7FB0AA68E6259A4009C5119346DD6,
	ThirdPersonCharacter__ctor_m2A343C6DA11522069E215C58B8C5F0F2916482B9,
	ThirdPersonUserControl_Start_m6E6473ABBBE127E5FED209498ED24663D0612682,
	ThirdPersonUserControl_Update_m673BE47FF260D53BF312CAB76F77C32D8CA22629,
	ThirdPersonUserControl_FixedUpdate_mDF454F6DBD58CBB0429BBC36502150EF9D360453,
	ThirdPersonUserControl__ctor_m7BD727F15D1289F92B24B581C9DDB5A7489A967E,
	FirstPersonController_Start_m87414ABA8CE33FC0DE5E9856C79A357E501D7308,
	FirstPersonController_Update_mF3FC7041AB276DC382BC8A4F61AEAFAFAEA04ED4,
	FirstPersonController_PlayLandingSound_m0E02AFCFF243AD8D37BA51E95A07CAB9E92B5B65,
	FirstPersonController_FixedUpdate_mBA424DA0A9AA19D6C2E50F798C372AA7543FD918,
	FirstPersonController_PlayJumpSound_m45E7E5399ABB1944DD6EDD9702E5FFB899886BC4,
	FirstPersonController_ProgressStepCycle_mF0FA5D7B32881E407247E42A6F07B1F49AB5717F,
	FirstPersonController_PlayFootStepAudio_mE2350EC1F9F5D34A04464C32EEBE1E7E82134179,
	FirstPersonController_UpdateCameraPosition_m9287722E622F693557E810274D76CF402849E7B5,
	FirstPersonController_GetInput_m20FF731BB9AE80DB53A15A87BB960E7552AF5730,
	FirstPersonController_RotateView_m52AE6EADF85961F72D76454E11913AFE0AB0C77A,
	FirstPersonController_OnControllerColliderHit_m359FEB09F912EF3311435D6CF9CAFB25CA6EBDDC,
	FirstPersonController__ctor_m067AA344391066103FAE6175AFC5C1717B4022F6,
	HeadBob_Start_m2956DB87FE33B62FC550D8E8581B1197E790BD8F,
	HeadBob_Update_mBF51EAFAB23AC5B5E176E02CCCA668AF6B20F4AC,
	HeadBob__ctor_mD8C44B9DC99CA8E61E23578A15973F48D1B1D077,
	MouseLook_Init_m6F89547F704698EFB173D980CEC974CD6D11CE1E,
	MouseLook_LookRotation_m12F5371B9F69C6E79A959B1707E2079EF176FE47,
	MouseLook_SetCursorLock_m74E47ED1CDCF73947446BA6F3109C7305B44A27E,
	MouseLook_UpdateCursorLock_mC03FF4763BE1894EDD81D90A96791E644BDA4DF9,
	MouseLook_InternalLockUpdate_m689C8345609827E54B2C45018C9FA2710641AE41,
	MouseLook_ClampRotationAroundXAxis_mB80B11F6AA879942BCC5B59AC99857BEC811CEBB,
	MouseLook__ctor_m4991C1F282EDF4C515B9C28106EC981F5D157CF2,
	RigidbodyFirstPersonController_get_Velocity_mA3844E469740CF2B014878B8BF192EF385675B28,
	RigidbodyFirstPersonController_get_Grounded_mB76301608244EA277030FED9FF07421B1DE37A55,
	RigidbodyFirstPersonController_get_Jumping_m7E2EACD44C0241B8423B97E1739DBE269FE204A6,
	RigidbodyFirstPersonController_get_Running_mE86F6DD182214508725455FC918CAF13BC1A462B,
	RigidbodyFirstPersonController_Start_m2330F14619B7112B794576FAE87657F51E16F998,
	RigidbodyFirstPersonController_Update_mC9D36ED411466D763F3B6726ED0264FE5815D040,
	RigidbodyFirstPersonController_FixedUpdate_m81ED45B8FE887F4C512FC12EA0EA4A83AECC370C,
	RigidbodyFirstPersonController_SlopeMultiplier_m5BC79F85397DC3492513C1AC8088A8E62DE47B37,
	RigidbodyFirstPersonController_StickToGroundHelper_m1ECAEA0961503D6BBF65AA20D056F4E803C13CA8,
	RigidbodyFirstPersonController_GetInput_m4DDAC49710C5FA777D29BF16A154A8B35D6EA8D8,
	RigidbodyFirstPersonController_RotateView_mD3A6BE7C73F1839CE62FBECC3A03E53C68AF8711,
	RigidbodyFirstPersonController_GroundCheck_m7843581359DD47F573ADD9059732FA20246C4FC0,
	RigidbodyFirstPersonController__ctor_mCCA1D3CC7960FE1BDB756F1CC3BC6D12DC5BC471,
	AbstractTargetFollower_Start_m5830AC9B925EF10A96832CB64E17703537ABA4D6,
	AbstractTargetFollower_FixedUpdate_m059E3F7EE069E830A1675B7E609B2C0992F5C9FE,
	AbstractTargetFollower_LateUpdate_mE6D4BAF43EA1C593A3439507476CAF6C360874F5,
	AbstractTargetFollower_ManualUpdate_m5FDFBD137B58BE58593D6D751BAA4C4A5134C158,
	NULL,
	AbstractTargetFollower_FindAndTargetPlayer_m0A72C9D084555F206759AF9BBC56C039170E497A,
	AbstractTargetFollower_SetTarget_m17CE0F35DAE4E4086CACD31BA14C05DA2589D3A9,
	AbstractTargetFollower_get_Target_m24234F14398E0958CEA1A476F649E762D6E17814,
	AbstractTargetFollower__ctor_m7CA75F5DEB95588875BF359F17F9A45F1AC28E18,
	AutoCam_FollowTarget_m49D07251C6021E9F838CC8747A0CF83EFCEE8552,
	AutoCam__ctor_mD9E4154D81A29B6C8C68297374653FB9CFAB3AC2,
	FreeLookCam_Awake_m9EAF9CF5A48AE92736DB798522FFFA95DF0C33A0,
	FreeLookCam_Update_mD83A18376B2B5F9970C8DA78E60DB12C1B508AFC,
	FreeLookCam_OnDisable_mF4EE05B50BF94C52C65ABE1F26A383F3F614DE54,
	FreeLookCam_FollowTarget_mADE713C6EA3D2A61B6FEFBAC2226C9519728478C,
	FreeLookCam_HandleRotationMovement_m835C5DFA2FD21F7C64057BDB0BAEA62B99A2D404,
	FreeLookCam__ctor_mB38818588467BA1204288BB062BBAABDCC0225D9,
	HandHeldCam_FollowTarget_m9BF734BC3214DBB892E0D2D507E2F9452B147FDB,
	HandHeldCam__ctor_m57D82D85C30C87D03E7EAB05EBDF656962DA2AB5,
	LookatTarget_Start_m55D1DA54A4762FF2AF364421AED7C54396705CBA,
	LookatTarget_FollowTarget_m363133638041D45CAB46C88DD61768E4E39DF534,
	LookatTarget__ctor_m765D6EC2839C0E8BDF17F849E28A0D472A37D25D,
	PivotBasedCameraRig_Awake_m59237BF4FB7603169D6E12D95C372D7A889E1F27,
	PivotBasedCameraRig__ctor_m3A67ED42B196F44E73ECF164084D9655F6BE700F,
	ProtectCameraFromWallClip_get_protecting_m1E2FFDFDBBEEA74AD539E8F35522047B7F14F9FB,
	ProtectCameraFromWallClip_set_protecting_m23B7F1BBEE76FDAFF187820178B1CDAE2A326F55,
	ProtectCameraFromWallClip_Start_mFD2770D700A02F262AF9926E4C5828B0C8AC544C,
	ProtectCameraFromWallClip_LateUpdate_mA71300721FF34772D041EC217AA182F5C9FFEDDC,
	ProtectCameraFromWallClip__ctor_m9BE30002DD3BC07052D7E11A448D6A1330E822A9,
	TargetFieldOfView_Start_mCBB06309FF6E440654164EDF0557C46AA5C673E7,
	TargetFieldOfView_FollowTarget_mA92F8333656949ACB3D8A4C383613C2B5791A5FB,
	TargetFieldOfView_SetTarget_mFBC0E9995E9CDF97FD74AAC1370400F5FCAB3373,
	TargetFieldOfView_MaxBoundsExtent_mBAAA9C398FF2E1C3F6F80FF5BC51D40CD445E471,
	TargetFieldOfView__ctor_m7F1799AEF5CD5A646C15D7C92B50668263FF525E,
	Camera2DFollow_Start_m71072967C7745D6C231D2AFCFDD0EFDEBCBA7EC5,
	Camera2DFollow_Update_mD05FC40A3456266BBA46F4992639929727B8C857,
	Camera2DFollow__ctor_m40D25F22E5C17685685B7E7C376DF65E67E72F85,
	CameraFollow_Awake_m2AE9908BCD97FB28E25FF2CE5DA1315D885BE232,
	CameraFollow_CheckXMargin_mC1C2AA7C94A848C8E08AFE9FB8F65DA680088F8A,
	CameraFollow_CheckYMargin_m747AE7DD96ACFB05A7CB8FBC71508F297B413F17,
	CameraFollow_Update_mAC56DE247C31FBF8F5A056C2C6CF7731F3C24990,
	CameraFollow_TrackPlayer_m70ECF5BDEC6392E7DD820A7B4581F480E8AC3BC8,
	CameraFollow__ctor_m95408B3D7ACB4D546C2737F50E0FD63E7C8C93CF,
	Platformer2DUserControl_Awake_m6CCAA55EE7FB2EE3FEDAF3322596837B978DA81D,
	Platformer2DUserControl_Update_m2EEEBF981AAAA00BB463E7CF69CE6D47D311998E,
	Platformer2DUserControl_FixedUpdate_m184BFC5272959EECCEA84FE8F9762B081EAE24B0,
	Platformer2DUserControl__ctor_mE97E82E0843CBD49694EF723E483A2D3EBEE3380,
	PlatformerCharacter2D_Awake_m0BFC93303AB7CD2421532ACF546493246A2AB21D,
	PlatformerCharacter2D_FixedUpdate_m545FE02332DD8F383739DC43D15394F69624E87D,
	PlatformerCharacter2D_Move_m69E938ADFEBF2FC01C538C2AAE344812FCE7511E,
	PlatformerCharacter2D_Flip_m1C6BEC4848490BA82CEBDEBA96504BFFD9087045,
	PlatformerCharacter2D__ctor_mCC19D93450D773CCB09D24844FAED11A2710886C,
	Restarter_OnTriggerEnter2D_mE683A3E5102CBDC04570D1A39E185017D35A066A,
	Restarter__ctor_mAFA21081B52C501F5F9C269C6C57DCFC33146E6B,
	CodelessIAPStoreListener_InitializeCodelessPurchasingOnLoad_m857FC491AFD5BF425FE16EF88AB6135C38CCBE26,
	CodelessIAPStoreListener_InitializePurchasing_m5275372EA20B03DEFD67B5A4F956919C9712A930,
	CodelessIAPStoreListener__ctor_m180B00F8FE57B89E1E2732E6F93176F0CEF1ED54,
	CodelessIAPStoreListener_get_Instance_m6D6C71B6D75F21C64499CE46A23DDC572B3DACAE,
	CodelessIAPStoreListener_CreateCodelessIAPStoreListenerInstance_m4A630850DFC863CB781CE2B0D9206A826C93B8B3,
	CodelessIAPStoreListener_get_StoreController_m4FE586D7DEE312A55932FB40044346EB2D4B65A9,
	CodelessIAPStoreListener_get_ExtensionProvider_m6EF90D4617310AF8D82CF0372F0E8E3755B366B4,
	CodelessIAPStoreListener_HasProductInCatalog_m47D805C2D47FBCB3CE1D3132B8C018CE15BE12E8,
	CodelessIAPStoreListener_GetProduct_mBD904E64348F60DD664B5D24685EFD98F77C334F,
	CodelessIAPStoreListener_AddButton_m21CBF0D2CAA3DB2C37986EDA26BC755B8A8EF03C,
	CodelessIAPStoreListener_RemoveButton_m26D317C3F8CB55588914A68E1B361822FFFD6CCC,
	CodelessIAPStoreListener_AddListener_m6BBCC3C5FD4FE7C7BC0B2FB7841DBB105D2B69EB,
	CodelessIAPStoreListener_RemoveListener_m3DE10AC107870807F9516B33194FE9A0AA9845B2,
	CodelessIAPStoreListener_InitiatePurchase_m47C17C863F18E5A9FF245AA4C06795D76EBE2006,
	CodelessIAPStoreListener_OnInitialized_m8CA8B9D11FB6BA61BDB2EE43B7BC1C30A88647C4,
	CodelessIAPStoreListener_OnInitializeFailed_mD27FEA46B1B30942024140E3C2740D46A0C5DD7A,
	CodelessIAPStoreListener_ProcessPurchase_m65BBA6E7861D2236652DAC619DA2CD4F840BD5C8,
	CodelessIAPStoreListener_OnPurchaseFailed_mAA5A118AD6E60BC2E66D85A829BF20230FB0815C,
	DemoInventory_Fulfill_m0AD2B65A00F24CC71DB1C93CB4C194E2C4F83D8D,
	DemoInventory__ctor_m5B17E344D7C129EE0C3B9C51544E12A2979E51F1,
	IAPButton_Start_m4AA7E280CEAC12A6791DD5480E00D89831E1B823,
	IAPButton_OnEnable_m2956708EFC322CD5F747A160554D2CA596A8B013,
	IAPButton_OnDisable_m15FF6ED10F88FE251AACE1D4B06458326650B535,
	IAPButton_PurchaseProduct_m03FDF1B08EE596CCD64BA6874EF032F4F00217D1,
	IAPButton_Restore_mF9E7AA79D9D19AE9E414DF6A64DBB8787CDA6EE4,
	IAPButton_OnTransactionsRestored_m2304ED0088AAA420B1AB1D65B2992ED07769CFE8,
	IAPButton_ProcessPurchase_mAD1C2C30A543271D9011A0612E37EA62714B2C09,
	IAPButton_OnPurchaseFailed_m7147EB0EA21DC3A07676D154824992208E5FD440,
	IAPButton_UpdateText_m3A7A35C58043862EC01692DC786CBA75C55AD298,
	IAPButton__ctor_m245B69180E5FEF72F72782ED661CD70932349510,
	IAPButton_U3CRestoreU3Eb__15_0_mBC6ED8A36791E853AE01B77900F2263B27A8A2E2,
	IAPConfigurationHelper_PopulateConfigurationBuilder_mB2177283D02794DCB7668FEC951AC61AC94CA3DE,
	IAPListener_OnEnable_mAE242FB56BBFBEBA38E86D1DA5A4EC1817EA5FE1,
	IAPListener_OnDisable_mB75FF6CDE820037D35C4490E0255E1B0F3AC6A33,
	IAPListener_ProcessPurchase_mE08DC15E1CC263B6B6C74248B2B342339632FC8A,
	IAPListener_OnPurchaseFailed_m088716B1A5F44E742992FCD038C486749BC6F597,
	IAPListener__ctor_mDB415F99CF40A742E06D2D0AD27021A342FBD898,
	AppleTangle_Data_m9D8150E6665861C333707D0519B5602EF14ECDFC,
	AppleTangle__ctor_mE80A22A474178259DD11042C9A47F2EFFA8E60EC,
	AppleTangle__cctor_m0FBAB41DA0FF906A73D5BD3EA37962A1AA529E48,
	GooglePlayTangle_Data_mEF0036018B42640819FBC784A455BDC795D9B731,
	GooglePlayTangle__ctor_m245442D64C7E6BB1166FDA40FF22CE52FD78E2A7,
	GooglePlayTangle__cctor_m92C3875F588587BD540B997A339B3CB74DA39215,
	ReplacementDefinition__ctor_m49E65F984C7161F3C2C5DD51172D8C9624AD31E5,
	ReplacementList__ctor_mEC91B69367F2501099D36FD66A5715B772DAACCB,
	Vector3andSpace__ctor_mFE5D5C1F196B500E04A45EC3230CE4FBD138F894,
	U3CDragObjectU3Ed__8__ctor_m5ED5AC5E5AB22F2FB9178DDCC2D99A933CB2C211,
	U3CDragObjectU3Ed__8_System_IDisposable_Dispose_m011A5F292C63CAACDE3E483BE60B65308D1E1CE4,
	U3CDragObjectU3Ed__8_MoveNext_mDC1BCC4C79BDB25C44A9DC9735D48FA2E175E071,
	U3CDragObjectU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m043FAB9E9E779B7ED3968BD00C707E31D47CA661,
	U3CDragObjectU3Ed__8_System_Collections_IEnumerator_Reset_mC00266C3A2C2B2C5CAA743701D45DC345EC96706,
	U3CDragObjectU3Ed__8_System_Collections_IEnumerator_get_Current_m26F191AB7590668B6FE2A7D0F9D5ABC771B90EA0,
	U3CFOVKickUpU3Ed__9__ctor_m2E70E0F14634E8A3208961FEA2B1664230D5757C,
	U3CFOVKickUpU3Ed__9_System_IDisposable_Dispose_mD4B00A512ECC65464CEF2F971595D3B28032A8A2,
	U3CFOVKickUpU3Ed__9_MoveNext_m512DBF15F770463827161931918513ECBB78759E,
	U3CFOVKickUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF59D92574D53698089582F9A0BE55EB4A9B9B256,
	U3CFOVKickUpU3Ed__9_System_Collections_IEnumerator_Reset_m09B3619F760A7A100802FBC2C44718B3AABD19CE,
	U3CFOVKickUpU3Ed__9_System_Collections_IEnumerator_get_Current_mBA658BB1928272A6E7E098ECD397E77C191D04FC,
	U3CFOVKickDownU3Ed__10__ctor_mAC26AAD43FBF2CEF4164498556AE1B030837944F,
	U3CFOVKickDownU3Ed__10_System_IDisposable_Dispose_mA336F42BB92EDDD223120F28B0A13EF29F9FF9C7,
	U3CFOVKickDownU3Ed__10_MoveNext_mF35885E2D181C34AA25B41E9212A468D59DE456C,
	U3CFOVKickDownU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72EA85FAA1FD7B04B65BB1C719D6472A9B8EC0EC,
	U3CFOVKickDownU3Ed__10_System_Collections_IEnumerator_Reset_mD9005227142121AFF35782081F84B5AB7F8FD579,
	U3CFOVKickDownU3Ed__10_System_Collections_IEnumerator_get_Current_mFD66D6F9B957FC0522A35FE0D3662667B896A08C,
	U3CDoBobCycleU3Ed__4__ctor_m3845D1AEC7D920CC91A343EECDAFADD29AA364DF,
	U3CDoBobCycleU3Ed__4_System_IDisposable_Dispose_m337964E05B4A29A899562BA187A6C43AA915FC03,
	U3CDoBobCycleU3Ed__4_MoveNext_m9452FC51F84697BCD114A5791B3137BA34CD2DE3,
	U3CDoBobCycleU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52A882F4C449D3A6AFE38E54266A62CCE9FEB71C,
	U3CDoBobCycleU3Ed__4_System_Collections_IEnumerator_Reset_m2F0D716C86EF00BBFB2E9150ECA135004FEAC486,
	U3CDoBobCycleU3Ed__4_System_Collections_IEnumerator_get_Current_m7BBD42AB52B89D8F7451CC6A79BA3C660BA93392,
	U3CResetCoroutineU3Ed__6__ctor_mE23E11148F2C3B9148F5CF189984AE4CD71E8F8C,
	U3CResetCoroutineU3Ed__6_System_IDisposable_Dispose_m8E5DEBEC17DEB14B5B28C805C11BD627FDAEF4A3,
	U3CResetCoroutineU3Ed__6_MoveNext_m62B0E26E3294631050E345318DDC863C31EFAA26,
	U3CResetCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m59F7FC20AC24226410D75D0B8D5DA40BEEC6C1A1,
	U3CResetCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_mDAC82C73DF62065BCD4C1CB44848A55185A3987E,
	U3CResetCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m432D1DC8AECAC892944242B8ADDF7CCD6FAD9E9F,
	U3CStartU3Ed__4__ctor_mD0A1B215E5F066646BCAF66C33A5F48D0989E090,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m1FE667ACD1B0DC25165D56369A3CF8733C0B206A,
	U3CStartU3Ed__4_MoveNext_m7AB511BE787031F2A04E72326E49E7B5F3B208C9,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F071490E4E71A9A719D10DAAF032F74B6B31363,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m32B0CA08CBECCFC5DEF3C43032A5658A24D3C7C0,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m3E718F7631CCED3184D3AB437310A802E3F5FAF0,
	Entry__ctor_m593D5A5177E927D55B4EDEA893C9A89DA0E3683C,
	Entries__ctor_m9FF702A552EFD09D5146A438883B9D3267428151,
	U3CActivateU3Ed__5__ctor_m04B00CBAD8695D22C759B14B4228011F72884E9F,
	U3CActivateU3Ed__5_System_IDisposable_Dispose_m6F5CC0A9E69B2ABB18B2F15DC8F1575F3899E175,
	U3CActivateU3Ed__5_MoveNext_mF97B09D381486D8A6E40E66208EF442D65683980,
	U3CActivateU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB8461E4FE3B48538BDB9C8FCA99CFB50C4D46C8,
	U3CActivateU3Ed__5_System_Collections_IEnumerator_Reset_m10D07A5F6216561973401AE2344F661E0C7E520C,
	U3CActivateU3Ed__5_System_Collections_IEnumerator_get_Current_m013FDADA4980E415D472066957CB1D762B166B8B,
	U3CDeactivateU3Ed__6__ctor_m43C0B814DE2C6DF5F445D122620F750158D95A9B,
	U3CDeactivateU3Ed__6_System_IDisposable_Dispose_mB5C406251AEA37C27D789602F621349E95B5077E,
	U3CDeactivateU3Ed__6_MoveNext_m690EC1B5BB66A6A7DEA9B82A9EC24767FB669A06,
	U3CDeactivateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A9DD75E5BEF9E464B4366FB4A5BBFA6C9624FD1,
	U3CDeactivateU3Ed__6_System_Collections_IEnumerator_Reset_m5C179259768EC874E6C752D62F84805BDC036AA3,
	U3CDeactivateU3Ed__6_System_Collections_IEnumerator_get_Current_m1F491BC143C92AF323241F437B1854040FBB960B,
	U3CReloadLevelU3Ed__7__ctor_m89F904013A652554E256373377BF93992BA1205E,
	U3CReloadLevelU3Ed__7_System_IDisposable_Dispose_m4EAEA10A25D3BCD8C4A41A7C305673C2702021A9,
	U3CReloadLevelU3Ed__7_MoveNext_m22DFAE90E48E5E9AE2E064903D8AA47C08EB9B48,
	U3CReloadLevelU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F1B7ED565E25B37FC48A22BDB4378F714F5B3A1,
	U3CReloadLevelU3Ed__7_System_Collections_IEnumerator_Reset_mF3AB1DBCA692E293D77D4D9294D0BC47CCB22CFB,
	U3CReloadLevelU3Ed__7_System_Collections_IEnumerator_get_Current_m62C25C33793EE76CA6A5FA59EC19F7EE078C4BD1,
	WaypointList__ctor_m6F4EDE811589F476A95CC3ECCA3BE59C54AF753A,
	RoutePoint__ctor_m0CF428B188D139A371423658C3A41B96137CDA09_AdjustorThunk,
	U3CStartU3Ed__4__ctor_mBDD727783F54367BFE2CED7C675B839450955B8C,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6E7E396A3AE4E0720223F5D8DCB4E8A55ED7BF01,
	U3CStartU3Ed__4_MoveNext_m4846C74D5F4A4FF39164BCFD48198D976AA0F953,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m134C857EC6C5EEB674DF4B22AE43D9CBD4D3596F,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m886FA065B7C64EA3A746502C9F6BF104B9801F07,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mEA0D5C2C7930B6E16648C869029DC54290E095E2,
	U3CStartU3Ed__1__ctor_m28BA84EE932E5505BA7BD35AC5F8007BA8B96564,
	U3CStartU3Ed__1_System_IDisposable_Dispose_m2411BD36258EF1A6EF59D488D875E1989C3BAE1C,
	U3CStartU3Ed__1_MoveNext_m69D4364C306F1FC8F21C33E29E6C125B2B9EEF2B,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m600B8E8FC9018CA7E4700F80CF7114DBD2A89C0F,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m8BE9B7F5466A03EDD437E3D16EB3E659B51ABC0E,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m60734E8242F26A6B6ADF449A1668715F5E393E00,
	U3COnCollisionEnterU3Ed__8__ctor_mE0412A81997D86406428B1A95EEDF564D6ED49E0,
	U3COnCollisionEnterU3Ed__8_System_IDisposable_Dispose_mE7DC56D7449DC66BB04E6EC60C7B143779E4AEB9,
	U3COnCollisionEnterU3Ed__8_MoveNext_m53C4680C5CFB7DB025A0854EDFBA101B1C74C851,
	U3COnCollisionEnterU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C70CC61071F2B35953F4283AA505C55E6940F69,
	U3COnCollisionEnterU3Ed__8_System_Collections_IEnumerator_Reset_m62403746C385587687DAFDAACBD369E71159685F,
	U3COnCollisionEnterU3Ed__8_System_Collections_IEnumerator_get_Current_m353A3041DBEED2FEBA3CCC26222620D9908BC733,
	VirtualAxis_get_name_mC3959CD36494EE1B06CAEA1675DD19E5FFCB9BD9,
	VirtualAxis_set_name_m2A44E0BF21BB426C9A14AB057D5EF41616B76096,
	VirtualAxis_get_matchWithInputManager_m2F68784B6C454EB26934401303E28C159980F315,
	VirtualAxis_set_matchWithInputManager_m326813FB9C39A5D63C98D4AE931384D6B67AE944,
	VirtualAxis__ctor_m9B094B00B2F0F1C6C474D3DA51419F4549540E53,
	VirtualAxis__ctor_mAC45A3BC043EA253666CCDE2762DB39475FED915,
	VirtualAxis_Remove_m0517C6C37E94CCC84337FD412982D1800E5CEFD6,
	VirtualAxis_Update_m639BD6EC869B61C712D4519290523C61745FF6C3,
	VirtualAxis_get_GetValue_mB0D352473A7E1F6A9402335FBD18625ADFCE0A69,
	VirtualAxis_get_GetValueRaw_mA75834F100AB39C130FEA7AE85677E4928E58397,
	VirtualButton_get_name_m836058DAC831C5BB481A422120939EB4D14CE55B,
	VirtualButton_set_name_mCC77CE771C89C23B47A2D9B027C7E754666A78A5,
	VirtualButton_get_matchWithInputManager_mD6924A44FFCFF72519BDDEAD61E3072CC3C3FCF3,
	VirtualButton_set_matchWithInputManager_mD438AFD4E212727BED9ECD1F0CBFE6243112AE3D,
	VirtualButton__ctor_mECADC4A0B8ACF0954720A84061800EA0F00D9FDD,
	VirtualButton__ctor_mBC57649412C90DFF3179B681B9D33BB88443FFD9,
	VirtualButton_Pressed_m596B075C829D1E8C500AF6694155488CF2250402,
	VirtualButton_Released_mC4B98C45864A5832601A90437E691119F28E25E6,
	VirtualButton_Remove_m0F66A404819C8B483DA3F02FDCEBDB005867D37D,
	VirtualButton_get_GetButton_m228F811AD3C4911C45AFEA7960E35F4A84B7A32D,
	VirtualButton_get_GetButtonDown_mB6BBC9E21BB477279E5D74926CFA633E671AC430,
	VirtualButton_get_GetButtonUp_m79C31A03EE6AC926E932FA1A28989A73B0257E43,
	AxisMapping__ctor_m2B8C914999C51C9568C81B4C1E6750BCAF66BE1F,
	U3CStartU3Ed__1__ctor_m67CE23090D68646148DEF07ECBEC73F47D6C2B97,
	U3CStartU3Ed__1_System_IDisposable_Dispose_m742D326FAD73D62F6EA840726E5D633444959A2F,
	U3CStartU3Ed__1_MoveNext_mDEA0D45B6339E08A90F11699ABD4CC14253C9083,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB747C2A701CEEDAF4ED3A50DE8FAED78F6F88E47,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m39A4E2321FFB793C383C8E723435921B7C786AA3,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m35A89B378AFF03387749E69BBF345C104D46975B,
	U3CStartSkidTrailU3Ed__18__ctor_m344B8023D623A699ED904AA4C6CC701FCF2A3420,
	U3CStartSkidTrailU3Ed__18_System_IDisposable_Dispose_mEEC56B8DD0472D10807B89C581C96DA95BFF8F31,
	U3CStartSkidTrailU3Ed__18_MoveNext_m4735C2EEFE7E83A4C89738BB83DAB7F69069453B,
	U3CStartSkidTrailU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCD092F7744E5ABFBC34309F1221837C2EFAAC6A3,
	U3CStartSkidTrailU3Ed__18_System_Collections_IEnumerator_Reset_mD3A710A3304A7564EF9189858EA480496EC41B8F,
	U3CStartSkidTrailU3Ed__18_System_Collections_IEnumerator_get_Current_m89DDE8F9BAA46DE9913128C9D92E27ABF635ABC0,
	AdvancedSetttings__ctor_mE02988E47F76E04FFE1264222FDCE2DC9A9A169E,
	ControlSurface__ctor_m132B3895DCFAFD6AC23218A4EEB856457D43812A,
	MovementSettings_UpdateDesiredTargetSpeed_m92BE3A22C9CF55BF12A0D441EBB51FF7A1779D69,
	MovementSettings__ctor_mF768FC3EE16A9A355B9FCD47EF76C8A4879FAF29,
	AdvancedSettings__ctor_m845AB1B94472E573D2C5DD585876E67B97C0AD23,
	RayHitComparer_Compare_m6EFBFB91A4F97CC5D2111878BCD211DCEA51DAE3,
	RayHitComparer__ctor_m2A11DF0646D70F2DAFC0751CD63F55A40D397D11,
	OnPurchaseCompletedEvent__ctor_mB5BA13357B2FA5082EF0ACFFC7AEA22B77338399,
	OnPurchaseFailedEvent__ctor_m63FF8EE5D613396B41404B3FBDA39618069CFBAA,
	OnPurchaseCompletedEvent__ctor_m89CDE2E04A0B4D52670EADC46E4E5B84B89A070C,
	OnPurchaseFailedEvent__ctor_m6729A97F19ECA2F73A9A0BC3654C81A145AFBC2E,
};
static const int32_t s_InvokerIndices[732] = 
{
	27,
	115,
	144,
	32,
	23,
	31,
	26,
	26,
	26,
	23,
	23,
	26,
	26,
	144,
	89,
	23,
	23,
	32,
	27,
	32,
	23,
	23,
	23,
	23,
	1856,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	1992,
	26,
	26,
	23,
	23,
	978,
	1437,
	23,
	23,
	1901,
	14,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	742,
	14,
	23,
	23,
	352,
	1901,
	23,
	14,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	28,
	28,
	23,
	23,
	23,
	23,
	742,
	352,
	14,
	23,
	1993,
	1437,
	1994,
	23,
	23,
	23,
	31,
	23,
	1995,
	1996,
	1995,
	1996,
	1995,
	1996,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	1997,
	23,
	14,
	23,
	23,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	28,
	1998,
	28,
	26,
	23,
	27,
	23,
	23,
	27,
	26,
	1999,
	1999,
	455,
	2000,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	862,
	10,
	10,
	2000,
	2001,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	175,
	121,
	121,
	165,
	165,
	165,
	165,
	0,
	1543,
	1543,
	2002,
	121,
	121,
	121,
	165,
	165,
	165,
	165,
	165,
	1574,
	1524,
	1458,
	1458,
	1458,
	23,
	352,
	23,
	23,
	23,
	1433,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1433,
	26,
	23,
	26,
	23,
	23,
	1432,
	1433,
	9,
	9,
	26,
	26,
	26,
	26,
	28,
	352,
	352,
	352,
	2003,
	9,
	9,
	9,
	26,
	26,
	26,
	26,
	26,
	978,
	1432,
	23,
	26,
	26,
	2003,
	26,
	26,
	26,
	26,
	26,
	978,
	9,
	9,
	9,
	1432,
	23,
	2003,
	9,
	9,
	9,
	26,
	26,
	26,
	26,
	26,
	978,
	1432,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	28,
	1544,
	23,
	89,
	31,
	742,
	352,
	742,
	742,
	742,
	742,
	352,
	742,
	352,
	23,
	23,
	455,
	1544,
	23,
	23,
	1438,
	23,
	1406,
	23,
	23,
	23,
	23,
	352,
	89,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	89,
	31,
	89,
	31,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	2004,
	23,
	742,
	352,
	742,
	352,
	89,
	31,
	742,
	352,
	742,
	352,
	742,
	742,
	352,
	742,
	352,
	742,
	352,
	742,
	352,
	742,
	352,
	742,
	352,
	23,
	2005,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	737,
	23,
	23,
	23,
	737,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	2006,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	23,
	23,
	26,
	23,
	23,
	2007,
	31,
	23,
	1433,
	23,
	42,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	352,
	23,
	352,
	6,
	23,
	26,
	23,
	23,
	23,
	23,
	27,
	27,
	31,
	23,
	23,
	2008,
	23,
	1432,
	89,
	89,
	89,
	23,
	23,
	23,
	742,
	23,
	1441,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	352,
	23,
	26,
	14,
	23,
	352,
	23,
	23,
	23,
	23,
	352,
	23,
	23,
	352,
	23,
	23,
	352,
	23,
	23,
	23,
	89,
	31,
	23,
	23,
	23,
	23,
	352,
	26,
	2002,
	23,
	23,
	23,
	23,
	23,
	89,
	89,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2009,
	23,
	23,
	26,
	23,
	3,
	3,
	23,
	4,
	3,
	14,
	14,
	9,
	28,
	26,
	26,
	26,
	26,
	26,
	27,
	32,
	115,
	144,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	115,
	144,
	23,
	23,
	32,
	2010,
	23,
	23,
	115,
	144,
	23,
	4,
	23,
	3,
	4,
	23,
	3,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	1430,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	26,
	89,
	31,
	26,
	118,
	23,
	352,
	742,
	742,
	14,
	26,
	89,
	31,
	26,
	118,
	23,
	23,
	23,
	89,
	89,
	89,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	1460,
	23,
	23,
	41,
	23,
	23,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	732,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
