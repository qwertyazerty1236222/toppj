﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.Social::get_Active()
extern void Social_get_Active_m3A0805FA14B55B8A297411DDF6295127A62C83FD (void);
// 0x00000002 System.Void UnityEngine.Social::set_Active(UnityEngine.SocialPlatforms.ISocialPlatform)
extern void Social_set_Active_m6068C76CDEDCBB996D5F397BDF9F1038FD99E7C6 (void);
// 0x00000003 System.Void UnityEngine.SocialPlatforms.Local::.ctor()
extern void Local__ctor_m18A130860CC620F4FEAA697A0111611959359178 (void);
// 0x00000004 System.Void UnityEngine.SocialPlatforms.Local::.cctor()
extern void Local__cctor_mF5DDF3546FB61F0A4E373CFFB3351314F5E9C66B (void);
// 0x00000005 UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::get_Instance()
extern void ActivePlatform_get_Instance_mC73120CFC6D5F26A916F84399DE7FE626419AC69 (void);
// 0x00000006 System.Void UnityEngine.SocialPlatforms.ActivePlatform::set_Instance(UnityEngine.SocialPlatforms.ISocialPlatform)
extern void ActivePlatform_set_Instance_m8F17B7F1A691A9985C31546F272D1BDD7F1504F9 (void);
// 0x00000007 UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::SelectSocialPlatform()
extern void ActivePlatform_SelectSocialPlatform_m27C01C886A801C5186461E31572B55A8068961F3 (void);
// 0x00000008 System.Boolean UnityEngine.SocialPlatforms.ILeaderboard::get_loading()
// 0x00000009 System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id()
// 0x0000000A UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope()
// 0x0000000B UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range()
// 0x0000000C UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope()
static Il2CppMethodPointer s_methodPointers[12] = 
{
	Social_get_Active_m3A0805FA14B55B8A297411DDF6295127A62C83FD,
	Social_set_Active_m6068C76CDEDCBB996D5F397BDF9F1038FD99E7C6,
	Local__ctor_m18A130860CC620F4FEAA697A0111611959359178,
	Local__cctor_mF5DDF3546FB61F0A4E373CFFB3351314F5E9C66B,
	ActivePlatform_get_Instance_mC73120CFC6D5F26A916F84399DE7FE626419AC69,
	ActivePlatform_set_Instance_m8F17B7F1A691A9985C31546F272D1BDD7F1504F9,
	ActivePlatform_SelectSocialPlatform_m27C01C886A801C5186461E31572B55A8068961F3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[12] = 
{
	4,
	165,
	23,
	3,
	4,
	165,
	4,
	89,
	14,
	10,
	1798,
	10,
};
extern const Il2CppCodeGenModule g_UnityEngine_GameCenterModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_GameCenterModuleCodeGenModule = 
{
	"UnityEngine.GameCenterModule.dll",
	12,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
