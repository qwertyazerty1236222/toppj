﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BasketGenOffline : MonoBehaviour
{
    public bool[] gz;
    public GameObject[] Points;
    public GameObject[] Goal;
    public Text Timer;
    public Text Score;
    private float timerf = 10f;
    private int rand;
    public GameObject[] DeletePoint;
    public Animator[] Collapse;
    public Animator BallGoal;
    private int count = 0;
    public int[] SetPoint;
    private bool flag;
    private float Acceleration;
    public Animator ballRotation;
    private bool update;
    private int[] bots;
    private string[] Listbots = { "Adam", "Sanya", "Antony", "E.B. Lan", "YOU" };
    // Start is called before the first frame update
    void Start()
    {
        GlobalDefines.GameTip4 = true;
        update = false;
        Acceleration = 1;
        flag = false;
        SetPoint = new int[5];
        Collapse = new Animator[5];
        DeletePoint = new GameObject[5];
        gz = new bool[5];
        for (int i = 0; i < 5; i++)
        {
            gz[i] = false;
        }
        SetPoint = Generate();
        for(int i = 0; i<5;i++)
        {
            DeletePoint[i] = Instantiate(Goal[i], Points[SetPoint[i]].transform, false);
            Collapse[i] = DeletePoint[i].GetComponent<Animator>();
        }    
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GZ1")
        {
            gz[0] = true;
        }
        if (other.tag == "GZ2")
        {
            gz[1] = true;
        }
        if (other.tag == "GZ3")
        {
            gz[2] = true;
        }
        if (other.tag == "GZ4")
        {
            gz[3] = true;
        }
        if (other.tag == "GZ5")
        {
            gz[4] = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "GZ1")
        {
            gz[0] = false;
        }
        if (other.tag == "GZ2")
        {
            gz[1] = false;
        }
        if (other.tag == "GZ3")
        {
            gz[2] = false;
        }
        if (other.tag == "GZ4")
        {
            gz[3] = false;
        }
        if (other.tag == "GZ5")
        {
            gz[4] = false;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!update && !GlobalDefines.GameTip4)
            if (Input.GetMouseButtonDown(0))
            {
                Tap();
            }
    }
    private void FixedUpdate()
    {
        if (!GlobalDefines.GameTip4)
        {
            if (!update)
            {
                if (!flag)
                    timerf -= Time.deltaTime;
                Timer.text = Mathf.RoundToInt(timerf).ToString();
                if (timerf <= 0)
                {
                    ballRotation.enabled = false;
                    update = true;
                    bots = new int[4];
                    bots = Bots.GenerateResult(int.Parse(Score.text), 3);
                    int[] all = new int[5];
                    all[0] = bots[0];
                    all[1] = bots[1];
                    all[2] = bots[2];
                    all[3] = bots[3];
                    all[4] = int.Parse(Score.text);
                    System.Array.Sort(all, Listbots);
                    if (Listbots[4] == "YOU")
                        GlobalDefines.OfflineWin = true;
                    GlobalDefines.ListOffline = Listbots;
                    GlobalDefines.ScoreOffline = all;
                    UnityEngine.Debug.LogError(Listbots + " " + all);
                    SceneManager.LoadScene("WinOffline");
                }
            }
        }
    }
    public void Tap()
    {
        if (gz[0] || gz[1] || gz[2] || gz[3] || gz[4])
        {
            if (gz[0])
            {
                Collapse[0].enabled = true;
                StartCoroutine(CollapseAnim(0));
            }
            if (gz[1])
            {
                Collapse[1].enabled = true;
                StartCoroutine(CollapseAnim(1));
            }
            if (gz[2])
            {
                Collapse[2].enabled = true;
                StartCoroutine(CollapseAnim(2));
            }
            if (gz[3])
            {
                Collapse[3].enabled = true;
                StartCoroutine(CollapseAnim(3));
            }
            if (gz[4])
            {
                Collapse[4].enabled = true;
                StartCoroutine(CollapseAnim(4));
            }

            count++;
            if (count == 5)
            {
                update = true;
                flag = true;
                count = 0;
                Score.text = (int.Parse(Score.text) + 1).ToString();
                BallGoal.enabled = true;
                StartCoroutine(BallGoalAnim());
                Acceleration += 0.1f;
                ballRotation.speed = Acceleration;
                timerf = 10f;
            }
        }
        else
        {
            ballRotation.enabled = false;
            update = true;
            bots = new int[4];
            bots = Bots.GenerateResult(int.Parse(Score.text), 4);
            int[] all = new int[5];
            all[0] = bots[0];
            all[1] = bots[1];
            all[2] = bots[2];
            all[3] = bots[3];
            all[4] = int.Parse(Score.text);
            System.Array.Sort(all, Listbots);
            if (Listbots[4] == "YOU")
                GlobalDefines.OfflineWin = true;
            GlobalDefines.ListOffline = Listbots;
            GlobalDefines.ScoreOffline = all;
            UnityEngine.Debug.LogError(Listbots + " " + all);
            SceneManager.LoadScene("WinOffline");
        }
    }
    int[] Generate()
    {
        int[] old;
        old = new int[5];
        rand = Random.Range(0, 11);
        old[0] = rand;
        rand = Random.Range(0, 11);
        while (rand == old[0])
        {
            rand = Random.Range(0, 11);
        }
        old[1] = rand;
        rand = Random.Range(0, 11);
        while (rand == old[0] || rand == old[1])
        {
            rand = Random.Range(0, 11);
        }
        old[2] = rand;
        rand = Random.Range(0, 11);
        while (rand == old[0] || rand == old[1] || rand == old[2])
        {
            rand = Random.Range(0, 11);
        }
        old[3] = rand;
        rand = Random.Range(0, 11);
        while (rand == old[0] || rand == old[1] || rand == old[2] || rand == old[3])
        {
            rand = Random.Range(0, 11);
        }
        old[4] = rand;
        return old;
    }
    IEnumerator CollapseAnim(int Zone)
    {
        yield return new WaitForSeconds(0.25f);
        gz[Zone] = false;
        Destroy(DeletePoint[Zone]);
    }
    IEnumerator BallGoalAnim()
    {
        yield return new WaitForSeconds(3.667f);
        BallGoal.enabled = false;
        SetPoint = Generate();
        for (int i = 0; i < 5; i++)
        {
            DeletePoint[i] = Instantiate(Goal[i], Points[SetPoint[i]].transform, false);
            Collapse[i] = DeletePoint[i].GetComponent<Animator>();
        }
        flag = false;
        update = false;
    }
}
