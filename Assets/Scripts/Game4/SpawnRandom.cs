﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnRandom : MonoBehaviour
{
    public static float AccelerationGame4;
    [SerializeField] private GameObject[] Circles;
    [SerializeField] private Transform Place;
    [SerializeField] private Text Score;
    private int rand;
    private int CheckScore;
    private GameObject RecCircle;
    bool flag = false;
    // Start is called before the first frame update
    void Start()
    {
        GlobalDefines.GameTip4 = true;
        AccelerationGame4 = 1;
        rand = Random.Range(0, Circles.Length);
        CheckScore = int.Parse(Score.text);
        RecCircle = Instantiate(Circles[rand], Place, false);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckScore != int.Parse(Score.text) && !flag)
        {
            flag = true;
            StartCoroutine(WaitForAnim());
            AccelerationGame4 += 0.1f;
            
        }
    }

    IEnumerator WaitForAnim()
    {
        yield return new WaitForSeconds(3.7f);
        Destroy(RecCircle);
        CheckScore = int.Parse(Score.text);
        rand = Random.Range(0, Circles.Length);
        RecCircle = Instantiate(Circles[rand], Place, false);
        flag = false;
    }
}
