﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Diagnostics;

[RequireComponent(typeof(Rigidbody))]
public class Offline2Game : MonoBehaviour
{
    private float AllCounter = -89.21f;
    public GameObject[] maps;
    private GameObject Player;
    private float speed;
    private float multip;
    private bool Jumping = false;
    public Text score;
    public GameObject GameOverBG;
    private bool dead;
    private float tmpCounter = 0;
    System.Random rnd = new System.Random();
    float timer = 10f;
    private bool update = true;
    private int[] bots;
    private string[] Listbots = { "Adam", "Sanya", "Antony", "E.B. Lan", "YOU" };
    void Start()
    {
       Player = (GameObject)this.gameObject;
        speed = 0.08f;
        multip = 1f;
        dead = false;
        GlobalDefines.GameTip2 = true;
    }
    public void Update()
    {

    }
    void FixedUpdate()
    {
        if (!GlobalDefines.GameTip2)
        {
            if (!dead && update)
            {
                tmpCounter += speed;
                if (tmpCounter >= 1)
                {
                    score.text = (int.Parse(score.text) + 1).ToString();
                    tmpCounter = 0;
                }


                Player.transform.localPosition += new Vector3(speed * multip, 0, 0);
                Camera.main.transform.localPosition = new Vector3(Player.transform.localPosition.x + 2, 0, -10);
                if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Mouse0))
                {
                    if (Jumping == false)
                    {
                        GetComponent<Rigidbody>().AddForce(new Vector3(0, speed * 210, 0), ForceMode.Impulse);
                        Jumping = true;
                    }
                }
            }
            else if (update)
            {
                update = false;
                bots = new int[4];
                bots = Bots.GenerateResult(int.Parse(score.text),2);
                int[] all = new int[5];
                //if (bots[0] < int.Parse(Score.text))
                all[0] = bots[0];
                // else
                // all[0] = bots[0] - 5;
                // if (bots[1] < int.Parse(Score.text))
                all[1] = bots[1];
                //else
                //  all[1] = bots[1] - 5;
                //if (bots[2] < int.Parse(Score.text))
                all[2] = bots[2];
                // else
                // all[2] = bots[2] - 5;
                //if (bots[3] < int.Parse(Score.text))
                all[3] = bots[3];
                // else
                // all[3] = bots[3] - 5;
                all[4] = int.Parse(score.text);
                System.Array.Sort(all, Listbots);
                if (Listbots[4] == "YOU")
                    GlobalDefines.OfflineWin = true;
                GlobalDefines.ListOffline = Listbots;
                GlobalDefines.ScoreOffline = all;
                UnityEngine.Debug.LogError(Listbots + " " + all);
                SceneManager.LoadScene("WinOffline");


            }
        }
    }
    void OnCollisionEnter()
    {
        Jumping = false;

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Respawn")
        {
            dead = true;
            Camera.main.transform.position = new Vector3(GameOverBG.transform.position.x, GameOverBG.transform.position.y, -10);
        }
        else if (other.tag == "ScorePlus")
        {
            score.text = (int.Parse(score.text) + 10).ToString();
        }
        else if(other.tag == "CheckPoint")
        {
            multip += 0.05f;
        }
    }
}