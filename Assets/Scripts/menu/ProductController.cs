﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class ProductController : MonoBehaviour
{
    [SerializeField] private Text Money;
    [SerializeField] private int[] Amount;
    // Start is called before the first frame update
    void Start()
    {
        PurchaseManager.OnPurchaseConsumable += PurchaseManager_OnPurchaseConsumable;
    }

    private void PurchaseManager_OnPurchaseConsumable(PurchaseEventArgs args)
    {
        if (args.purchasedProduct.definition.id == "money_80") 
            Money.text = (int.Parse(Money.text) + Amount[0]).ToString();
        else if (args.purchasedProduct.definition.id == "money_160")
            Money.text = (int.Parse(Money.text) + Amount[1]).ToString();
        else if (args.purchasedProduct.definition.id == "money_500")
            Money.text = (int.Parse(Money.text) + Amount[2]).ToString();
    }
}
