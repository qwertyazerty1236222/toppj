﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Swipe : MonoBehaviour
{

    public float scrollSpeed;
    public float camMoveSpeed = 5.0f;
    public float bounceOffset = 7.0f;
    public float xMin, xMax, zMin, zMax;
    private Rigidbody rb;

    Vector3 newPosition = Vector3.zero;
    public void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }
    void Update()
    {
        if (GlobalDefines.cameraActive)
        {
            if (Input.touchCount > 0)
            {
                var touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        newPosition = transform.position;
                        break;

                    case TouchPhase.Moved:
                        var delta = touch.deltaPosition * scrollSpeed;
                        newPosition.x += delta.x;
                        newPosition.z += delta.y;
                        newPosition.x = Mathf.Clamp(newPosition.x, xMin - bounceOffset, xMax + bounceOffset);
                        newPosition.z = Mathf.Clamp(newPosition.z, zMin - bounceOffset, zMax + bounceOffset);
                        transform.GetComponent<Rigidbody>().velocity = newPosition;
                        var velocity = new Vector3(touch.deltaPosition.x, 0, touch.deltaPosition.y);
                        rb.velocity = velocity * scrollSpeed * camMoveSpeed;
                        break;
                }
            }

            var p = transform.position;

            transform.position = (Vector3.Lerp(p, newPosition, camMoveSpeed * Time.deltaTime));

            if (p.x > xMax)
            {
                rb.velocity = Vector3.zero;
                p.x = xMax - bounceOffset;
            }
            if (p.x < xMin)
            {
                rb.velocity = Vector3.zero;
                p.x = xMin + bounceOffset;
            }
            if (p.z > zMax)
            {
                rb.velocity = Vector3.zero;
                p.z = zMax - bounceOffset;
            }
            if (p.z < zMin)
            {
                rb.velocity = Vector3.zero;
                p.z = zMin + bounceOffset;
            }
            newPosition = p;
        }
    }
}