﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class but : MonoBehaviour
{

    public GameObject LoadPanel;
    public GameObject panel;
    public GameObject SettingsPanel;
    public InputField nameField;
    public InputField nameFieldFirst;
    public Text ID;
    public Text Money;
    public GameObject camera1;
    public GameObject NamePanel;
    public GameObject PayPanel1;
    public GameObject PayPanel2;
    public GameObject PayPanel3;
    public GameObject PayPanel4;
    public GameObject OfOOn1;
    public GameObject OfOOn2;
    public GameObject OfOOn3;
    public GameObject OfOOn4;
    public GameObject NotMoney;
    public GameObject DonatPanel;
    public GameObject Start1;
    public GameObject Start2;
    public GameObject Start3;
    public GameObject Start4;
    public GameObject T1;
    public GameObject T2;
    public GameObject T3;
    public GameObject T4;
    public GameObject T5;
    public GameObject T6;
    public GameObject STOP;
    public GameObject WAIT;
    public Text Diams;
    private bool Enter;
    private Collider ColliderGameFirst;
    private Collider ColliderGameSecond;
    private Collider ColliderGameThird;
    private Collider ColliderGameFourth;

    // Start is called before the first frame update
    void Start()
    {
        ColliderGameFirst = Start1.GetComponent<BoxCollider>();
        ColliderGameSecond = Start2.GetComponent<SphereCollider>();
        ColliderGameThird = Start3.GetComponent<BoxCollider>();
        ColliderGameFourth = Start4.GetComponent<BoxCollider>();
        Enter = false;
        StartCoroutine(Connect.FirstLog(PlayFabLogin.ReturnMobileID()));
        GlobalDefines.Loaded = false;
        GlobalDefines.cameraActive = true;
        GlobalDefines.StoryTip1 = false;
        ID.text = PlayFabLogin.ReturnMobileID();
        GlobalDefines.waitTip = false;
        ColliderGameFirst.enabled = true;
        ColliderGameSecond.enabled = true;
        ColliderGameThird.enabled = true;
        ColliderGameFourth.enabled = true;
        NamePanel.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {

        if (!GlobalDefines.Loaded)
        {
            ColliderGameFirst.enabled = false;
            ColliderGameSecond.enabled = false;
            ColliderGameThird.enabled = false;
            GlobalDefines.cameraActive = false;
            LoadPanel.SetActive(true);
        }
        else
        {
            LoadPanel.SetActive(false);
            ColliderGameFirst.enabled = true;
            ColliderGameSecond.enabled = true;
            ColliderGameThird.enabled = true;
            ColliderGameFourth.enabled = true;
        }
        if (GlobalDefines.WaitPlayer)
        {
            ColliderGameFirst.enabled = false;
            ColliderGameSecond.enabled = false;
            ColliderGameThird.enabled = false;
            GlobalDefines.cameraActive = false;
            panel.SetActive(true);
        }
        if (GlobalDefines.FirstLogin && GlobalDefines.Loaded)
        {
            ColliderGameFirst.enabled = false;
            ColliderGameSecond.enabled = false;
            ColliderGameThird.enabled = false;
            GlobalDefines.cameraActive = false;
            NamePanel.SetActive(true);
        }
        else if(GlobalDefines.StoryTip1)
        {
            NamePanel.SetActive(false);
            T1.SetActive(true);
        }
        else if (GlobalDefines.StoryTip2)
        {
            T2.SetActive(true);
        }
        else if (GlobalDefines.StoryTip3)
        {
            T3.SetActive(true);
        }
        else if (GlobalDefines.StoryTip4)
        {
            T4.SetActive(true);
        }
        else if (GlobalDefines.StoryTip5)
        {
            T5.SetActive(true);
        }
        else if (GlobalDefines.StoryTip6)
        {
            T6.SetActive(true);
            Enter = true;
        }
        else if(Enter)
        {
           
            ColliderGameFirst.enabled = true;
            ColliderGameSecond.enabled = true;
            ColliderGameThird.enabled = true;
            ColliderGameFourth.enabled = true;
            GlobalDefines.cameraActive = true;
            NamePanel.SetActive(false);
            Enter = false;
        }
        Debug.Log(Enter + " " + GlobalDefines.cameraActive);
    }
    public void Game()
    {
        if (CheckMoney())
        {
           
            GlobalDefines.cameraActive = false;
            ColliderGameFirst.enabled = false;
            ColliderGameSecond.enabled = false;
            ColliderGameThird.enabled = false;
            ColliderGameFourth.enabled = false;
            PayPanel1.SetActive(false);
            GlobalDefines.RecGame = 1;
            GlobalDefines.WaitPlayer = true;
            GlobalDefines.Diamonds = false;
            StartCoroutine(Connect.loadIn(this, 1));
            //camera1.GetComponent<CameraMovement>().enabled = true;
            
        }
        else
            NotEnoughMoney(1);
    }
    public void GameDiams()
    {
        if (CheckDiamonds())
        {
            
            GlobalDefines.cameraActive = false;
            ColliderGameFirst.enabled = false;
            ColliderGameSecond.enabled = false;
            ColliderGameThird.enabled = false;
            ColliderGameFourth.enabled = false;
            PayPanel1.SetActive(false);
            GlobalDefines.RecGame = 1;
            GlobalDefines.WaitPlayer = true;
            GlobalDefines.Diamonds = true;
            StartCoroutine(Connect.loadIn(this, 1));

        }
        else
            NotEnoughMoney(1);
    }
    public void Game2()
    {
        if (CheckMoney())
        {

            GlobalDefines.cameraActive = false;
            ColliderGameFirst.enabled = false;
            ColliderGameSecond.enabled = false;
            ColliderGameThird.enabled = false;
            ColliderGameFourth.enabled = false;
            PayPanel2.SetActive(false);
            GlobalDefines.RecGame = 2;
            GlobalDefines.WaitPlayer = true;
            StartCoroutine(Connect.loadIn(this, 2));
            
        }
        else
            NotEnoughMoney(2);
    }
    public void GameDiams2()
    {
        if (CheckDiamonds())
        {

            GlobalDefines.cameraActive = false;
            ColliderGameFirst.enabled = false;
            ColliderGameSecond.enabled = false;
            ColliderGameThird.enabled = false;
            ColliderGameFourth.enabled = false;
            PayPanel2.SetActive(false);
            GlobalDefines.RecGame = 2;
            GlobalDefines.WaitPlayer = true;
            GlobalDefines.Diamonds = true;
            StartCoroutine(Connect.loadIn(this, 2));

        }
        else
            NotEnoughMoney(2);
    }


    public void Game3()
    {
        if (CheckMoney())
        {
            GlobalDefines.cameraActive = false;
            ColliderGameFirst.enabled = false;
            ColliderGameSecond.enabled = false;
            ColliderGameThird.enabled = false;
            ColliderGameFourth.enabled = false;
            PayPanel3.SetActive(false);
            GlobalDefines.RecGame = 3;
            GlobalDefines.WaitPlayer = true;
            StartCoroutine(Connect.loadIn(this, 3));
            
        }
        else
            NotEnoughMoney(3);
    }
    public void GameDiams3()
    {
        if (CheckDiamonds())
        {
            GlobalDefines.cameraActive = false;
            ColliderGameFirst.enabled = false;
            ColliderGameSecond.enabled = false;
            ColliderGameThird.enabled = false;
            ColliderGameFourth.enabled = false;
            PayPanel3.SetActive(false);
            GlobalDefines.RecGame = 3;
            GlobalDefines.WaitPlayer = true;
            GlobalDefines.Diamonds = true;
            StartCoroutine(Connect.loadIn(this, 3));

        }
        else
            NotEnoughMoney(3);
    }

    public void Game4()
    {
        if (CheckMoney())
        {
            GlobalDefines.cameraActive = false;
            ColliderGameFirst.enabled = false;
            ColliderGameSecond.enabled = false;
            ColliderGameThird.enabled = false;
            ColliderGameFourth.enabled = false;
            PayPanel4.SetActive(false);
            GlobalDefines.RecGame = 4;
            GlobalDefines.WaitPlayer = true;
            StartCoroutine(Connect.loadIn(this, 4));
            
        }
        else
            NotEnoughMoney(4);
    }
    public void GameDiams4()
    {
        if (CheckDiamonds())
        {
            GlobalDefines.cameraActive = false;
            ColliderGameFirst.enabled = false;
            ColliderGameSecond.enabled = false;
            ColliderGameThird.enabled = false;
            ColliderGameFourth.enabled = false;
            PayPanel4.SetActive(false);
            GlobalDefines.RecGame = 4;
            GlobalDefines.WaitPlayer = true;
            GlobalDefines.Diamonds = true;
            StartCoroutine(Connect.loadIn(this, 4));

        }
        else
            NotEnoughMoney(4);
    }

    void Settings()
    {
        ColliderGameFirst.enabled = false;
        ColliderGameSecond.enabled = false;
        ColliderGameThird.enabled = false;
        GlobalDefines.cameraActive = false;
        SettingsPanel.SetActive(true);
    }

    public void ExitSettings()
    {
        ColliderGameFirst.enabled = true;
        ColliderGameSecond.enabled = true;
        ColliderGameThird.enabled = true;
        GlobalDefines.cameraActive = true;
        SettingsPanel.SetActive(false);
    }

    void ChangeName()
    {
        StartCoroutine(Connect.Change(PlayFabLogin.ReturnMobileID(), nameField.text));
    }
    public void ChangeNameFirst()
    {
        GlobalDefines.FirstLogin = false;
        StartCoroutine(Connect.Change(PlayFabLogin.ReturnMobileID(), nameFieldFirst.text));

    }
    public void Cancel()
    {
        ColliderGameFirst.enabled = true;
        ColliderGameSecond.enabled = true;
        ColliderGameThird.enabled = true;
        StopAllCoroutines();
        StartCoroutine(Connect.ExitLoad(this, GlobalDefines.RecGame));
        GlobalDefines.WaitPlayer = false;
    }
    public void EnterGame(int game)
    {
        ColliderGameFirst.enabled = false;
        ColliderGameSecond.enabled = false;
        ColliderGameThird.enabled = false;
        GlobalDefines.cameraActive = false;
        if (game == 1)
        {
            PayPanel1.SetActive(true);
            OfOOn1.SetActive(false);
        }
        else if (game == 2)
        {
            PayPanel2.SetActive(true);
            OfOOn2.SetActive(false);
        }
        else if (game == 3)
        {
            PayPanel3.SetActive(true);
            OfOOn3.SetActive(false);
        }
        else if (game == 4)
        {
            PayPanel4.SetActive(true);
            OfOOn4.SetActive(false);
        }
    }
    public void NotEnoughMoney(int game)
    {
        ColliderGameFirst.enabled = true;
        ColliderGameSecond.enabled = true;
        ColliderGameThird.enabled = true;
        ColliderGameFourth.enabled = true;
        StartCoroutine(anim(NotMoney));
        GlobalDefines.cameraActive = true;
        if (game == 1)
        {
            PayPanel1.SetActive(false);
            OfOOn1.SetActive(false);
        }
        else if (game == 2)
        {
            PayPanel2.SetActive(false);
            OfOOn2.SetActive(false);
        }
        else if (game == 3)
        {
            PayPanel3.SetActive(false);
            OfOOn3.SetActive(false);
        }
        else if (game == 4)
        {
            PayPanel4.SetActive(false);
            OfOOn4.SetActive(false);
        }
    }
    public bool CheckMoney()
    {
        if (int.Parse(Money.text) >= 50)
        {
            return true;
        }
        else
            return false;
    }
    public bool CheckDiamonds()
    {
        if (int.Parse(Diams.text) >= 1)
        {
            return true;
        }
        else
            return false;
    }
    public void OnApplicationQuit()
    {

        StartCoroutine(Connect.Disconnect(this, PlayFabLogin.ReturnMobileID(), GlobalDefines.RecGame, 0));
    }
    public IEnumerator anim(GameObject a)
    {
        a.SetActive(true);
        yield return new WaitForSeconds(2);
        a.SetActive(false);
    }
    public void Donat()
    {
        ColliderGameFirst.enabled = false;
        ColliderGameSecond.enabled = false;
        ColliderGameThird.enabled = false;
        GlobalDefines.cameraActive = false;
        DonatPanel.SetActive(true);
    }
    public void ExitDonat()
    {
        ColliderGameFirst.enabled = true;
        ColliderGameSecond.enabled = true;
        ColliderGameThird.enabled = true;
        GlobalDefines.cameraActive = true;
        DonatPanel.SetActive(false);
    }

    public void ChangeTip(int num)
    {
        
        if(num == 1)
        {

            GlobalDefines.StoryTip1 = false;
            GlobalDefines.StoryTip2 = true;
            T1.SetActive(false);
        }
        if (num == 2)
        {
            GlobalDefines.StoryTip2 = false;
            T2.SetActive(false);
            STOP.SetActive(true);
            WAIT.SetActive(true);
            GlobalDefines.waitTip = true;
        }
        if (num == 3)
        {
            GlobalDefines.StoryTip3 = false;
            GlobalDefines.StoryTip4 = true;
            T3.SetActive(false);
            STOP.SetActive(false);
            WAIT.SetActive(false);
        }
        if (num == 4)
        {
            GlobalDefines.StoryTip4 = false;
            GlobalDefines.StoryTip5 = true;
            T4.SetActive(false);
        }
        if (num == 5)
        {
            GlobalDefines.StoryTip5 = false;
            GlobalDefines.StoryTip6 = true;
            T5.SetActive(false);
        }
        if (num == 6)
        {
            Money.text = "150";
            GlobalDefines.StoryTip6 = false;
            T6.SetActive(false);
            GlobalDefines.waitTip = false;
           
        }
        

    }

    public void StartStoryTip()
    {
        GlobalDefines.StoryTip1 = true;
    }
    public static void GoToMagaz()
    {
        GlobalDefines.StoryTip3 = true;
    }
    public void StartOfflineGame(int Game)
    {
        GlobalDefines.Money = int.Parse(Money.text);
        GlobalDefines.Diam = int.Parse(Diams.text);
        GlobalDefines.IsOffline = true;
        SceneManager.LoadScene("Game"+Game.ToString()+"Offline");
    }
}
