﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayFabLogin : MonoBehaviour
{
    const string ID = "7FC64";
    public InputField Login;
    public Text Money;
    public Text Diams;
    private string myID;
    // Start is called before the first frame update
    void Start()
    {
        Debug.ClearDeveloperConsole();
        LoginIn();
    }
    public static string ReturnMobileID()
    {
        string deviceID = SystemInfo.deviceUniqueIdentifier;
        return deviceID;
    }
    public void LoginIn()
    {
        if (string.IsNullOrEmpty(PlayFabSettings.TitleId))
        {
            PlayFabSettings.TitleId = ID;
        }
        PlayFabClientAPI.LoginWithAndroidDeviceID(new LoginWithAndroidDeviceIDRequest
        {
            CreateAccount = true,
            AndroidDeviceId = ReturnMobileID(),
            TitleId = ID,
            
        }, 
        onLoginSuccess, 
        onLoginFailure);
    }
    public void GetPlayerData()
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest()
        {
            PlayFabId = myID,
            Keys = null
        }, UserDataSuccess, onLoginFailure);
    }
    private void onLoginSuccess(LoginResult result)
    {
        myID = result.PlayFabId;
        GetPlayerData();
        //PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest()
        //{
        //    DisplayName = Login.text

        //}, result =>
        //{
        //    GlobalDefines.Name = Login.text;
        //    Debug.Log("The player's display name is now: " + result.DisplayName);
        //}, onLoginFailure);
    }
    public void SetUserData()
    {
        string MoneyData = Money.text;
        string DiamsData = Diams.text;
        PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>()
            {
                //key value pair, saving the allskins array as a string to the playfab cloud
                {"money", MoneyData},
                {"diams", DiamsData}
            }
        }, SetDataSuccess, onLoginFailure);
    }

    void UserDataSuccess(GetUserDataResult result)
    {
        if (result.Data == null)
        {
            GlobalDefines.Loaded = true;
            GlobalDefines.cameraActive = true;
            Money.text = "0";
            Diams.text = "0";
        }
        else if(result.Data.ContainsKey("money") && result.Data.ContainsKey("diams"))
        {

            GlobalDefines.Loaded = true;
            GlobalDefines.cameraActive = true;
            Money.text = result.Data["money"].Value;
            Diams.text = result.Data["diams"].Value;
        }
        //else if (result.Data.ContainsKey("money") && !result.Data.ContainsKey("diams"))
        //{
        //    GlobalDefines.Loaded = true;
        //    GlobalDefines.cameraActive = true;
        //    Money.text = result.Data["money"].Value;
        //    Diams.text = "0";
        //}
        //else 
        //{
        //    GlobalDefines.Loaded = true;
        //    GlobalDefines.cameraActive = true;
        //    Diams.text = result.Data["diams"].Value;
        //    Money.text = "0";
        //}
    }
    void SetDataSuccess(UpdateUserDataResult result)
    {
        Debug.Log("YEEES"+result.DataVersion);
    }
    private void onLoginFailure(PlayFabError error)
    {
        GlobalDefines.Loaded = false;
    }
    // Update is called once per frame
    void Update()
    {
        if(GlobalDefines.Save)
        {
            if(!GlobalDefines.Diamonds)
                Money.text = (int.Parse(Money.text) - 50).ToString();
            else
                Diams.text = (int.Parse(Diams.text) - 1).ToString();
            GlobalDefines.Money = int.Parse(Money.text);
            GlobalDefines.Diam = int.Parse(Diams.text);
            GlobalDefines.Diamonds = false;
            SetUserData();
            GlobalDefines.Save = false;

        }
        if(GlobalDefines.Win)
        {
            Money.text = (GlobalDefines.Money + 200).ToString();
            SetUserData();
            GlobalDefines.Win = false;
        }
        if (GlobalDefines.OfflineWin)
        {
            Diams.text = (GlobalDefines.Diam + 1).ToString();
            SetUserData();
            GlobalDefines.OfflineWin = false;
        }
    }
    
}
