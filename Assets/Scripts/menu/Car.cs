﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Car : MonoBehaviour
{
    public Transform targetrec;
    public Transform target;
    public Transform target2;
    public NavMeshAgent person;
    public bool flag = false;
    public float min;
    // Start is called before the first frame update
    void Start()
    {
        person.SetDestination(target.position);

    }

    // Update is called once per frame
    void Update()
    {
        if (targetrec == target && person.remainingDistance <= min)
        {
            person.SetDestination(target2.position);
            targetrec = target2;
            flag = true;
        }
        else if (targetrec == target2 && person.remainingDistance <= min)
        {
            person.SetDestination(target.position);
            targetrec = target;
            flag = false;
        }
    }
}
