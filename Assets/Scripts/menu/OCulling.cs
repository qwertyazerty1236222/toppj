﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OCulling : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 8)
        {
            other.gameObject.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            other.gameObject.SetActive(false);
        }
    }
}
