﻿using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandActiveOffline : MonoBehaviour
{
    public GameObject Obj;
    public GameObject[] objs;
    public int range = 7;
    private int rand;
    public RawImage[] GreenZone;
    public RawImage[] WhiteZone;
    public float timer;
    public float timerPlus;
    public Text time;
    public Text Score;
    public GameObject TimePlus;
    public GameObject TimeMinus;
    private bool update;
    private int[] bots;
    private string[] Listbots = { "Adam", "Sanya", "Antony", "E.B. Lan", "YOU" };
    // Start is called before the first frame update
    void Start()
    {
        update = true;
        GlobalDefines.GameTip1 = true;
        rand = Random.Range(0, range - 1);
        for (int i = 0; i < range; i++)
        {
            if (i == rand)
                objs[i].active = true;
            else
                objs[i].active = false;
        }
    }

    void Update()
    {
        if (!GlobalDefines.GameTip1)
        {
            if (timer > 0)
                time.text = Mathf.RoundToInt(timer).ToString();
            // time1.text = Mathf.RoundToInt(timer).ToString();
            timer -= Time.deltaTime;
    
            if (timer <= 0 && update)
            {
                update = false;
                bots = new int[4];
                bots = Bots.GenerateResult(int.Parse(Score.text),1);
                int[] all = new int[5];
                //if (bots[0] < int.Parse(Score.text))
                    all[0] = bots[0];
               // else
                   // all[0] = bots[0] - 5;
               // if (bots[1] < int.Parse(Score.text))
                    all[1] = bots[1];
                //else
                  //  all[1] = bots[1] - 5;
                //if (bots[2] < int.Parse(Score.text))
                    all[2] = bots[2];
               // else
                   // all[2] = bots[2] - 5;
                //if (bots[3] < int.Parse(Score.text))
                    all[3] = bots[3];
               // else
                   // all[3] = bots[3] - 5;
                all[4] = int.Parse(Score.text);
                System.Array.Sort(all, Listbots);
                if (Listbots[4] == "YOU")
                    GlobalDefines.OfflineWin = true;
                GlobalDefines.ListOffline = Listbots;
                GlobalDefines.ScoreOffline = all;
                SceneManager.LoadScene("WinOffline");
            }
        }
    }
   
    // Update is called once per frame
    void bump()
    {

                if (WhiteZone[rand].rectTransform.position.x < GreenZone[rand].rectTransform.position.x + 25f && WhiteZone[rand].rectTransform.position.x > GreenZone[rand].rectTransform.position.x - 25f)
                {
                    Obj.GetComponent<Animator>().enabled = true;
                    Score.text = (int.Parse(Score.text) + 1).ToString();
                   // Score1.text = (int.Parse(Score1.text) + 1).ToString();
                    timer += timerPlus;
                    StartCoroutine(animator(TimePlus));
                    Debug.Log(WhiteZone[rand].rectTransform.position.x + " True " + GreenZone[rand].rectTransform.position.x);
                }
                else
                {
                    Obj.GetComponent<Animator>().enabled = false;
                    timer -= timerPlus*2;
                    StartCoroutine(animator(TimeMinus));
                    Debug.Log(WhiteZone[rand].rectTransform.position.x + " False " + GreenZone[rand].rectTransform.position.x);
                }

        rand = Random.Range(0, range-1);
        for (int i = 0; i < range; i++)
        {
            if (i == rand)
                objs[i].active = true;
            else
                objs[i].active = false; 
        }
    }
    public IEnumerator animator(GameObject time)
    {
        time.active = true;
        yield return new WaitForSeconds(0.5f);
        time.active = false;
    }
}
