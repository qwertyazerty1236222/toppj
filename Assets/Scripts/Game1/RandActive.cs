﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandActive : MonoBehaviour
{
    public GameObject Obj;
    public GameObject[] objs;
    public int range = 7;
    private int rand;
    public RawImage[] GreenZone;
    public RawImage[] WhiteZone;
    public float timer;
    public float timerPlus;
    public GameObject panel;
    public Text time;
    public Text Score;
  //  public Text time1;
   // public Text Score1;
    public GameObject TimePlus;
    public GameObject TimeMinus;
    private bool update;
    // Start is called before the first frame update
    void Start()
    {
        update = true;
        GlobalDefines.GameTip1 = true;
        rand = Random.Range(0, range - 1);
        for (int i = 0; i < range; i++)
        {
            if (i == rand)
                objs[i].active = true;
            else
                objs[i].active = false;
        }
    }

    void Update()
    {
        if (!GlobalDefines.GameTip1)
        {
            if (timer > 0)
                time.text = Mathf.RoundToInt(timer).ToString();
            // time1.text = Mathf.RoundToInt(timer).ToString();
            timer -= Time.deltaTime;
            if (timer <= 0 && update)
            {
                update = false;
                Debug.LogError(Score.text + " " + GlobalDefines.RecGame + " " + GlobalDefines.RecRoom);
                GlobalDefines.WaitPlayerForExit = true;
                GlobalDefines.returnsFirstGame = int.Parse(Score.text);
                StartCoroutine(Connect.loadOut(this, 1));
            }

            if (GlobalDefines.WaitPlayerForExit)
            {
                panel.SetActive(true);
            }
            else
                panel.SetActive(false);
        }
    }
   
    // Update is called once per frame
    void bump()
    {

                if (WhiteZone[rand].rectTransform.position.x < GreenZone[rand].rectTransform.position.x + 25f && WhiteZone[rand].rectTransform.position.x > GreenZone[rand].rectTransform.position.x - 25f)
                {
                    Obj.GetComponent<Animator>().enabled = true;
                    Score.text = (int.Parse(Score.text) + 1).ToString();
                   // Score1.text = (int.Parse(Score1.text) + 1).ToString();
                    timer += timerPlus;
                    StartCoroutine(animator(TimePlus));
                    Debug.Log(WhiteZone[rand].rectTransform.position.x + " True " + GreenZone[rand].rectTransform.position.x);
                }
                else
                {
                    Obj.GetComponent<Animator>().enabled = false;
                    timer -= timerPlus*2;
                    StartCoroutine(animator(TimeMinus));
                    Debug.Log(WhiteZone[rand].rectTransform.position.x + " False " + GreenZone[rand].rectTransform.position.x);
                }

        rand = Random.Range(0, range-1);
        for (int i = 0; i < range; i++)
        {
            if (i == rand)
                objs[i].active = true;
            else
                objs[i].active = false; 
        }
    }
    public IEnumerator animator(GameObject time)
    {
        time.active = true;
        yield return new WaitForSeconds(0.5f);
        time.active = false;
    }
}
