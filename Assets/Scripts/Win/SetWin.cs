﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SetWin : MonoBehaviour
{

    public Text Player;
    public Text Score;
    private float timer = 8;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!GlobalDefines.IsOffline)
        {
            Player.text = GlobalDefines.RecWinner;
            Score.text = GlobalDefines.RecWinScore.ToString();
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                SceneManager.LoadScene("SampleScene");
            }
        }
        else
        {
            
            string[] list = GlobalDefines.ListOffline;
            string tmp = list[4] + "\n" + list[3] + "\n" + list[2] + "\n" + list[1] + "\n" + list[0];
            Player.text = tmp;
            int[] scorelist = GlobalDefines.ScoreOffline;
            tmp = scorelist[4].ToString() + "\n" + scorelist[3].ToString() + "\n" + scorelist[2].ToString() + "\n" + scorelist[1].ToString() + "\n" + scorelist[0].ToString();
            Score.text = tmp;
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                GlobalDefines.IsOffline = false;
                SceneManager.LoadScene("SampleScene");
            }
        }
    }
}
