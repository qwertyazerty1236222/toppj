﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenTips : MonoBehaviour
{
    public GameObject tip;

    public void ClickTip(int Game)
    {
        if(Game == 1)
        {
            GlobalDefines.GameTip1 = false;
        }
        if (Game == 2)
        {
            GlobalDefines.GameTip2 = false;
        }
        if (Game == 3)
        {
            GlobalDefines.GameTip3 = false;
        }
        if (Game == 4)
        {
            GlobalDefines.GameTip4 = false;
        }
        tip.SetActive(false);
    }
}
