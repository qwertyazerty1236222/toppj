#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("UU1EAXNOTlUBYmARPzYsERcRFRNo+Ve+EjVEgFa16AwjIiAhIIKjIIqCULNmcnTgjg5gktnawlHsx4JtKX8RoyAwJyJ0PAEloyApEaMgJRFeYIm52PDrR70FSjDxgprFOgviPjcRNScidCUiMixgUVFNRAFzTk5VljqcsmMFMwvmLjyXbL1/QulqoTYnES4nInQ8MiAg3iUkESIgIN4RPBGjJZoRoyKCgSIjICMjICMRLCcon9VSus/zRS7qWG4V+YMf2FneSulNRAFoT0IPEAcRBScidCUqMjxgUVFNRAFiRFNVSEdIQkBVSE5PAWBUVUlOU0hVWBA3ETUnInQlIjIsYFGJ/V8DFOsE9Pgu90r1gwUCMNaAjU9FAUJOT0VIVUhOT1IBTkcBVFJEKQonICQkJiMgNz9JVVVRUhsODlZTQEJVSEJEAVJVQFVETERPVVIPERIXexFDECoRKCcidCUnMiN0chAyHAdGAasSS9Yso+7/yoIO2HJLekWhNQrxSGa1Vyjf1UqsD2GH1mZsXniGJChdNmF3MD9V8paqAhpmgvROJCEioyAuIRGjICsjoyAgIcWwiCgHEQUnInQlKjI8YFFRTUQBYkRTVUNNRAFSVUBPRUBTRQFVRFNMUgFASEdIQkBVSE5PAWBUVUlOU0hVWBANAUJEU1VIR0hCQFVEAVFOTUhCWFVIR0hCQFVEAUNYAUBPWAFRQFNVWxGjIFcRLycidDwuICDeJSUiIyAPYYfWZmxeKX8RPicidDwCJTkRN6o4qP/Yak3UJooDESPJOR/ZcSjy6DhT1Hwv9F5+utMEIpt0rmx8LNABQE9FAUJEU1VIR0hCQFVITk8BUVgBQFJSVExEUgFAQkJEUVVAT0JEZF8+bUpxt2Co5VVDKjGiYKYSq6ABYmARoyADESwnKAunaafWLCAgIEauKZUB1uqNDQFOUZceIBGtlmLu4UISVtYbJg13yvsuAC/7m1I4bpQ+sPo/ZnHKJMx/WKUMyheDdm10zRQTEBUREhd7NiwSFBETERgTEBURDhGg4icpCicgJCQmIyMRoJc7oJIXuG0MWZbMrbr90la601fzVhFu4AFORwFVSUQBVUlETwFAUVFNSEJAtL9bLYVmqnr1NxYS6uUubO81SPBWVg9AUVFNRA9CTkwOQFFRTURCQPgXXuCmdPiGuJgTY9r59FC/X4BzkBF5zXslE61Jkq48/0RS3kZ/RJ0nInQ8LyU3JTUK8UhmtVco39VKrCwnKAunaafWLCAgJCQhIqMgICF9oyAhJygLp2mn1kJFJCARoNMRCyeUG4zVLi8hsyqQADcPVfQdLPpDNxEwJyJ0JSsyK2BRUU1EAWhPQg8QJScyI3RyEDIRMCcidCUrMitgUVFFFAI0ajR4PJK11te9v+5xm+B5cQunaafWLCAgJCQhEUMQKhEoJyJ0rlKgQec6eigOs5PZZWnRQRm/NNQFw8rwllH+LmTABuvQTFnMxpQ2Nj6koqQ6uBxmFtOIumGvDfWQsTP5Js1cGKKqcgHyGeWQnrtuK0reCt1zRE1IQE9CRAFOTwFVSUhSAUJEUy68HNIKaAk76d/vlJgv+H899+occYur9PvF3fEoJhaRVFQA");
        private static int[] order = new int[] { 6,27,24,33,17,12,59,29,59,50,33,17,12,54,43,35,46,59,56,32,52,43,33,42,59,44,39,28,31,46,32,45,40,44,54,43,57,43,46,43,48,41,56,57,58,57,48,47,54,56,50,59,52,57,55,57,56,59,59,59,60 };
        private static int key = 33;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
