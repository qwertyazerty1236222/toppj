#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("UQQm9ru86J6Hmn1ud6+Ogfey+B3H1ZkKf/I+XYqNvWl9zjFLUJCWm21XpY+lGqwS64x1I4W5EIa/4SsIpmGz4rX9yiNQIA4bv3WXvGn1o7qRH5ceO+E+Ea4XjdDkKTAjLYP733lH3ChKVvpcsybmJoll1gKBPxbpEy9IG0nlV2aYxbKa5yqGXm4XUxnBqoleua7TAk9O7OddDK9eZnV/eb2p5ufgIe4ddmKLjyZsMEnVry9R1Ty620vG2t0hpbnr+U0tXtwc01UKcJjrfC7/EMXtk4csgkHd79y+59pZV1ho2llSWtpZWViJWo5/B0s+PD7lJr1IXzNarpjSclp8fl4DL09o2ll6aFVeUXLeEN6vVVlZWV1YW9DQFG/676H9nVpbWVhZ");
        private static int[] order = new int[] { 3,12,8,7,9,8,8,11,9,10,10,12,13,13,14 };
        private static int key = 88;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
