﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BasketTap : MonoBehaviour
{
    private GameObject panel;
    private Text time;
    private Text score;
    public Animator anim;
    private int count;
    public Animator BallGoal;
    public bool[] gz;
    public GameObject[] Zones;
    public Animator[] Collapse;
    private float timer;
    private bool flag;
    private bool update;
    void Start()
    {
        update = false;
        flag = false;
        timer = 10f;
        Collapse[0].enabled = false;
        Collapse[1].enabled = false;
        Collapse[2].enabled = false;
        Collapse[3].enabled = false;
        Collapse[4].enabled = false;
        anim.speed = SpawnRandom.AccelerationGame4;
        panel = GameObject.Find("panel");
        
        score = GameObject.Find("Score").GetComponent<Text>();
        time = GameObject.Find("Timer").GetComponent<Text>();
        count = 0;
        gz = new bool[5];
        for(int i =0; i<5; i++)
        {
            gz[i] = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "GZ1")
        {
            gz[0] = true;
        }
        if (other.tag == "GZ2")
        {
            gz[1] = true;
        }
        if (other.tag == "GZ3")
        {
            gz[2] = true;
        }
        if (other.tag == "GZ4")
        {
            gz[3] = true;
        }
        if (other.tag == "GZ5")
        {
            gz[4] = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "GZ1")
        {
            gz[0] = false;
        }
        if (other.tag == "GZ2")
        {
            gz[1] = false;
        }
        if (other.tag == "GZ3")
        {
            gz[2] = false;
        }
        if (other.tag == "GZ4")
        {
            gz[3] = false;
        }
        if (other.tag == "GZ5")
        {
            gz[4] = false;
        }
    }
    public void Tap()
    {
        if(gz[0] || gz[1] || gz[2] || gz[3] || gz[4])
        {
            if(gz[0])
            {
                Collapse[0].enabled = true;
                StartCoroutine(CollapseAnim(0));
            }
            if (gz[1])
            {
                Collapse[1].enabled = true;
                StartCoroutine(CollapseAnim(1));
            }
            if (gz[2])
            {
                Collapse[2].enabled = true;
                StartCoroutine(CollapseAnim(2));
            }
            if (gz[3])
            {
                Collapse[3].enabled = true;
                StartCoroutine(CollapseAnim(3));
            }
            if (gz[4])
            {
                Collapse[4].enabled = true;
                StartCoroutine(CollapseAnim(4));
            }

            count++;
            if(count == 5)
            {
                flag = true;
                score.text = (int.Parse(score.text) + 1).ToString();
                BallGoal.enabled = true;
            }
            
            
        }
        else
        {
            update = true;
            GlobalDefines.WaitPlayerForExit = true;
            GlobalDefines.returnsFourthGame = int.Parse(score.text);
            StartCoroutine(Connect.loadOut(this, 4));
        }

        
    }
    private void Update()
    {
        if (GlobalDefines.WaitPlayerForExit)
        {
            panel.SetActive(true);
        }
        else
            panel.SetActive(false);
        if (!update && !GlobalDefines.GameTip4)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Tap();
            }
        }
        
    }
    private void FixedUpdate()
    {
        if (!GlobalDefines.GameTip4)
        {
            if (!update)
            {
                if (!flag)
                    timer -= Time.deltaTime;
                time.text = Mathf.RoundToInt(timer).ToString();
                if (timer <= 0)
                {
                    update = true;
                    GlobalDefines.WaitPlayerForExit = true;
                    GlobalDefines.returnsFourthGame = int.Parse(score.text);
                    StartCoroutine(Connect.loadOut(this, 4));
                }
            }
        }
    }

    IEnumerator CollapseAnim(int Zone)
    {
        yield return new WaitForSeconds(0.25f);
        Zones[Zone].SetActive(false);
    }
}
