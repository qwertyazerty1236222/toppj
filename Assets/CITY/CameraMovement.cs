﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

public class CameraMovement : MonoBehaviour
{
    public float speed;
    public int UnModifie;
    public int[] ogr;
    void Update()
    {
        if (GlobalDefines.cameraActive)
        {
            if (transform.position.x <= ogr[0] || transform.position.z >= ogr[1] || transform.position.x >= ogr[2] || transform.position.z <= ogr[3])
            {
                if (transform.position.x <= ogr[0])
                {
                    transform.position = new Vector3(Mathf.Lerp(transform.position.x, transform.position.x - 75, speed * Time.deltaTime), 250, transform.position.z);
                    transform.position = new Vector3(Mathf.Lerp(transform.position.x, transform.position.x + 300, speed * Time.deltaTime), 250, transform.position.z);
                    //gameObject.transform.Translate(new Vector2(100, 0) * Time.deltaTime * speed);
                }
                else
                if (transform.position.z >= ogr[1])
                {
                    transform.position = new Vector3(transform.position.x, 250, Mathf.Lerp(transform.position.z, transform.position.z - 150, speed * Time.deltaTime));
                    //gameObject.transform.Translate(new Vector2(0, -100) * Time.deltaTime * speed);
                }
                else
                if (transform.position.x >= ogr[2])
                {
                    transform.position = new Vector3(Mathf.Lerp(transform.position.x, transform.position.x - 150, speed * Time.deltaTime), 250, transform.position.z);
                    //gameObject.transform.Translate(new Vector2(-100, 0) * Time.deltaTime * speed);
                }
                else
                if (transform.position.z <= ogr[3])
                {
                    transform.position = new Vector3(transform.position.x, 250, Mathf.Lerp(transform.position.z, transform.position.z + 150, speed * Time.deltaTime));
                    //gameObject.transform.Translate(new Vector2(0, 100) * Time.deltaTime * speed);
                }

            }
            else
            if (Input.touchCount == 1 && !(transform.position.x <= ogr[0] || transform.position.z >= ogr[1] || transform.position.x >= ogr[2] || transform.position.z <= ogr[3]))
            {
                float deltaPosX = Input.GetTouch(0).deltaPosition.x;
                gameObject.transform.Translate(new Vector2(-deltaPosX, 0) * Time.deltaTime * speed);
                float deltaPosY = Input.GetTouch(0).deltaPosition.y;
                gameObject.transform.Translate(new Vector2(0, -deltaPosY) * Time.deltaTime * speed);

                //transform.position = new Vector3(Mathf.Lerp(transform.position.x, deltaPosX, speed * Time.deltaTime), 250, Mathf.Lerp(transform.position.z, deltaPosY, speed * Time.deltaTime));
            }
        }
    }
}