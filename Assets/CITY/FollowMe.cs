﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMe : MonoBehaviour
{
    public GameObject Follower;
    public Transform[] p;
    public float speed = 4.0f;
    private int i = 1;
    // Start is called before the first frame update
    void Start()
    {
        Follower.transform.position = p[0].transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(p[i].transform);
        Vector3 delta = p[i].transform.position - transform.position;
        delta.Normalize();
        float nspeed = speed * Time.deltaTime;
        transform.position += (delta * nspeed);
        if (transform.position == p[i].transform.position && transform.position != p[0].transform.position)
            i++;
        else if (transform.position == p[0].transform.position)
            i = 1;
    }
}