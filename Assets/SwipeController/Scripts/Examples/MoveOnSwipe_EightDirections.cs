﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GG.Infrastructure.Utils.Swipe;

public class MoveOnSwipe_EightDirections : MonoBehaviour
{
    [Header("Available movements:")]
    [SerializeField] private int[] size;
    [SerializeField] private GameObject Camera;
    [SerializeField] private bool _up = true;
    [SerializeField] private bool _down = true;
    [SerializeField] private bool _left = true;
    [SerializeField] private bool _right = true;
    [SerializeField] private bool _upLeft = true;
    [SerializeField] private bool _upRight = true;
    [SerializeField] private bool _downLeft = true;
    [SerializeField] private bool _downRight = true;



    public void OnSwipeHandler(string id)
    {
        switch(id)
        {
            case DirectionId.ID_UP:
                if(1000 > Camera.transform.position.z + size[0])
                    MoveUp();
                break;

            case DirectionId.ID_DOWN:
                if (0 < Camera.transform.position.z - size[1])
                    MoveDown();
                break;

            case DirectionId.ID_LEFT:
                if (0 < Camera.transform.position.x - size[1])
                    MoveLeft();
                break;

            case DirectionId.ID_RIGHT:
                if (1000 > Camera.transform.position.x + size[1])
                    MoveRight();
                break;

            case DirectionId.ID_UP_LEFT:
                MoveUpLeft();
                break;

            case DirectionId.ID_UP_RIGHT:
                MoveUpRight();
                break;

            case DirectionId.ID_DOWN_LEFT:
                MoveDownLeft();
                break;

            case DirectionId.ID_DOWN_RIGHT:
                MoveDownRight();
                break;
        }
    }

    private void MoveDownRight()
    {
        if (_downRight)
        {
            transform.position += Vector3.down + Vector3.right;
        }
    }

    private void MoveDownLeft()
    {
        if (_downLeft)
        {
            transform.position += Vector3.down + Vector3.left;
        }
    }

    private void MoveUpRight()
    {
        if (_upRight)
        {
            transform.position += Vector3.up + Vector3.right;
        }
    }

    private void MoveUpLeft()
    {
        if (_upLeft)
        {
            transform.position += Vector3.up + Vector3.left;
        }
    }

    private void MoveRight()
    {
        if (_right)
        {
            transform.position += Vector3.right*48;
            Camera.GetComponent<Rigidbody>().AddForce(Vector3.right * 13, ForceMode.Impulse);
        }
    }

    private void MoveLeft()
    {
        if (_left)
        {
            transform.position += Vector3.left*48;
            Camera.GetComponent<Rigidbody>().AddForce(Vector3.left * 13, ForceMode.Impulse);
        }
    }

    private void MoveDown()
    {
        if (_down)
        {
            transform.position += Vector3.back*48;
            Camera.GetComponent<Rigidbody>().AddForce(Vector3.back * 13, ForceMode.Impulse);
        }
    }

    private void MoveUp()
    {
        if (_up)
        {
            transform.position += Vector3.forward*48;
            Camera.GetComponent<Rigidbody>().AddForce(Vector3.forward*13, ForceMode.Impulse);
            
        }
    }
}
